#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from readsnap import readsnap
from fourier_components import *
from phase_of_bar import *

def main():
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/sim_55/"
  # fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/bar-pattern-test/"
  fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/hernquist/output/"

  radii = np.linspace(0, 20, 21)

  m = 2 # fourier amplitude

  # time variable
  totsnap = 120
  timepersnap = 0.1 
  time = range(totsnap)
  time = np.asarray(time)
  time = time * timepersnap

  plt.style.use('seaborn-bright')

  jump = 3 # skip through particle data

  fig, axs = plt.subplots()
  
  famp = []
  phase = []
  for snap in range(totsnap):
    ext = res.snap_mask(snap, 3) # file's mask (last three digits)

    dd = readsnap(fname, ext, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']  # disc particle velocities

    # xyz = load_snap('Coordinates', 2, fname, ext)
    # vel = load_snap('Velocities', 2, fname, ext)

    # remove center of mass (disc)
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    # pd = np.asarray([x, y, z])
    # vd = np.asarray([vx, vy, vz])
    # pd = pd.T
    # vd = vd.T

    # x, y, z, vx, vy, vz = adjust_center_mass(xyz[:, 0], xyz[:, 1], xyz[:, 2], vel[:, 0], vel[:, 1], vel[:, 2])

    # print("\n############# Fourier Amplitude (snapshot %s of %s) #############\n" % (str(snap+1), str(totsnap)))
    # #r, numpart, A0 = calculate_Am_simple(pd[::jump, 0], pd[::jump, 1], radii, 0, mp[0])
    # r, numpart, Am = calculate_Am_simple(pd[::jump, 0], pd[::jump, 1], radii, m)
    # r, numpart, Bm = calculate_Am_simple(pd[::jump, 0], pd[::jump, 1], radii, m, c = 'b')
    
    # op = np.sqrt(Am**2 + Bm**2) / numpart

    # famp.append(np.max(op))
    # ind = np.argmax(op)
    
    # ang = convert_radians_to_degrees(0.5 * np.arctan2(Bm, Am), positive = True)
    # phase.append(np.mean(ang))
    # #phase.append(np.mean(op) / np.sqrt(np.mean(op**2)))

    # print("\n###############################################\n")

    print("\n############# Phase of Bar (snapshot %s of %s) #############\n" % (str(snap+1), str(totsnap)))
    # r, ph = phase_of_bar_direct(pd[::jump, 0], pd[::jump, 1], radii)
    # r, A2, ph = phase_of_bar_annuli_2d(pd[:, 0], pd[:, 1], radii)
    r, A2, ph = phase_of_bar_annuli_2d(x, y, radii)
    # ph = convert_radians_to_degrees(ph, positive=False)
    # ph = ph % 90

    bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
    # ph_thres = [ph[i] for i in range(len(ph)) if np.abs(ph[i]-ph[0]) < bt]
    ph_thres = []
    for i in range(len(ph)):
      if np.abs(ph[i]-ph[0]) >= bt:
        break
      else:
        ph_thres.append(ph[i])
    print(ph_thres)

    phase.append(np.mean(ph_thres))
 
    print("\n###############################################\n")

  # famp = np.asarray(famp)
  phase = np.asarray(phase)

  nphase = []
  ntphase = []
  buf = 0
  for i in range(len(phase)-1):
    if phase[i+1] < 0 and phase[i] > 0:
      df = 2*np.pi - (np.abs(phase[i+1]) + phase[i])
    else:
      df = np.abs(phase[i+1]-phase[i])
    buf = buf + df
    ntphase.append(df)
    nphase.append(buf)
  nphase = np.asarray(nphase)

  # 300 represents number of points of time data
  # tnew = np.linspace(time.min(), time.max(), 300) 

  #spl = make_interp_spline(time, famp, k=3)  # type: BSpline
  #famp_smooth = spl(tnew)
  # spl = make_interp_spline(time, phase, k=3)  # type: BSpline
  # phase_smooth = spl(tnew)

  # calculate derivative of phase with the central finite differences method
  dt = time[1] - time[0]
  # dphase = np.gradient(phase, dt)
  # dphase = np.gradient(ntphase, dt)
  dphase = ntphase/dt

  coef = np.polyfit(time[1:], nphase, 1)
  poly1d_fn = np.poly1d(coef)

  print("Omega_b = %s\n" % ((poly1d_fn(time[1:])[-1] - poly1d_fn(time[1:])[0]) / (time[-1] - time[1])))

  axs.plot(time[20:], dphase[19:], 'k-')
  # axs[1].plot(time[1:], nphase, 'k-')
  # axs[1].plot(time[1:], poly1d_fn(time[1:]), 'b--')

  axs.grid(True)
  # axs[1].grid(True)

  plt.show()

  plt.close()

if __name__ == '__main__':
  main()