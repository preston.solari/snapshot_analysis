#!/usr/bin/env python

# script to calculate ASE, ISE, MASE and MISE values
# for a set or realizations

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad # for integration
from sklearn.metrics import mean_squared_error

sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/scripts/")

from den_map import data
import resonances as res
from accelerations import acc_components_cyl, radii_avg_1c
import readsnap as reads
from plot_den_maps import adjust_center_mass_unit
from mass_assignment_secant_method import volumetric_density, volumetric_density_imp

np.set_printoptions(threshold=np.inf)

# Function to calculate scale length a for a Hernquist distribution
def calculate_a(r_vir,c):
  return (r_vir/c)*(np.sqrt(2*(np.log(1+c) - c/(1+c))))

# Function to calculate the halo mass
def halo_mass(v200,md,G,hubble):
  m200=np.power(v200,3)/(10*G*hubble)
  mdisc=md*m200
  return (m200 - mdisc)

# Function to calculate the virial radius
def virial_radius(v200,hubble):
  return v200/(10*hubble)

def density_hernquist(radii, mh, rs, scale='linear'):
  if scale == 'linear':
    fp = 0.01
    den = np.array([mh*rs/2./np.pi/r/(r+rs)**3 if r > 0 else mh*rs/2/np.pi/(radii[1]*fp)/(radii[1]*fp+rs)**3 for r in radii])
  elif scale == 'log':
    den = np.array([mh*rs/2./np.pi/r/(r+rs)**3 for r in radii])
  else:
    print("density_hernquist: Unknown scale {0}".format(scale))
    return np.array([0])
  return den

# force from a Hernquist mass distribution (real)
def force_hernquist(x, y, z, rs, mh, G):
  r = np.sqrt(x**2 + y**2 + z**2)

  ax = -G*mh*x/r/(r+rs)**2
  ay = -G*mh*y/r/(r+rs)**2
  az = -G*mh*z/r/(r+rs)**2

  return ax, ay, az

# Estimation of ASE error
def ASE(acc_comp,acc_real):
  # inner=np.sqrt((acc_comp[0] - acc_real[0])**2 +
  #               (acc_comp[1] - acc_real[1])**2 +
  #               (acc_comp[2] - acc_real[2])**2)
  
  ac = np.sqrt(acc_comp[0]**2 + acc_comp[1]**2 + acc_comp[2]**2)
  ar = np.sqrt(acc_real[0]**2 + acc_real[1]**2 + acc_real[2]**2)
  
  inner = ac-ar

  return np.mean(inner**2)

# Function to integrate
def fun(r,a,G,mh,eps):
  fac=2*a*(G**2)*(mh**2)
  f1=fac/(r*(np.power(a+r,3)))
  f2=r/np.power(r**2+eps**2,1.5)
  f3=1/((r+a)**2)
  f=np.sqrt((f2-f3)**2)**2
  return f1*f

# Estimation of ISE error
def ISE(fs,(rv,n,init,a,G,mh,eps)):
  # array with measurements
  div=np.linspace(init,rv,n+1)

  # calculate integral for len(div) divisions
  I=0
  for i in range(len(div)-1):
    res=quad(fs,div[i],div[i+1],args=(a,G,mh,eps))
    I=I+res[0]

  return I

def hist_a(radii, rs, mh, G):
  ar = [G*mh/(r+rs)**2 if r > 0 else 0 for r in radii]
  return np.array(ar)

def main():
  G = 43007.1
  md = 0.0
  hubble = 0.1
  c = 10.0
  v200 = 160.0

  rv = virial_radius(v200, hubble)
  mh = halo_mass(v200, md, G, hubble)*.95
  rs = calculate_a(rv, c)
  
  fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/halo_short/"
  model = ""
  
  frun = fname+model+'/'
  dh = reads.readsnap(frun, 3, 1) # disk particles' data

  ph = dh['p']
  vh = dh['v']
  ah = dh['a']
  mp = dh['m'][0]

  x, y, z = adjust_center_mass_unit(ph[:,0], ph[:,1], ph[:,2])
  vx, vy, vz = adjust_center_mass_unit(vh[:,0], vh[:,1], vh[:,2])
  ax, ay, az = adjust_center_mass_unit(ah[:,0], ah[:,1], ah[:,2])
  ax = ah[:,0]
  ay = ah[:,1]
  az = ah[:,2]
  
  a_ax, a_ay, a_az = force_hernquist(x, y, z, rs, mh, G)
  
  print(ASE((ax, ay, az), (a_ax, a_ay, a_az)))
  print(mean_squared_error(np.sqrt(ax**2+ay**2+az**2), np.sqrt(a_ax**2+a_ay**2+a_az**2)))
  print(a_ay.shape)
  
  plt.plot(y[::], ay[::], 'r,', alpha=0.6)
  plt.plot(y[::], a_ay[::], 'k,', alpha=0.6)
  plt.xlim(-100,100)
  # plt.ylim(-100,100)
  plt.show()
  plt.clf()
  
  sys.exit()
  
if __name__ == '__main__':
  main()


#def main()#fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/halos/SGS1/accelerations" # rerun
  
  # constants
  G = 43007.1
  md = 0.0
  hubble = 0.1
  c = 10.0
  v200 = 160.0

  rv = virial_radius(v200, hubble)
  mh = halo_mass(v200, md, G, hubble)*.95
  rs = calculate_a(rv, c)
  
  r_maxh = 1062.8449
  mh2 = mh*(((r_maxh**2/(r_maxh+rs)**2))**(-1))

  print("Properties of halo:")
  print("\tr_200 = {0}".format(rv))
  print("\tM_h = {0}".format(mh))
  print("\tM_h2 = {0}".format(mh2))
  print("\tr_s = {0}".format(rs))
  print("\n")

  #fname = "/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/halos/" # rerun
  #fname = "/home/cronex/Documentos/Doctorado/GalIC/Model_H1/"
  fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/halo_short/"
  #model="SGS1"
  model=""

  initsnap = 1
  totsnap = 1
  skip = 1

  ts = 0.01
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = time * ts

  max_r = 160
  nrings = 60
  scale = 'log'
  # scale = 'linear'
  jump = 1
  # radii = np.linspace(0, max_r, nrings+1)
  radii = np.logspace(-0.3, np.log10(max_r), nrings, endpoint=True)

  for snap in rsnap:
    #ext=res.snap_mask(snap,4)
    frun = fname+model+'/'
    dh = reads.readsnap(frun, snap, 1) # disk particles' data

    ph = dh['p']
    vh = dh['v']
    ah = dh['a']
    mp = dh['m'][0]

    x, y, z = adjust_center_mass_unit(ph[:,0], ph[:,1], ph[:,2])
    vx, vy, vz = adjust_center_mass_unit(vh[:,0], vh[:,1], vh[:,2])
    ax, ay, az = adjust_center_mass_unit(ah[:,0], ah[:,1], ah[:,2])
    # ax, ay, az = ah[:,0], ah[:,1], ah[:,2]
    

    r = np.sqrt(x**2+y**2+z**2)
    print(max(r))
    ind_r = np.where(r <= max_r)
    r = r[ind_r]

    x = x[ind_r]
    y = y[ind_r]
    z = z[ind_r]
    ax = ax[ind_r]
    ay = ay[ind_r]
    az = az[ind_r]
    
    a_ax, a_ay, a_az = force_hernquist(x, y, z, rs, mh2, G)
    # print(ax[:100])
    # print(a_ax[:100])

    ase = ASE((ax, ay, az), (a_ax, a_ay, a_az))
    print("ASE = %s" % ase)

    na = np.sqrt(ax**2 + ay**2 + az**2)
    naa = np.sqrt(a_ax**2 + a_ay**2 + a_az**2)
    print(np.mean(np.sqrt((ax-a_ax)**2+(ay-a_ay)**2+(az-a_az)**2)))
    print(np.mean((ax-a_ax)**2+(ay-a_ay)**2+(az-a_az)**2))
    print(np.mean((ax-a_ax)+(ay-a_ay)+(az-a_az)))
    
    adat = radii_avg_1c(r, na, radii)
    aadat = radii_avg_1c(r, naa, radii)

    # aadat_avg = np.array([(aadat[1][i]+aadat[1][i+1])/2.0 for i in range(len(aadat[1])-1)])

    newr, den_s = volumetric_density_imp(x, y, z, mp, max_r, nrings, scale=scale)
    den_a = density_hernquist(radii, mh2, rs, scale)


    # ar, at, az = acc_components_cyl((x,y,z),(vx,vy,vz),(ax,ay,az))
    # adat = radii_avg(r, ar, at, az, radii)
    # aar, aat, aaz = acc_components_cyl((x,y,z),(vx,vy,vz),(a_ax,a_ay,a_az))
    # adat = radii_avg(r, ar, at, az, radii)

  fig, axs = plt.subplots()

  # axs.plot(adat[0], adat[1], 'k-')
  # axs.plot(aadat[0], aadat[1], 'k--')
  # axs.plot(aadat[0][:-1], aadat_avg, 'k.-')
  
  axs.plot(newr, den_s, 'k-')
  axs.plot(radii, den_a, 'k--')

  # dr = 200
  # edgesa = np.arange(min(na), max(na), (max(na)-min(na))/dr)
  # edgesaa = np.arange(min(naa), max(naa), (max(naa)-min(naa))/dr)

  # resulta, bins = np.histogram(na, bins=edgesa, normed=False)
  # resultaa, bins = np.histogram(naa, bins=edgesaa, normed=False)

  # axs.plot(edgesa[:-1], resulta, 'k-', label='$a_{t=0.1\,Gy}$')
  # axs.plot(edgesaa[:-1], resultaa, 'b--', label='$a_{t=0.1\,Gy}$')
  
  #axs.set_ylim(0, 1.2)
  axs.set_xscale('log')
  axs.set_yscale('log')

  plt.show()
  plt.clf()