import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter

from den_map import data

# Main definition
def main():
  # fdir="/home/cronex/Documentos/Doctorado/sims/sm-04-10-2018/"
  # fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  # fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  fdir="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"
  
  # listsims = ['12','11','13','14','15','16',
  #             '22','21','23','24','25','26',
  #             '32','31','33','34','35','36',
  #             '42','41','43','44','45','46',
  #             '52','51','53','54','55','56',
  #             '62','61','63','64','65','66'] 
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  lensims = len(listsims)
  
  plt.style.use('seaborn-bright')

  fig, axs = plt.subplots(dpi=130)
  
  colors = plt.cm.jet(np.linspace(0,1,lensims))
  
  
  for k in range(lensims):
    # frun=fdir+"sim_"+model+"/"
    # frun = fdir+"sim_"+listsims[k]+"/"+"toomreQ_000.dat"
    frun = fdir+listsims[k]+"/toomre/toomre_"+listsims[k]+"_1.8G.txt"
    
    pd = data(frun) # raw data for toomre's parameter
    r, Q = pd[0].astype(float), pd[1].astype(float)
    
    # axs.plot(r, Q, '-', color=colors[k], lw=0.7, label=names[k])
    axs.plot(r, Q, '-', color=colors[k], lw=0.7, label=listsims[k])
  
  axs.set_xlim(0.5, 7)
  axs.set_ylim(1, 4)
  axs.set_xlabel("Radius [kpc]", fontsize=10)
  axs.set_ylabel("Q", fontsize=10)
  axs.legend(loc='center left', bbox_to_anchor=(1, 0.60), fontsize=6)
    
  axs.tick_params(axis='both',which='both',direction='in',
                  top=True,left=True,right=True,bottom=True,
                  labelsize=7)
    
  plt.tight_layout()
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()