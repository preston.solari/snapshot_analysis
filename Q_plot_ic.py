#!/usr/bin/env python

# code to plot Q parameter for any snapshot

import numpy as np
import matplotlib.pyplot as plt

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
#from readsnap import readsnap
from readsnap_no_acc import readsnap
from mass_assignment_secant_method import volumetric_density, mass_dist, surface_density

def main():
  fdir="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/python/"

  listsims=["hernquist", "nfw", "iso", "bur", "ein"] 
  namesim=["hernquist", "nfw", "iso", "burkert", "einasto"] 
  # listsims=["hernquist"]
  # namesim=["hernquist"]
  lensims = len(listsims)

  names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  ls = ['--', '-', '-.', '+-', '.-']
  rmax = 30
  mass = 1.11e-05
  G = 43007.1

  fig, axs = plt.subplots(2,1)

  plt.style.use('seaborn-bright')

  for sim in range(len(listsims)):
    frun = fname + "toomreQ_" + listsims[sim] + ".txt" # location of the file
    fsnap = fdir + namesim[sim] + "/output/toomreQ_120.dat"

    dat = data(frun)
    r, Q = dat[0], dat[1]

    for i in range(len(r)):
      if r[i] >= rmax:
        ind = i
        break
    
    fdat = data(fsnap)
    fr, fQ = fdat[0], fdat[1]
    
    axs[0].plot(r[::7], Q[::7], ls[sim], label = names[sim])
    axs[1].plot(fr, fQ, ls[sim], label = names[sim])

  for ax in axs.flat:
    ax.tick_params(axis='both',which='both',direction='in',
                    top=True,left=True,right=True,bottom=True,
                    labelsize=10)

    ax.set_xlabel("Radius [kpc]")
    ax.set_ylabel(r'$Q$')
    ax.set_xlim(0, rmax)
    ax.set_ylim(0, 10)

  axs[0].legend(loc='upper right',shadow=True)

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
