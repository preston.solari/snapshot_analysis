#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter

from den_map import data

# Main definition
def main():
  # fdir="/home/cronex/Documentos/Doctorado/sims/sm-04-10-2018/"
  fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"

  models=['11','34','46','55']
  mod_name=['SGS2','SGS16','SGS24','SGS29']
  spines=[0, 3, 1, 2]
  ts=0.6

  snaps=['000','008','014','020'] # snapshots to graph
  line_patterns=['-','-.','--','-*']

  plt.style.use('seaborn-bright')

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'normal',
             'size':7}

  rowp=1
  colp=4
  fig,axs=plt.subplots(rowp, colp, sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0.03, 'wspace': 0.05}, dpi=130)
  
  k=j=0
  for model in models:
    frun=fdir+"sim_"+model+"/"
    i=0
    for snap in snaps:
      fnamed=frun+"toomreQ_"+snap+".dat"
      pd=data(fnamed) # raw data for toomre's parameter
      
      if k == 2:
        if snap in ['000','020']:
          axs[k].plot(pd[0],pd[1],line_patterns[i],lw=0.7,label="_nolegend_")
        else:
          axs[k].plot(pd[0],pd[1],line_patterns[i],lw=0.7,label="")
      else:
        axs[k].plot(pd[0],pd[1],line_patterns[i],lw=0.7,label=r'$t = %s \,\,\mathrm{[Gyrs]}$' %(float(snap)*ts))
      axs[k].text(0.5,5.2,mod_name[models.index(model)],**fontlabel)
      
      lw = 1.1
      for side in axs[k].spines.keys():
        if spines[k] == 0: # barred 
          axs[k].spines[side].set_color('deepskyblue')
          axs[k].spines[side].set_linewidth(lw)
        elif spines[k] == 1: # transient
          axs[k].spines[side].set_color('gold')
          axs[k].spines[side].set_linewidth(lw)
        elif spines[k] == 2: # completely unbarred
          axs[k].spines[side].set_color('orangered')
          axs[k].spines[side].set_linewidth(lw)
        elif spines[k] == 3: # delayed bar
          axs[k].spines[side].set_color('limegreen')
          axs[k].spines[side].set_linewidth(lw)

      if k == 0:
        axs[k].legend(loc='lower right',shadow=True, fontsize=6)
      if k == 2:
        axs[k].legend(labels=["bar present","bar disappears"], loc="lower right", shadow=True, fontsize=6)

      i+=1
    k+=1

  for ax in axs.flat:
    ax.grid()
    ax.label_outer()
    ax.set_xlim(0.25,14)
    ax.set_ylim(0.5,5.5)
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=8)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel(r'$\mathrm{Radius} \,\,\mathrm{[kpc]}$',fontsize=10, labelpad=-5)
  plt.ylabel(r'$Q$',fontsize=11, labelpad=-10)

  plt.show()

if __name__ == '__main__':
  main()
