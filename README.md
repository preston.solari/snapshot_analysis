# snapshot_analysis

Dedicated to python (and other languages) script to analyze GADGET (and perhaps other codes) snapshots of isolated disc galaxies.

This include:
- Phase-space particle plotting
- Density maps
- Fourier analysis
- Distorsion parameter
- Toomre criterion (Q)
- Bar longitude through distorsion parameter
- Rotation curve and dispersion velocities
- Pattern speed and phase of bar
- Perturbation analysis (Lindblad resonances)
- Spectral analysis through Lindblad resonances
- Velocity maps
- Radial distribution of particles and RDF
- Angular momentum
- Corotation ratio of bars
- Interparticle separation in phase-space
- Orbital analysis
