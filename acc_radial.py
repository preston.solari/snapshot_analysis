import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/scripts")

from plot_den_maps import adjust_center_mass_unit
from accelerations import acc_components_cyl, radii_avg_1c, radii_avg_3d
from MASE import calculate_a, force_hernquist, halo_mass, virial_radius
import readsnap as reads
from load_from_snapshot import load_from_snapshot as load_snap

def main():
  G = 43007.1
  md = 0.0
  hubble = 0.1
  c = 10.0
  v200 = 160.0

  rv = virial_radius(v200, hubble)
  mh = halo_mass(v200, md, G, hubble)
  rs = calculate_a(rv, c)
  
  fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/halo_short/"
  # fname = "/home/cronex/Documentos/Doctorado/eqforce/out/"
  fname = "/home/cronex/Documentos/Doctorado/gizmo-public/sims/halo_test/output_coarse/"
  model = ""
  
  frun = fname+model+'/'
  # dh = reads.readsnap(frun, 3, 1) # disk particles' data
  ph = load_snap('Coordinates', 1, fname, 5)
  vh = load_snap('Velocities', 1, fname, 5)
  ah = load_snap('Acceleration', 1, fname, 5)
  # ah = load_snap('keys', 1, fname, 5)

  # ph = dh['p']
  # vh = dh['v']
  # ah = dh['a']
  # mp = dh['m'][0]
  
  x, y, z = adjust_center_mass_unit(ph[:,0], ph[:,1], ph[:,2])
  vx, vy, vz = adjust_center_mass_unit(vh[:,0], vh[:,1], vh[:,2])
  ax, ay, az = adjust_center_mass_unit(ah[:,0], ah[:,1], ah[:,2])
  ax = ah[:,0]
  ay = ah[:,1]
  az = ah[:,2]
  
  a_ax, a_ay, a_az = force_hernquist(x, y, z, rs, mh, G)
  
  a_real = np.sqrt(ax**2 + ay**2 + az**2)
  a_theo = np.sqrt(a_ax**2 + a_ay**2 + a_az**2)
  
  r = np.sqrt(x**2 + y**2 + z**2)
  max_r = 20
  nrings = 100
  radii = np.linspace(0, max_r, nrings+1)
  nr, a_real_avg, a_real_sigma_avg = radii_avg_1c(r, a_real, radii)
  nr, a_theo_avg, a_theo_sigma_avg = radii_avg_1c(r, a_theo, radii)
  
  fig, axs = plt.subplots()
  
  axs.plot(nr, np.cumsum(a_real_avg), label="GIZMO")
  axs.plot(nr, np.cumsum(a_theo_avg), label="Analytic")
  
  #axs.errorbar(nr, np.cumsum(ia_real_avg), yerr=a_real_sigma_avg)
  # axs.plot(nr, a_real_avg)
  
  axs.legend(loc="best")
  
  plt.tight_layout()
  plt.show()
  plt.clf()
  

if __name__ == '__main__':
  main()
