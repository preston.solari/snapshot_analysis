#!/usr/bin/env python

# Python program to calculate the angular momentum transfer of a set of simulations

import matplotlib.pyplot as plt
import numpy as np

from den_map import data
import resonances as res
from plot_particles_disc import snaps_range
from ang_momentum_norm import angular_momentum_xyz, angular_momentum_xyz_norm
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa

# Main definition
def main():
  # fname="/home/cronex/Documentos/Doctorado/gadget4-marzo/sims-predoc/"
  fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  
  # list of simulations
  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"] 
  # listsims=["SGS8", "SGS9", "SGS13", "SGS14"] 
  #listsims=["SGS15", "SGS19", "SGS20", "SGS21"] 
  #listsims=["SGS25", "SGS26", "SGS27"] 
  listsims=["SGS8"]
  # listsims=["hernquist", "nfw"] 
  lensims = len(listsims)

  # names of simulations
  # names = ['SGS1', 'SGS2', 'SGS7', 'SGS8']
  names = ["SGS8", "SGS9", "SGS13", "SGS14"] 
  #names = ["SGS15", "SGS19", "SGS20", "SGS21"] 
  #names=["SGS25", "SGS26", "SGS27"]
  #names=["SGS8"]
  # names = ["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  ls = ['-', '--', '-.', ':', 'x', '*']

  # List with all the timestamps
  initsnap = 0
  totsnap = 120
  skip = 6
  rsnap = range(initsnap,totsnap,skip)
  time = np.asarray(rsnap)
  time = time * 0.1

  jump = 1

  Ldt_f = [] 
  Lht_f = []
  Ldti_f = []
  Lhti_f = []

  fig,axs = plt.subplots(1, 1, sharex = True, sharey = True,
                         gridspec_kw = {'hspace': 0.03, 'wspace': 0.03})
  
  plt.style.use('seaborn-bright')

  fontlabel = {'fontname':'DejaVu Sans Mono',
               'weight':'bold',
               'size':11}

  # Main loop to plot the angular momentum of all the simulations in 'listsims' list
  row = col = 0
  for sim in range(lensims):
    # frun = fname + listsims[sim] + "/output/" # location of the file
    frun = fname + listsims[sim] + "/" # location of the file

    # Lists to store the disc and halo momentum information
    Ld = []
    Lh = []

    for snap in rsnap:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)

      print("\n############# Plotting angular momentum for %s (of %s) - snapshot %s of %s #############\n" % (str(sim+1), str(lensims), str(snap+1), str(totsnap)))

      # Files with the positions and velocities of all the particles for disc and halo
      dd = rsa.readsnap(frun, ext, 2)
      dh = rsa.readsnap(frun, ext, 1)

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities
      ph = dh['p']  # halo particle positions
      vh = dh['v']  # halo particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      pd = np.asarray([x[::jump], y[::jump], z[::jump]])
      vd = np.asarray([vx[::jump], vy[::jump], vz[::jump]])
      pd = pd.T
      vd = vd.T
      Nd = float(len(pd))
      
      # remove center of mass (halo)
      x, y, z, vx, vy, vz = adjust_center_mass(ph[:, 0], ph[:, 1], ph[:, 2], vh[:, 0], vh[:, 1], vh[:, 2])
      ph = np.asarray([x[::jump], y[::jump], z[::jump]])
      vh = np.asarray([vx[::jump], vy[::jump], vz[::jump]])
      ph = ph.T
      vh = vh.T
      Nh = float(len(ph))

      rd = np.max(np.sqrt(pd[:, 0]**2 + pd[:, 1]**2 + pd[:, 2]**2)) # extension of disc
      rh = np.max(np.sqrt(ph[:, 0]**2 + ph[:, 1]**2 + ph[:, 2]**2)) # extension of halo
      nratio = Nh/Nd

      Lxd, Lyd, Lzd = angular_momentum_xyz_norm(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2], 0, rd)    # angular momentum of disc
      Lxh, Lyh, Lzh = angular_momentum_xyz_norm(ph[:, 0], ph[:, 1], ph[:, 2], vh[:, 0], vh[:, 1], vh[:, 2], 0, rh)    # angular momentum of halo

      Ld.append([Lxd, Lyd, Lzd])
      Lh.append([Lxh, Lyh, Lzh])

    Ld = np.asarray(Ld)
    Lh = np.asarray(Lh)

    # normalizing with initial momentum for both components
    Ld_0 = np.sqrt(Ld[0, 0]**2 + Ld[0, 1]**2 + Ld[0, 2]**2)
    # Ld_0 = Ld[0, 0]**2 + Ld[0, 1]**2 + Ld[0, 2]**2
    # Lh_0 = Lh[0, 0]**2 + Lh[0, 1]**2 + Lh[0, 2]**2
    # Lt_0 = np.sqrt(Ld_0 + Lh_0)

    # determine total ang. momentum
    Ldt = []
    Lht = []
    for i in range(len(Ld)):
      Ldt.append(np.sqrt(Ld[i, 0]**2 + Ld[i, 1]**2 + Ld[i, 2]**2))
      Lht.append(np.sqrt(Lh[i, 0]**2 + Lh[i, 1]**2 + Lh[i, 2]**2))
    Ldt = np.asarray(Ldt)
    Lht = np.asarray(Lht)
    

    #Ld = Ld / Ld_0
    #Lh = Lh / Ld_0
    Ldt = Ldt / Ld_0
    # Ldt = Ldt / Lt_0
    Lht = Lht / Ld_0 / Ldt * nratio
    print(Lht, Ldt, nratio)

    # get final values for total Ld and Lh
    #Ldt_f.append(Ldt[-1])
    #Lht_f.append(Lht[-1])

    # get total (Ld_final - Ld_initial) and (Lh_final - Ld_initial)
    #Ldti_f.append(Ldt[-1] - Ldt[0])
    #Lhti_f.append(Lht[-1] - Lht[0])

    # axs.plot(time, Ldt, ls[sim], label = names[sim])
    axs.plot(time, Ldt, '-', label = r'$L_t$ (disc) (' + names[sim] + ')')
    axs.plot(time, Lht, '.-', label = r'$L_t$ (halo) (' + names[sim] + ')')

    # axs[col].text(0.5, 0.5, names[sim],
    #                    bbox = dict(facecolor = 'white', edgecolor = 'none', alpha = 0.8),
    #                    **fontlabel)

    if sim == lensims-1:
      # axs[col]. legend(loc="best")
      axs. legend(loc="best")
    
    # col = col + 1

  # for ax in axs.flat:
  #   ax.grid()
  #   ax.label_outer()
  #   ax.set_xlim(0, np.max(time))
  #   ax.set_ylim(0, 1.1)
  #   ax.tick_params(axis = 'both', which = 'both', direction = 'in',
  #                  top = True, left = True, right = True, bottom = True,
  #                  labelsize = 10)
                  
  axs.grid()
  # axs.label_outer()
  axs.set_xlim(0, np.max(time))
  axs.set_ylim(0, 1.1)
  axs.set_xlabel('Time [Gyrs]', fontsize = 12)
  axs.set_ylabel(r'$L_{\mathrm{d,h}}/L_{0}$', fontsize = 12)
  axs.tick_params(axis = 'both', which = 'both', direction = 'in',
                  top = True, left = True, right = True, bottom = True,
                  labelsize = 10)

  # fig.add_subplot(111, frameon = False)
  # plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  # plt.grid(False)
  # plt.xlabel('Time [Gyrs]', fontsize = 12)
  # plt.ylabel(r'$L_{\mathrm{t}}/L_{0}$', fontsize = 12)

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
