#!/usr/bin/env python

# Python program to calculate the angular momentum transfer of a set of simulations

import matplotlib.pyplot as plt
import numpy as np
import sys

from den_map import data
import resonances as res
from plot_particles_disc import snaps_range
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa

# Function to compute the angular momentum
def angular_momentum_xyz(x, y, z, vx, vy, vz, massp):
  """
  Arguments:
    * x, y, z    --> position of particle
    * vx, vy, vz --> velocity of particle
    * massp      --> mass of the component's particle
    * r_low      --> low-limit for radius measurement
    * r_up       --> upper-limit for radius measurement
  """
  Lx = 0
  Ly = 0
  Lz = 0

  for i in range(len(x)):
    Lx = Lx + massp * (y[i]*vz[i] - z[i]*vy[i])
    Ly = Ly + massp * (z[i]*vx[i] - x[i]*vz[i])
    Lz = Lz + massp * (x[i]*vy[i] - y[i]*vx[i])

  # tot_mass = massp * len(x)

  # Lt = np.sqrt(Lx**2 + Ly**2 + Lz**2) / tot_mass

  # Lx = Lx / tot_mass
  # Ly = Ly / tot_mass
  # Lz = Lz / tot_mass

  return Lx, Ly, Lz

# Function to compute the angular momentum (normalized with number of particles)
def angular_momentum_xyz_norm(x, y, z, vx, vy, vz, r_low, r_up, Nmax = None):
  """
  Arguments:
    * x, y, z    --> position of particle
    * vx, vy, vz --> velocity of particle
    * r_low      --> low-limit for radius measurement
    * r_up       --> upper-limit for radius measurement
    * Nmax       --> upper limit for number of particles
  """

  if Nmax == None:
    Nmax = len(x)
  
  Lx = 0
  Ly = 0
  Lz = 0

  N = 0
  for i in range(len(x)):
    r = np.sqrt(x[i]**2 + y[i]**2 + z[i]**2)
    if r_low <= r <= r_up: 
      Lx = Lx + (y[i]*vz[i] - z[i]*vy[i])
      Ly = Ly + (z[i]*vx[i] - x[i]*vz[i])
      Lz = Lz + (x[i]*vy[i] - y[i]*vx[i])
      
      N = N + 1
      if N == Nmax:
        break

  Lx = Lx / float(N)
  Ly = Ly / float(N)
  Lz = Lz / float(N)

  return Lx, Ly, Lz

# Function to compute the angular momentum (normalized with number of particles)
def angular_momentum_rad(x, y, z, vx, vy, vz, radii):
  Lx = []
  Ly = []
  Lz = []
  newr = []
  
  rad = np.sqrt(x**2 + y**2 + z**2)  

  for i in range(len(radii)-1):
    ind = np.where((rad > radii[i]) & (rad <= radii[i+1]))
    N = float(len(ind))
    Lx.append(np.sum(y[ind]*vz[ind] - z[ind]*vy[ind])/N)
    Ly.append(np.sum(z[ind]*vx[ind] - x[ind]*vz[ind])/N)
    Lz.append(np.sum(x[ind]*vy[ind] - y[ind]*vx[ind])/N)
    
    newr.append((radii[i+1]+radii[i])/2.0)

  return np.array(newr), np.array(Lx), np.array(Ly), np.array(Lz)
  
  
# Main definition
def main():
  #fdir = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/" # root directory of simulations
  # fdir = "./" # root directory of simulations
  # fdir = "/run/media/cronex/HV620S/primer-art-rerun/"
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"

  listsims=['SGS36'] # simulations
  listsims = ["SGS5o", "SGS5", "SGS5o_eqforce"]
  # listsims = ["SGS5o"]
  #listsims=['16','26','35','46','55','56','66'] # list of simulations in grid (unbarred)
  #listsims=['11','23','33','42','51','54','63'] # list of simulations in grid (barred)
  lensims = len(listsims)

  masspd = [1.3885e-05, 1.5075e-05] # mass of disc particles
  Nd = 240000.0                     # disc's particle number

  massph = [1.1485e-04, 1.1449e-04] # mass of halo particles
  Nh = 800000.0                     # halo's particle number

  nsnaps = 240  # number of snapshots to plot
  totsnap = 240 # total snapshots
  initsnap = 1 # initial snapshot
  skip = 20
  ts = 0.05

  # List with all the timestamps 
  rsnap1 = range(initsnap,totsnap+1,skip)
  time1 = np.asarray(rsnap1)
  time1 = time1 * ts
  
  rsnap2 = range(initsnap,120+1,skip/2)
  time2 = np.asarray(rsnap2)
  time2 = time2 * 0.1
  
  rsnaps = [rsnap1, rsnap2, rsnap1]
  times = [time1, time2, time1]

  #steps = snaps_range(initsnap, totsnap, nsnaps) # steps between snapshots
  
  row = 1
  col = lensims
  fig, axs = plt.subplots(row, col, sharex = 'col', sharey = 'row',
                       gridspec_kw = {'hspace': 0.0, 'wspace': 0.0},
                       figsize = (6 * lensims, 6))

  plt.style.use('seaborn-bright')

  fontlabel = {'fontname': 'DejaVu Sans Mono',
               'weight':' bold',
               'size': 10}

  fonts = 12

  Lmind = []  # minimum of L (disc)
  Lmaxd = []  # maximum of L (disc)
  Lminh = []  # minimum of L (halo)
  Lmaxh = []  # maximum of L (halo)

  # Main loop to plot the angular momentum of all the simulations in 'listsims' list
  if isinstance(axs, np.ndarray):
    gaxs = axs.flat
  else:
    gaxs = [axs]
  # sys.exit()
  for sim,ax in zip(range(len(listsims)), gaxs):
    #frun = fdir + "sim_" + listsims[sim] + "/" # location of the file
    frun = fdir + listsims[sim] + "/" # location of the file

    # Lists to store the disc and halo momentum information
    Ld = []
    Lh = []

    # tot_massd = Nd * masspd[sim] # total mass of the disc
    # tot_massh = Nh * massph[sim] # total mass of the halo

    rsnap = rsnaps[sim]
    time = times[sim]
    for snap in rsnap:
      # ext = res.snap_mask(snap, 3) # file's mask (last three digits)

      # Files with the positions and velocities of all the particles for disc and halo
      #fsnapd = frun + "particles_python_disc_" + ext + ".dat" 
      #fsnaph = frun + "particles_python_halo_" + ext + ".dat"

      #pd = data(fsnapd, 1) # obtaining positions for all the particles
      #ph = data(fsnaph, 1) # obtaining positions for all the particles
      dd = rsa.readsnap(frun, snap, 2)
      # dh = rsa.readsnap(frun, snap, 1)

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities
      # ph = dh['p']  # halo particle positions
      # vh = dh['v']  # halo particle velocities

      rd = np.max(np.sqrt(pd[:,0]**2 + pd[:,1]**2 + pd[:,2]**2)) # extension of disc
      # rh = np.max(np.sqrt(ph[:,0]**2 + ph[:,1]**2 + ph[:,2]**2)) # extension of halo
      
      #Lxd, Lyd, Lzd = angular_momentum_xyz(pd[0], pd[1], pd[2], pd[3], pd[4], pd[5], masspd[sim], tot_massd, 0, rd)    # angular momentum of disc
      #Lxh, Lyh, Lzh = angular_momentum_xyz(ph[0], ph[1], ph[2], ph[3], ph[4], ph[5], massph[sim], tot_massh, 0, rh)    # angular momentum of halo

      Lxd, Lyd, Lzd = angular_momentum_xyz_norm(pd[:,0], pd[:,1], pd[:,2], vd[:,0], vd[:,1], vd[:,2], 0, rd)    # angular momentum of disc
      # Lxh, Lyh, Lzh = angular_momentum_xyz_norm(ph[:,0], ph[:,1], ph[:,2], vh[:,0], vh[:,1], vh[:,2], 0, rh)    # angular momentum of halo

      Ld.append([Lxd, Lyd, Lzd])
      # Lh.append([Lxh, Lyh, Lzh])

    Ld = np.asarray(Ld)
    # Lh = np.asarray(Lh)

    # Calculate the minimum and maximum values of Ld and Lh to limit plot axes
    # Lmind.append(np.min(Ld))
    # Lmaxd.append(np.max(Ld))
    
    # Lminh.append(np.min(Lh))
    # Lmaxh.append(np.max(Lh))

    # normalizing with initial momentum for both components
    Ld_0 = np.sqrt(Ld[0, 0]**2 + Ld[0, 1]**2 + Ld[0, 2]**2)
    # Ld_0 = Ld[0, 0]**2 + Ld[0, 1]**2 + Ld[0, 2]**2
    # Lh_0 = Lh[0, 0]**2 + Lh[0, 1]**2 + Lh[0, 2]**2
    # Lt_0 = np.sqrt(Ld_0 + Lh_0)

    # determine total ang. momentum
    Ldt = []
    Lht = []
    for i in range(len(Ld)):
      Ldt.append(np.sqrt(Ld[i, 0]**2 + Ld[i, 1]**2 + Ld[i, 2]**2))
      # Lht.append(np.sqrt(Lh[i, 0]**2 + Lh[i, 1]**2 + Lh[i, 2]**2))
    Ldt = np.asarray(Ldt)
    # Lht = np.asarray(Lht)
    

    #Ld = Ld / Ld_0
    #Lh = Lh / Ld_0
    Ldt = Ldt / Ld_0
    print(Ldt)
    # Ldt = Ldt / Lt_0
    #Lht = Lht / Ld_0 / Ldt * nratio
    #print(Lht, Ldt, nratio)

    # Plotting of Lx, Ly, and Lz for the disc
    # axs[0, sim].plot(time, Ld[:, 0], 'k-', label = r'$L_x$')
    # axs[0, sim].plot(time, Ld[:, 1], 'k--', label = r'$L_y$')
    # axs[0, sim].plot(time, Ld[:, 2], 'k.-', label = r'$L_z$')

    # # Plotting of Lx, Ly, and Lz for the halo
    # axs[1, sim].plot(time, Lh[:, 0], 'k-')
    # axs[1, sim].plot(time, Lh[:, 1], 'k--')
    # axs[1, sim].plot(time, Lh[:, 2], 'k.-')

    # ax.plot(time, Ldt[:, 2], 'k-', label = r'$L_z$')
    ax.plot(time, Ldt, 'k-', label = r'$L_z$ '+listsims[sim])
    # axs[0].plot(time, Ld[:, 1], 'k--', label = r'$L_y$')
    # axs[0].plot(time, Ld[:, 2], 'k.-', label = r'$L_z$')

    # Plotting of Lx, Ly, and Lz for the halo
    # ax.plot(time, Lht[:, 2], 'k-')
    # ax.plot(time, Lht, 'k-')
    # axs[1].plot(time, Lht[:, 1], 'k--')
    # axs[1].plot(time, Lht[:, 2], 'k.-')


    # Legend
    # if sim == lensims-1:
    #   #axs[0, sim].legend(loc = 'lower right', shadow = True)
    #   ax.legend(loc = 'lower right', shadow = True)
    ax.legend(loc = 'lower right', shadow = True)

  # To set title (model name)
  #axs[0, 0].set_title('SGS6 (faint bar)')
  #axs[0, 1].set_title('SGS9 (strong bar)')

  # Axes limits
  #xlim = (np.min(time), np.max(time))
  #Llimmin = Lmind + Lminh 
  #Llimmax = Lmaxd + Lmaxh 
  # Properties of plot axes
  # i = 0
  # for ax in axs.flat:
  #   ax.label_outer()
  #   #ax.set_xlim(xlim[0], xlim[1])
  #   #ax.set_ylim(Llimmin[i], Llimmax[i])
  #   #ax.set(adjustable = 'box', aspect = 'equal')
  #   ax.tick_params(axis = 'both', which = 'both', direction = 'in',
  #                  top = True, left = True, right = True, bottom = True,
  #                  labelsize = fonts)
  #   i = i + 1
  
  for ax in axs.flat:
    # ax.label_outer()
    #ax.set_xlim(xlim[0], xlim[1])
    #ax.set_ylim(Llimmin[i], Llimmax[i])
    #ax.set(adjustable = 'box', aspect = 'equal')
    ax.set_xlabel("Time [Gyrs]")
    ax.set_ylabel(r"$L_z/L_0$ (disc)")
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                   top = True, left = True, right = True, bottom = True,
                   labelsize = fonts)
  

  # 'Phanthom' plot to use just one label for all the plots' axes
  # fig.add_subplot(111, frameon = False)
  # plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  # plt.grid(False)
  # plt.xlabel(r'Time [Gyrs]', fontsize = fonts)
  # plt.ylabel(r'$L_x,\,L_y,\,L_z \,\,$ [kpc km s$^{-1}$]', fontsize = fonts)


  # Setting the values for all axes.
  xlim = (np.min(time), np.max(time))
  ylim = (0, 1.1)
  plt.setp(axs, xlim = xlim, ylim = ylim)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
