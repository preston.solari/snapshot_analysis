#!/usr/bin/env python

# Program to draw eta vs. radius beside density map
# with ellipses denoting the bas's longitude

from distorsion_parameter import distorsion_parameter
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import seaborn as sns
from scipy.interpolate import griddata as gd, interpn
import scipy.ndimage as ndimage
import matplotlib.colors as colors

from plot_particles_disc import snaps_range
import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from readsnap import readsnap

from mpl_toolkits.axes_grid1.inset_locator import inset_axes


# Example of making your own norm.  Also see matplotlib.colors.
# From Joe Kington: This one gives two different linear ramps:
class MidpointNormalize(colors.Normalize):
  def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
    self.midpoint = midpoint
    colors.Normalize.__init__(self, vmin, vmax, clip)

  def __call__(self, value, clip=None):
    # I'm ignoring masked values and all kinds of edge cases to make a
    # simple example...
    x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
    return np.ma.masked_array(np.interp(value, x, y))

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero.

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower offset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax / (vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highest point in the colormap's range.
          Defaults to 1.0 (no upper offset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

# Function to create scatter plot of density for a number of particles
def density_scatter(x , y, sort = True, bins = 20, **kwargs):
  """
  Scatter plot colored by 2d histogram
  """
  data, x_e, y_e = np.histogram2d(x, y, bins = bins)
  z = interpn((0.5*(x_e[1:] + x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1])), data ,np.vstack([x,y]).T, method = "splinef2d", bounds_error = False)

  # Sort the points by density, so that the densest points are plotted last
  if sort :
    idx = z.argsort()
    x, y, z = x[idx], y[idx], z[idx]

  return x,y,z

def main():
  # fdir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/sim_12/"
  fdir=["/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/hernquist/output",
        "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/nfw/output",
        "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/einasto/output"]

  fig,axs=plt.subplots(3,2,gridspec_kw={'hspace': 0.22, 'wspace': 0.02},
                       figsize=(8,12))

  plt.style.use('seaborn-bright')

  lal=17 # limits of right panel axes
  larx=(0,17) # limits of central panel axes (x)
  lary=(0.0,0.8) # limits of central panel axes (y)

  maxeta = 1.15

  ####### Hernquist #######
  lbh_l=9.17   # L1 bar longitude
  # lbh_u=13.20 # L2 bar longitude
  # lbq_2=8.75  # Lq bar longitude

  ####### NFW #######
  lbn_l=8.79   # L1 bar longitude
  # lbn_u=13.20 # L2 bar longitude
  # lbq_2=8.75  # Lq bar longitude

  ####### Einasto #######
  lbe_l=4.47   # L1 bar longitude
  # lbe_u=5.0 # L2 bar longitude
  # lbq_2=8.75  # Lq bar longitude

  ####### All bar measurements #######
  allbar = (lbh_l, lbn_l, lbe_l)
  # allbar = np.asarray(allbar)

  inc=33
  inc=(inc*np.pi)/180 # inclination in radians

  an=np.linspace(0, 2*np.pi, 100) # data to plot circles (with or without inclination) for L1 and L2
  rmax = [15, 20, 7]
  jump = 10

  max_vel=120 # in km s^-1

  origin='lower' # origin for density map

  # Colorbar properties of density map
  xins = inset_axes(axs[-1,-1],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[-1,-1].transAxes,
                    borderpad=0,
                    )
  
  for sim in range(len(fdir)):
    dd = readsnap(fdir[sim], 120, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']  # disc particle velocities

    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    x = x[::jump]
    y = y[::jump]
    # vt, vr, vz = res.velocities(x, y, vx, vy, vz)

    rad = np.linspace(0, rmax[sim], 30)

    ####################### Left panel ###############################
    eta = []
    r = []
    for j in range(len(rad)-1):
      nx = []
      ny = []
      for i in range(len(x)):
        nr = np.sqrt(x[i]**2 + y[i]**2)
        if rad[j] <= nr <= rad[j+1]:
          nx.append(x[i])
          ny.append(y[i])
      nx = np.asarray(nx)
      ny = np.asarray(ny)
      eta.append(distorsion_parameter(nx, ny))
      r.append((rad[j+1] + rad[j])/2)
    
    eta = np.asarray(eta)
    r = np.asarray(r)

    ietamax = np.argmax(eta)

    uplimi = 0
    for i in range(ietamax, len(eta)):
      if eta[i] < 0.1*eta[ietamax]:
        uplimi = i
        break
    
    print(r[uplimi])
    
    axs[sim, 0].plot(r,eta,'k-', linewidth = 2, label = r"$\eta$")
    axs[sim, 0].plot((allbar[sim],allbar[sim]),(0,maxeta),'y--',label=r'$L_l$') # lower-limit
    axs[sim, 0].plot((r[uplimi],r[uplimi]),(0,maxeta),'r-.',label=r'$L_u$') # upper-limit
    # axs[sim, 0].fill_between(r,eta,facecolor='blue',alpha=0.25,label=r'$\eta$') # filling for eta

    # legend for eta curve
    axs[sim, 0].legend(loc="upper left", shadow = True, fancybox = True)

    # axes labels
    axs[sim, 0].set_xlabel(r'Radius [kpc]',fontsize=15)
    axs[sim, 0].set_ylabel(r'$\eta$',fontsize=15)

    # axs[sim, 0].grid(zorder=0) # grid

    ####################### Right panel ##############################

    lev=np.asarray([10, 20, 50, 100, 200, 300, 600, 1100, 1500]) # levels for density contour map
    # calculate the 2D density of the data given
    counts,xbins,ybins=np.histogram2d(x,y,bins=60)
    # make the contour plot
    axs[sim, 1].contour(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
        ybins.min(),ybins.max()],origin=origin,colors='cyan',linewidths=0.7,
        linestyles='solid', alpha=0.6)

    #plt.contourf(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
    #    ybins.min(),ybins.max()],origin=origin,cmap='plasma')

    # scatter to draw density gradient
    cmap=plt.cm.gray
    cmap=shiftedColorMap(cmap,start=0.0,midpoint=0.5,stop=1.0,name='shifted')

    xi,yi,zi=density_scatter(x,y,axs[sim, 1],bins=[60,60])
    im=axs[sim, 1].scatter(xi,yi,c=zi,s=80,alpha=0.5,cmap=cmap,norm=MidpointNormalize(midpoint=80.))
    #im=axs[sim, 1].scatter(xi,yi,c=zi,s=40,alpha=0.5,cmap='shifted')

    # vmin=0
    # vmax=1000

    # ellipse indicating bar's longitude
    # axs[sim, 1].plot(lb1_2*np.cos(an)/np.cos(inc), lb1_2*np.sin(an),color='red',linewidth=2.0,linestyle='-')
    # axs[sim, 1].plot(lb2_2*np.cos(an)/np.cos(inc), lb2_2*np.sin(an),color='lime',linewidth=2.0,linestyle='--')
    # axs[sim, 1].plot(lbq_2*np.cos(an)/np.cos(inc), lbq_2*np.sin(an),color='black',linewidth=2.0,linestyle='-.')
    axs[sim, 1].plot(allbar[sim]*np.cos(an), allbar[sim]*np.sin(an),color='yellow',linewidth=2.0,linestyle='--')
    axs[sim, 1].plot(r[uplimi]*np.cos(an),r[uplimi]*np.sin(an),color='red',linewidth=2.0,linestyle='-.')
    # axs[sim, 1].plot(lbq_2, lbq_2,color='black',linewidth=2.0,linestyle='-.')

    # color bar
    if sim == len(fdir)-1:
      # cblim=np.linspace(vmin,vmax,1000)
      # cbtick=np.linspace(vmin,vmax,9)
      # cbar=fig.colorbar(im,cax=xins,boundaries=cblim,ticks=cbtick)
      cbar=fig.colorbar(im,cax=xins)
      cbar.set_label(r'$N_{\mathrm{d}}$',fontsize=12,labelpad=-1)
      cbar.ax.tick_params(labelsize=12)

    # axes labels
    axs[sim, 1].set_xlabel(r'$x\,$ [kpc]',fontsize=15)
    axs[sim, 1].set_ylabel(r'$y\,$ [kpc]',labelpad=-1.5,fontsize=15)

    ##################################################################

    # axes limits
    axs[sim, 0].set_xlim(0, rmax[sim])
    axs[sim, 0].set_ylim(0.0, maxeta)
    axs[sim, 1].set_xlim(-rmax[sim], rmax[sim])
    axs[sim, 1].set_ylim(-rmax[sim], rmax[sim])

  # shape of subplots and format for ticks
  for ax in axs.flat:
    x0,x1=ax.get_xlim()
    y0,y1=ax.get_ylim()
    ax.set_aspect((x1-x0)/(y1-y0))
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=13.5)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
