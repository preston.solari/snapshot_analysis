import numpy as np

from readsnap_no_acc import readsnap
from resonances import velocities, avg_velocities, snap_mask, omegar_fun, kappa, surf_density
from plot_den_maps import adjust_center_mass

def main():
  frun = "/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/SGS1"
  ftoomre = "/home/ia/alexlg/analysis/toomre/"

  model = frun.strip().split("/")
  model = [word for word in model if word != ''][-1]
 
  ts = 0.05
  snap = 40
  timesnap = int(snap*ts)

  dd = readsnap(frun, snap, 2)

  pd = dd['p']
  vd = dd['v']
  md = dd['m']
  x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

  print("Calculating particle velocities...")
  vt, vr, vz = velocities(x, y, vx, vy, vz)

  G = 43007.1
  rmax = 10.0
  nrings = 40.0
  radii = np.linspace(0, rmax+rmax/nrings, nrings+1)

  rp = np.sqrt(x**2 + y**2)

  print("Calculating average velocities...")
  nrad, avg_vel = avg_velocities((rp, vt, vr, vz), radii)
  avg_vt = avg_vel[1]
  avg_sigmar = avg_vel[3]
  kap = kappa((omegar_fun(nrad, avg_vt)))
  
  nrad, den = surf_density(rp, nrad, md[0])
  #print(den)
  
  Q = avg_sigmar[:-1]*kap/3.36/G/den

  nrad = np.round(nrad, 2)
  avg_vt = np.round(avg_vt, 2)
  avg_sigmar = np.round(avg_sigmar, 2)
  kap = np.round(kap, 2)
  den = np.round(den, 2)
  Q = np.round(Q, 2)

  for i in range(len(nrad)):
    print("%s %s %s %s" % (str(nrad[i]), str(avg_sigmar[i]), str(kap[i]), str(Q[i])))
  
  f = open(ftoomre+'toomre_'+model+'_'+str(timesnap)+'G.txt', 'w+')
  for i in range(len(nrad)):
    f.write("%s %s\n" % (nrad[i], Q[i]))
  f.close()

if __name__ == '__main__':
  main()
