#!/usr/bin/env python

from distorsion_parameter import distorsion_parameter
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import seaborn as sns
from scipy.interpolate import griddata as gd, interpn
import scipy.ndimage as ndimage
import matplotlib.colors as colors

from plot_particles_disc import snaps_range
import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from readsnap import readsnap

def main():
  fdir=["/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/hernquist/output",
        "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/nfw/output",
        "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/einasto/output"]
  
  fig,axs=plt.subplots(3,1, sharex=True,sharey=True,
                       gridspec_kw={'hspace': 0.0, 'wspace': 0.02},figsize=(8,12))
  
  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':13}

  plt.style.use('seaborn-bright')

  ####### Hernquist #######
  lbh_l=9.17   # L1 bar longitude
  lbh_u=12.55 # L2 bar longitude

  ####### NFW #######
  lbn_l=8.79   # L1 bar longitude
  lbn_u=13.44 # L2 bar longitude

  ####### Einasto #######
  lbe_l=4.47   # L1 bar longitude
  lbe_u=6.87 # L2 bar longitude

  ####### All bar measurements #######
  allbar = ((lbh_l, lbh_u), (lbn_l,lbn_u), (lbe_l,lbe_u))
  allbar = np.asarray(allbar)

  name=["Hernquist", "NFW", "Einasto"]

  rmax = [17, 20, 16]
  bar_p = [16, 11, 17]
  jump = 2

  for sim in range(len(fdir)):
    dd = readsnap(fdir[sim], 120, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']  # disc particle velocities

    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    x = x[::jump]
    y = y[::jump]
    vx = vx[::jump]
    vy = vy[::jump]
    vz = vz[::jump]

    pr = res.particle_radius(x,y)
    pv = res.velocities(x,y,vx,vy,vz)

    values=(pr,) + pv

    rad = np.linspace(0.1, rmax[sim], 50)
    newr, avg = res.avg_velocities(values, rad)

    axs[sim].plot(newr, avg[1]/rad[:-1], 'k-', linewidth=2.5)
    axs[sim].plot((0, rmax[sim]), (bar_p[sim], bar_p[sim]), 'k--', linewidth=2.1, alpha=0.7)
    axs[sim].plot((allbar[sim, 0], allbar[sim, 1]), (bar_p[sim], bar_p[sim]), 'r-', linewidth=2.2)

    axs[sim].text(12,29,name[sim],bbox=dict(fc='white',ec='none',alpha=0.7),
                              **fontlabel)

  for ax in axs.flat:
    #ax.label_outer()
    ax.grid(True)
    ax.set_xlim(0, 16)
    ax.set_ylim(0, 38)
    #ax.set_xlabel("Radius [kpc]", fontsize=14)
    #ax.set_ylabel("$\Omega_{\mathrm{b}}$ [km s$^{-1}$ kpc$^{-1}$]", fontsize=14)
    #ax.set(adjustable='box',aspect='equal')
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=13)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel("Radius [kpc]", fontsize=14)
  plt.ylabel("$\Omega$ [km s$^{-1}$ kpc$^{-1}$]", fontsize=14)

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()