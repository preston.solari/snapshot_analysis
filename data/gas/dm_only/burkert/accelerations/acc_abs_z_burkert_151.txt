0.05000 765.25102
0.15000 1682.92068
0.25000 2637.35329
0.35000 3328.72165
0.45000 4083.73031
0.55000 4467.01832
0.65000 4919.84787
0.75000 5179.58297
0.85000 5398.50291
0.95000 5508.14893
1.05000 5496.94701
1.15000 5566.96295
1.25000 5401.59308
1.35000 5391.26213
1.45000 5286.08995
1.55000 5208.11254
1.65000 5103.65107
1.75000 5006.19294
1.85000 4863.36320
1.95000 4830.25918
2.05000 4688.63617
2.15000 4524.16006
2.25000 4441.47296
2.35000 4375.73318
2.45000 4209.48147
2.55000 4163.07028
2.65000 4023.18024
2.75000 3916.48945
2.85000 3809.49655
2.95000 3711.12881
3.05000 3645.62544
3.15000 3513.90807
3.25000 3386.40517
3.35000 3301.26784
3.45000 3242.81374
3.55000 3156.87851
3.65000 3061.74314
3.75000 3002.22958
3.85000 2930.61027
3.95000 2843.01544
4.05000 2747.09393
4.15000 2714.10059
4.25000 2668.57490
4.35000 2562.49360
4.45000 2512.81018
4.55000 2496.00316
4.65000 2378.29368
4.75000 2315.95737
4.85000 2261.42446
4.95000 2174.88610
5.05000 2133.03676
5.15000 2092.12484
5.25000 2027.63882
5.35000 1996.52745
5.45000 1937.18246
5.55000 1856.59112
5.65000 1863.56515
5.75000 1794.99027
5.85000 1756.08942
5.95000 1737.71146
6.05000 1681.90119
6.15000 1662.43555
6.25000 1605.34815
6.35000 1566.73071
6.45000 1538.51852
6.55000 1496.97452
6.65000 1465.69875
6.75000 1424.71812
6.85000 1415.09433
6.95000 1377.45712
7.05000 1372.81218
7.15000 1331.20289
7.25000 1292.24348
7.35000 1262.72587
7.45000 1240.64115
7.55000 1211.39853
7.65000 1173.93992
7.75000 1146.59011
7.85000 1125.87009
7.95000 1106.51020
8.05000 1091.44128
8.15000 1055.38852
8.25000 1048.51233
8.35000 1028.22489
8.45000 1004.65854
8.55000 966.48541
8.65000 955.27730
8.75000 942.49547
8.85000 913.59619
8.95000 898.03569
9.05000 884.21821
9.15000 851.39390
9.25000 844.57824
9.35000 818.18814
9.45000 803.14202
9.55000 803.10949
9.65000 791.04469
9.75000 762.76699
9.85000 752.97139
9.95000 732.44019
10.05000 710.99135
10.15000 696.78648
10.25000 692.69185
10.35000 664.74020
10.45000 657.36151
10.55000 635.41932
10.65000 628.76093
10.75000 624.59939
10.85000 610.12011
10.95000 580.59943
11.05000 579.41024
11.15000 564.06073
11.25000 548.60154
11.35000 550.28477
11.45000 529.77955
11.55000 522.26487
11.65000 514.99870
11.75000 507.68552
11.85000 495.81306
11.95000 483.25902
12.05000 478.68394
12.15000 467.47637
12.25000 448.18075
12.35000 456.37245
12.45000 434.66988
12.55000 417.96406
12.65000 420.02538
12.75000 408.44662
12.85000 398.82754
12.95000 394.23113
13.05000 387.58354
13.15000 384.64527
13.25000 367.30319
13.35000 369.95039
13.45000 345.63675
13.55000 355.35720
13.65000 347.45055
13.75000 345.57984
13.85000 336.93798
13.95000 333.33062
14.05000 316.56918
14.15000 311.68030
14.25000 305.56982
14.35000 305.97190
14.45000 301.97568
14.55000 293.59743
14.65000 285.22771
14.75000 283.79994
14.85000 273.70963
14.95000 263.19998
15.05000 263.38277
15.15000 262.83906
15.25000 251.68658
15.35000 253.54456
15.45000 250.19181
15.55000 243.34056
15.65000 237.86040
15.75000 240.55947
15.85000 234.99844
15.95000 229.80424
16.05000 221.96175
16.15000 213.29125
16.25000 215.11205
16.35000 215.21534
16.45000 211.25613
16.55000 206.90094
16.65000 194.83991
16.75000 191.79928
16.85000 196.68772
16.95000 197.28927
17.05000 190.27814
17.15000 186.43857
17.25000 178.42641
17.35000 183.00104
17.45000 174.17782
17.55000 181.07796
17.65000 169.39557
17.75000 169.81196
17.85000 169.49014
17.95000 168.53264
18.05000 158.86998
18.15000 157.62271
18.25000 156.53197
18.35000 153.18458
18.45000 145.94723
18.55000 149.24894
18.65000 136.99228
18.75000 144.17197
18.85000 140.03198
18.95000 141.94732
19.05000 130.45566
19.15000 128.75682
19.25000 130.42167
19.35000 124.45811
19.45000 132.21841
19.55000 124.86050
19.65000 128.40565
19.75000 119.05079
19.85000 119.12837
19.95000 114.47885
