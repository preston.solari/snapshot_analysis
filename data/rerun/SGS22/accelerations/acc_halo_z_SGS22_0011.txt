0.05000 1154.86910
0.15000 2369.61756
0.25000 3437.14832
0.35000 4118.95878
0.45000 4838.28886
0.55000 5170.72433
0.65000 5554.60142
0.75000 5572.33308
0.85000 5570.36428
0.95000 5614.60395
1.05000 5649.43039
1.15000 5484.97643
1.25000 5483.99784
1.35000 5413.94733
1.45000 5140.86602
1.55000 5016.20138
1.65000 4908.79987
1.75000 4861.72686
1.85000 4676.23590
1.95000 4545.39896
2.05000 4408.44751
2.15000 4259.61670
2.25000 4191.89979
2.35000 4130.59218
2.45000 3918.53444
2.55000 3819.03779
2.65000 3827.55724
2.75000 3704.26529
2.85000 3536.92087
2.95000 3424.62492
3.05000 3335.64506
3.15000 3226.71713
3.25000 3170.68445
3.35000 3135.79256
3.45000 3018.43833
3.55000 2876.55099
3.65000 2813.88012
3.75000 2794.08283
3.85000 2677.64231
3.95000 2589.02173
4.05000 2564.54480
4.15000 2466.49888
4.25000 2445.47589
4.35000 2342.05653
4.45000 2281.22343
4.55000 2239.78570
4.65000 2198.78878
4.75000 2103.72643
4.85000 2043.68525
4.95000 2026.76409
5.05000 1966.67768
5.15000 1891.16079
5.25000 1853.56208
5.35000 1803.97336
5.45000 1750.64259
5.55000 1725.62427
5.65000 1689.35313
5.75000 1619.85691
5.85000 1619.45300
5.95000 1550.53364
6.05000 1526.13022
6.15000 1503.67288
6.25000 1483.25233
6.35000 1426.95513
6.45000 1390.77222
6.55000 1378.82374
6.65000 1354.61748
6.75000 1314.50837
6.85000 1274.91339
6.95000 1255.67275
7.05000 1218.34064
7.15000 1186.93159
7.25000 1169.41104
7.35000 1142.52197
7.45000 1114.94919
7.55000 1088.32438
7.65000 1056.07833
7.75000 1043.90256
7.85000 1000.40380
7.95000 975.17846
8.05000 956.46133
8.15000 925.08285
8.25000 911.74707
8.35000 876.44259
8.45000 861.68738
8.55000 858.15967
8.65000 813.24317
8.75000 804.94460
8.85000 779.38514
8.95000 749.19562
9.05000 739.97374
9.15000 718.71611
9.25000 727.65889
9.35000 686.56946
9.45000 678.87717
9.55000 673.57317
9.65000 649.49296
9.75000 639.17873
9.85000 629.08527
9.95000 603.23278
10.05000 594.96094
10.15000 576.53973
10.25000 564.64197
10.35000 554.28688
10.45000 542.68184
10.55000 526.14722
10.65000 521.11862
10.75000 520.86365
10.85000 490.58761
10.95000 477.96597
11.05000 470.06353
11.15000 459.06894
11.25000 447.81165
11.35000 442.65030
11.45000 433.02184
11.55000 423.43268
11.65000 423.71200
11.75000 407.31570
11.85000 400.46971
11.95000 390.81615
12.05000 381.37897
12.15000 386.25114
12.25000 367.06190
12.35000 367.84020
12.45000 348.29184
12.55000 343.30619
12.65000 349.44920
12.75000 337.96793
12.85000 331.80374
12.95000 312.93266
13.05000 314.04664
13.15000 299.63000
13.25000 299.88639
13.35000 290.80979
13.45000 272.93180
13.55000 268.35256
13.65000 270.77908
13.75000 261.29361
13.85000 261.33921
13.95000 260.81539
14.05000 247.54469
14.15000 247.74573
14.25000 239.68834
14.35000 239.89468
14.45000 234.25069
14.55000 231.75239
14.65000 225.20016
14.75000 223.31553
14.85000 222.06448
14.95000 221.65577
15.05000 214.70404
15.15000 206.15112
15.25000 206.38864
15.35000 197.02491
15.45000 194.09644
15.55000 200.82298
15.65000 187.99861
15.75000 182.23747
15.85000 176.36444
15.95000 171.92797
16.05000 172.67983
16.15000 176.68059
16.25000 166.01915
16.35000 159.74541
16.45000 165.79193
16.55000 153.86950
16.65000 157.73372
16.75000 149.71759
16.85000 148.96099
16.95000 144.60922
17.05000 137.68484
17.15000 131.36347
17.25000 136.98274
17.35000 126.23534
17.45000 122.96026
17.55000 128.38599
17.65000 129.19178
17.75000 118.67441
17.85000 109.90520
17.95000 117.51241
18.05000 114.82257
18.15000 115.97540
18.25000 112.11253
18.35000 110.58496
18.45000 104.69050
18.55000 104.22647
18.65000 107.59176
18.75000 110.35021
18.85000 100.94527
18.95000 101.29908
19.05000 104.18431
19.15000 103.11896
19.25000 98.31840
19.35000 97.16750
19.45000 95.84912
19.55000 91.97781
19.65000 89.26531
19.75000 90.82864
19.85000 85.48554
19.95000 90.56830
