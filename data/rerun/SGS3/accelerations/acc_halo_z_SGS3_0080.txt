0.05000 2951.92795
0.15000 6757.46687
0.25000 9182.43459
0.35000 10760.16116
0.45000 11939.00392
0.55000 12042.08847
0.65000 12262.75766
0.75000 12092.45103
0.85000 11805.48628
0.95000 11356.23043
1.05000 10754.09763
1.15000 10241.21244
1.25000 9777.12327
1.35000 9256.85090
1.45000 8758.54431
1.55000 8214.11089
1.65000 7853.59531
1.75000 7484.68017
1.85000 7122.11965
1.95000 6741.85229
2.05000 6458.78488
2.15000 6190.03847
2.25000 5897.82536
2.35000 5587.88166
2.45000 5380.42017
2.55000 5206.39046
2.65000 4990.56142
2.75000 4794.00841
2.85000 4583.25247
2.95000 4498.16816
3.05000 4325.99210
3.15000 4145.88251
3.25000 3974.61225
3.35000 3810.97348
3.45000 3745.18631
3.55000 3567.40385
3.65000 3482.43264
3.75000 3341.89157
3.85000 3207.71590
3.95000 3123.95399
4.05000 3030.49978
4.15000 2951.65693
4.25000 2886.43783
4.35000 2739.86491
4.45000 2694.73077
4.55000 2561.96127
4.65000 2468.21730
4.75000 2390.10380
4.85000 2291.51664
4.95000 2184.75827
5.05000 2158.27480
5.15000 2053.44551
5.25000 1913.14116
5.35000 1847.87772
5.45000 1779.67661
5.55000 1664.26959
5.65000 1620.17846
5.75000 1541.10858
5.85000 1493.92870
5.95000 1436.66927
6.05000 1355.69623
6.15000 1334.31296
6.25000 1270.53796
6.35000 1248.03915
6.45000 1228.31021
6.55000 1197.75490
6.65000 1150.71268
6.75000 1102.04182
6.85000 1094.89543
6.95000 1047.19158
7.05000 1018.35874
7.15000 996.23176
7.25000 957.02435
7.35000 930.70841
7.45000 912.09320
7.55000 896.82728
7.65000 857.33776
7.75000 821.06026
7.85000 810.77977
7.95000 773.21397
8.05000 763.65473
8.15000 720.26578
8.25000 714.00328
8.35000 692.61550
8.45000 668.90010
8.55000 642.31469
8.65000 638.84758
8.75000 618.36572
8.85000 600.46678
8.95000 592.78427
9.05000 584.55263
9.15000 553.99754
9.25000 554.06980
9.35000 537.70086
9.45000 535.23265
9.55000 523.32405
9.65000 499.91084
9.75000 488.60294
9.85000 483.32348
9.95000 472.19477
10.05000 462.41142
10.15000 455.62687
10.25000 440.28051
10.35000 446.52496
10.45000 439.45982
10.55000 426.73531
10.65000 410.09855
10.75000 407.37375
10.85000 399.33953
10.95000 389.38718
11.05000 381.13371
11.15000 369.39020
11.25000 371.53216
11.35000 354.08472
11.45000 358.43503
11.55000 353.50165
11.65000 336.33676
11.75000 332.22114
11.85000 331.64462
11.95000 313.16249
12.05000 312.23721
12.15000 308.48069
12.25000 300.59384
12.35000 293.31891
12.45000 280.36143
12.55000 276.46021
12.65000 274.85931
12.75000 271.48982
12.85000 258.06208
12.95000 256.29039
13.05000 250.39723
13.15000 237.13887
13.25000 243.98854
13.35000 230.55404
13.45000 223.95801
13.55000 222.45226
13.65000 215.02271
13.75000 215.38475
13.85000 211.14646
13.95000 202.39777
14.05000 200.31032
14.15000 193.06617
14.25000 189.53325
14.35000 177.76699
14.45000 177.38904
14.55000 183.17614
14.65000 179.05351
14.75000 180.50123
14.85000 162.69045
14.95000 164.87608
15.05000 164.27039
15.15000 162.65490
15.25000 169.25851
15.35000 156.19913
15.45000 155.16191
15.55000 151.68002
15.65000 155.75498
15.75000 149.51582
15.85000 145.45353
15.95000 152.11884
16.05000 147.79781
16.15000 154.86255
16.25000 141.47263
16.35000 144.94573
16.45000 141.20066
16.55000 140.86073
16.65000 139.20052
16.75000 132.84553
16.85000 128.57101
16.95000 125.48688
17.05000 126.23635
17.15000 134.39065
17.25000 126.79756
17.35000 126.96173
17.45000 119.80722
17.55000 127.60048
17.65000 119.74330
17.75000 111.32707
17.85000 114.92662
17.95000 110.95450
18.05000 105.53284
18.15000 100.57441
18.25000 109.24105
18.35000 101.15248
18.45000 103.81232
18.55000 94.67551
18.65000 94.24952
18.75000 88.27840
18.85000 93.43551
18.95000 91.01309
19.05000 101.31539
19.15000 90.02586
19.25000 96.94358
19.35000 99.87516
19.45000 89.51112
19.55000 88.75663
19.65000 95.63531
19.75000 101.20587
19.85000 100.06176
19.95000 92.18936
