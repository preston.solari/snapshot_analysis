0.05000 1648.16968
0.15000 3158.59660
0.25000 4381.69079
0.35000 5527.20443
0.45000 6206.02789
0.55000 6554.74486
0.65000 6697.31173
0.75000 6724.13992
0.85000 6786.35859
0.95000 6679.58534
1.05000 6489.83377
1.15000 6289.52302
1.25000 6189.66769
1.35000 5928.76748
1.45000 5769.54593
1.55000 5570.60256
1.65000 5418.68121
1.75000 5253.62458
1.85000 5002.33544
1.95000 4864.49027
2.05000 4739.99457
2.15000 4623.04631
2.25000 4461.07242
2.35000 4304.13480
2.45000 4206.00064
2.55000 4057.37299
2.65000 3940.92565
2.75000 3832.98585
2.85000 3746.03373
2.95000 3595.62573
3.05000 3472.51978
3.15000 3394.50781
3.25000 3301.78117
3.35000 3172.80497
3.45000 3105.65651
3.55000 3007.84572
3.65000 2927.35707
3.75000 2807.79835
3.85000 2749.98347
3.95000 2655.82078
4.05000 2587.97826
4.15000 2533.74237
4.25000 2445.09401
4.35000 2347.44084
4.45000 2300.13682
4.55000 2267.26466
4.65000 2190.77044
4.75000 2139.18307
4.85000 2064.00309
4.95000 1990.29278
5.05000 1939.59570
5.15000 1878.11525
5.25000 1825.69897
5.35000 1760.89175
5.45000 1717.97248
5.55000 1644.86621
5.65000 1622.57236
5.75000 1567.42486
5.85000 1514.15387
5.95000 1481.00509
6.05000 1421.07087
6.15000 1383.78898
6.25000 1342.54984
6.35000 1310.18484
6.45000 1252.68597
6.55000 1215.57368
6.65000 1199.18764
6.75000 1140.53413
6.85000 1115.28565
6.95000 1084.01237
7.05000 1063.70386
7.15000 1037.36145
7.25000 1020.26869
7.35000 980.97264
7.45000 947.44130
7.55000 941.11466
7.65000 900.92840
7.75000 888.09922
7.85000 877.31158
7.95000 859.74659
8.05000 833.23524
8.15000 796.27073
8.25000 775.34135
8.35000 762.57323
8.45000 748.38596
8.55000 720.98370
8.65000 711.79549
8.75000 689.55895
8.85000 658.41652
8.95000 651.43269
9.05000 627.95633
9.15000 608.28569
9.25000 593.73004
9.35000 582.01613
9.45000 563.67705
9.55000 550.21113
9.65000 542.27665
9.75000 515.37116
9.85000 509.01178
9.95000 504.35652
10.05000 493.88908
10.15000 486.83503
10.25000 468.88947
10.35000 458.11197
10.45000 451.16963
10.55000 436.86733
10.65000 428.46580
10.75000 421.32595
10.85000 417.30269
10.95000 409.24049
11.05000 397.00452
11.15000 378.08291
11.25000 373.41004
11.35000 367.97990
11.45000 358.46859
11.55000 354.76814
11.65000 342.29954
11.75000 336.94034
11.85000 326.90920
11.95000 316.30375
12.05000 301.47176
12.15000 309.32414
12.25000 298.14626
12.35000 286.83017
12.45000 288.71200
12.55000 286.72141
12.65000 267.78603
12.75000 267.35643
12.85000 253.35547
12.95000 253.72290
13.05000 244.65457
13.15000 232.87633
13.25000 229.47206
13.35000 229.06391
13.45000 221.79560
13.55000 218.33865
13.65000 214.38885
13.75000 213.00890
13.85000 203.22355
13.95000 209.77478
14.05000 192.83304
14.15000 190.82268
14.25000 191.22124
14.35000 186.86066
14.45000 178.97708
14.55000 175.93027
14.65000 174.47229
14.75000 173.10737
14.85000 163.24535
14.95000 157.54825
15.05000 165.01172
15.15000 153.82548
15.25000 153.51134
15.35000 148.73146
15.45000 152.43282
15.55000 145.43886
15.65000 154.18671
15.75000 141.18059
15.85000 147.53646
15.95000 151.84692
16.05000 141.51601
16.15000 135.12648
16.25000 136.14816
16.35000 131.10932
16.45000 129.35800
16.55000 135.11194
16.65000 126.25565
16.75000 132.32538
16.85000 118.71360
16.95000 127.44947
17.05000 120.04082
17.15000 115.83892
17.25000 108.64559
17.35000 105.84681
17.45000 118.99581
17.55000 103.33003
17.65000 104.36961
17.75000 104.85836
17.85000 101.71761
17.95000 94.26875
18.05000 100.75280
18.15000 94.92508
18.25000 92.60182
18.35000 94.90184
18.45000 91.66341
18.55000 85.89652
18.65000 84.80098
18.75000 93.21901
18.85000 80.03712
18.95000 80.42505
19.05000 82.60230
19.15000 81.47475
19.25000 80.53844
19.35000 83.47356
19.45000 77.72426
19.55000 76.42511
19.65000 80.31092
19.75000 80.73571
19.85000 69.20092
19.95000 73.50222
