import sys
import os
import numpy as np

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
#from readsnap_no_acc import readsnap
import readsnap_no_acc as reads
from fourier_components import *
from ang_momentum_norm import angular_momentum_xyz_norm

def angular_momentum_rad(x, y, z, vx, vy, vz, radii):
  Lx = []
  Ly = []
  Lz = []
  newr = []
  
  rad = np.sqrt(x**2 + y**2 + z**2)
  N = len(x)

  for i in range(len(radii)-1):
    ind = np.where((rad > radii[i]) & (rad <= radii[i+1]))
    #N = float(len(ind))
    Lx.append(np.sum(y[ind]*vz[ind] - z[ind]*vy[ind])/N)
    Ly.append(np.sum(z[ind]*vx[ind] - x[ind]*vz[ind])/N)
    Lz.append(np.sum(x[ind]*vy[ind] - y[ind]*vx[ind])/N)
    
    newr.append((radii[i+1]+radii[i])/2.0)

  return np.array(newr), np.array(Lx), np.array(Ly), np.array(Lz)

def angular_momentum_rad2(x, y, z, vx, vy, vz, radii):
  Lxt = []
  Lyt = []
  Lzt = []
  newr = []

  rad = np.sqrt(x**2 + y**2 + z**2)
  N = len(x)

  for i in range(len(radii)-1):
    Lx = Ly = Lz = 0
    ind = np.where((rad > radii[i]) & (rad <= radii[i+1]))[0]
    for j in ind:
      Lx = Lx + (y[j]*vz[j] - z[j]*vy[j])
      Ly = Ly + (z[j]*vx[j] - x[j]*vz[j])
      Lz = Lz + (x[j]*vy[j] - y[j]*vx[j])
      
    Lxt.append(Lx / float(N))
    Lyt.append(Ly / float(N))
    Lzt.append(Lz / float(N))

    newr.append((radii[i+1]+radii[i])/2.0)
  return np.array(newr), np.array(Lxt), np.array(Lyt), np.array(Lzt)

def main():
  # fname = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original   
  # fname = "/media/cronex/TOSHIBA EXT/sims/primert-art-rerun/" # rerun
  fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl 
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/eqforce/" # rerun atocatl 
  # fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/softening/high/" # rerun atocatl 
  
  # listsims = ['12','11','13','14','15','16',
  #            '22','21','23','24','25','26',
  #            '32','31','33','34','35','36',
  #            '42','41','43','44','45','46',
  #            '52','51','53','54','55','56',
  #            '62','61','63','64','65','66']
  
  # listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14', # 120 snaps
  #             'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #             'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS5']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7', # 240 snaps
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  listsims = ['SGS34', 'SGS36'] # 1200 snaps
  #listsims = ['002','003','004','006','007','008','009']
  # listsims = ['002','003','004']
  listsims = ['SGS5o']
  
  lensims = len(listsims)

  max_r = 20
  nrings = 80
  jump = 1
  radii = np.linspace(0, max_r, nrings+1)

  initsnap = 0
  totsnap = 240
  ssnap = 1
  ts = 0.05
  rsnap = range(initsnap, totsnap+1, ssnap)
  time = np.asarray(rsnap)
  time = time * ts
  
  for k in range(lensims):
    frun = fname + listsims[k] + "/" # location of the file   
    # frun = fname + "sim_" + listsims[k] + "/" # location of the file   
    
    # Lists to store the disc and halo momentum information
    Ld = []
    Lh = []
    for snap in rsnap:
      print("Calculating angular momentum of %s (%s of %s)..." % (listsims[k], snap, str(totsnap)))
      ext = res.snap_mask(snap/ssnap, 3) # file's mask (last three digits)
      dd = reads.readsnap(frun, snap, 2) # disk particles' data
      
      pd = dd['p']  # disc particle positions
      vd = dd['v']

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      vx = vx[::jump]
      vy = vy[::jump]
      vz = vz[::jump]
      
      rd = np.max(np.sqrt(x**2 + y**2 + z**2)) # extension of disc
      
      # calculate angular momentum
      Lxd, Lyd, Lzd = angular_momentum_xyz_norm(x, y, z, vx, vy, vz, 0, rd)
      
      Ld.append([Lxd, Lyd, Lzd])
    
    Ld = np.asarray(Ld)
    Ld_0 = np.sqrt(Ld[0, 0]**2 + Ld[0, 1]**2 + Ld[0, 2]**2)
    
    Ldt = []
    for i in range(len(Ld)):
      Ldt.append(np.sqrt(Ld[i, 0]**2 + Ld[i, 1]**2 + Ld[i, 2]**2))
    Ldt = np.asarray(Ldt)
    
    Ldt = Ldt / Ld_0
    
    # write file
    #fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/angular_momentum/" # cronex rerun
    fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/all/angular_momentum/" # cronex rerun
    #fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/"+listsims[k]+"/angular_momentum/" # atocatl rerun
    # fdat="/home/ia/alexlg/snapshot_analysis/data/softening/high/"+listsims[k]+"/angular_momentum/" # atocatl rerun
    # fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"+names[listsims.index(listsims[k])]+"/angular_momentum/" # cronex original
    if not os.path.exists(fdat): # create path fdat if does not exist
      os.makedirs(fdat)

    # f = open(fdat+"angular_momentum_"+names[listsims.index(listsims[k])]+"_"+ext+".txt", "w+")
    f = open(fdat+"angular_momentum_"+listsims[k]+".txt", "w+")
    for i in range(len(time)):
      f.write("%.3f %.3f\n" % (time[i], Ldt[i]))
    f.close()


if __name__ == '__main__':
  main()
