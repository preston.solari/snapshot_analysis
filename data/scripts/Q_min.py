import os
import sys
import numpy as np

#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")
sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

from den_map import data

def main():
  #fmin = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/"
  #fmin = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/"
  fmin="/home/ia/alexlg/snapshot_analysis/data/rerun/all/toomre/"
  if not os.path.exists(fmin): # create path fdat if does not exist
    os.makedirs(fmin)
  frun = "/home/ia/alexlg/snapshot_analysis/data/rerun/"
  #frun = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/"
  
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  t05 = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6','SGS7',
              'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
              'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  t1 = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
              'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
              'SGS25','SGS26','SGS27','SGS29','SGS35']
  t01 = ['SGS34', 'SGS36']


  #ts = 0.6
  #snap = 1
  #timestamp = np.round(ts*snap,1)

  for sim in names:
    if sim in t1: 
      ts = 0.1
      snap = 0 
    if sim in t05:
      ts = 0.05
      snap = 0
    if sim in t01:
      ts = 0.01
      snap = 0
    
    timesnap = str(np.round(snap*ts,1))

    f = open(fmin+'toomre_min_'+timesnap+'G.txt', 'a+') # original
    
    fdat = frun+sim+"/toomre/toomre_"+sim+"_"+timesnap+"G.txt"
    dat = data(fdat)
    Qmin = np.min(dat[1])

    f.write("%s %s\n" % (sim, Qmin))
  f.close()

if __name__ == "__main__":
  main()
