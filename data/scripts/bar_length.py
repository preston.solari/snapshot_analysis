import sys
import os
import numpy as np

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as reads
from fourier_components import *
from phase_of_bar import bar_len_phase_imp

def bar_len_phase_simple(r, ph, bt, rthres):
  ir = np.where(r > rthres)[0][0]
  ph_ref = ph[ir] # reference phase
  for p in ph[ir:]:
    if p <= -np.pi/4 and ph_ref >= np.pi/4:
      if np.abs(2*np.pi-ph_ref+p) > bt:
        return r[np.where(ph==p)[0][0]]
    elif p >= np.pi/4 and ph_ref <= -np.pi/4:
      if np.abs(2*np.pi+ph_ref-p) > bt: 
        return r[np.where(ph==p)[0][0]]
    else:
      if np.abs(p-ph_ref) > bt: 
        return r[np.where(ph==p)[0][0]]

def main():
  fdir = "/home/ia/alexlg/snapshot_analysis/data/rerun/"
  flen = "/home/ia/alexlg/snapshot_analysis/data/rerun/all/bar_length/"
  if not os.path.exists(flen): # create path flen if does not exist
    os.makedirs(flen)
 
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS5']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['12','11','13','14','15','16',
  #            '22','21','23','24','25','26',
  #            '32','31','33','34','35','36',
  #            '42','41','43','44','45','46',
  #            '52','51','53','54','55','56',
  #            '62','61','63','64','65','66']
  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  lensims = len(listsims)

  initsnap = 0
  totsnap = 120
  skip = 1
  rsnap = np.arange(initsnap, totsnap+1, skip)
  ts = 0.1
  time = np.round(rsnap*ts, 2) # timestep
  rthres = 1.0 

  for model in listsims:
    frun = fdir+model+"/phase_bar/"
    bl = []
    for snap in rsnap:
      ext = res.snap_mask(snap, 4)
      dat = data(frun+"phase_"+model+"_"+ext+".txt")
      r, ph = dat[0], dat[1]
      
      bt = np.arcsin(0.3) # threshold from Athanassoula & Misiriotis
      bl.append(np.round(bar_len_phase_simple(r, ph, bt, rthres), 2)) # bar length
    f = open(flen+"bar_length_"+model+".txt", "w+")
    #print(bl)
    for i in range(len(time)):
      f.write("%s %s\n" % (time[i], bl[i]))
    f.close()

if __name__ == '__main__':
  main()
