import os
import sys
import numpy as np

#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")
sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

from readsnap_no_acc import readsnap
from resonances import velocities, avg_velocities, snap_mask, omegar_fun, kappa, surf_density
from plot_den_maps import adjust_center_mass

def main():
  #frun = "/home/cronex/Documentos/Doctorado/sims/sm-04-10-2018/"   # original
  #frun = "/run/media/cronex/HV620S/primer-art-rerun/"  # rerun
  #frun = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original
  #frun = "/run/media/cronex/HV620S/primer-art-rerun/"  # rerun
  # frun="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  #frun="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/softening/low/" # rerun atocatl
  frun="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/softening/high/" # rerun atocatl
  #frun="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"
  #model = frun.strip().split("/")
  #model = [word for word in model if word != ''][-1]
  #listsims = ['12','11','13','14','15','16',
  #             '22','21','23','24','25','26',
  #             '32','31','33','34','35','36',
  #             '42','41','43','44','45','46',
  #             '52','51','53','54','55','56',
  #             '62','61','63','64','65','66']

  #names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #         'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #         'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #         'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #         'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #         'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  
  #names = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6','SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #names = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23', 
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  names = ['SGS34', 'SGS36']
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14', # 120 snaps
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23', 
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6','SGS7', # 240 snaps
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  listsims = ['SGS34', 'SGS36'] # 1200 snaps
  listsims = ['hernquist','piso','einasto','burkert']
  #listsims = ['002','003','004','006','007','008','009']
  listsims = ['002','003','004']
  
  ts = 0.05
  snap = 20
  timesnap = snap*ts

  for sim in listsims:
    #fsnap = frun+"/sim_"+sim
    fsnap = frun+sim
    #fsnap = frun+sim+'/output/'

    dd = readsnap(fsnap, snap, 2)

    print("========== Processing %s (%s of %s) ==========\n" % (sim, str(listsims.index(sim)+1), str(len(listsims))))
    
    pd = dd['p']
    vd = dd['v']
    md = dd['m']
    print(md[0])
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

    print("Calculating particle velocities...")
    vt, vr, vz = velocities(x, y, vx, vy, vz)

    G = 43007.1
    rmax = 20.0
    nrings = 80.0
    radii = np.linspace(0, rmax+rmax/nrings, nrings+1)

    rp = np.sqrt(x**2 + y**2)

    print("Calculating average velocities...")
    nrad, avg_vel = avg_velocities((rp, vt, vr, vz), radii)
    avg_vt = avg_vel[1]
    avg_sigmar = avg_vel[3]
    kap = kappa((omegar_fun(nrad, avg_vt)))
    
    nrad, den = surf_density(rp, nrad, md[0])
    print(avg_vt)
    
    Q = avg_sigmar[:-1]*kap/3.36/G/den

    nrad = np.round(nrad, 2)
    avg_vt = np.round(avg_vt, 5)
    avg_sigmar = np.round(avg_sigmar, 5)
    kap = np.round(kap, 5)
    den = np.round(den, 5)
    Q = np.round(Q, 5)

    for i in range(len(nrad)):
      print("%s %s %s %s" % (str(nrad[i]), str(avg_sigmar[i]), str(kap[i]), str(Q[i])))
    
    #ftoomre="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"+names[listsims.index(sim)]+ "/toomre/" # for original simulations
    #ftoomre="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/"+sim+"/toomre/" # for rerun simulations
    # ftoomre="/home/ia/alexlg/snapshot_analysis/data/rerun/"+names[listsims.index(sim)]+ "/toomre/"
    ftoomre="/home/ia/alexlg/snapshot_analysis/data/softening/high/"+sim+"/toomre/"
    #ftoomre="/home/ia/alexlg/snapshot_analysis/data/gas/"+sim+"/toomre/"
    if not os.path.exists(ftoomre): # create path fdat if does not exist
      os.makedirs(ftoomre)

    #f = open(ftoomre+'toomre_'+names[listsims.index(sim)]+'_'+str(np.round(timesnap,1))+'G.txt', 'w+') # original
    f = open(ftoomre+'toomre_'+sim+'_'+str(np.round(timesnap,1))+'G.txt', 'w+') # rerun
    for i in range(len(nrad)):
      f.write("%s %s %s %s\n" % (nrad[i], Q[i], kap[i], den[i]))
    f.close()

if __name__ == '__main__':
  main()
