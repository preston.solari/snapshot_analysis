import sys
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.interpolate import make_interp_spline, BSpline

sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")
# sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass_unit
from readsnap_no_acc import readsnap
# import readsnap_no_acc as readnoacc
from mass_assignment_secant_method import surface_density, volumetric_density_imp

def main():
  fdir = "/media/cronex/ADATA HD330/softening/high/"
  listsims = ["002", "003", "004"]
  lensims = len(listsims)
  
  isnap=0
  fsnap=240
  ss=1

  #tstamp = 0.05
  nlines = range(isnap, fsnap+1, ss)
  #time = (nlines-1)*tstamp

  for k in range(lensims):
    frun = fdir + listsims[k] + "/" # location of the file
    # frun = fname + listsims[k] + "/output/" # location of the file
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file

    for i in nlines:
      ext=res.snap_mask(i/ss,3)
      dd = readsnap(frun, i, 2, extension='') # disk particles' data

      pd = dd['p']  # disc particle positions
      mp = dd['m'][0]

      # remove center of mass (disc)
      x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

      print("\n============== Calculating density of simulation %s (%s of %s) - snapshot %s ==============\n"
            % (listsims[k], str(k+1), str(lensims), str(i)))

      # pr=res.particle_radius(x,y)

      nr, den = volumetric_density_imp(x, y, z, mp, rmax=20)
      nr, sden = surface_density(x, y, mp, rmax=20)
      
      # print(nrv)
      # print(nrs)

      # fv="/home/ia/alexlg/snapshot_analysis/data/softening/high/"+listsims[k]+"/velocities/" # atocatl
      fv="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/softening/high/"+listsims[k]+ "/density/" # for original simulations
      if not os.path.exists(fv): # create path fdat if does not exist
        os.makedirs(fv)
      f = open(fv+"den_"+listsims[k]+"_"+ext+".txt","w+")
      for j in range(len(nr)):
        f.write("{:.5f} {:.5f} {:.5f}\n".format(nr[j], sden[j], den[j]))
      f.close()

if __name__ == '__main__':
  main()