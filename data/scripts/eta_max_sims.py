#!/usr/bin/env python

# script to extract eta max of every simulation

import sys
import os
import numpy as np
#import subprocess

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

def eta_max(d_file):
  f=open(d_file,"r")

  lines=f.readlines()
  li=[line.split() for line in lines]

  # Formatting data
  
  #times=[float(el[0]) for el in li] # obtaining the values of times
  etas=[float(el[1]) for el in li]  # values of eta
  
  f.close()
  
  return str(etas[-1])

def main():
  fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  #fname="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/eta/"
  fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/all/eta/" # atocatl
  n=6 # dimensions of directory's square matrix

  mass=['0.035', '0.038', '0.041', '0.044', '0.047', '0.05']
  soft=['0.1', '0.05', '0.025', '0.01', '0.005', '0.001']

  f=open(fdat+"eta_data_final.dat","w")
  
  sn=1
  for i in range(n):
    for j in range(n):
      fsim="SGS"+str(sn)
      fnamee=fdat+"eta_"+fsim+".txt" # eta for simulation
      print("Printing eta for %s (%s of %s)..." % (fsim, str(sn), str(n*n)))
 
      etaf=eta_max(fnamee)
    
      f.write(fsim+' '+etaf+' '+mass[i]+' '+soft[j]+'\n')

      sn+=1

  f.close()

if __name__ == '__main__':
  main()
