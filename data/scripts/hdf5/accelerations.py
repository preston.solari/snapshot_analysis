import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass_unit
#from readsnap_no_acc import readsnap
#import readsnap_no_acc as reads
import readsnap as reads
from fourier_components import *
from load_from_snapshot import load_from_snapshot as load

def acc_components_sph(coord, vel, acc):
  x, y, z = coord[0], coord[1], coord[2]
  vx, vy, vz = vel[0], vel[1], vel[2]
  ax, ay, az = acc[0], acc[1], acc[2]
  
  r = np.sqrt(x**2 + y**2 + z**2)
  phi = np.arccos(z/r)
  theta = np.arctan2(y,x)
  
  # derivatives of quantities
  dr = (x*vx+y*vy+z*vz)/r
  d2r = (x*ax+y*ay+z*az + vx**2+vy**2+vz**2)/r - ((x*vx+y*vy+z*vz)**2)/r**3
  dtheta = (x*vy-y*vx)/(x**2+y**2)
  d2theta = ((x**2+y**2)*(x*ay-y*ax) - 2*(x*vy-y*vx)*(x*vx+y*vy))/(x**2+y**2)**2
  dphi = (-1/(np.sqrt(1-z**2/r**2))) * (vz*r**2 - z*(x*vx+y*vy+z*vz))/r**3
  
  t1 = (-1/(np.sqrt(1-z**2/r**2))) * ((x*vx+y*vy*z*vz)*vz + az*r**2 - z*(x*ax+vx**2+y*ay+vy**2+z*az+vz**2))/r**3 \
       - 2*((vz*r**2 - z*(x*vx+y*vy+z*vz)) * (x*vx+y*vy*z*vz))/r**5
  t2 = (-(x*vx+y*vy+z*vz)/r/np.sqrt(r**2-z**2)
        + r*(x*vx+y*vy)/np.power(r**2-z**2,3./2.)) * (vz*r**2 - z*(x*vx+y*vy+z*vz))/r**3 
  d2phi = t1 + t2

  # radial acceleration
  ar = d2r - r*dtheta**2*np.sin(phi)**2 - r*dphi**2
  at = r*d2theta*np.sin(phi) + 2*dr*dtheta*np.sin(phi) + 2*r*dtheta*dphi*np.cos(phi)
  aa = r*d2phi + 2*dr*dphi - r*dtheta**2*np.sin(phi)*np.cos(phi)
  
  return ar, at, aa

def acc_components_cyl(coord, vel, acc):
  x, y, z = coord[0], coord[1], coord[2]
  vx, vy, vz = vel[0], vel[1], vel[2]
  ax, ay, az = acc[0], acc[1], acc[2]
 
  r = np.sqrt(x**2 + y**2)
  theta = np.arctan2(y,x)

  # derivatives
  dr = (x*vx + y*vy)/r
  d2r = (x*ax+y*ay+vx**2+vy**2)/r - (x*vx+y*vy)**2/r**3
  dtheta = (x*vy-y*vx)/r**2
  d2theta = r**2*(x*ay-y*ax)/r**4 - 2*(x*vy-y*vx)*(x*vx+y*vy)/r**4
  
  # accelerations
  ar = d2r - r*dtheta**2
  at = r*d2theta + 2*dr*dtheta
  
  return ar, at, az

def radii_avg_3d(r, c1, c2, c3, radii):
  new_r = []
  avg_c1 = []
  avg_c2 = []
  avg_c3 = []
  sigma_avg_c1 = []
  sigma_avg_c2 = []
  sigma_avg_c3 = []

  for i in range(len(radii)-1):
    ir = np.where((r >= radii[i]) & (r <= radii[i+1]))
    c1r = c1[ir]
    c2r = c2[ir]
    c3r = c3[ir]
    
    if len(c1r):
      avg_c1.append(np.mean(c1r))
      sigma_c1 = (c1r-np.mean(c1r))**2
      sigma_avg_c1.append(np.sqrt(np.mean(sigma_c1)))
    else:
      avg_c1.append(0)
      sigma_avg_c1.append(0)
    if len(c2r):
      avg_c2.append(np.mean(c2r))
      sigma_c2 = (c2r-np.mean(c2r))**2
      sigma_avg_c2.append(np.sqrt(np.mean(sigma_c2)))
    else:
      avg_c2.append(0)
      sigma_avg_c2.append(0)
    if len(c3r):
      avg_c3.append(np.mean(c3r))
      sigma_c3 = (c3r-np.mean(c3r))**2
      sigma_avg_c3.append(np.sqrt(np.mean(sigma_c3)))
    else:
      avg_c3.append(0)
      sigma_avg_c3.append(0)
    
    new_r.append((radii[i]+radii[i+1])/2.)

  new_r = np.array(new_r)
  avg_c1 = np.array(avg_c1)
  avg_c2 = np.array(avg_c2)
  avg_c3 = np.array(avg_c3)
  sigma_avg_c1 = np.array(sigma_avg_c1)
  sigma_avg_c2 = np.array(sigma_avg_c2)
  sigma_avg_c3 = np.array(sigma_avg_c3)
  
  return (new_r, avg_c1, avg_c2, avg_c3,
          sigma_avg_c1, sigma_avg_c2, sigma_avg_c3)

def radii_avg_1c(r, c1, radii):
  new_r = []
  avg_c1 = []
  sigma_avg_c1 = []

  for i in range(len(radii)-1):
    ir = np.where((r >= radii[i]) & (r <= radii[i+1]))
    c1r = c1[ir]
    
    if len(c1r):
      avg_c1.append(np.mean(c1r))
      sigma_c1 = (c1r-np.mean(c1r))**2
      sigma_avg_c1.append(np.sqrt(np.mean(sigma_c1)))
    else:
      avg_c1.append(0)
      sigma_avg_c1.append(0)

    new_r.append((radii[i]+radii[i+1])/2.)

  new_r = np.array(new_r)
  avg_c1 = np.array(avg_c1)
  sigma_avg_c1 = np.array(sigma_avg_c1)
  
  return (new_r, avg_c1, sigma_avg_c1)

def avg_abs_z(r, z, radii):
  new_r = []
  avg_z = []

  for i in range(len(radii)-1):
    ir = np.where((r >= radii[i]) & (r <= radii[i+1]))
    nz = np.abs(z[ir])
    
    if len(nz):
      avg_z.append(np.mean(nz))
    else:
      avg_z.append(0)

    new_r.append((radii[i]+radii[i+1])/2.)

  new_r = np.array(new_r)
  avg_z = np.array(avg_z)
  
  return new_r, avg_z

def main():
  # fname = "/home/cronex/GD-gaia/Doctorado/bar_len_letter/python/particles_python_disc_020_12.dat"
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/bar-pattern-test/"
  #fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/SGS8/"
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/halos/" # rerun atocatl
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  fname="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"
  #fname = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS5']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['12','11','13','14','15','16',
  #            '22','21','23','24','25','26',
  #            '32','31','33','34','35','36',
  #            '42','41','43','44','45','46',
  #            '52','51','53','54','55','56',
  #            '62','61','63','64','65','66']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']

  #listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  #listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  listsims = ['hernquist','piso','einasto','burkert']
  lensims = len(listsims)
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  max_r = 20
  nrings = 200
  jump = 1
  radii = np.linspace(0, max_r, nrings+1)

  initsnap = 0
  totsnap = 240
  ssnap = 1
  timestep = 0.05
  rsnap = range(initsnap, totsnap+1, ssnap)

  for k in range(lensims):
    #frun = fname + listsims[k] + "/" # location of the file
    frun = fname + listsims[k] + "/hdf5/output/" # location of the file
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file
    for snap in rsnap:
      print("Calculating accelerations of %s (%s of %s)..." % (listsims[k], snap, str(totsnap)))
      #print("Calculating z-abs accelerations of %s (%s of %s)..." % (listsims[k], snap, str(totsnap)))
      ext = res.snap_mask(snap/ssnap, 3) # file's mask (last three digits)
      #dd = reads.readsnap(frun, snap, 2) # disk particles' data

      #pd = dd['p']  # disc particle positions
      #vd = dd['v']  # velocities
      #ad = dd['a']  # accelerations

      #pd = load("keys", 2, frun, snap)
      #sys.exit()
      pd = load("Coordinates", 2, frun, snap)
      vd = load("Velocities", 2, frun, snap)
      ad = load("Acceleration", 2, frun, snap)

      # remove center of mass (disc)
      x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
      vx, vy, vz = adjust_center_mass_unit(vd[:, 0], vd[:, 1], vd[:, 2])
      ax, ay, az = adjust_center_mass_unit(ad[:, 0], ad[:, 1], ad[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      vx = vx[::jump]
      vy = vy[::jump]
      vz = vz[::jump]
      ax = ax[::jump]
      ay = ay[::jump]
      az = az[::jump]
      
      #ar, at, aa = acc_components_sph((x,y,z),(vx,vy,vz),(ax,ay,az))
      ar, at, aa = acc_components_cyl((x,y,z),(vx,vy,vz),(ax,ay,az))

      r = np.sqrt(x**2 + y**2 + z**2)
      acc_data = radii_avg_3d(r, ar, at, aa, radii)
      nr = acc_data[0]
      ar, at, aa = acc_data[1], acc_data[2], acc_data[3]
      sar, sat, saa = acc_data[4], acc_data[5], acc_data[6]

      nr, az_avg = avg_abs_z(r, az, radii)
      #print(nr)

      fdat="/home/ia/alexlg/snapshot_analysis/data/gas/dm_only/"+listsims[k]+"/accelerations/" # atocatl
      #fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/halos/"+listsims[k]+"/accelerations/" # atocatl
      #fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"+names[listsims.index(listsims[k])]+"/phase_bar/" # original
      if not os.path.exists(fdat): # create path fdat if does not exist
        os.makedirs(fdat)

      #f = open(fdat+"phase_"+names[listsims.index(listsims[k])]+"_"+ext+".txt", "w+")
      #f = open(fdat+"acc_cyl_"+listsims[k]+"_"+ext+".txt", "w+")
      #f = open(fdat+"acc_sph_"+listsims[k]+"_"+ext+".txt", "w+")
      #f = open(fdat+"acc_halo_sph_"+listsims[k]+"_"+ext+".txt", "w+")
      f = open(fdat+"acc_abs_z_"+listsims[k]+"_"+ext+".txt", "w+")
      for i in range(len(nr)):
        #f.write("%.5f %.5f %.5f %.5f %.5f %.5f %.5f\n" % (nr[i], ar[i], at[i], aa[i], sar[i], sat[i], saa[i]))
        f.write("%.5f %.5f\n" % (nr[i], az_avg[i]))
      f.close()

if __name__ == '__main__':
  main()
 
