#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import sys
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.interpolate import make_interp_spline, BSpline

#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")
sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
# from readsnap_no_acc import readsnap
import readsnap_no_acc as readnoacc
from phase_of_bar import *
from load_from_snapshot import load_from_snapshot as load

# calculate distorsion parameter
def distorsion_parameter(x, y, rmax = None):
  mixx = miyy = mixy = 0
  if isinstance(x, (list, tuple, np.ndarray)):
    if len(x) != len(y):
      print("distorsion_parameter: arguments must have the same length.")
      return 0
    
    if rmax == None:
      rmax = np.max(np.sqrt(x**2 + y**2))

    for i in range(len(x)):
      rad = np.sqrt(x[i]**2 + y[i]**2)
      if rad <= rmax:
        mixx = mixx + x[i]**2
        miyy = miyy + y[i]**2
        mixy = mixy + x[i] * y[i]   
  elif isinstance(x, (int, float)):
    mixx = x**2
    miyy = y**2
    mixy = x * y    
    
  etap = (mixx-miyy) / (mixx + miyy)
  etax = 2 * mixy / (mixx + miyy)

  return np.sqrt(etap**2 + etax**2)

# Main definition
def main():
  # fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fname="/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original
  # fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  #fname="/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  fname="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"  

  # fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/eta/"
  # fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/all/eta/" # atocatl
  fdat="/home/ia/alexlg/snapshot_analysis/data/gas/all/eta/" # atocatl
  #fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/eta/"
  if not os.path.exists(fdat): # create path fdat if does not exist
    os.makedirs(fdat)

  # listsims=["hernquist"]
  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"]
  # listsims=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15"]
  # listsims=["SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"] 
  # listsims=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"] 
  # listsims=["sim_21", "sim_23", "sim_32", "sim_31", "sim_33"] 
  # listsims=["sim_42", "sim_41", "sim_43", "sim_52", "sim_51", "sim_53"] 
  # listsims=["sim_15", "sim_25", "sim_35", "sim_45", "sim_55", "sim_65"] 
  # listsims=["SGS1"]
  # listsims=["sim_15"]
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #         'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #         'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #         'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #         'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #         'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  
  # listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #             'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #             'SGS25','SGS26','SGS27','SGS29','SGS35']
  # listsims = ['SGS11', 'SGS13','SGS15']

  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #             'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #             'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  # listsims = ['12','11','13','14','15','16',
  #             '22','21','23','24','25','26',
  #             '32','31','33','34','35','36',
  #             '42','41','43','44','45','46',
  #             '52','51','53','54','55','56',
  #             '62','61','63','64','65','66']
  #listsims=["12"]
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS19r', 'SGS21r']
  #listsims = ['SGS31r','SGS34','SGS36']
  listsims = ['hernquist', 'piso', 'burkert', 'einasto']
  lensims = len(listsims)

  # names=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"] 
  #names=["SGS6"]

  # names = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #          'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #          'SGS25','SGS26','SGS27','SGS29','SGS35']
  #names = ['SGS34', 'SGS36']
  #names = ['SGS19r', 'SGS21r']
  # names = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #          'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #          'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  # names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]

  # time variable
  initsnap = 0
  totsnap = 240
  ssnap = 1
  timestep = 0.05
  rsnap = range(initsnap, totsnap+1, ssnap)
  time = np.asarray(rsnap)
  time = time * timestep

  rmax = 20
  rings = 60
  radii = np.linspace(0, rmax, rings)

  # plt.style.use('seaborn-bright')
  #ls = ['solid', 'dashed', 'dotted', 'dashdot']
  feta = []

  jump = 1 # skip through particle data

  for k in range(lensims):
    # frun = fname + listsims[k] + "/output/" # location of the file
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file
    # frun = fname + listsims[k] + "/" # location of the file in atocatl
    frun = fname + listsims[k] + "/hdf5/output/" # location of the file
    eta = []
    for snap in rsnap:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)

      # dd = readnoacc.readsnap(frun, snap, 2) # disk particles' data
      pd = load("Coordinates", 2, frun, snap)
      vd = load("Velocities", 2, frun, snap)

      # pd = dd['p']  # disc particle positions
      # vd = dd['v']  # disc particle velocities
      # mp = dd['m']  # mass of disc particles

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      # determine bar's longitude
      #print("\n############# Calculating bar's longitude #############\n")
      #r, A2, ph = phase_of_bar_annuli_2d(x, y, radii)

      #bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      #ph_thres = []
      #final_ph = ph_thres # final values of phase
      #lenph = 0 # length of phase
      #indph = 0 # index of current phase
      #baseph = ph[0] # reference phase
      #for i in range(len(ph)):
      #  if np.abs(ph[i]-baseph) < bt:
      #    ph_thres.append(ph[i]) 
      #    if len(ph_thres) > lenph: # phase remains semi-constant
      #      final_ph = ph_thres
      #      lenph = len(final_ph)
      #      indph = i
      #  else:
      #    ph_thres = [] # empty phase
      #    baseph = ph[i] # drag new reference phase
      #
      #avgph = np.mean(final_ph)
      #print(final_ph, avgph)
      #ir = list(ph).index(ph[indph])
      #print("\n###############################################\n")

      print("\n############# Distorsion paramerer for %s (of %s) - snapshot %s of %s #############\n" % (str(k+1), str(lensims), str(snap), str(totsnap)))
      #rlim = r[ir] # extension of bar
      #val = distorsion_parameter(x[::jump], y[::jump], rmax = rlim)
      val = distorsion_parameter(x[::jump], y[::jump])
      eta.append(val)
      print("\n###############################################\n")
    
    eta = np.asarray(eta)
    print("Eta (%s) = %s" % (listsims[k], eta[-1]))
    feta.append(eta[-1])

    # 300 represents number of points of time data
    # tnew = np.round(np.linspace(time.min(), time.max(), 300), 2)
    tnew = np.round(time,2)
    fetar = np.round(eta,5)

    # spl = make_interp_spline(time, eta, k = 3)  # type: BSpline
    # famp_smooth = np.round(spl(tnew), 5)

    # f = open(fdat+'eta_'+names[k]+'.txt', 'w+') # original
    f = open(fdat+'eta_'+listsims[k]+'.txt', 'w+') # rerun
    for i in range(len(tnew)):
      f.write("%s %s\n" % (str(tnew[i]),str(fetar[i])))
    f.close()
    
if __name__ == '__main__':
  main()
