#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import sys
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.interpolate import make_interp_spline, BSpline

#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")
sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
#from readsnap_no_acc import readsnap
#import readsnap_no_acc as readnoacc
import readsnap as reads
from phase_of_bar import *
from accelerations import radii_avg_1c
from load_from_snapshot import load_from_snapshot as load

# calculate distorsion parameter
def distorsion_parameter(x, y, rmax = None):
  mixx = miyy = mixy = 0
  if isinstance(x, (list, tuple, np.ndarray)):
    if len(x) != len(y):
      print("distorsion_parameter: arguments must have the same length.")
      return 0
    
    if rmax == None:
      rmax = np.max(np.sqrt(x**2 + y**2))

    for i in range(len(x)):
      rad = np.sqrt(x[i]**2 + y[i]**2)
      if rad <= rmax:
        mixx = mixx + x[i]**2
        miyy = miyy + y[i]**2
        mixy = mixy + x[i] * y[i]   
  elif isinstance(x, (int, float)):
    mixx = x**2
    miyy = y**2
    mixy = x * y    
    
  etap = (mixx-miyy) / (mixx + miyy)
  etax = 2 * mixy / (mixx + miyy)

  return np.sqrt(etap**2 + etax**2)

def radii_avg_eta(x, y, radii):
  new_r = []
  avg_eta = []

  r = np.sqrt(x**2 + y**2)
  for i in range(len(radii)-1):
    ir = np.where((r >= radii[i]) & (r < radii[i+1]))
    nx = x[ir]
    ny = y[ir]

    if len(x):
      eta = distorsion_parameter(nx, ny)
      avg_eta.append(eta)
    else:
      avg_eta.append(0)

    new_r.append((radii[i]+radii[i+1])/2.)

  new_r = np.array(new_r)
  avg_eta = np.array(avg_eta)

  return new_r, avg_eta

# Main definition
def main():
  # fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  #fname="/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/softening/low/" # rerun atocatl
  fname="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"
  # fname="/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun


  # listsims=["hernquist"]
  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"]
  # listsims=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15"]
  # listsims=["SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"] 
  # listsims=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"] 
  # listsims=["sim_21", "sim_23", "sim_32", "sim_31", "sim_33"] 
  # listsims=["sim_42", "sim_41", "sim_43", "sim_52", "sim_51", "sim_53"] 
  # listsims=["sim_15", "sim_25", "sim_35", "sim_45", "sim_55", "sim_65"] 
  # listsims=["SGS1"]
  # listsims=["sim_15"]
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #         'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #         'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #         'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #         'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #         'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['12','11','13','14','15','16',
  #            '22','21','23','24','25','26',
  #            '32','31','33','34','35','36',
  #            '42','41','43','44','45','46',
  #            '52','51','53','54','55','56',
  #            '62','61','63','64','65','66']
  #listsims=["12"]
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS19r', 'SGS21r']

  #listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  #listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  #listsims = ['002','003','004','006','007','008','009']
  listsims=["hernquist", "piso", "burkert", "einasto"]
  lensims = len(listsims)

  # names=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"] 
  #names=["SGS6"]

  # names = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #          'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #          'SGS25','SGS26','SGS27','SGS29','SGS35']
  #names = ['SGS34', 'SGS36']
  #names = ['SGS19r', 'SGS21r']
  # names = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #          'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #          'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  # names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]

  # time variable
  initsnap = 0
  totsnap = 240
  ssnap = 1
  timestep = 0.05
  rsnap = range(initsnap, totsnap+1, ssnap)
  time = np.asarray(rsnap)
  time = time * timestep

  rmax = 20
  rings = 80
  radii = np.linspace(0, rmax, rings+1)

  jump = 1 # skip through particle data

  for k in range(lensims):
    # frun = fname + listsims[k] + "/output/" # location of the file
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file
    frun = fname + listsims[k] + "/hdf5/output/" # location of the file in atocatl
    for snap in rsnap:
      ext = res.snap_mask(snap/ssnap, 3) # file's mask (last three digits)

      pd = load("Coordinates", 2, frun, snap)
      vd = load("Velocities", 2, frun, snap)
      ad = load("Acceleration", 2, frun, snap)

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]

      print("\n############# Distorsion paramerer for %s (of %s) - snapshot %s of %s #############\n" % (str(k+1), str(lensims), str(snap), str(totsnap)))
      nr, etar = radii_avg_eta(x, y, radii)
      print("\n###############################################\n")
    
      #fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/eta/"
      #fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/"+listsims[k]+"/eta/" # atocatl
      #fdat="/home/ia/alexlg/snapshot_analysis/data/softening/low/"+listsims[k]+"/eta/" # atocatl
      fdat="/home/ia/alexlg/snapshot_analysis/data/gas/dm_only/"+listsims[k]+"/eta/" # atocatl
      # fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/eta/"
      if not os.path.exists(fdat): # create path fdat if does not exist
        os.makedirs(fdat)

      #f = open(fdat+'eta_'+names[k]+'.txt', 'w+') # original
      f = open(fdat+'eta_'+listsims[k]+'_'+ext+'.txt', 'w+') # original
      for i in range(len(nr)):
        f.write("%.5f %.5f\n" % (nr[i], etar[i]))
      f.close()
    
if __name__ == '__main__':
  main()
