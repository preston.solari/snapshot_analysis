#!/usr/bin/env python

# script to calculate the Fourier components of a snapshot

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter

#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")
sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from load_from_snapshot import load_from_snapshot as load
from phase_of_bar import *

def calculate_Am(x, y, radii, theta, m, mp):
  Am = []
  newr = []

  print("Calculating Am (m = %s)..." % m)

  pr = np.sqrt(x**2 + y**2)

  dt = theta[1] - theta[0]

  for i in range(len(radii)-1):
    A = 0
    for k in range(len(theta)-1):
      mass_ring = number_particles_ring_sector(x, y, radii[i], radii[i+1], theta[k], theta[k+1]) * mp
      for j in range(len(pr)):
        if radii[i] <= pr[j] <= radii[i+1]:
          tp = np.arctan2(y[j], x[j])
          if theta[k] <= tp <= theta[k+1]:
            A = A + mass_ring * np.cos(m*tp) * dt
    Am.append(A / np.pi)
    newr.append((radii[i] + radii[i+1]) / 2)

  Am = np.asarray(Am)
  newr = np.asarray(newr)

  print("Done.")

  return newr, Am

def calculate_Bm(x, y, radii, theta, m, mp):
  Bm = []
  newr = []

  print("Calculating Bm (m = %s)..." % m)

  pr = np.sqrt(x**2 + y**2)

  dt = theta[1] - theta[0]

  for i in range(len(radii)-1):
    B = 0
    for k in range(len(theta)-1):
      mass_ring = number_particles_ring_sector(x, y, radii[i], radii[i+1], theta[k], theta[k+1]) * mp
      for j in range(len(pr)):
        if radii[i] <= pr[j] <= radii[i+1]:
          tp = np.arctan2(y[j], x[j])
          if theta[k] <= tp <= theta[k+1]:
            B = B + mass_ring * np.sin(m*tp) * dt
    Bm.append(B / np.pi)
    newr.append((radii[i] + radii[i+1]) / 2)

  Bm = np.asarray(Bm)
  newr = np.asarray(newr)

  print("Done.")

  return newr, Bm

def calculate_Am_simple(x, y, radii, m, c = 'a'):
  pr = np.sqrt(x**2 + y**2)

  Am = []
  newr = []
  numpart = []

  for i in range(len(radii)-1):
    A = 0
    N = 0
    for j in range(len(pr)):
      if radii[i] <= pr[j] <= radii[i+1]:
        tp = np.arctan2(y[j], x[j])
        if c == 'a':
          A = A + np.cos(m*tp)
        elif c == 'b':
          A = A + np.sin(m*tp)
        else:
          print("There is no %s coefficient." % (c))
          return
        
        N = N + 1

    Am.append(A)
    newr.append((radii[i] + radii[i+1]) / 2)
    numpart.append(N)
  
  Am = np.asarray(Am)
  newr = np.asarray(newr)
  numpart = np.asarray(numpart)

  return newr, numpart, Am

def number_particles_ring_sector(x, y, ri, re, t1, t2):
  npart = 0

  pr = np.sqrt(x**2 + y**2)

  if len(pr) > 1:
    for i in range(len(pr)):
      tp = np.arctan2(y[i], x[i])
      if (ri <= pr[i] <= re) and (t1 <= tp <= t2 ):
        npart = npart + 1
  elif len(pr) == 1:
    tp = np.arctan2(y[0], x[0])
    if (ri <= pr <= re) and (t1 <= tp <= t2 ):
      npart = npart + 1
  else:
    npart = 0

  return npart

def main():
  #fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  #fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  #fname = "/home/cronex/Documentos/Doctorado/gadget4-diciembre/ugc-628-test/"
  #fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  #fname="/home/cronex/Documentos/Doctorado/ugc-presentacion/"
  #fname="/run/media/cronex/HV620S/primer-art-rerun/"
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  fname="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"
  #fname = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original

  #fstrength="/home/ia/alexlg/snapshot_analysis/data/rerun/all/strength/" # atocatl
  #fps="/home/ia/alexlg/snapshot_analysis/data/rerun/all/pattern_speed/" # atocatl
  #fstrength="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/strength/"
  #fps="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/strength/"
  fstrength="/home/ia/alexlg/snapshot_analysis/data/gas/all/strength/" # atocatl
  fps="/home/ia/alexlg/snapshot_analysis/data/gas/all/pattern_speed/" # atocatl
  if not os.path.exists(fstrength): # create path fdat if does not exist
    os.makedirs(fstrength)
  if not os.path.exists(fps): # create path fdat if does not exist
    os.makedirs(fps)
  #mp = [1.3885e-5, 1.5075e-5, 1.6265e-5, 1.7455e-5, 1.8646e-5, 1.9836e-5] # particle mass of every model
  #mp = [1.3885e-5, 1.5075e-5] # particle mass of every model
  m = 2 # fourier mode to compute

  # list of simulations
  #listsims = ['21', '23', '32', '31']
  #listsims = ['33', '42', '41', '43']
  #listsims = ['52', '51', '53']
  #listsims = ['15', '25', '35', '45', '55', '65']
  #listsims = ['21']
  #listsims = ['SGS5', 'SGS8', 'SGS9', 'SGS11', 'SGS13', 'SGS14']
  #listsims = ['SGS15', 'SGS17', 'SGS19', 'SGS20', 'SGS21', 'SGS23']
  #listsims = ['SGS25', 'SGS26', 'SGS27', 'SGS29', 'SGS35']
  #listsims = ['SGS5', 'SGS11', 'SGS17', 'SGS23', 'SGS29', 'SGS35']
  #listsims = ['SGS19','SGS21']
  #listsims = ['SGS5']
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS27','SGS29', 'SGS35']
  #listsims = ['12','11','13','14','15','16',
  #             '22','21','23','24','25','26',
  #             '32','31','33','34','35','36',
  #             '42','41','43','44','45','46',
  #             '52','51','53','54','55','56',
  #             '62','61','63','64','65','66']
  #listsims=["hernquist", "nfw", "iso", "burkert", "einasto"] 
  #listsims=["hernquist", "nfw", "einasto"] 
  #listsims=["hernquist", "nfw"]
  #listsims=["hernquist", "hernquist_gas_only_0.05", "hernquist_gas_only_0.2", "hernquist_gas_sfr_0.2"]

  #listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  listsims=["hernquist", "piso", "burkert", "einasto"] 
  lensims = len(listsims)

  # names of simulations
  #names = ['SGS1', 'SGS2', 'SGS7', 'SGS8']
  #names = ['SGS8', 'SGS9', 'SGS13', 'SGS14']
  #names = ['SGS15', 'SGS19', 'SGS20', 'SGS21']
  #names = ['SGS25', 'SGS26', 'SGS27']
  #names = ['SGS5', 'SGS8', 'SGS9', 'SGS11', 'SGS13', 'SGS14']
  #names = ['SGS15', 'SGS17', 'SGS19', 'SGS20', 'SGS21', 'SGS23']
  #names = ['SGS25', 'SGS26', 'SGS27', 'SGS29', 'SGS35']
  #names = ['SGS5', 'SGS11', 'SGS17', 'SGS23', 'SGS29', 'SGS35']
  #names = ['SGS19','SGS21']
  #names = ['SGS5']
  #names = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #names = ['SGS34', 'SGS36'
  #names = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  #names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"] 
  #names=["Hernquist", "NFW", "Einasto"]
  #names=["Hernquist", "Hernquist with gas only", "Hernquist with gas (SFR)"]
  #names = ["HSG", "HGO-05", "HGO-2", "HGS-2"]

  # softenings and masses
  #soft = ['0.1', '0.05']
  #masses = ['0.035', '0.038']

  # time variable
  initsnap = 0
  totsnap = 240
  skip = 1
  ts = 0.05
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = np.round(time * ts,5)
  print(time)

  #rmax = [25, 30, 70, 70, 8]
  #rmax = [25, 30, 8]
  rmax=20

  jump = 1 # skip through particle data

  a2f = []
  for k in range(lensims):
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file
    #frun = fname + listsims[k] + "/output/" # location of the file
    #frun = fname + listsims[k] # location of the file
    frun = fname + listsims[k] + "/hdf5/output/" # location of the file

    famp = [] # fourier amplitude (m = 2) for snapshot
    phase = []
    #radii = np.linspace(0, rmax[k], rmax[k]+1)
    rings = 60
    radii = np.linspace(0, rmax, rings)

    for snap in rsnap:
      ext = res.snap_mask(snap/skip, 3) # file's mask (last three digits)

      #dd = rsa.readsnap(frun, snap, 2) # disk particles' data
      pd = load("Coordinates", 2, frun, snap)
      vd = load("Velocities", 2, frun, snap)

      #pd = dd['p']  # disc particle positions
      #vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      print("\n############# Fourier Amplitude for %s (of %s) - snapshot %s of %s #############\n" % (str(k+1), str(lensims), str(snap), str(totsnap)))
      r, numpart, Am = calculate_Am_simple(x[::jump], y[::jump], radii, m)
      r, numpart, Bm = calculate_Am_simple(x[::jump], y[::jump], radii, m, c = 'b')

      famp.append(np.max(np.sqrt(Am**2 + Bm**2) / numpart))

      print("\n############# Phase of Bar (snapshot %s of %s) #############\n" % (str(snap), str(totsnap)))
      r, A2, ph = phase_of_bar_annuli_2d(x[::jump], y[::jump], radii)

      # bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      # # ph_thres = [ph[i] for i in range(len(ph)) if np.abs(ph[i]-ph[0]) < bt]
      # ph_thres = []
      # for i in range(len(ph)):
      #   if np.abs(ph[i]-ph[0]) >= bt:
      #     break
      #   else:
      #     ph_thres.append(ph[i])
      # print(ph_thres)

      # phase.append(np.mean(ph_thres))
      
      #====================== Bar phase ======================#
      bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      ph_thres = []
      final_ph = ph_thres # final values of phase
      lenph = 0 # length of phase
      indph = 0 # index of current phase
      baseph = ph[0] # reference phase
      for i in range(len(ph)):
        if np.abs(ph[i]-baseph) < bt:
          ph_thres.append(ph[i]) 
          if len(ph_thres) > lenph: # phase remains semi-constant
            final_ph = ph_thres
            lenph = len(final_ph)
            indph = i
        else:
          ph_thres = [] # empty phase
          baseph = ph[i] # drag new reference phase
      
      print(final_ph)
      phase.append(np.mean(final_ph))
      #ir = list(ph).index(ph[indph])
      #====================== Bar phase ======================#

  
      print("\n###############################################\n")
    
    # Soften force amplitude curve
    famp = np.asarray(famp)
    # famp_smooth = savgol_filter(famp, 11, 3)

    a2f.append(famp[-1]) # last A2 value of current snapshot

    # 300 represents number of points of time data
    #tnew = np.linspace(time.min(), time.max(), 200)
    tnew = np.round(time,2) 

    #spl = make_interp_spline(time, famp, k = 2)  # type: BSpline
    #famp_smooth = np.round(spl(tnew),5)
    famp_smooth = np.round(famp,5)
    
    #====================== Bar phase ======================#
    phase = np.asarray(phase)

    nphase = []
    ntphase = []
    buf = 0
    for i in range(len(phase)-1):
      if phase[i+1] < 0 and phase[i] > 0:
        df = 2*np.pi - (np.abs(phase[i+1]) + phase[i])
      else:
        df = np.abs(phase[i+1]-phase[i])
      buf = buf + df
      ntphase.append(df)
      nphase.append(buf)
    nphase = np.asarray(nphase)

    # calculate derivative of phase with the central finite differences method
    dt = time[1] - time[0]
    dphase = ntphase/dt
    dphase = np.round(savgol_filter(dphase, 11, 3),5)

    coef = np.polyfit(time[1:], nphase, 1)
    poly1d_fn = np.poly1d(coef)

    print("Omega_b = %s\n" % ((poly1d_fn(time[1:])[-1] - poly1d_fn(time[1:])[0]) / (time[-1] - time[1])))
    #====================== Bar phase ======================#
    
    #====================== Write strengths and pattern speeds to file ====================#
    fs=open(fstrength+'strength_'+listsims[k]+'.txt','w+')
    fp=open(fps+'pattern_speed_'+listsims[k]+'.txt','w+')
    for i in range(len(tnew)):
      fs.write("%s %s\n" % (str(tnew[i]), str(famp_smooth[i])))
    for i in range(len(time)-1):
      fp.write("%s %s\n" % (str(time[i+1]), str(dphase[i])))
    fs.close()
    fp.close()
    #====================== Write strengths and pattern speeds to file ====================#

if __name__ == '__main__':
  main()
