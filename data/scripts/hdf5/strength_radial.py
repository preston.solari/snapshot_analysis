import os
import sys
import numpy as np

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import readsnap_no_acc as reads
import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from fourier_components import calculate_Am_simple
from load_from_snapshot import load_from_snapshot as load

def main():
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/softening/low/" # rerun atocatl
  fname="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"

  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  #listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  #listsims = ['002','003','004','006','007','008','009']
  listsims=["hernquist", "piso", "burkert", "einasto"]
  lensims = len(listsims)

  max_r = 20
  nrings = 80
  jump = 1
  radii = np.linspace(0, max_r, nrings+1)

  initsnap = 0
  totsnap = 240
  ssnap = 1
  timestep = 0.05
  rsnap = range(initsnap, totsnap+1, ssnap)
  times = np.array(rsnap)*timestep
  m = 2

  for k in range(lensims):
    #frun = fname + listsims[k] + "/" # location of the file
    frun = fname + listsims[k] + "/hdf5/output/" # location of the file
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file

    #fs="/home/ia/alexlg/snapshot_analysis/data/rerun/"+listsims[k]+"/strength/" # atocatl
    #fs="/home/ia/alexlg/snapshot_analysis/data/softening/low/"+listsims[k]+"/strength/" # atocatl
    fs="/home/ia/alexlg/snapshot_analysis/data/gas/dm_only/"+listsims[k]+"/strength/" # atocatl
    if not os.path.exists(fs): # create path fs if does not exist
      os.makedirs(fs)

    for snap in rsnap:
      print("Calculating strength of %s (%s of %s)..." % (listsims[k], str(snap), str(totsnap)))
      ext = res.snap_mask(snap/ssnap, 3) # file's mask (last three digits)
      
      pd = load("Coordinates", 2, frun, snap)
      vd = load("Velocities", 2, frun, snap)
      ad = load("Acceleration", 2, frun, snap)

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]

      r, numpart, Am = calculate_Am_simple(x, y, radii, m)
      r, numpart, Bm = calculate_Am_simple(x, y, radii, m, c = 'b')

      famp = np.sqrt(Am**2 + Bm**2) / numpart
      
      f = open(fs+"strength_"+listsims[k]+"_"+ext+".txt", "w+")
      for i in range(len(r)):
        f.write("%.5f %.5f\n" % (r[i], famp[i]))
      f.close()

if __name__ == '__main__':
  main()
