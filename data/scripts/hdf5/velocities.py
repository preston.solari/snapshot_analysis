#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import os
import sys
import numpy as np

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
# from readsnap import readsnap
# import readsnap_no_acc  as rsnap
import readsnap as rsnap
from load_from_snapshot import load_from_snapshot as load
from phase_of_bar import *
from distorsion_parameter import distorsion_parameter

def vel_components_sph(coord, vel):
  x, y, z = coord[0], coord[1], coord[2]
  vx, vy, vz = vel[0], vel[1], vel[2]

  r = np.sqrt(x**2 + y**2 + z**2)
  theta = np.arctan2(y,x)
  phi = np.arccos(z/r)

  # derivatives
  dr = (x*vx+y*vy+z*vz)/r
  dtheta = (x*vy-y*vx)/(x**2+y**2)
  dphi = (-1/(np.sqrt(1-z**2/r**2))) * (vz*r**2 - z*(x*vx+y*vy+z*vz))/r**3

  # velocity components
  vr = dr
  vt = r*dtheta*np.sin(phi)
  va = r*dphi

  return vr, vt, va

def vel_components_cyl(coord, vel):
  x, y, z = coord[0], coord[1], coord[2]
  vx, vy, vz = vel[0], vel[1], vel[2]

  r = np.sqrt(x**2 + y**2)
  theta = np.arctan2(y,x)

  # derivatives
  dr = (x*vx + y*vy)/r
  dtheta = (x*vy-y*vx)/r**2

  # accelerations
  vr = dr
  vt = r*dtheta

  return vr, vt, vz

# Main definition
def main():
  #fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fname = "/run/media/cronex/Elements/primer-art-rerun/"
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  fname="/home/ia/alexlg/gizmo-public/run/gas/dm_only/"
  #fname = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original

  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"] 
  # listsims=["hernquist"]
  # listsims1=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15"]
  # listsims=["SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"]
  # listsims=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"]
  # listsims=["SGS5"]
  # listsims=["sim_21", "sim_23", "sim_32", "sim_31", "sim_33"]
  # listsims=["sim_42", "sim_41", "sim_43", "sim_52", "sim_51", "sim_53"] 
  # listsims=["sim_15", "sim_25", "sim_35", "sim_45", "sim_55", "sim_65"] 
  listsims = ['12','11','13','14','15','16',
              '22','21','23','24','25','26',
              '32','31','33','34','35','36',
              '42','41','43','44','45','46',
              '52','51','53','54','55','56',
              '62','61','63','64','65','66'] 
  # listsims = ['12']
  #listsims=["SGS5"]
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
              'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
              'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']

  #listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  #listsims=["hernquist", "piso", "burkert", "einasto"]
  listsims=["piso", "burkert", "einasto"]
  #listsims=["hernquist"] 
  lensims = len(listsims)

  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  init_radii=res.radii_alt(30, 100)
  
  isnap=0
  fsnap=240
  ss=1

  #tstamp = 0.05
  nlines = range(isnap, fsnap+1, ss)
  #time = (nlines-1)*tstamp

  for k in range(lensims):
    # frun = fname + listsims[k] + "/output/" # location of the file
    frun = fname + listsims[k] + "/hdf5/output/" # location of the file
    # frun = fname + listsims[k] + "/output/" # location of the file
    # frun = fname + "sim_" + listsims[k] + "/" # location of the file

    for i in nlines:
      ext=res.snap_mask(i/ss, 3)
      # dd = rsnap.readsnap(frun, i, 2, extension='') # disk particles' data
      # print(load('keys', 2, frun, i))
      #print(i)
      #sys.exit()
      pd = load('Coordinates', 2, frun, i)
      #pd = pd.T
      vd = load('Velocities', 2, frun, i)
      #vd = vd.T

      # pd = dd['p']  # disc particle positions
      # vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      print("\n============== Calculating rotation curve simulation %s (%s of %s) - snapshot %s ==============\n"
            % (listsims[k], str(k+1), str(lensims), str(i)))

      pr=res.particle_radius(x,y)
      pv=res.velocities(x,y,vx,vy,vz)

      values=(pr,) + pv

      # init_radii=res.radii_alt(rarr[k],50)
      newr,avg=res.avg_velocities(values,init_radii)
      vr, vt, vz, sr, st, sz = avg[0],avg[1], avg[2], avg[3], avg[4], avg[5]
      newr = np.round(newr,5)
      vr = np.round(vr,5)
      vt = np.round(vt,5)
      vz = np.round(vz,5)
      sr = np.round(sr,5)
      st = np.round(st,5)
      sz = np.round(sz,5)
      
      print(vt)

      #fv="/home/ia/alexlg/snapshot_analysis/data/rerun/"+listsims[k]+"/velocities/" # atocatl
      fv="/home/ia/alexlg/snapshot_analysis/data/gas/"+listsims[k]+"/velocities/" # atocatl
      #fv="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"+names[k]+ "/velocities/" # for original simulations
      if not os.path.exists(fv): # create path fdat if does not exist
        os.makedirs(fv)
      f=open(fv+"vel_"+listsims[k]+"_"+ext+".txt","w+")
      for j in range(len(newr)):
        f.write("%s %s %s %s %s %s %s\n" % (newr[j], vr[j], vt[j], vz[j], sr[j], st[j], sz[j]))
      f.close()

if __name__ == '__main__':
  main()
