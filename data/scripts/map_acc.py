#!/usr/local/bin python2

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
import os
import sys

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask
#from plot_den_maps import adjust_center_mass

def main():
  #fdir="/run/media/cronex/HV620S/sm-04-10-2018/sim_12/"
  #fdir="/run/media/cronex/HV620S/primer-art-rerun/SGS5/"
  #fdir = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  #fdir = "/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/SGS36/" # rerun
  fdir="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl

  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  listsims = ['SGS34', 'SGS36']

  initsnap = 1
  totsnap = 1200
  skip = 1199
  rsnap = range(initsnap,totsnap+1,skip)
  
  for model in listsims:
    frun=fdir+model
    for snap in rsnap:
      print("Processing model "+model+" (%s of %s)" % (str(snap), str(totsnap)))

      dd = readsnap(frun, initsnap, 2) # disk particles' data

      a = dd['a']  # disc particle accelerations
      p = dd['p']  # disc particle positions

      ax, ay, az = a[:, 0], a[:, 1], a[:, 2]
      norma1 = np.sqrt(ax**2 + ay**2 + az**2)

      dd = readsnap(frun, totsnap, 2) # disk particles' data

      a = dd['a']  # disc particle accelerations
      pd = dd['p']  # disc particle positions
      ids = dd['id']

      ax, ay, az = a[:, 0], a[:, 1], a[:, 2]
      norma30 = np.sqrt(ax**2 + ay**2 + az**2)  
  
      # get accelerations
      dr = 200
      edgesa30 = np.arange(min(norma30), max(norma30), (max(norma30)-min(norma30))/dr)
      edgesa1 = edgesa30
      result1, bins1 = np.histogram(norma1, bins=edgesa1, normed=False)
      result30, bins30 = np.histogram(norma30, bins=edgesa30, normed=False)
      result = np.abs(result30-result1)
      
      # write accelerations histogram to file
      fv="/home/ia/alexlg/snapshot_analysis/data/rerun/all/delta_acc/" # atocatl
      if not os.path.exists(fv): # create path fdat if does not exist
        os.makedirs(fv)
      f = open(fv+"delta_acc_"+model+".txt","w+")
      for i in range(len(edgesa30[:-1])):
        f.write("%.2f %.2f\n" % (edgesa30[i], result[i]))
      f.close()
  
if __name__ == '__main__':
  main()
