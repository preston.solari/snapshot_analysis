#!/usr/bin/env python

# script to calculate the phase of a bar (if any) in a snapshot

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
#from readsnap_no_acc import readsnap
import readsnap_no_acc as reads
from fourier_components import *

def phase_of_bar_complete_2d(x, y, threshold):
  phi = []
  for i in range(len(x)):
    r = np.sqrt(x[i]**2 + y[i]**2)
    if threshold[0] <= r <= threshold[1]:
      phi.append(np.arctan2(y[i], x[i]))
  phi = np.asarray(phi)
  args = np.sin(2 * phi)
  argc = np.cos(2 * phi)

  a2 = np.mean(argc)
  b2 = np.mean(args)

  phase = np.arctan2(a2, b2)

  return np.mean(phase)

def phase_of_bar_annuli_2d(x, y, radii):
  A2 = []
  phase = []
  newr = []

  pr = np.sqrt(x**2 + y**2)

  for i in range(len(radii)-1):
    phi = []
    for j in range(len(pr)):
      if radii[i] <= pr[j] <= radii[i+1]:
        phi.append(np.arctan2(y[j], x[j]))
        #phi.append(np.arctan(y[j]/x[j]))
    phi = np.asarray(phi)
    argc = np.cos(2 * phi)
    args = np.sin(2 * phi)

    a2 = np.mean(argc)
    b2 = np.mean(args)

    A22 = (a2**2 + b2**2) / 2

    A2.append(np.sqrt(np.mean(A22)) * np.mean(phi))
    phase.append(np.arctan2(b2, a2))
    newr.append((radii[i] + radii[i+1]) / 2)

  A2 = np.asarray(A2)
  phase = np.asarray(phase)
  newr = np.asarray(newr)

  return newr, A2, phase

def convert_radians_to_degrees(angles, positive = False):
  if isinstance(angles, (list, tuple, np.ndarray)):
    if positive:
      ang = [angle + 2*np.pi if angle < 0 else angle for angle in angles]
    else:
      ang = angles
    ang = np.asarray(ang)
  elif isinstance(angles, (int, float)):
    if positive and (angles < 0):
      ang = angles + 2*np.pi
    else:
      ang = float(angles)
  
  return ang * 180 / np.pi

def bar_len_phase(aphase, cphase, radii, max_diff):
  if max_diff <= 0:
    print("max_diff parameter cannot be less or equal.")
    exit()

  i = 0
  while (np.abs(aphase[i] - cphase) < max_diff):
    i = i + 1

  return radii[i]

def phase_of_bar_direct(x, y, radii):
  phi_avg = []
  newr = []

  pr = np.sqrt(x**2 + y**2)

  for i in range(len(radii)-1):
    phi = []
    for j in range(len(pr)):
      if radii[i] <= pr[j] <= radii[i+1]:
        phi.append(np.arctan2(y[j], x[j]))
    phi = np.asarray(phi)

    phi_avg.append(np.mean(phi))
    newr.append((radii[i] + radii[i+1]) / 2)

  phi_avg = np.asarray(phi_avg)
  newr = np.asarray(newr)

  return newr, phi_avg
  
def main():
  # fname = "/home/cronex/GD-gaia/Doctorado/bar_len_letter/python/particles_python_disc_020_12.dat"
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/bar-pattern-test/"
  #fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/SGS8/"
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  # fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/softening/high/" # rerun atocatl
  #fname = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original   
  fname = "/home/cronex/Documentos/Doctorado/eqforce/"
  
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS5']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['12','11','13','14','15','16',
  #            '22','21','23','24','25','26',
  #            '32','31','33','34','35','36',
  #            '42','41','43','44','45','46',
  #            '52','51','53','54','55','56',
  #            '62','61','63','64','65','66'] 

  listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  #listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  #listsims = ['002','003','004','006','007','008','009']
  listsims = ['002','003','004']
  listsims = ['out']
  lensims = len(listsims)
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  max_r = 20
  nrings = 80
  jump = 1
  radii = np.linspace(0, max_r, nrings+1)

  initsnap = 0
  totsnap = 240
  ssnap = 1
  timestep = 0.05
  rsnap = range(initsnap, totsnap+1, ssnap)
  
  for k in range(lensims):
    frun = fname + listsims[k] + "/" # location of the file   
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file   
    for snap in rsnap:
      print("Calculating phase of %s (%s of %s)..." % (listsims[k], snap, str(totsnap)))
      ext = res.snap_mask(snap/ssnap, 3) # file's mask (last three digits)
      dd = reads.readsnap(frun, snap, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]

      r, A2, aphase = phase_of_bar_annuli_2d(x, y, radii)
      r = np.round(r,2)
      aphase = np.round(aphase, 5)
          
      #fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/"+listsims[k]+"/phase_bar/" # atocatl
      # fdat="/home/ia/alexlg/snapshot_analysis/data/softening/high/"+listsims[k]+"/phase_bar/" # atocatl
      fdat="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"+names[listsims.index(listsims[k])]+"/phase_bar/" # original
      if not os.path.exists(fdat): # create path fdat if does not exist
        os.makedirs(fdat)

      #f = open(fdat+"phase_"+names[listsims.index(listsims[k])]+"_"+ext+".txt", "w+")
      f = open(fdat+"phase_"+listsims[k]+"_"+ext+".txt", "w+")
      for i in range(len(r)):
        f.write("%f %f\n" % (r[i], aphase[i]))
      f.close()

if __name__ == '__main__':
  main()
