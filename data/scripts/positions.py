import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass_unit
#from readsnap_no_acc import readsnap
#import readsnap_no_acc as reads
import readsnap as reads
from fourier_components import *

def main():
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/bar-pattern-test/"
  #fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/halos/" # rerun atocatl
  fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  #fname = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS5']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['12','11','13','14','15','16',
  #            '22','21','23','24','25','26',
  #            '32','31','33','34','35','36',
  #            '42','41','43','44','45','46',
  #            '52','51','53','54','55','56',
  #            '62','61','63','64','65','66']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']

  #listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  #listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  #listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  listsims = ['SGS31lp', 'SGS31o', 'SGS31r',
              'SGS34lp', 'SGS34o', 'SGS34',
              'SGS36lp', 'SGS36']
  lensims = len(listsims)
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  initsnap = 0
  totsnap = 1200
  ssnap = 1
  jump = 1
  timestep = 0.01
  rsnap = range(initsnap, totsnap+1, ssnap)

  for k in range(lensims):
    frun = fname + listsims[k] + "/" # location of the file
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file
    for snap in rsnap:
      print("Printing positions of %s (%s of %s)..." % (listsims[k], snap, str(totsnap)))
      #print("Calculating z-abs accelerations of %s (%s of %s)..." % (listsims[k], snap, str(totsnap)))
      ext = res.snap_mask(snap/ssnap, 4) # file's mask (last three digits)
      dd = reads.readsnap(frun, snap, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # velocities

      # remove center of mass (disc)
      x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
      vx, vy, vz = adjust_center_mass_unit(vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      vx = vx[::jump]
      vy = vy[::jump]
      vz = vz[::jump]

      fdat="/home/ia/alexlg/snapshot_analysis/data/rerun/"+listsims[k]+"/positions/" # atocatl
      if not os.path.exists(fdat): # create path fdat if does not exist
        os.makedirs(fdat)

      f = open(fdat+"pos_"+listsims[k]+"_"+ext+".txt", "w+")
      for i in range(len(x)):
        f.write("%.5f %.5f %.5f %.5f %.5f %.5f\n" % (x[i], y[i], z[i], vx[i], vy[i], vz[i]))
      f.close()

if __name__ == '__main__':
  main()
 
