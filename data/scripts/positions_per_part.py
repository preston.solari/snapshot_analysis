import sys
import os
#sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/NONANLibrary/python/")
sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import numpy as np
np.set_printoptions(threshold=np.inf)

from den_map import data
import resonances as res
import readsnap as reads
from plot_den_maps import adjust_center_mass_unit

def main():
  #fdir = "/home/ia/alexlg/snapshot_analysis/data/rerun/" # rerun
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/" # rerun
  
  #fname = "/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl 
  fname = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun atocatl 
  
  listsims = ['SGS31r']
  #listsims = ['SGS34']
  #listsims = ['SGS36']
  #listsims = ['SGS31r', 'SGS34', 'SGS36']

  initsnap = 0
  totsnap = 20
  skip = 1
  jump = 1
  rsnap = range(initsnap,totsnap+1,skip)
  
  #npart = 80000
  #npart = 240000
  npart = 720000
  
  for sim in listsims:
    #fsave = fdir+sim+"/positions_per_part/" # atocatl
    fsave = fname+"/positions_per_part/"+sim+"/" # cronex-pc
    if not os.path.exists(fsave): # create path fdat if does not exist
      os.makedirs(fsave)

    frun = fname + sim + "/" # location of the file

    # dd = reads.readsnap(frun, 0, 2) # disk particles' data

    # ids = dd['id']
    # ids = list(ids[:5])
    #kid = {id:n for n, id in enumerate(ids)}

    ids = [2206340, 2831625, 2851304, 2984955, 1792549, 1679456, 2449301] # SGS31r ID's 
    
    #dict_part = {n:[] for n in range(len(ids))}
    dict_part = {id:[] for id in ids}
    for snap in rsnap:
      print("Getting data for {0} ({1} of {2}) - snapshot {3} ({4} of {5})".format(sim, listsims.index(sim)+1, len(listsims), \
            snap, rsnap.index(snap)+1, len(rsnap)))

      dd = reads.readsnap(frun, snap, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      pid = dd['id']
      pid = list(pid)

      # remove center of mass (disc)
      x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      
      for i in dict_part:
        p = [x[pid.index(i)], y[pid.index(i)], z[pid.index(i)]]
        dict_part[i].append(p)

    dict_part = {id:np.array(arr) for id, arr in dict_part.items()}
    for k in dict_part:
      num = res.snap_mask(k, len(str(max(ids))))
      fdat = fsave+"pos_per_part_"+sim+"_"+num+".txt"
      f = open(fdat, 'w+')
      #f = open(fdat, 'a')
      np.savetxt(f, dict_part[k])
      #f.write('\n')
      f.close()
    
    f = open(fsave+"ids.txt", 'w+')
    for id in ids:
      num = res.snap_mask(id, len(str(max(ids))))
      f.write("{0}\n".format(num))
    f.close()

if __name__ == '__main__':
  main()
