#!/usr/local/bin python3

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from operator import itemgetter

sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")

from readsnap_no_acc import readsnap
from resonances import snap_mask
from plot_den_maps import adjust_center_mass
from resonances import velocities
from spectral_analysis_all_disc_particles import calculate_correct_omega
from phase_of_bar import phase_of_bar_annuli_2d
from den_map import data

np.set_printoptions(threshold=np.inf)

def get_random_ids(ids, num=1000):
  min = 0
  max = len(ids)

  index = np.random.randint(low=min, high=max, size=num)
  
  return list(itemgetter(*index)(ids))

def main():
  #fname = "/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  fname="/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/" # rerun atocatl
  model="SGS1"

  initsnap = 100
  totsnap = 240
  skip = 1

  ts = 0.05
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = time * ts

  rmax = 12
  rings = 36
  radii = np.linspace(0, rmax, rings)

  dd = readsnap(fname+model, initsnap, 2)

  pd = dd['p']
  vd = dd['v']
  id = dd['id']
  id = list(id)
  #md = dd['m']
  x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
  
  npart = 100000
  new_ids = get_random_ids(id, npart)

  # get omega_b
  #fpat = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/pattern_speed/"
  fpat = "/home/ia/alexlg/snapshot_analysis/data/rerun/all/pattern_speed/" 
  namef = "pattern_speed_"+model+".txt"
  patd = data(fpat+namef)
  omegab = patd[1]

  omi = initsnap-1
  omega = np.zeros(np.shape(new_ids))
  r = np.zeros(np.shape(new_ids))
  for snap in rsnap:
    ext = snap_mask(snap, 3) # file's mask (last three digits)

    dd = readsnap(fname+model, snap, 2)

    pd = dd['p']
    vd = dd['v']
    id = dd['id']
    id = list(id)
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

    print("\n############# Performing spectral analysis (snapshot %s of %s) #############" % (str(snap), str(totsnap)))

    # process omega and kappa
    x = np.array([x[id.index(i)] for i in new_ids])
    y = np.array([y[id.index(i)] for i in new_ids])
    z = np.array([z[id.index(i)] for i in new_ids])
    vx = np.array([vx[id.index(i)] for i in new_ids])
    vy = np.array([vy[id.index(i)] for i in new_ids])
    vz = np.array([vz[id.index(i)] for i in new_ids])

    # nx = np.zeros(len(new_ids))
    # ny = np.zeros(len(new_ids))
    # nz = np.zeros(len(new_ids))
    # nvx = np.zeros(len(new_ids))
    # nvy = np.zeros(len(new_ids))
    # nvz = np.zeros(len(new_ids))
    # for i, j in enumerate(new_ids):
    #   nx[i] = x[id.index(j)]  
    #   ny[i] = y[id.index(j)]  
    #   nz[i] = z[id.index(j)]  
    #   nvx[i] = vx[id.index(j)]  
    #   nvy[i] = vy[id.index(j)]  
    #   nvz[i] = vz[id.index(j)]  

    rn = np.sqrt(x**2+y**2)
    # rn = np.sqrt(nx**2+ny**2)
    vt, vr, vz = velocities(x, y, vx, vy, vz)
    # vt, vr, vz = velocities(nx, ny, nvx, nvy, nvz)

    omegan = vt/r
    kappa = r*((omegan**2-omega**2)/(rn-r)**2) + 4*omega**2

    ratio = (omegan-omegab[omi]) / np.sqrt(kappa)
    print(omegab[omi])
    # print(ratio)

    # update values
    omega = omegan
    r = rn
    omi+=skip
  
  ratio = ratio[~np.isnan(ratio)]
  print(ratio)
  dr=200
  minr = -3.0
  maxr = 2.0
  edges = np.arange(minr, maxr, (maxr-minr)/dr)

  result, bins = np.histogram(ratio, bins=edges, normed=False)
  
  frat = "/home/ia/alexlg/snapshot_analysis/data/rerun/all/spectral_ratio/"
  if not os.path.exists(frat): # create path fdat if does not exist
        os.makedirs(frat)
  f = open(frat+"spectral_"+model+".txt", "w+")

  for j in range(len(edges[:-1])):
    f.write("%s %s\n" % (edges[j], result[j]))
  f.close()

if __name__ == '__main__':
  main()
