import os
import sys
from xml.parsers.expat import model
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from scipy import interpolate

#sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/scripts/")
sys.path.insert(0,"/home/ia/alexlg/snapshot_analysis/data/scripts")

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask
#from plot_den_maps import adjust_center_mass
from velocities import vel_components_cyl
from accelerations import acc_components_cyl
from density_scatter import density_scatter
from scatter_colormap import scatter_cm


def density_scatter(x, y, a = None, sort = True, bins = 20, ext = None):
  
  if a is None:
    data , x_e, y_e = np.histogram2d(x, y, bins = bins, density = True)
    z = interpn( ( 0.5*(x_e[1:] + x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data , np.vstack([x,y]).T , method = "splinef2d", bounds_error = False)

    #To be sure to plot all data
    z[np.where(np.isnan(z))] = 0.0

    if ext is not None:
      data = np.where((data >= ext[0]) & (data <= ext[1]), data, 0)

  else: 
    if ext is not None:
      z = np.where((a >= ext[0]) & (a <= ext[1]), a, 0)
    else:
      z = a

  # Sort the points by density, so that the densest points are plotted last
  if sort :
    idx = z.argsort()
    x, y, z = x[idx], y[idx], z[idx]

  return x, y, z

def main():
  #fdir = "/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  fdir = "/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  # fdir = "/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/halos/" # rerun
  fdir = "/home/ia/alexlg/Gadget-2.0.7/Gadget2/sims/primer-art-rerun/"
  fdir2="/home/ia/alexlg/snapshot_analysis/data/rerun/"
  listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
              'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
              'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']

  initsnap = 0
  totsnap = 120
  skip = 1
  rsnap = range(initsnap,totsnap+1,skip)
  
  lim = 500
  amin = 0
  amax = -10000
  
  for model in listsims:
    for snap in rsnap:
      ext = snap_mask(snap/skip, 4) # file's mask (last three digits)

      dd = readsnap(fdir+model+'/', snap, 2)

      pd = dd['p']
      vd = dd['v']
      ad = dd['a']
      id = dd['id']
      id = list(id)
      #md = dd['m']
      x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
      vx, vy, vz= adjust_center_mass_unit(vd[:, 0], vd[:, 1], vd[:, 2])
      ax, ay, az = adjust_center_mass_unit(ad[:, 0], ad[:, 1], ad[:, 2])

      r = np.sqrt(x**2 + y**2 + z**2)
      theta = np.arctan2(y, x)
      vr, vt, vz = vel_components_cyl((x,y,z), (vx, vy, vz))
      ar, at, az = acc_components_cyl((x, y, z), (vx, vy, vz), (ax, ay, az))
    
      xd, yd, zd = density_scatter(np.log10(r), vr, bins=[30,30], ext=(0, 0.02))
      xa, ya, za = density_scatter(np.log10(r), vr, a, ext=(-25000, 1000))

      fv = fdir2+model+'/surface_of_section/'
      if not os.path.exists(fv): # create path if does not exist
        os.makedirs(fv)
      f=open(fv+"sos_den_"+model+"_"+ext+".txt", "w+")
      for i in range(len(xd)):
        f.write("%.5f %.5f %.5f\n" % (xd[i], yd[i], zd[i]))
      f.close()
      f=open(fv+"sos_acc_"+model+"_"+ext+".txt", "w+")
      for i in range(len(xa)):
        f.write("%.5f %.5f %.5f\n" % (xa[i], ya[i], za[i]))
      f.close()

if __name__ == '__main__':
  main()
