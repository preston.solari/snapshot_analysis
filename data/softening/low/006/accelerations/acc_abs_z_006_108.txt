0.05000 17633.82884
0.15000 22483.66342
0.25000 22087.41349
0.35000 19523.32576
0.45000 16967.18761
0.55000 15687.64658
0.65000 13989.50287
0.75000 12861.07208
0.85000 11627.17037
0.95000 10844.99056
1.05000 10053.25618
1.15000 9517.94385
1.25000 8904.04114
1.35000 8181.75250
1.45000 7963.57555
1.55000 7428.44070
1.65000 7089.59844
1.75000 6828.24550
1.85000 6325.58894
1.95000 6228.61013
2.05000 5923.27228
2.15000 5539.63020
2.25000 5402.02960
2.35000 5242.25629
2.45000 5023.16188
2.55000 4893.05143
2.65000 4575.02608
2.75000 4392.56827
2.85000 4247.11359
2.95000 4190.91393
3.05000 4039.61388
3.15000 3802.73253
3.25000 3657.02070
3.35000 3491.81521
3.45000 3449.18013
3.55000 3291.85914
3.65000 3155.41972
3.75000 3069.20449
3.85000 2978.97579
3.95000 2999.18517
4.05000 2885.52594
4.15000 2715.40568
4.25000 2648.26599
4.35000 2630.11521
4.45000 2516.78949
4.55000 2507.45572
4.65000 2385.79499
4.75000 2366.34037
4.85000 2297.50897
4.95000 2225.55350
5.05000 2138.41968
5.15000 2067.57458
5.25000 2001.56228
5.35000 1929.61412
5.45000 1899.69922
5.55000 1853.36833
5.65000 1768.68153
5.75000 1731.90094
5.85000 1678.05533
5.95000 1645.24292
6.05000 1660.70106
6.15000 1541.26567
6.25000 1543.70595
6.35000 1515.55478
6.45000 1439.23368
6.55000 1385.61099
6.65000 1348.90981
6.75000 1314.33292
6.85000 1319.29534
6.95000 1300.07144
7.05000 1192.08704
7.15000 1193.69977
7.25000 1185.38970
7.35000 1173.99136
7.45000 1153.54750
7.55000 1114.54958
7.65000 1075.95465
7.75000 1047.63886
7.85000 1028.66296
7.95000 962.47396
8.05000 970.18249
8.15000 973.18005
8.25000 934.51466
8.35000 899.90588
8.45000 875.10297
8.55000 893.63253
8.65000 841.67991
8.75000 848.09824
8.85000 797.11299
8.95000 779.64724
9.05000 750.56832
9.15000 749.70765
9.25000 689.28796
9.35000 720.41996
9.45000 694.19616
9.55000 685.63945
9.65000 663.92953
9.75000 641.26390
9.85000 642.65787
9.95000 646.05085
10.05000 615.64904
10.15000 621.14363
10.25000 601.11860
10.35000 575.76344
10.45000 575.42124
10.55000 563.95825
10.65000 547.39778
10.75000 553.71430
10.85000 564.46525
10.95000 548.59848
11.05000 505.49141
11.15000 527.40214
11.25000 514.52019
11.35000 482.74345
11.45000 462.92197
11.55000 491.32981
11.65000 459.01550
11.75000 451.97400
11.85000 443.23380
11.95000 433.95209
12.05000 442.40813
12.15000 420.78864
12.25000 426.64363
12.35000 420.60398
12.45000 394.99466
12.55000 404.67838
12.65000 395.18172
12.75000 384.67903
12.85000 359.93280
12.95000 381.43293
13.05000 366.75191
13.15000 336.43153
13.25000 367.53728
13.35000 344.02296
13.45000 339.68801
13.55000 320.80006
13.65000 304.73430
13.75000 335.78852
13.85000 309.69703
13.95000 311.41886
14.05000 292.39900
14.15000 298.25111
14.25000 310.39663
14.35000 293.04950
14.45000 272.53144
14.55000 269.94948
14.65000 256.40246
14.75000 267.88503
14.85000 255.01933
14.95000 269.57253
15.05000 250.85762
15.15000 244.74804
15.25000 222.90921
15.35000 245.80910
15.45000 233.59964
15.55000 221.65326
15.65000 232.30054
15.75000 224.22647
15.85000 206.62643
15.95000 202.77968
16.05000 202.86115
16.15000 201.60061
16.25000 203.52714
16.35000 211.54477
16.45000 201.36594
16.55000 193.28596
16.65000 204.02322
16.75000 194.70473
16.85000 184.98066
16.95000 186.87592
17.05000 189.76571
17.15000 173.04708
17.25000 174.14567
17.35000 178.70814
17.45000 164.01123
17.55000 163.80060
17.65000 187.10087
17.75000 174.92509
17.85000 168.28150
17.95000 143.29460
18.05000 163.20062
18.15000 151.18949
18.25000 148.18726
18.35000 155.37077
18.45000 146.76770
18.55000 138.70605
18.65000 139.43496
18.75000 149.49090
18.85000 136.34252
18.95000 122.88438
19.05000 134.85877
19.15000 136.94418
19.25000 140.96208
19.35000 139.21444
19.45000 130.63852
19.55000 139.94801
19.65000 124.47580
19.75000 115.36986
19.85000 130.69946
19.95000 129.35714
