#!/usr/bin/env python

# Python program to graph the isodensity map of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import seaborn as sns
from scipy.interpolate import griddata as gd
import scipy.ndimage as ndimage

from vel_map import data as vdata

# This function obtains and formats the data from a file with 
# all the properties
def data(d_file,step=1):
  print("Reading file...")

  f=open(d_file,"r")
  
  ud=f.readlines()
  lines=ud[0::step]
  li=[line.split() for line in lines]

  l=len(li[0])

  print("Obtaining data...")
  
  p=[]
  for i in range(l):
    p.append([float(e[i]) if e[i].replace('.','',1).isdigit() else e[i] for e in li])
  cp=np.array(p)

  print("Done.")

  f.close()

  return cp

# Funtion to draw velocity contours
def vel_contours(x,y,v,rx,ry,rv,iname,method='linear'):
  print("Creating data grid...")

  pxy=np.asarray([[i,j] for ex,i in enumerate(x) for ey,j in enumerate(y) if ex == ey]) # creating array for all x,y positions
  vel=np.asarray(v) # array with all velocities
  
  xi,yi=np.mgrid[-rx:rx:80j,-ry:ry:80j] # grid definition

  # data grid depending on a method of choosing
  # default method is 'linear'
  if method == 'nearest':
    vm=gd(pxy,vel,(xi,yi),method='nearest') 
  elif method == 'linear':
    vm=gd(pxy,vel,(xi,yi),method='linear') 
  elif method == 'cubic':
    vm=gd(pxy,vel,(xi,yi),method='cubic')
  else:
    print("There is no method %s in function mapping_scipy." % method)
  
  print("Done.")

  vm=ndimage.gaussian_filter(vm,sigma=1.5,order=0)

  print("Done.")
  
  return xi,yi,vm

# Function to draw density countors
def density_estimation(x,y,nbins):
  k=st.kde.gaussian_kde([x,y])
  xi,yi=np.mgrid[x.min():x.max():nbins*1j,y.min():y.max():nbins*1j]
  zi=k(np.vstack([xi.flatten(),yi.flatten()]))
  return xi,yi,zi

# Main definition
def main():
  fdir="/home/kaicudon/Documentos/Maestria/Tesis/sm-04-10-2018/sim_11/"
  
  fig=plt.figure(figsize=(8.4,8.4646))
  plt.clf()

  plt.rcParams.update({'font.size': 17})

  la=20 # axis

  ####### SGS26 #######
  lb1_26=10.2   # L1 bar longitude
  lb2_26=14.25 # L2 bar longitude
  lbq_26=9.25  # Lq bar longitude

  ####### SGS2 #######
  lb1_2=8.2   # L1 bar longitude
  lb2_2=12.25 # L2 bar longitude
  lbq_2=8.75  # Lq bar longitude

  ####### SGS13 #######
  lb1_13=9.8   # L1 bar longitude
  lb2_13=12.8 # L2 bar longitude
  lbq_13=8.25  # Lq bar longitude

  inc=33
  inc=(inc*np.pi)/180 # inclination in radians

  an=np.linspace(0, 2*np.pi, 100) # data to plot circles (with or without inclination) for L1 and L2

  lev=np.asarray([5.,12.,50.,150.,280.,500.,900.]) # levels for density contour map

  plt.axis([-la, la, -la, la])

  max_vel=120 # in km s^-1

  cmap='coolwarm'
  origin='lower'
  
  x,y=data(fdir+"density_map_002.dat") # extract x, y coordinates for particle positions
  xv,yv,v=vdata(fdir+"colormap_data_002.dat") # extract x, y coordinates for velocities

  #plt.axis('equal')            # all axes equal

  # calculate and plot velocity contours
 
  xi,yi,vm=vel_contours(xv,yv,v,la,la,max_vel,'nearest')
  plt.contour(xi,yi,vm,15,linewidths=1.2,cmap=cmap)
  plt.contourf(xi,yi,vm,15,vmax=max_vel,vmin=-max_vel,cmap=cmap)

  # calculate the 2D density of the data given

  #cmap='OrRd'
  counts,xbins,ybins=np.histogram2d(x,y,bins=60)
  # make the contour plot
  plt.contour(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
      ybins.min(),ybins.max()],origin=origin,colors='k',linewidths=0.7,
      linestyles='solid')

  # Color bar for velocity contours
  #fs=21
  #cb=plt.colorbar()
  #cb.set_label(r'$V_{\mathrm{obs}}$', fontsize=fs)

  plt.xlabel(r"$x \,\,\mathrm{[kpc]}$")
  plt.ylabel(r"$y \,\,\mathrm{[kpc]}$")

  #plt.savefig(iname)
  plt.show()

if __name__ == '__main__':
  main()
