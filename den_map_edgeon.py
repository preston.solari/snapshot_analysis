#!/usr/bin/env python

# Python program to graph the edge-on isodensity map of several snapshots

import matplotlib.pyplot as plt
import matplotlib.colors
import numpy as np
import scipy.stats as st
import seaborn as sns
from scipy.interpolate import griddata as gd
import scipy.ndimage as ndimage
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from midpoint_colorbar import MidPointNorm

from plot_den_maps import adjust_center_mass
from readsnap import readsnap

# rotate particles of a particular snapshot
def rotate_snap(x, y, z, rot = 30):
  # convert rot to radians
  rad = rot * np.pi / 180

  # apply rotation to coordinates
  newx = x * np.cos(rad) - y * np.sin(rad)
  newy = x * np.sin(rad) + y * np.cos(rad)
  newz = z

  return newx, newy, newz

# Main definition
def main():
  fdir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  sims = ['sim_11', 'sim_33', 'sim_56']
  name = ['SGS2', 'SGS15', 'SGS30']

  st = 1

  rotate = [-12, 37.5, 0]

  fig, axs = plt.subplots(3, 1)
  # fig, axs = plt.subplots(1, 3)

  # Colorbar properties of (density map
  xins = inset_axes(axs[len(sims)-1],
                    width = "3%",  # width = 5% of parent_bbox width
                    height ="100%",  # height : 50%
                    loc = 'lower left',
                    bbox_to_anchor = (1.02, 0., 1, 1),
                    bbox_transform = axs[len(sims)-1].transAxes,
                    borderpad = 0,
                    )
  
  fontlabel = {'fontname':'DejaVu Sans Mono',
               'color':'white',
               'weight':'bold',
               'size':11}
  
  #lev = np.asarray([100, 150, 300, 500, 700, 1200, 1500, 2000, 2500, 3300, 5500]) # levels for density contour map
  lev = np.logspace(1, 3.7, num = 12)
  #lev = np.linspace(500, 3000, 10)

  cmap='jet'
  origin='lower'

  for sim in range(len(sims)):
    fsnap = fdir + sims[sim] + '/'

    # Read snapshots in binary format (GADGET-2)
    dd = readsnap(fsnap, '020', 2)
    pd = dd['p']  # halo particle positions
    vd = dd['v']  # halo particle velocities
    masspd = dd['m']

    # remove center of mass (disc)
    # xd, yd, zd, vxd, vyd, vzd = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    xd, yd, zd, vxd, vyd, vzd = adjust_center_mass(pd[::st, 0], pd[::st, 1], pd[::st, 2], vd[::st, 0], vd[::st, 1], vd[::st, 2])
    xd, yd, zd = rotate_snap(xd, yd, zd, rot = rotate[sim])

    # calculate the 2D density of the data given
    counts, xbins, ybins = np.histogram2d(xd, zd, bins = 60)
    # make the contour plot
    norm = MidPointNorm(midpoint = 500, vmin = lev.min(), vmax = lev.max())
    cm = axs[sim].contourf(counts.T, levels = lev, extent = [xbins.min(), xbins.max(),
                           ybins.min(), ybins.max()], origin = origin, norm = norm,
                           extend = 'both', cmap = cmap)
    axs[sim].contour(cm, colors = 'k', linewidths = 0.7, linestyles = 'solid')
    
    # Put colorbar
    if sim == len(sims)-1:
      #norm = matplotlib.colors.Normalize(vmin = lev[0], vmax = lev[-1])
      sm = matplotlib.cm.ScalarMappable(norm = norm, cmap = cm.cmap)
      sm.set_array([])
      cbar = fig.colorbar(sm, ticks = np.concatenate((np.linspace(0, 500, 5), np.linspace(500, 5000, 5))), cax = xins)
      cbar.set_label(r'$\Sigma_{N}$', fontsize = 10)
      cbar.ax.tick_params(labelsize = 8)
    
    # Name of each model
    axs[sim].text(12, 3, name[sim], **fontlabel)

  lx = 15
  ly = 5
  for ax in axs.flat:
    ax.label_outer()
    ax.set_xlim(-lx, lx)
    ax.set_ylim(-ly, ly)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                   top = True, left = True, right = True, bottom = True,
                   labelsize = 10)

  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel(r'$x$ [kpc]', fontsize = 12)
  plt.ylabel(r'$z$ [kpc]', fontsize = 12)

  #plt.savefig(iname)
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
