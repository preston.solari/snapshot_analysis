#!/usr/bin/env python

# Python program to graph the isodensity map of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import seaborn as sns
from scipy.interpolate import griddata as gd
import scipy.ndimage as ndimage

from plot_particles_disc import snaps_range
import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
# from readsnap import readsnap
import readsnap_no_acc as readnoacc

from mpl_toolkits.axes_grid1.inset_locator import inset_axes

# This function obtains and formats the data from a file with 
# all the positions
def data(d_file,step=1):
  print("Reading file...")

  f=open(d_file,"r")
  
  ud=f.readlines()
  lines=ud[0::step]
  li=[line.split() for line in lines]

  l=len(li[0])

  print("Done.")
  
  print("Obtaining positions...")
  
  p=[]
  for i in range(l):
    p.append([float(e[i]) for e in li])
  cp=np.array(p)

  print("Done.")

  f.close()

  return cp

# Funtion to draw velocity contours
def vel_contours(x,y,v,rx,ry,rv,iname,method='linear'):
  print("Creating data grid...")

  pxy=np.asarray([[i,j] for ex,i in enumerate(x) for ey,j in enumerate(y) if ex == ey]) # creating array for all x,y positions
  vel=np.asarray(v) # array with all velocities
  
  xi,yi=np.mgrid[-rx:rx:80j,-ry:ry:80j] # grid definition

  # data grid depending on a method of choosing
  # default method is 'linear'
  if method == 'nearest':
    vm=gd(pxy,vel,(xi,yi),method='nearest') 
  elif method == 'linear':
    vm=gd(pxy,vel,(xi,yi),method='linear') 
  elif method == 'cubic':
    vm=gd(pxy,vel,(xi,yi),method='cubic')
  else:
    print("There is no method %s in function mapping_scipy." % method)
  
  print("Done.")

  vm=ndimage.gaussian_filter(vm,sigma=1.5,order=0)

  print("Done.")
  
  return xi,yi,vm

# Function to draw density countors
def density_estimation(x,y,nbins):
  k=st.kde.gaussian_kde([x,y])
  xi,yi=np.mgrid[x.min():x.max():nbins*1j,y.min():y.max():nbins*1j]
  zi=k(np.vstack([xi.flatten(),yi.flatten()]))
  return xi,yi,zi

# Main definition
def main():
  # fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  #fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/einasto/output"
  #name = "Einasto"
  
  # listsims = ['12','11','13','14','15','16']
  # listsims = ['12','11']
  listsims = ['11','25','35','46','55']
  # name = ["SGS1", "SGS2", "SGS3", "SGS4", "SGS5", "SGS6"]
  name = ["SGS2", "SGS11", "SGS17", "SGS24", "SGS29"]

  nsnaps=5 # number of snapshots to plot
  totsnap=20 # total snapshots
  initsnap=1 # initial snapshot
  step = 10
  ts = 0.6

  steps=snaps_range(initsnap,totsnap,nsnaps) # steps between snapshots
  #steps=[4,6,7,9,14,20]
  #steps=[3,7,14,20]

  la=18 # axis

  inc=33
  inc=(inc*np.pi)/180 # inclination in radians

  an=np.linspace(0, 2*np.pi, 100) # data to plot circles (with or without inclination) for L1 and L2

  lev=np.asarray([10.,20.,60.,150.,300.,480.,700.,1080.]) # levels for density contour map
  #lev=np.asarray([5.,12.,50.,150.,280.,500.,900.]) # levels for density contour map
  #lev=np.asarray([5.,12.,50.,120.,180.,250.,400.]) # levels for density contour map

  #plt.axis([-la, la, -la, la])

  max_vel=120 # in km s^-1

  cmap='jet'
  origin='lower'

  fig,axs=plt.subplots(len(listsims), nsnaps, sharex='col', sharey='row',
                       gridspec_kw={'hspace': 0.0, 'wspace': 0.0},
                       figsize=(10,10), dpi=120)

  plt.style.use('seaborn-bright')

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':8}
  
  # Colorbar properties of velocity map
  xins = inset_axes(axs[len(listsims)-1,nsnaps-1],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[len(listsims)-1,nsnaps-1].transAxes,
                    borderpad=0,
                    )
  rows = 0
  for sims in range(len(listsims)):
    fname = fdir + "/sim_" + listsims[sims] + "/"
    columns=0
    for snap in steps:
      ext=res.snap_mask(snap,3)

      dd = readnoacc.readsnap(fname, ext, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      vt, vr, vz = res.velocities(x, y, vx, vy, vz)

      x = x[::step]
      y = y[::step]
      theta = np.arctan2(y, x)
      x = x/np.cos(inc)
      vty = vt[::step]*np.cos(theta)
      vo = vty*np.sin(inc)

      # calculate the 2D density of the data given
      #cmap='OrRd'
      counts,xbins,ybins=np.histogram2d(x,y,bins=60)
      # make the contour plot
      axs[rows,columns].contour(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
          ybins.min(),ybins.max()],origin=origin,colors='w',linewidths=1.2,
          linestyles='solid',alpha=1.0)

      # calculate and plot velocity contours
      niv = 15
      xi,yi,vm=vel_contours(x,y,vo,la,la,max_vel,'nearest')
      axs[rows,columns].contour(xi,yi,vm,niv,linewidths=0.9,colors='k')
      cm=axs[rows,columns].contourf(xi,yi,vm,niv,vmax=max_vel,vmin=-max_vel,cmap=cmap)

      # Put colorbar
      if(rows == len(listsims)-1 and columns == nsnaps-1):
        cbar=fig.colorbar(cm,cax=xins)
        cbar.set_label(r'$v_{\mathrm{obs}}$[km s$^{-1}$]',fontsize=13)
        cbar.ax.tick_params(labelsize=12)

      # For text indicating model
      if columns == 0:
        axs[rows,columns].text(-14.5,-13.0,name[sims],bbox=dict(fc='white',ec='none',alpha=0.7),
                                **fontlabel)

      # text indicating time
      if rows == 0:
        axs[rows,columns].text(9.7,10.8,
                          r'$t = %s$' %(float(snap)*ts),bbox=dict(fc='white',ec='none',alpha=0.7),
                          **fontlabel)

      columns=columns+1
    rows=rows+1

  for ax in axs.flat:
    ax.label_outer()
    ax.set(adjustable='box',aspect='equal')
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=13)
    ax.set_xticks([])
    ax.set_yticks([])

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  #plt.xlabel(r'$x \,\,\mathrm{[kpc]}$')
  #plt.ylabel(r'$y \,\,\mathrm{[kpc]}$',labelpad=-2)

  xlim = (-la, la)
  ylim = (-la, la)

  # Setting the values for all axes.
  plt.setp(axs, xlim=xlim, ylim=ylim)

  #plt.gca().set_aspect('equal', adjustable='box') # for equal axis
  #plt.axis('equal')

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
