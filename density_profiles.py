import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from mass_assignment_secant_method import surface_density
from plot_den_maps import adjust_center_mass_unit
import readsnap_no_acc as rsnap
import resonances as res

def main():
    fdir = "/home/cronex/Documentos/Doctorado/eqforce/"

    listsims = ["out", "out"]

    isnap=0
    fsnap=240
    ss=120
    num_snaps = 6

    ts = 0.05 # timestep of snapshot
    nlines = np.linspace(isnap, fsnap, num_snaps).astype(int)
    time = nlines*ts

    rowp = 2 # rows of plot
    colp = 2 # columns of plot
    fig, axs = plt.subplots(rowp, colp, sharex=True, sharey=True,
            gridspec_kw={'hspace': 0.1, 'wspace': 0.08}, dpi=120)

    axb1=rowp-1
    axb2=colp-1
    xins = inset_axes(axs[axb1, axb2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[axb1, axb2].transAxes,
                    borderpad=0,
                    )
    
    norm = mpl.colors.Normalize(vmin=time.min(), vmax=time.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
    cmap.set_array([])

    row = 0
    col = 0
    for sim in listsims:
        frun = fdir+sim+"/"
        
        for i in nlines:
            ext=res.snap_mask(i,3)
            dd = rsnap.readsnap(frun, i, 2) # disk particles' data

            pd = dd['p']  # disc particle positions
            vd = dd['v']  # disc particle velocities
            mp = dd['m'][0]

            # remove center of mass (disc)
            x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

            print("\n Plotting density...")

            nr, den = surface_density(x, y, mp, rmax=20)
        
            if(row == axb1 and col == axb2 and i == nlines[-1]):
                cbar=fig.colorbar(cmap,cax=xins,ticks=time)
                cbar.set_label(r'Time [Gyrs]',fontsize=8)
                cbar.ax.tick_params(labelsize=6.7)

            axs[row,col].plot(nr, den, '-', color=cmap.to_rgba(i*ts), lw = 1.0)
            
        col += 1
        if col % colp == 0:
            col = 0
            row += 1

    plt.show()
    plt.clf()


if __name__ == '__main__':
    main()
