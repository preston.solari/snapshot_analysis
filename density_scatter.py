import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize 
from scipy.interpolate import interpn
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import griddata as gd
from scipy.interpolate import interp2d
import matplotlib.ticker as tk
import scipy.ndimage as ndimage

def density_scatter( x , y, ax = None, sort = True, bins = 20,
                     cmap = plt.cm.jet, ext = None, sci = False,
                     **kwargs ):
  """
  Scatter plot colored by 2d histogram
  """
  if ax is None :
    fig , ax = plt.subplots()
  else:
    fig , ax = ax[0], ax[1]
  
  data , x_e, y_e = np.histogram2d(x, y, bins = bins, density = True)
  #if ext is not None:
    #data = data[np.where((data >= ext[0]) & (data <= ext[1]))]
    # data = np.asarray([d[np.where((d >= ext[0]) & (d <= ext[1]))] for d in data])
  #  data = np.where((data >= ext[0]) & (data <= ext[1]), data, 0)

  z = interpn( ( 0.5*(x_e[1:] + x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data, np.vstack([x,y]).T, method = "splinef2d", bounds_error = False)    
  #To be sure to plot all data
  z[np.where(np.isnan(z))] = 0.0

  # Sort the points by density, so that the densest points are plotted last
  if sort :
    idx = z.argsort()
    x, y, z = x[idx], y[idx], z[idx]

  if ext is not None:
    ax.scatter(x, y, c=z, cmap=cmap, vmin=ext[0], vmax=ext[1], **kwargs)
    norm = Normalize(vmin = ext[0], vmax = ext[1])
  else:
    ax.scatter(x, y, c=z, cmap=cmap, **kwargs)
    norm = Normalize(vmin = np.min(z), vmax = np.max(z))
  
  # norm = Normalize(vmin = np.min(z), vmax = np.max(z))

  sm = cm.ScalarMappable(norm = norm, cmap = cmap)
  sm.set_array([])
  if sci:
    fmt = tk.ScalarFormatter(useMathText=True)
    fmt.set_powerlimits((0, 0))
    cbar = fig.colorbar(sm, ax=ax, format=fmt)
  else:
    cbar = fig.colorbar(sm, ax=ax)
  cbar.ax.set_ylabel('Density')
  

  return ax


# This function creates a grid and countours to map
# the velocities of all the system using scipy's griddata
def mapping_scipy(x, y, v, rx, ry, rv, ax=None, method='linear',
                  gridsize=80j, sigma=1.5, levels=15, cmap=plt.cm.jet,
                  lw_contours=1.5, color_contours='k', fs_contours=21,
                  xlabel='', ylabel='', cb_label=''):
  
  if ax is None:
    fig, ax = plt.subplots()
  else:
    fig, ax = ax[0], ax[1]

  print("Creating data grid...")
  fs=fs_contours

  pxy=np.asarray([[i,j] for ex,i in enumerate(x) for ey,j in enumerate(y) if ex == ey]) # creating array for all x,y positions
  vel=np.asarray(v) # array with all velocities
  
  xi,yi=np.mgrid[-rx:rx:gridsize,-ry:ry:gridsize] # grid definition

  # data grid depending on a method of choosing
  # default method is 'linear'
  if method == 'nearest':
    vm=gd(pxy,vel,(xi,yi),method='nearest') 
  elif method == 'linear':
    vm=gd(pxy,vel,(xi,yi),method='linear') 
  elif method == 'cubic':
    vm=gd(pxy,vel,(xi,yi),method='cubic')
  else:
    print("There is no method %s in function mapping_scipy." % method)
  
  print("Done.")

  vm=ndimage.gaussian_filter(vm,sigma=sigma,order=0)

  print("Plotting data into the grid...")

  ax=plt.subplots()
  # plotting contours
  CS=plt.contour(xi,yi,vm,levels,linewidths=lw_contours,colors=color_contours)
  CS=plt.contourf(xi,yi,vm,levels,vmax=rv[1],vmin=rv[0],cmap=cmap)

  # plotting grid
  plt.imshow(vm.T, extent=(-rx,rx,-ry,ry), origin='lower', cmap=cmap)
  cb=plt.colorbar()
  cb.set_label(cb_label, fontsize=fs_contours)
  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)
  #plt.plot(pxy[:,0], pxy[:,1], 'k.', ms=1)
  #plt.title('Velocity map using '+method+' interpolation')
  plt.gcf().set_size_inches(14, 10)

  for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                ax.get_xticklabels() + ax.get_yticklabels()):
      item.set_fontsize(fs_contours)

  cb.ax.set_yticklabels(cb.ax.get_yticklabels(), fontsize=fs_contours)

  print("Done.")

  return ax

def main():
  x = np.random.normal(size=100000)
  y = x * 3 + np.random.normal(size=100000)
  density_scatter(x, y, bins = [50,50], marker='.', s=5)
  plt.show()
  plt.clf()

if __name__ == "__main__":
  main()
