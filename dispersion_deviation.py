# - *- coding: utf- 8 - *-

#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
import readsnap_no_acc  as rsnap
from phase_of_bar import *
from distorsion_parameter import distorsion_parameter
from mass_assignment_secant_method import surface_density
from distorsion_parameter import distorsion_parameter

# Main definition
def main():
  #fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  #fname = "/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fname = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  fname = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  # fdat = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/strength/"
  fdat = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/strength/"

  # listsims=["SGS8"]
  # listsims=["SGS5", "SGS8", "SGS9", "SGS11", "SGS13", "SGS14", "SGS15", "SGS17", 
  #           "SGS19", "SGS20", "SGS21", "SGS23", "SGS25", "SGS26", "SGS27", "SGS29", "SGS34", "SGS35"]
  # listsims=["sim_21", "sim_23", "sim_32", "sim_31", "sim_33", "sim_42", "sim_41", "sim_43", "sim_52", "sim_51", "sim_53"] 
  listsims = ['12','11','13','14','15','16',
              '22','21','23','24','25','26',
              '32','31','33','34','35','36',
              '42','41','43','44','45','46',
              '52','51','53','54','55','56',
              '62','61','63','64','65','66']
  # listsims = ['12']
  # listsims120 = ['SGS5','SGS8','SGS9','SGS11', 'SGS13', 'SGS14', # 120 snaps
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  # # listsims = ['SGS14', 'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  # #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  # # listsims = ['SGS13']
  # listsims240 = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7', # 240 snaps
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  # listsims1200 = ['SGS34', 'SGS36'] # 1200 snaps
  # # listsims = ['SGS6']
  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #          'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #          'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #          'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #          'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #          'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  lensims = len(listsims)

  # names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #          'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #          'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #          'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #          'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #          'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  rmax = 20
  ring = 20
  init_radii = res.radii_alt(rmax, ring)
  # rarr = [25, 30, 70, 70, 15]

  isnap = 0
  fsnap = 18

  ss = 2
  firstsnap = 0
  lastsnap = 2

  timestamp = 0.6
  nlines = np.arange(isnap, fsnap+1, ss)
  time = np.ceil((nlines)*timestamp)
  G = 43007.1

  fil = open(fname + "disp-dev-rerun-complete-snap-0-2.txt", "w+")
  
  for k in range(lensims):
    # frun = fname + listsims[k] + "/" # location of the file
    frun = fname + "sim_" + listsims[k] + "/" # location of the file

    print("\n============== Calculating deviation for simulation %s ==============\n"
           % (listsims[k]))
    
    # for rerun simulations
    # if listsims[k] in listsims120:
    #   lastsnap = 120
    # elif listsims[k] in listsims240:
    #   lastsnap = 240
    # elif listsims[k] in listsims1200:
    #   lastsnap = 1200
    
  #   fil = open(fname + "disp-dev-orig-" + str(names[k]) + ".txt", "w")
  #   for snap in nlines:
  #     exti = res.snap_mask(snap, 3) # file's mask (last three digits)

  #     ddi = rsnap.readsnap(frun, exti, 2) # disk particles' data

  #     pdi = ddi['p']  # disc particle positions
  #     vdi = ddi['v']  # disc particle velocities
  #     mpi = ddi['m'][0]  # mass of particles

  #     # remove center of mass (disc)
  #     xi, yi, zi, vxi, vyi, vzi = adjust_center_mass(pdi[:, 0], pdi[:, 1], pdi[:, 2], vdi[:, 0], vdi[:, 1], vdi[:, 2])

  #     pri=res.particle_radius(xi,yi)
  #     pvi=res.velocities(xi,yi,vxi,vyi,vzi)

  #     valuesi=(pri,) + pvi

  #     newri,avgi=res.avg_velocities(valuesi,init_radii)

  #     # next snapshot
  #     extf = res.snap_mask(snap+ss, 3) # file's mask (last three digits)

  #     ddf = rsnap.readsnap(frun, extf, 2) # disk particles' data

  #     pdf = ddf['p']  # disc particle positions
  #     vdf = ddf['v']  # disc particle velocities
  #     mpf = ddf['m'][0]  # mass of particles
  #     print(mpf)
 
  #     # remove center of mass (disc)
  #     xf, yf, zf, vxf, vyf, vzf = adjust_center_mass(pdf[:, 0], pdf[:, 1], pdf[:, 2], vdf[:, 0], vdf[:, 1], vdf[:, 2])

  #     prf=res.particle_radius(xf,yf)
  #     pvf=res.velocities(xf,yf,vxf,vyf,vzf)

  #     valuesf=(prf,) + pvf

  #     newrf,avgf=res.avg_velocities(valuesf,init_radii)
      
  #     dispr = (avgf[3] - avgi[3])**2 / avgi[3]**2
  #     dispr = np.sqrt(np.mean(dispr))

  #     dispt = (avgf[4] - avgi[4])**2 / avgi[4]**2
  #     dispt = np.sqrt(np.mean(dispt))

  #     dispz = (avgf[5] - avgi[5])**2 / avgi[5]**2
  #     dispz = np.sqrt(np.mean(dispz))

      # Toomre
      # determine angular velocity, epicyclic frequency and resonances (CR, ILR and OLR) per annuli
      # print("Calculating kappa...\n")
      # mod_r = init_radii[1:]
      # #print(len(mod_r),len(avgf[1]))
      # omg_val = res.omegar_fun(mod_r, avgf[1])
      # kap = res.kappa(omg_val)

      # den = surface_density(xf, yf, mpf, rmax, ring)
      # #print(len(kap),len(den[1]),len(avgf[3][1:]))
      # print(avgf[3][1:],kap,den[1])

      # Q = avgf[3][1:]*kap/3.36/G/den[1]
      # print(Q)
      # Q = np.mean(Q)

    #   fnamed = frun+"toomreQ_"+extf+".dat"
    #   pd = data(fnamed) # raw data for toomre's parameter
    #   Qf = np.mean(pd[1])

    #   if list(nlines).index(snap) == 0:
    #     # mod_r = init_radii[1:]
    #     # omg_val = res.omegar_fun(mod_r, avgi[1])
    #     # kap = res.kappa(omg_val)

    #     # den = surface_density(xf, yf, mpi, rmax, ring)
    #     # #print(len(kap),len(den[1]),len(avgi[3][1:]))

    #     # Q = np.mean(avgi[3][1:]*kap/3.36/G/den[1])
    #     fnamed = frun+"toomreQ_"+exti+".dat"
    #     pd = data(fnamed) # raw data for toomre's parameter
    #     Qi = np.mean(pd[1])

    #     fil.write(str(round((snap)*timestamp, 1)) + ' ' + str(0) + ' ' + str(0) + ' ' + str(0) + ' ' + str(Qi) + '\n')

    #   fil.write(str(round((snap+ss)*timestamp, 1)) + ' ' + str(dispr) + ' ' + str(dispt) + ' ' + str(dispz) + ' ' + str(Qf) + '\n')
    
    # fil.close()

  # listsims = ['12', '22', '33', '53', '62', '66']
  # names = ['SGS1', 'SGS7', 'SGS15', 'SGS27', 'SGS31', 'SGS36']
  # names = ['SGS1', 'SGS7', 'SGS31', 'SGS36']

  # fig, axs = plt.subplots(1, 4, gridspec_kw={'hspace': 0.6, 'wspace': 0.4}, figsize=(20, 7))

  # lstyle = ['-', '--', '.-', '-+']
  # lenls = len(lstyle)
  # lsi = 0
  # for name in names:
  #   file = fname + "disp-dev-orig-" + name + ".txt"
  #   pd = data(file)

  #   r = pd[0]
  #   dr = pd[1]
  #   dt = pd[2]
  #   dz = pd[3]
  #   Q = pd[4]

  #   axs[0].plot(r, dr, lstyle[lsi % lenls], label = name)
  #   axs[1].plot(r, dt, lstyle[lsi % lenls], label = name)
  #   axs[2].plot(r, dz, lstyle[lsi % lenls], label = name)
  #   axs[3].plot(r, Q, lstyle[lsi % lenls], label = name)

  #   axs[0].legend(loc = 'best', shadow = True)

  #   lsi += 1

  # for ax in axs.flat:
  #   ax.set_xlabel("Time [Gyrs]")
  #   ax.tick_params(axis='both',which='both',direction='in',
  #                  top=True,left=True,right=True,bottom=True,
  #                  labelsize=10)
  
  # fs = 14
  # axs[0].set_ylabel(r'$\sigma_{R}^{f}-\sigma_{R}^{i}$', fontsize=fs)
  # axs[1].set_ylabel(r'$\sigma_{\theta}^{f}-\sigma_{\theta}^{i}$', fontsize=fs)
  # axs[2].set_ylabel(r'$\sigma_{z}^{f}-\sigma_{z}^{i}$', fontsize=fs)
  # axs[3].set_ylabel(r'$Q_{mean}$', fontsize=fs)

  # plt.show()

  # plt.clf()

    #####################################################################################################
    # initial condition
    ddi = rsnap.readsnap(frun, firstsnap, 2) # disk particles' data

    pdi = ddi['p']  # disc particle positions
    vdi = ddi['v']  # disc particle velocities

    # remove center of mass (disc)
    xi, yi, zi, vxi, vyi, vzi = adjust_center_mass(pdi[:, 0], pdi[:, 1], pdi[:, 2], vdi[:, 0], vdi[:, 1], vdi[:, 2])

    pri=res.particle_radius(xi,yi)
    pvi=res.velocities(xi,yi,vxi,vyi,vzi)

    valuesi=(pri,) + pvi

    newri,avgi=res.avg_velocities(valuesi,init_radii)

    # last snapshot
    ddf = rsnap.readsnap(frun, lastsnap, 2) # disk particles' data

    pdf = ddf['p']  # disc particle positions
    vdf = ddf['v']  # disc particle velocities

    # remove center of mass (disc)
    xf, yf, zf, vxf, vyf, vzf = adjust_center_mass(pdf[:, 0], pdf[:, 1], pdf[:, 2], vdf[:, 0], vdf[:, 1], vdf[:, 2])

    prf=res.particle_radius(xf,yf)
    pvf=res.velocities(xf,yf,vxf,vyf,vzf)

    valuesf=(prf,) + pvf

    newrf,avgf=res.avg_velocities(valuesf,init_radii)
    print(avgi[3])
    print(avgf[3])

    dispr = (avgf[3] - avgi[3])**2 / avgi[3]**2
    dispr = np.sqrt(np.mean(dispr))

    dispt = (avgf[4] - avgi[4])**2 / avgi[4]**2
    dispt = np.sqrt(np.mean(dispt))

    dispz = (avgf[5] - avgi[5])**2 / avgi[5]**2
    dispz = np.sqrt(np.mean(dispz))

    eta = distorsion_parameter(xf, yf)

    # Q_min
    # ext = res.snap_mask(firstsnap,3)
    # fnamed = frun+"toomreQ_"+ext+".dat"
    # pd = data(fnamed) # raw data for toomre's parameter
    # Q = np.min(pd[1])
    
    # fourier component A_2
    fstr = fdat+"strength_"+listsims[k]+".txt"
    sd = data(fstr)
    a2 = sd[1,-1]
    
    # fil.write(listsims[k] + ' ' + str(Q) + ' ' + str(dispr) + ' ' + str(dispt) + ' ' + str(dispz) + ' ' + str(eta) + ' ' + str(a2) + '\n')
    fil.write(listsims[k] + ' ' + str(dispr) + ' ' + str(dispt) + ' ' + str(dispz) + ' ' + str(eta) + ' ' + str(a2) + '\n')
    #####################################################################################################
  
  # fil.close()

if __name__ == '__main__':
  main()