#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.interpolate import make_interp_spline, BSpline

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
# from readsnap_no_acc import readsnap
import readsnap_no_acc as readnoacc
from phase_of_bar import *

# calculate distorsion parameter
def distorsion_parameter(x, y, rmax = None):
  mixx = miyy = mixy = 0
  if isinstance(x, (list, tuple, np.ndarray)):
    if len(x) != len(y):
      print("distorsion_parameter: arguments must have the same length.")
      return 0
    
    if rmax == None:
      rmax = np.max(np.sqrt(x**2 + y**2))

    for i in range(len(x)):
      rad = np.sqrt(x[i]**2 + y[i]**2)
      if rad <= rmax:
        mixx = mixx + x[i]**2
        miyy = miyy + y[i]**2
        mixy = mixy + x[i] * y[i]   
  elif isinstance(x, (int, float)):
    mixx = x**2
    miyy = y**2
    mixy = x * y    
    
  etap = (mixx-miyy) / (mixx + miyy)
  etax = 2 * mixy / (mixx + miyy)

  return np.sqrt(etap**2 + etax**2)

# Main definition
def main():
  # fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  fname="/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"

  # listsims=["hernquist"]
  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"]
  # listsims=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15"]
  # listsims=["SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"] 
  # listsims=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"] 
  # listsims=["sim_21", "sim_23", "sim_32", "sim_31", "sim_33"] 
  # listsims=["sim_42", "sim_41", "sim_43", "sim_52", "sim_51", "sim_53"] 
  # listsims=["sim_15", "sim_25", "sim_35", "sim_45", "sim_55", "sim_65"] 
  # listsims=["SGS6"]
  # listsims = ['12','11','13','14','15','16']
  # listsims = ['12','11','13','14']

  listsims = ['12','11','13','14','15','16',
              '22','21','23','24','25','26',
              '32','31','33','34','35','36',
              '42','41','43','44','45','46',
              '52','51','53','54','55','56',
              '62','61','63','64','65','66']
  # listsims=["15"]
  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #          'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #          'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #          'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #          'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #          'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  lensims = len(listsims)

  # names=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"] 
  #names=["SGS6"]
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  # names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]

  markers=['-','--','-.','+','x','*']
  # markers=['+','x','*']
  
  # time variable
  initsnap = 0
  totsnap = 20
  ssnap = 1
  timestep = 0.6
  rsnap = range(initsnap, totsnap, ssnap)
  time = np.asarray(rsnap)
  time = time * timestep

  rmax = 20
  rings = 20
  radii = np.linspace(0, rmax, rings)

  plt.style.use('seaborn-bright')
  #ls = ['solid', 'dashed', 'dotted', 'dashdot']
  feta = []

  jump = 10 # skip through particle data

  sims_per_sp=6 # graphs per subplot

  rowp=2
  colp=3
  fig,axs=plt.subplots(rowp, colp, sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0, 'wspace': 0})

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':10}

  r=c=0 # indexes for row and column of subplots
  for k in range(lensims):
    # frun = fname + listsims[k] + "/output/" # location of the file
    # frun = fname + listsims[k] + "/" # location of the file
    frun = fname + 'sim_' + listsims[k] + "/" # location of the file

    eta = []
    for snap in rsnap:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)

      dd = readnoacc.readsnap(frun, ext, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities
      # mp = dd['m']  # mass of disc particles

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      # determine bar's longitude
      #print("\n############# Calculating bar's longitude #############\n")
      #r, A2, ph = phase_of_bar_annuli_2d(x, y, radii)

      #bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      #ph_thres = []
      #final_ph = ph_thres # final values of phase
      #lenph = 0 # length of phase
      #indph = 0 # index of current phase
      #baseph = ph[0] # reference phase
      #for i in range(len(ph)):
      #  if np.abs(ph[i]-baseph) < bt:
      #    ph_thres.append(ph[i]) 
      #    if len(ph_thres) > lenph: # phase remains semi-constant
      #      final_ph = ph_thres
      #      lenph = len(final_ph)
      #      indph = i
      #  else:
      #    ph_thres = [] # empty phase
      #    baseph = ph[i] # drag new reference phase
      #
      #avgph = np.mean(final_ph)
      #print(final_ph, avgph)
      #ir = list(ph).index(ph[indph])
      #print("\n###############################################\n")

      print("\n############# Distorsion paramerer for %s (of %s) - snapshot %s of %s #############\n" % (str(k+1), str(lensims), str(snap+1), str(totsnap)))
      #rlim = r[ir] # extension of bar
      #val = distorsion_parameter(x[::jump], y[::jump], rmax = rlim)
      val = distorsion_parameter(x[::jump], y[::jump])
      eta.append(val)
      print("\n###############################################\n")
    
    eta = np.asarray(eta)
    print("Eta (%s) = %s" % (listsims[k], eta[-1]))
    feta.append(eta[-1])

    # 300 represents number of points of time data
    tnew = np.linspace(time.min(), time.max(), 300) 

    spl = make_interp_spline(time, eta, k = 3)  # type: BSpline
    famp_smooth = spl(tnew)

    ms = 5.0
    if k > 3:
      ms = 6.0
    axs[r,c].plot(tnew, famp_smooth, markers[k % sims_per_sp],
                       linewidth = 2.2, markersize = ms, label = names[k])
    # axs.plot(tnew, famp_smooth, '-',
    #                    linewidth = 1.7, markersize = 2, label = names[k])
    axs[r,c].plot([0,12.0],[0.02,0.02],'--k', linewidth=1.9)
    # axs[r,c].plot([0,12.0],[0.03,0.03],'--',color='gray', linewidth=1.9)
    # Put legend in plot
    if k % sims_per_sp == (sims_per_sp-1) and k != 0:
      leg = axs[r,c].legend(loc = "upper left", ncol = 1, shadow = True, fancybox = True) 
      for hand in leg.legendHandles:
        hand._legmarker.set_markersize(8) 
      c+=1
    
    if c == colp:
      r+=1
      c=0

  for ax in axs.flat:  
    ax.grid()
    ax.label_outer()
    # ax.set_xlabel("Time [Gyrs]", fontsize = 16)
    # ax.set_ylabel("$\eta$", fontsize = 16)
    ax.set_xlim(0, np.max(time))
    ax.set_yscale('log')
    ax.set_ylim(0.001, 0.5)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 14)

  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('Time [Gyrs]', fontsize = 15)
  plt.ylabel("$\eta$", fontsize = 16, labelpad=15)

  print(feta)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
