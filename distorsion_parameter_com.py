#!/usr/local/bin python

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.interpolate import make_interp_spline, BSpline

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
# from readsnap_no_acc import readsnap
import readsnap_no_acc as readnoacc
from phase_of_bar import *
from distorsion_parameter import distorsion_parameter

def main():
  fnamer="/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  fnameo="/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"

  
  # rerun = ['SGS5', 'SGS8', 'SGS9', 'SGS11', 'SGS13', 
  #          'SGS14','SGS15', 'SGS17', 'SGS19', 'SGS20',
  #          'SGS21', 'SGS23', 'SGS25', 'SGS26', 'SGS27',
  #          'SGS29', 'SGS35']
  rerun = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19r', 'SGS20', 'SGS21r', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  #rerun = ['SGS13', 'SGS21r', 'SGS3', 'SGS33']
  ts01 = ['SGS5', 'SGS8', 'SGS9', 'SGS11', 'SGS13', 
          'SGS14','SGS15', 'SGS17', 'SGS19r', 'SGS20',
          'SGS21r', 'SGS23', 'SGS25', 'SGS26', 'SGS27',
          'SGS29', 'SGS35']
  ts005 = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS6',
           'SGS7', 'SGS10', 'SGS12', 'SGS16', 'SGS18',
           'SGS22', 'SGS24', 'SGS28', 'SGS30', 'SGS31', 'SGS32', 'SGS33']
  ts001 = ['SGS34','SGS36']
  orig = ['12','11','13','14','15','16',
          '22','21','23','24','25','26',
          '32','31','33','34','35','36',
          '42','41','43','44','45','46',
          '52','51','53','54','55','56',
          '62','61','63','64','65','66']
  #orig = ['32','43','13','63']
  # orig = ['15','21','23','25','32',
  #         '31','33','35','42','41',
  #         '43','45','52','51','53',
  #         '55','65']
  
  # time variable
  initsnap = 0
  totsnapr = [120, 240, 1200]
  totsnapo = 20
  skip = [6, 12, 60]
  tsr = [0.1, 0.05, 0.01]
  tso = 0.6
  rsnap1 = range(initsnap,totsnapr[0]+1,skip[0])
  rsnap05 = range(initsnap,totsnapr[1]+1,skip[1])
  rsnap01 = range(initsnap,totsnapr[2]+1,skip[2])
  rsnapo = range(initsnap,totsnapo+1)
  timer1 = np.asarray(rsnap1)
  timer05 = np.asarray(rsnap05)
  timer01 = np.asarray(rsnap01)
  timeo = np.asarray(rsnapo)
  timer1 = timer1 * tsr[0]
  timer05 = timer05 * tsr[1]
  timer01 = timer01 * tsr[2]
  timeo = timeo * tso
  
  m = 2 # fourier mode to compute

  rmax=(10,20)

  plt.style.use('seaborn-bright')
  #ls = ['solid', 'dashed', 'dotted', 'dashdot']
  ls = ['-', '--', '-.', ':', 'x', '*']

  jumpr = 40 # skip through particle data
  jumpo = 10 # skip through particle data

  nrow = 6
  ncol = 6
  fig,axs=plt.subplots(nrow, ncol, sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0.07, 'wspace': 0.1})

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':9}

  row, col = 0, 0
  for org, re in zip(orig[:], rerun[:]):
    fruno = fnameo + "sim_" + org + "/" # location of the file
    frunr = fnamer + re + "/" # location of the file
    #frun = fname + listsims[k] # location of the file

    fampo = [] # fourier amplitude (m = 2) for snapshot
    fampr = [] # fourier amplitude (m = 2) for snapshot
    etao=[]
    etar=[]
    rings = 20
    radiio = np.linspace(0, rmax[0], rings)
    radiir = np.linspace(0, rmax[1], rings)

    #col = 0

    for snap in rsnapo:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)
      
      ######## Original simulation ########
      dd = rsa.readsnap(fruno, ext, 2) # disk particles' data
      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      print("\n############# Distorsion paramerer for %s (of %s) - snapshot %s of %s #############\n" % (str(orig.index(org)), str(len(orig)), str(snap+1), str(totsnapo)))
      #rlim = r[ir] # extension of bar
      #val = distorsion_parameter(x[::jump], y[::jump], rmax = rlim)
      val = distorsion_parameter(x[::jumpo], y[::jumpo])
      etao.append(val)
      print("\n###############################################\n")
      ######## Original simulation ########

    if re in ts01: 
      rsnapr = rsnap1
      timer = timer1
    if re in ts005: 
      rsnapr = rsnap05
      timer = timer05
    if re in ts001: 
      rsnapr = rsnap01
      timer = timer01
    for snap in rsnapr:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)
      if snap > 999:
        ext = res.snap_mask(snap, 4) # file's mask (last three digits)

      ######## Rerun simulation ########
      dd = rsa.readsnap(frunr, ext, 2) # disk particles' data
      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      print("\n############# Distorsion paramerer for %s (of %s) - snapshot %s of %s #############\n" % (str(rerun.index(re)), str(len(rerun)), str(snap+1), str(totsnapr)))
      #rlim = r[ir] # extension of bar
      #val = distorsion_parameter(x[::jump], y[::jump], rmax = rlim)
      val = distorsion_parameter(x[::jumpr], y[::jumpr])
      etar.append(val)
      print("\n###############################################\n")
      ######## Rerun simulation ########
    
    # Soften force amplitude curve
    etao = np.asarray(etao)
    etar = np.asarray(etar)
    # famp_smooth = savgol_filter(famp, 11, 3)

    # 300 represents number of points of time data
    tnewr = np.linspace(timer.min(), timer.max(), 200) 
    tnewo = np.linspace(timeo.min(), timeo.max(), 200) 

    splo = make_interp_spline(timeo, etao, k = 2)  # type: BSpline
    splr = make_interp_spline(timer, etar, k = 2)  # type: BSpline
    famp_smootho = splo(tnewo)
    famp_smoothr = splr(tnewr)
    #famp_smoothr = etar
    
    axs[row, col].plot(tnewo, famp_smootho, 'k-',
                       linewidth = 2.0, markersize = 4, label = re+' original')
    axs[row, col].plot(tnewr, famp_smoothr, 'k--',
                       linewidth = 2.0, markersize = 4, label = re+' rerun')
    axs[row,col].plot([0,12.0],[0.02,0.02],'--b', linewidth=1.9)
    axs[row,col].plot([0,12.0],[0.03,0.03],'--',color='gray', linewidth=1.9)
    axs[row, col].legend(loc = "best", ncol = 1, shadow = True, fancybox = True, fontsize=7)

    col += 1
    if col % ncol == 0:
      col = 0
      row += 1
  
  for ax in axs.flat:
    ax.grid()
    ax.label_outer()
    # ax.set_xlabel("Time [Gyrs]", fontsize = 16)
    # ax.set_ylabel("$\eta$", fontsize = 16)
    ax.set_xlim(0, np.max(timeo))
    ax.set_yscale('log')
    ax.set_ylim(0.001, 0.5)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 11)

  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('Time [Gyrs]', fontsize = 12)
  plt.ylabel(r'$\eta$', fontsize = 12)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
