import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# sys.path.insert(0, "/home/ia/alexlg/snapshot_analysis/")
# sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/")

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass_unit
#from readsnap_no_acc import readsnap
#import readsnap_no_acc as reads
import readsnap as reads
from fourier_components import *

def main():
  # fname="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/eta/" # eta
  fname="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/strength/" # a_2

  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  lensims = len(listsims)
  
  mass = [0.035, 0.035, 0.035, 0.035, 0.035, 0.035,
          0.037, 0.037, 0.037, 0.037, 0.037, 0.037,
          0.041, 0.041, 0.041, 0.041, 0.041, 0.041,
          0.044, 0.044, 0.044, 0.044, 0.044, 0.044,
          0.047, 0.047, 0.047, 0.047, 0.047, 0.047,
          0.05, 0.05, 0.05, 0.05, 0.05, 0.05]
  
  soft = [0.1, 0.05, 0.025, 0.01, 0.005, 0.001,
          0.1, 0.05, 0.025, 0.01, 0.005, 0.001,
          0.1, 0.05, 0.025, 0.01, 0.005, 0.001,
          0.1, 0.05, 0.025, 0.01, 0.005, 0.001,
          0.1, 0.05, 0.025, 0.01, 0.005, 0.001,
          0.1, 0.05, 0.025, 0.01, 0.005, 0.001]
  
  
  threshold = 0.02
  
  fig, axs = plt.subplots(dpi=120)
  
  for k in range(lensims):
    # frun = fname + 'eta_' + listsims[k] + '.txt' # location of the file
    frun = fname + 'strength_' + listsims[k] + '.txt' # location of the file
    pd = data(frun)
    # time, eta = pd[0].astype(float), pd[1].astype(float)
    time, stren = pd[0].astype(float), pd[1].astype(float)
    
    # ind = np.where(eta > 0.02)[0][0]
    # axs.plot(listsims[k], time[ind], 'ko')
    axs.plot(soft[k]/mass[k], stren[-1], 'ko')
  
  axs.grid(False)
  # axs.set_xlim(0, np.max(mass))
  # axs.set_ylim(0, 0.8)
  axs.tick_params(axis = 'both', which = 'both', direction = 'in',
                  top = True, left = True, right = True, bottom = True,
                  labelsize = 5)
  
  plt.xticks(rotation=45, ha='center')
  
  plt.show()
  plt.clf()
    

if __name__ == '__main__':
  main()