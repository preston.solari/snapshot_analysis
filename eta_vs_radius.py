#!/usr/bin/env python

# This script graphs eta against radius for any simulation

import numpy as np
import matplotlib.pyplot as plt

from den_map import data

def main():
  filedir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"

  #sims=(('11','SGS2'),('22','SGS7'),('33','SGS15')) # tuple of simulations to process
  sims=(('12','SGS1'),('34','SGS16'),('65','SGS35')) # tuple of simulations to process
  snaps=('010','020',) # tuple with snapshots to plot

  l1=(10.2,11.5,8.9)
  l2=(11.0,13.1,9.9)
  lq=(10.5,12.0,9.4)

  plt.style.use('seaborn-bright')

  fig,axs=plt.subplots(len(sims), len(snaps), sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0.0, 'wspace': 0.0},
                       figsize=(2*len(snaps),2*len(sims)))

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':9}

  # Plotting eta vs. radius
  for sim,name in sims:
    simname=filedir+'sim_'+sim+'/'

    for snap in snaps:
      snapname=simname+'eta_vs_rad_'+snap+'.dat'

      r,eta=data(snapname) # get data for radius and distorsion parameter

      # Derivative of eta curve
      # print(r, eta)
      diff = np.gradient(eta, r)

      # Number of rows and columns for plotting grid
      row=sims.index((sim,name))
      col=snaps.index(snap)

      axs[row, col].plot(r, diff, 'k-')

  #     # Largest value of longitudes
  #     ml=np.max([l1[row],l2[row],lq[row]])

  #     # Adjusting plotting grid depending on the
  #     # number of simulations to plot
  #     if (len(sims) < 2) and (len(snaps) < 2):
  #       axs.plot(r,eta,'-')
  #       axs.fill_between(r,eta,where=r <= ml,facecolor='blue',alpha=0.4,label=r'$\eta$') # filling for eta
  #       axs.plot((l1[row],l1[row]),(0,1),'k--')
  #       axs.plot((l2[row],l2[row]),(0,1),'k--')
  #       axs.plot((lq[row],lq[row]),(0,1),'k--')
  #       axs.legend(loc='upper right',shadow=True)
  #       axs.text(1,0.815,name,**fontlabel)
  #     else:
  #       if len(sims) < 2:
  #         axs[col].plot(r,eta,'-')
  #         axs[col].fill_between(r,eta,where=r <= ml,facecolor='blue',alpha=0.4,label=r'$\eta$')
  #         axs[col].plot((l1[row],l1[row]),(0,1),'k--')
  #         axs[col].plot((l2[row],l2[row]),(0,1),'k--')
  #         axs[col].plot((lq[row],lq[row]),(0,1),'k--')
  #         if col == 0:
  #           axs[col].legend(loc='upper right',shadow=True)
  #         axs[col].text(1,0.815,name,**fontlabel)
  #       elif len(snaps) < 2:
  #         axs[row].plot(r,eta,'-')
  #         axs[row].fill_between(r,eta,where=r <= ml,facecolor='blue',alpha=0.4,label=r'$\eta$')
  #         axs[row].plot((l1[row],l1[row]),(0,1),'k--')
  #         axs[row].plot((l2[row],l2[row]),(0,1),'k--')
  #         axs[row].plot((lq[row],lq[row]),(0,1),'k--')
  #         if row == 0:
  #           axs[row].legend(loc='upper right',shadow=True)
  #         axs[row].text(1,0.815,name,**fontlabel)
  #       else:
  #         axs[row,col].plot(r,eta,'-')
  #         axs[row,col].fill_between(r,eta,where=r <= ml,facecolor='blue',alpha=0.4,label=r'$\eta$')
  #         axs[row,col].plot((l1[row],l1[row]),(0,1),'k--')
  #         axs[row,col].plot((l2[row],l2[row]),(0,1),'k--')
  #         axs[row,col].plot((lq[row],lq[row]),(0,1),'k--')
  #         if col == row  == 0:
  #           axs[row,col].legend(loc='upper right',shadow=True)
  #         if col == 0:
  #           axs[row,col].text(1,0.815,name,**fontlabel)

  # # Formatting axes
  # for ax in axs.flat:
  #   ax.grid()
  #   ax.label_outer()
  #   #ax.set(adjustable='box',aspect='equal')
  #   ax.set_xlim(0,16)
  #   ax.set_ylim(0,0.9)
  #   ax.tick_params(axis='both',which='both',direction='in',
  #                  top=True,left=True,right=True,bottom=True,
  #                  labelsize=10)

  # fig.add_subplot(111, frameon=False)
  # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  # plt.grid(False)
  # plt.xlabel(r'$\eta$',fontsize=12)
  # plt.ylabel('Radius [kpc]',fontsize=12)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
