from cProfile import label
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import lognorm
from scipy.optimize import curve_fit

from den_map import data
from resonances import snap_mask

# Define lognormal PDF as function of x, mu, and sigma
def lognorm_fit(x, mu, sigma):
    y = (1/(sigma*x*np.sqrt(2*np.pi))) * np.exp(-1* ((np.log(x) - mu)**2/(2*sigma**2)))
    return y

def main():
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/" # rerun
  fdir2 = "/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun


  ffit = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/fit_az/" # rerun
  if not os.path.exists(ffit): # create path fdat if does not exist
        os.makedirs(ffit)
  #fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/halos/" # rerun

  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS5','SGS8','SGS9','SGS11']
  #listsims = ['SGS1','SGS2','SGS3','SGS4', 'SGS5','SGS6']
  #listsims = ['SGS1', 'SGS2']
  #listsims = ['SGS1']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']
  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  
  firstsnap = 1
  initsnap = 10
  totsnap = 120
  skip = initsnap

  ts = 0.1
  rsnap = range(initsnap,totsnap+1,skip)
  rsnap = [firstsnap] + rsnap[:]
  time = np.asarray(rsnap)
  time = time * ts

  data_to_fit = 'accelerations'
  suffix = 'acc_halo_z_'
  extension = '.txt'

 # data_fitted = 'fit_az'
  suffix_fitted = 'fit_az_'
  extension_fitted = '.txt'

  row = 6
  col = 6
                      
  limx = 12
  limy = 1.5
  per_axx = 0.05
  per_axy = 0.1

  fig, axs = plt.subplots(row, col, sharex = True, sharey = True,
                          gridspec_kw = {'hspace': 0.15, 'wspace': 0.08}, 
                          figsize=(20,12),dpi=150)
  
  r = c = 0
  for sim in listsims:
    fname = fdir+sim+'/'+data_to_fit+'/'

    #f = open(ffit+"fit_az_"+sim+".txt", 'w+')

    fitted_data = data(ffit+suffix_fitted+sim+extension_fitted)
    time_fit, mu, sigma = fitted_data[0], fitted_data[1], fitted_data[2]


    fit_max = []
    errx = []
    erry = []
    for snap in rsnap:
      ext = snap_mask(snap, 4) # file's mask
      pd = data(fname+suffix+sim+'_'+ext+extension)

      x_axis, y_axis = pd[0], pd[1]
      width = x_axis[1]-x_axis[0] 
      y_axis_norm = y_axis/width/np.sum(y_axis)
      (muc, sigma), conv = curve_fit(lognorm_fit, x_axis, y_axis_norm)
      errx.append(np.sqrt(conv[0,0]))
      erry.append(np.sqrt(conv[1,1]))

      #axs[r,c].plot(x_axis, y_axis_norm, 'kx', markersize=5, label='Simulation')

      # ind_fit = rsnap.index(snap)      
      # fit = lognorm_fit(x_axis, mu[ind_fit], sigma[ind_fit])
      # fit_max.append(max(fit))
      #axs[r,c].plot(x_axis, fit, '--', color='blue', linewidth=1.4, label='Fit')

      # axs[r,c].text(limx*per_axx, limy*per_axy, 'T=%.1f Gyrs' % (time_fit[ind_fit]), fontsize=8)
      # axs[r,c].text(limx*per_axx, limy*per_axy*0.9, '$\mu=%.2f$' % (mu[ind_fit]), fontsize=8)
      # axs[r,c].text(limx*per_axx, limy*per_axy*0.85, '$\sigma=%.2f$' % (sigma[ind_fit]), fontsize=8)

      # if r == 0 and c == col-1:
        # axs[r,c].legend(loc='upper left', bbox_to_anchor=(1.02, 1), fontsize=8)
    # fit_max = np.array(fit_max)
    # axs[r,c].plot(time_fit, fit_max, 'k.')
    # axs[r,c].text(limx*per_axx, limy*per_axy, sim, fontsize=8)
    # axs[r,c].plot(time_fit, mu, 'k.')
    axs[r,c].errorbar(time_fit, mu, yerr=erry, xerr=errx, marker=',', linestyle='none')
    print(erry, errx)
    #axs[r,c].plot(time_fit, np.exp(mu-sigma**2), 'k.')
    # axs[r,c].plot(time_fit, np.exp(mu), 'k.')
    axs[r,c].text(limx*per_axx, limy*per_axy, sim, fontsize=8)

    c += 1
    if c % col == 0:
      r += 1
      c = 0

      #(mu, sigma), conv = curve_fit(lognorm_fit, x_axis, y_axis_norm)
      #(mu, sigma), conv = curve_fit(lognorm_fit, x_axis, y_axis)
      #f.write("%.2f %.5f %.5f\n" % (time[rsnap.index(snap)], mu, sigma))
    #f.close()

  for ax in axs.flat:
    for axis in ['top','bottom','left','right']:
      ax.spines[axis].set_linewidth(0.7)
    ax.grid(linewidth=0.4)
    ax.label_outer()
    ax.set_xlim(0, limx)
    ax.set_ylim(0.9, limy)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                  top = True, left = True, right = True, bottom = True,
                  labelsize = 9, width=0.7)
  
  for ax in axs.flat[row*col:]:
    ax.remove()
  
  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  #plt.xlabel('Radius [kpc]', fontsize = 12)
  plt.xlabel('T [Gyrs]', fontsize = 12)
  #plt.ylabel('$a_{z}$ [km$^{2}$ s$^{-2}$ kpc$^{-1}$]', fontsize = 12, labelpad=5.0)
  #plt.ylabel('$\mu$', fontsize = 12, labelpad=5.0)
  plt.ylabel('$R$ [kpc]', fontsize = 12, labelpad=5.0)
  #plt.title('Best fit for $a_{z}$', fontdict={'fontsize': 14})
  plt.title('Median of distribution', fontdict={'fontsize': 14})

    # fv = fdir2+"/fit_plot/"
    # if not os.path.exists(fv): # create path if does not exist
    #   os.makedirs(fv)
    # iname = "plot_fit_"+sim+".png"
    # plt.savefig(fv+iname)

    # plt.close()

  plt.show()
  plt.clf()


if __name__ == '__main__':
  main()