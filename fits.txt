[[Model]]
    Model(vc_nfwd)
[[Fit Statistics]]
    # fitting method   = leastsq
    # function evals   = 14
    # data points      = 100
    # variables        = 1
    chi-square         = 258.112967
    reduced chi-square = 2.60720169
    Akaike info crit   = 96.8227160
    Bayesian info crit = 99.4278862
[[Variables]]
    c:  8.04882261 +/- 0.01337590 (0.17%) (init = 1)
[[Model]]
    Model(vc_isod)
[[Fit Statistics]]
    # fitting method   = leastsq
    # function evals   = 23
    # data points      = 100
    # variables        = 1
    chi-square         = 85603.8214
    reduced chi-square = 864.685064
    Akaike info crit   = 677.231502
    Bayesian info crit = 679.836672
[[Variables]]
    c:  10.2415456 +/- 0.20154293 (1.97%) (init = 1)
[[Model]]
    Model(vc_burkertd)
[[Fit Statistics]]
    # fitting method   = leastsq
    # function evals   = 23
    # data points      = 100
    # variables        = 1
    chi-square         = 67904.3003
    reduced chi-square = 685.902023
    Akaike info crit   = 654.068446
    Bayesian info crit = 656.673616
[[Variables]]
    c:  9.14296873 +/- 0.17107694 (1.87%) (init = 1)
[[Model]]
    Model(vc_einastod)
[[Fit Statistics]]
    # fitting method   = leastsq
    # function evals   = 11
    # data points      = 100
    # variables        = 1
    chi-square         = 10383.0472
    reduced chi-square = 104.879265
    Akaike info crit   = 466.275950
    Bayesian info crit = 468.881120
[[Variables]]
    c:  16.0233108 +/- 0.20818541 (1.30%) (init = 1)

