import sys
import numpy as np
import matplotlib.pyplot as plt

from mass_assignment_secant_method import spread_particles
from readsnap import readsnap

def force_spline(ri, rmax, s, epsilon, mass=1):
  r = np.linspace(ri, rmax, s)
  
  h = 2.8*epsilon
  
  h_inv = 1.0 / h
  h3_inv = h_inv * h_inv * h_inv

  u = r * h_inv
  
  acc = np.zeros((u.shape[0], 3))
  
  
  for i in range(len(u)):
    if r[i] >= h:
      fac = mass/r[i]**3
    else:
      if u[i] < 0.5:
        fac = mass * h3_inv * (10.666666666667 + u[i] * u[i] * (32.0 * u[i] - 38.4))
      else:
        fac = mass * h3_inv * (21.333333333333 - 48.0 * u[i] +
                            38.4 * u[i] * u[i] - 10.666666666667 * u[i] * u[i] * u[i] - 0.066666666667 / (u[i] * u[i] * u[i]))

    acc_x = r[i] * fac
    acc_y = r[i] * fac
    acc_z = r[i] * fac
          
    acc[i] = [acc_x, acc_y, acc_z]
  return u, acc


def inverse_force_spline(ei, emax, s, rad, mass=1):
  eps = np.linspace(ei, emax, s)
  
  h = 2.8*eps
  
  h_inv = 1.0 / h
  h3_inv = h_inv * h_inv * h_inv

  u = rad * h_inv
  
  acc = np.zeros((u.shape[0], 3))
  
  
  for i in range(len(u)):
    if rad >= h[i]:
      fac = mass/rad**3
    else:
      if u[i] < 0.5:
        fac = mass * h3_inv[i] * (10.666666666667 + u[i] * u[i] * (32.0 * u[i] - 38.4))
      else:
        fac = mass * h3_inv[i] * (21.333333333333 - 48.0 * u[i] +
                            38.4 * u[i] * u[i] - 10.666666666667 * u[i] * u[i] * u[i] - 0.066666666667 / (u[i] * u[i] * u[i]))

    acc_x = rad * fac
    acc_y = rad * fac
    acc_z = rad * fac
          
    acc[i] = [acc_x, acc_y, acc_z]
  return u, acc

def calculate_gadget_timestep(a, epsilon, error):
  return np.sqrt(2*error*epsilon/a)

def calculate_epsilon(a, ts, error):
  return ts**2*a/2/error

def main():
  N = 2000
  G = 43007.1
  
  v200 = 160
  mass = v200**3 / G
  
  ri = 0
  rmax = 2
  s = 100000
  epsilon = [0.1, 0.05, 0.025, 0.01, 0.005, 0.001]
  # epsilon = [0.01, 0.005, 0.001]
  # epsilon = [0.01, 0.005]
  epsilon = [0.005]
  
  fdir = "/home/cronex/Descargas/"
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  
  listsims = ["SGS4o", "SGS5o"]
  
  dcolors = ["k", "r"]
  
  init = 20
  fin = 239
  step = 80
  snaps = range(init, fin+1, step)
  
  fig, axs = plt.subplots()
  
  # for sim in listsims:
  #   fname = fdir + sim + "/"
  #   for snap in snaps:
  #     data = readsnap(fname, snap, 2)
      
  #     acc = data["a"]
  #     ts = data["t"]
  #     mass = data["m"][0]
  #     print(min(ts))
  #     macc = np.sqrt(acc[:,0]**2 + acc[:,1]**2 + acc[:,2]**2)
      
  #     times = np.unique(ts)
      
  #     if snap == snaps[0]:
  #       axs.plot(macc, ts, dcolors[listsims.index(sim)]+'.', ms=1.3, 
  #                label=r"$\Delta t$ sim ($\epsilon=$"+str(epsilon[listsims.index(sim)])+")")
  #     else:
  #       axs.plot(macc, ts, dcolors[listsims.index(sim)]+'.', ms=1.3, label="_nolegend_")
  
  # data = readsnap(fdir+"SGS6o/", 20, 2)
  # acc = data["a"]
  # ts = data["t"]
  # print(min(ts))
  # macc = np.sqrt(acc[:,0]**2 + acc[:,1]**2 + acc[:,2]**2)  
  
  # axs.plot(macc, ts, 'b.', ms=1.3,
  #          label=r"$\Delta t$ sim ($\epsilon=$"+str(epsilon[listsims.index(sim)])+")") 
  
  # axt = axs.twinx()
  # for t in times:
  #   eps = calculate_epsilon(macc, t, error=0.025)
  #   axt.plot(macc, eps)
  
  # plt.show()
  # sys.exit()
  
  
  timesteps = []
  for e in epsilon:
    data = readsnap(fdir+"SGS4o", 2, 2)
    mass = data['m'][0]
    print(mass)
    
    u, acc = force_spline(ri, rmax, s, e, mass=mass)
    macc = np.sqrt(acc[:,0]**2 + acc[:,1]**2 + acc[:,2]**2)*G
    
    # print(acc[:10,0])
    
    # fig, axs = plt.subplots(1,2)
    
    # axs.plot(u, -macc, 'k-')
    # axs.set_xlim(0, 1.5)
    
    u, acc = inverse_force_spline(0.001, 0.1, s, 0.1, mass=mass)
    
    axs.plot(u, -macc, 'k-')
    
    
    # data = readsnap(fdir+"SGS4o", 2, 2)
    
    # xyz = data["p"]
    # acc = data["a"]
    # macc = np.sqrt(acc[:,0]**2 + acc[:,1]**2 + acc[:,2]**2)*G
    
    # ord_macc = sorted()
    # ts = calculate_gadget_timestep(macc, e, error=0.025)
    # # timesteps.append(ts)
    # # axs.plot(macc, ts, dcolors[epsilon.index(e)]+'-', label=r"$\Delta t$ theo ($\epsilon={}$)".format(e))
    # axs.plot(macc, ts, '-', label=r"$\Delta t$ theo ($\epsilon={}$)".format(e))
    # axs.set_ylim(0, 0.0005)
    # axs.set_xlim(0, 400000)
  
    
  
    # axs[1].plot(macc, ts, 'k-')
  
  # axs.set_yscale("log")
  # print(min(timesteps))
  # axs.plot(epsilon, timesteps, 'ko-')
  
  # axs.set_xlabel("Accelerations")
  # axs.set_ylabel("Timesteps")
  
  # leg = axs.legend(loc="best")
  # leg.legendHandles[0]._legmarker.set_markersize(8)
  # leg.legendHandles[1]._legmarker.set_markersize(8)
  # leg.legendHandles[2]._legmarker.set_markersize(8)
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
