#!/usr/local/bin python3

from math import atan2
import numpy as np
import matplotlib.pyplot as plt
import random as rand
from scipy.special import kv, iv

G = 43007.1
mdisk = 5

def exp_disk(diskh, rmax):
  flago = True
  flagi = True
  
  while flago:
    q = rand.random()
    r = 1.0
    while flagi:
      f = (1 + r) * np.exp(-r) + q - 1
      f1 = -r * np.exp(-r)
      
      rold = r
      r = r - f / f1
      if np.abs(r-rold)/r <= 1e-7:
        flagi = False

    r *= diskh
    
    phi = rand.random() * np.pi * 2
    x = r * np.cos(phi)
    y = r * np.sin(phi)

    r2 = x**2 + y**2
    if r2 <= rmax**2:
      flago = False
  return x, y  

def scatter_particles(diskh, rmax, n = 1000):
  part = np.zeros((2, n))
  for i in range(n):
    x, y = exp_disk(diskh, rmax)
    part[0, i] = x
    part[1, i] = y
  return part

def particle_velocities_disk(part, diskh):
  x, y = part[0], part[1]
  r = np.sqrt(x**2 + y**2)
  phi = np.arctan2(y, x)
  
  yk = 0.5*r/diskh
  vc = np.sqrt(2*mdisk*G*yk**2/diskh*(iv(0,yk)*kv(0,yk)-iv(1,yk)*kv(1,yk)))
  pot = -mdisk*G*yk/diskh*(iv(0,yk)*kv(1,yk)-iv(1,yk)*kv(0,yk))
  
  return vc, pot

def vel_cartesian_components(x, y, vr, va):
  r = np.sqrt(x**2 + y**2) # radii

  # vx, vy
  vx = (x*vr - y*va)/r  
  vy = (y*vr + x*va)/r
  
  return vx, vy

# surface_density_ring: calculates the surface density of an
# exponential disk particle with coordinates (x,y), around its radius 
# rp and a ring of radii rp+delta and rp-delta. The maximum radius
# of the calculation is given by rmax and the minimum is 0
def surface_density_ring(x, y, r, mp, rmax = 100, delta = 0.1):
  rp = np.sqrt(x**2 + y**2)
  
  rplower = rp-delta
  rpupper = rp+delta
  if rplower < 0:
    rplower = 0
  if rpupper > rmax:
    rpupper = rmax
    
  newr = np.array([nr for nr in r if rplower < nr < rpupper])
  sd = len(newr)*mp/np.pi*(rpupper**2 - rplower**2)
  return sd

def avg_tangential_vel(rad_arr,tan_arr,radii):
  vel_avgt=[] # arrays with all the average velocities of every ring 
  newr = []
  for i in range(len(radii)-1):
    vel_rt=[]
    for j in range(len(rad_arr)):
      if radii[i] <= rad_arr[j] <= radii[i+1]:
        vel_rt.append(tan_arr[j])

    if len(vel_rt):
      vel_rt=np.asarray(vel_rt)
      vel_avgt.append(np.mean(vel_rt)) # average tangential velocity in ring
    else:
      vel_avgt.append(0)
    newr.append((radii[i+1]+radii[i])/2)

  return np.array(newr), np.array(vel_avgt)

def main():
  diskh = 3.0
  rmax = 30
  n = 1000
  #Q = 2
  mp = float(mdisk)/n
  
  part = scatter_particles(diskh, rmax, n)
  vc, pot = particle_velocities_disk(part, diskh)
  x, y = part[0], part[1]
  r = np.sqrt(x**2 + y**2)
  omega = vc/r
  dpot = np.gradient(pot)
  #dpot2 = np.diff(pot)
  d2pot = np.diff(pot, 2)
  #kappa2 = np.abs(3*dpot[1:-1]/r[1:-1] + d2pot)
  kappa2 = 3*dpot[1:-1]/r[1:-1] + d2pot
  kappa2 = np.insert(kappa2, -1, [np.mean(kappa2), kappa2[0]])
  print(len(kappa2), np.sqrt(kappa2))
  
  surf_den = np.array([surface_density_ring(xp, yp, r, mp, rmax=60)
                       for xp, yp in zip(x, y)])
  dispr_crit = np.mean(3.36*G*surf_den/np.sqrt(kappa2))
  dispr2 = dispr_crit*np.exp(r/diskh)
  dispphi2 = dispr2*kappa2/4/omega**2
  #print(np.sqrt(dispphi2))
  
  vphi_ran = np.array([np.random.normal(0, np.sqrt(dp), 1) for dp in dispphi2])
  #print(vphi_ran)
  # vr_ran = np.array([np.random.normal(0, np.sqrt(dp), 1) for dp in dispr2])
  
  # stream_vel = np.sqrt(dispr2*(1 - kappa2/4/omega**2 - 2*r/diskh) - vc**2)
  
  # vx, vy = vel_cartesian_components(x, y, vr_ran, vphi_ran+stream_vel)
  
  radii = np.linspace(0, rmax, 50)
  # newr, avg_tan_vel = avg_tangential_vel(r, vphi_ran, radii)
  newr, avg_tan_vel = avg_tangential_vel(r, np.sqrt(kappa2), radii)
  
  fig, axs = plt.subplots()
  # # axs.hist(vc, bins = 20)
  # #axs.plot(part[0], part[1], 'k.')
  # axs.plot(newr, avg_tan_vel, 'k-')
  axs.plot(newr, avg_tan_vel, 'k-')
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()