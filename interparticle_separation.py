#!/usr/bin/env python

# This script graphs interparticle separation against radius for any simulation

import numpy as np
import matplotlib.pyplot as plt
import random

from den_map import data
import resonances as res
from readsnap_no_acc import readsnap
from plot_den_maps import adjust_center_mass

# Function to calculate interparticle separation of a set of rings
# associated with a disc
def lambda_separation(fname,radii,h=0):
  lam=[]
  rad=[]

  pos=data(fname) # positions of all particles
  radius=res.particle_radius(fname) # radii of all particles

  x,y,z=pos[0],pos[1],pos[2]

  # for all the disc
  if(h == 0):
    for i in range(len(radii)-1):
      xn=[]
      yn=[]
      zn=[]

      for j in range(len(radius)):
        if(radii[i] <= radius[j] <= radii[i+1]):
          xn.append(x[j])
          yn.append(y[j])
          zn.append(z[j])

      # calculate interparticle separation for every particle in ring 'i'
      if(len(xn) >= 2):
        l=[]
        for j in range(len(xn)):
          for k in range(j,len(xn)):
            if(k != j):
              # lambda for particle 'j' vs. N-1 particles
              li=np.sqrt((xn[j]-xn[k])**2 + (yn[j]-yn[k])**2)
              l.append(li)

        # add mean separation for ring 'i'
        l=np.asarray(l)
        lam.append(np.mean(l))
      else:
        lam.append(0)

      # also add 'mean' radius
      rad.append((radii[i] + radii[i+1])/2.)

    lam,rad=np.asarray((lam,rad))

  # for a fraction (-h,h) of the disc
  elif(h > 0):
    for i in range(len(radii)-1):
      xn=[]
      yn=[]
      zn=[]

      for j in range(len(radius)):
        if((radii[i] <= radius[j] <= radii[i+1]) and (-h <= z[j] <= h)):
          xn.append(x[j])
          yn.append(y[j])
          zn.append(z[j])

      # calculate interparticle separation for every particle in ring 'i'
      if(len(xn) >= 2):
        l=[]
        for j in range(len(xn)):
          for k in range(j,len(xn)):
            if(k != j):
              # lambda for particle 'j' vs. N-1 particles
              li=np.sqrt((xn[j]-xn[k])**2 + (yn[j]-yn[k])**2)
              l.append(li)

        # add mean separation for ring 'i'
        l=np.asarray(l)
        lam.append(np.mean(l))
      else:
        lam.append(0)

      # also add 'mean' radius
      rad.append((radii[i] + radii[i+1])/2.)

    lam,rad=np.asarray((lam,rad))

  else:
    print("Argument h in lambda_separation cannot be negative.\n")

  return lam,rad

# Function to calculate interparticle separation for linearly
# spaced spherical shells
def lambda_separation_3d(fname,radii):
  lam=[]
  rad=[]

  pos=data(fname) # positions of all particles
  radius=res.particle_radius_sphere(fname) # radii of all particles

  N=len(radius)-1 # number of particles

  npr = 0 # number of particles for the last radius
  for r in radius:
    if(radii[-2] <= r <= radii[-1]):
      npr = npr + 1
  # remove exceeding number of particles
  if(npr*(len(radii)-1) > N):
    s = (npr*(len(radii)-1) - N)/(len(radii)-1)
    npr = npr - s

  x,y,z=pos[0],pos[1],pos[2]

  for i in range(len(radii)-1):
    xn=[]
    yn=[]
    zn=[]

    nps = 0
    for j in range(len(radius)):
      if((radii[i] <= radius[j] <= radii[i+1]) and (nps < npr)):
        xn.append(x[j])
        yn.append(y[j])
        zn.append(z[j])
        nps = nps + 1

    # calculate interparticle separation for every particle in ring 'i'
    if(len(xn) >= 2):
      l=[]
      for j in range(len(xn)):
        for k in range(j,len(xn)):
          if(k != j):
            # lambda for particle 'j' vs. N-1 particles
            li=np.sqrt((xn[j]-xn[k])**2 + (yn[j]-yn[k])**2 + (zn[j]-zn[k])**2)
            l.append(li)

      # add mean separation for ring 'i'
      l=np.asarray(l)
      lam.append(np.mean(l))
    else:
      lam.append(0)

    # also add 'mean' radius
    rad.append((radii[i] + radii[i+1])/2.)

  lam,rad=np.asarray((lam,rad))

  return lam,rad


# Function to calculate interparticle separation for logarithmically
# spaced spherical shells
def lambda_separation_shell(fname,radii):
  lam=[]
  rad=[]

  pos=data(fname) # positions of all particles
  radius=res.particle_radius_sphere(fname) # radii of all particles

  x,y,z=pos[0],pos[1],pos[2]

  for i in range(len(radii)-1):
    xn=[]
    yn=[]
    zn=[]

    for j in range(len(radius)):
      if(radii[i] <= radius[j] <= radii[i+1]):
        xn.append(x[j])
        yn.append(y[j])
        zn.append(z[j])

    # calculate interparticle separation for every particle in ring 'i'
    if(len(xn) >= 2):
      l=[]
      for j in range(len(xn)):
        for k in range(j,len(xn)):
          if(k != j):
            # lambda for particle 'j' vs. N-1 particles
            li=np.sqrt((xn[j]-xn[k])**2 + (yn[j]-yn[k])**2 + (zn[j]-zn[k])**2)
            l.append(li)

      # add mean separation for ring 'i'
      l=np.asarray(l)
      lam.append(np.mean(l))
    else:
      lam.append(0)

    # also add 'mean' radius
    rad.append((radii[i] + radii[i+1])/2.)

  lam,rad=np.asarray((lam,rad))

  return lam,rad

# calculate mean interparticle separation according to Gabbasov et al. (2006)
def lambda_separation_gab(x, y, z, radii):
  dvol = [] # volume of shells

  for i in range(len(radii)-1):
    vri = (4.0/3.0) * np.pi * np.power(radii[i], 3) # inner sphere
    vro = (4.0/3.0) * np.pi * np.power(radii[i+1], 3) # outer sphere
    dvol.append(vro-vri)

  #prad = res.particle_radius_sphere(fname)
  prad = np.sqrt(x**2 + y**2 + z**2)

  dn = [] # number of particles in each shell
  mrad = [] # mean radius
  for i in range(len(radii)-1):
    npart = 0
    for j in range(len(prad)):
      if radii[i] <= prad[j] <= radii[i+1]:
        npart = npart + 1
    dn.append(npart)
    mrad.append((radii[i] + radii[i+1])/2.0)

  dvol = np.asarray(dvol)
  dn = np.asarray(dn)

  print(dvol, dn, radii)

  lamb = np.power(dvol/dn, 1.0/3.0)

  return lamb, mrad

# Function to shuffle lines in a file
def shuffle_file(fname):
  lines=open(fname).readlines()
  random.shuffle(lines)
  open(fname,'w').writelines(lines)

def main():
  #fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/sim_11/particles_python_disc_000.dat"
  #fname="/Users/alexlopez/GD-gaia/Doctorado/Trabajo_con_Ruslan/particles_python_combined_001.dat"
  #fname="/Users/alexlopez/GD-gaia/Doctorado/Trabajo_con_Ruslan/particles_python_halo_001.dat"
  fname="/run/media/cronex/HV620S/sm-04-10-2018/sim_66/"
  snap = '000'

  max_r=18
  rings=18
  h=0

  rad=res.radii_alt(max_r,rings)

  dd = readsnap(fname, snap, 1)
  pd = dd['p']  # disc particle positions
  vd = dd['v']  # disc particle velocities

  # remove center of mass (disc)
  x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
  #l,r=lambda_separation(fname,rad,h)
  l,r=lambda_separation_gab(x, y, z,rad)
  #print(r,l)

  #radl=res.radii_log(max_r,rings,0.001)
  #ll,rl=lambda_separation_3d(fname,rad)
  #ll,rl=np.log10(ll),np.log10(rl)
  #print(rl,ll)

  ############### Plotting lambda ##################
  plt.style.use('seaborn-bright')

  fig,axs=plt.subplots()

  #axs[0].plot(r,l,'k.-')
  axs.plot(r,l,'b*-')

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
