#!/usr/local/bin python2

import os
from xml.parsers.expat import model
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from scipy import interpolate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask

def main():
  #fdir="/media/cronex/HV620S/sm-04-10-2018/sim_12/"
  #fdir="/media/cronex/HV620S/primer-art-rerun/SGS5/"
  #fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  model = "SGS36"

  fdir2 = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/orbit_plot/" # rerun
  # fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/halos/" # rerun
  # model = "SGS1"

  initsnap = 1150
  totsnap = 1200
  skip = 1
  rsnap = range(initsnap,totsnap+1,skip)

  '''  accelerations histogram '''
  # dd = readsnap(fdir+model+'/', initsnap, 2) # disk particles' data

  # a = dd['a']  # disc particle accelerations
  # p = dd['p']  # disc particle positions

  # ax, ay, az = a[:, 0], a[:, 1], a[:, 2]
  # norma1 = np.sqrt(ax**2 + ay**2 + az**2)

  # dd = readsnap(fdir+model+'/', totsnap, 2) # disk particles' data

  # a = dd['a']  # disc particle accelerations
  # pd = dd['p']  # disc particle positions
  # ids = dd['id']

  # ax, ay, az = a[:, 0], a[:, 1], a[:, 2]
  # norma30 = np.sqrt(ax**2 + ay**2 + az**2)

  # r1 = 300000
  # r2 = 650000
  # index20 = np.where(norma30 > r2)[0]
  # index30 = np.where((norma30 > r1) & (norma30 <= r2))[0]
  # #print(len(index20))
  # x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

  # x2 = x[index20]
  # y2 = y[index20]
  # z2 = z[index20]

  # x3 = x[index30]
  # y3 = y[index30]
  # z3 = z[index30]
  # idsr = ids[index20]
  '''  accelerations histogram '''

  ####### indices '''
  # nids = get_random_ids(idsr, 5)
  # nids = idsr
  # dict_ids = {id: [] for id in nids}
  # print(nids)
  # print(len(nids))
  # print(idsr)
  
  # nids = np.array([2817882, 1286894, 2047104, 250436, 2039759, 2021200, 487409]) # SGS36 ID's
  # nids = np.array([2047104, 250436, 2039759, 487409])
  # nids = np.array([2817882])
  nids = [231250, 2020477,  740333, 1539751, 2024516, 2326053, 3088526, 3086963,  484178,
          1507725,  734473, 1545556, 2548236, 3075244, 2854157,  990377, 2291543, 2823178,
          2339015, 2026724, 3074798, 3103364,  226647, 2047104, 2315159,  227902, 1017998,
          2597140, 2556624, 2844266, 1291199, 2039759,  216722,  763646, 1245721, 3085024,
          517640,  250436,  486324, 2817882, 2056047,  519189, 1286894, 1000661, 1526557,
          1031508, 2021200, 3070561, 1533896,  486268, 2337711, 2859635,  724395,  214157,
          746393, 1802788, 2853043, 2816323,  212250, 1540032, 3084815,  487409, 2058387,
          221989, 2546181, 2060631, 1787851, 3115622, 2812942, 1517248,  227339,  204448,
          1008705, 2022089,  212784, 2286944, 2803902, 2564948, 1249609, 3062189, 2333772,
          748257, 2062242, 2812993, 2307587, 1279443,  753062, 2058605,  980952, 1031472,
          1516443, 3095372]
  # nids = [1516443, 3095372]
  nids = [2022089]

  # # nids = [2206340, 2831625, 2851304, 2984955, 1792549, 1679456, 2449301] # SGS31r ID's 
  # nids = [2835343, 1922039, 1157225, 2206340, 1541603, 2449810, 2831625, 2851304, 2984955,
  #         2051684,  765978,  774488, 2206133, 2590445, 1792549, 1679456, 2449301, 1151678,
  #         2702893, 2209652,  376855, 3107692, 2186929,  252667, 1799542, 1681632, 1664085,
  #         1012735, 2833830, 1543921, 1553585,  883919,  765306, 1534328,  244139,  379571,
  #         2065647, 2332261, 2832452, 1926706]


  # nids = [1512385, 1254936, 1009562, 1792485, 2304649, 731436, 2552323, 496660] # SGS34 ID's
  # nids = [1512385, 1254936, 1009562, 1792485, 2304649,  731436, 2552323, 496660, 2336743,
  #         1765169, 1768714, 211236, 2571508, 1780488, 1272437, 771081, 2324277,  777038,
  #         489369,  774499, 1282182, 3062577,  778559, 1291120,  774285, 1539524, 1555125,
  #         2043000] # SGS34 ID's
  # nids = [1765169, 774499]

  dict_ids = {id: [] for id in nids}
  ####### indices


  # #fig, axs = plt.subplots()
  # mark=['ko','r*','bs']
  
  # ''' Orbits '''
  #maxx = minx = maxy = miny = 0
  for snap in rsnap:
    # ext = snap_mask(snap, 3) # file's mask (last three digits)

    dd = readsnap(fdir+model+'/', snap, 2)

    pd = dd['p']
    #vd = dd['v']
    id = dd['id']
    id = list(id)
    #md = dd['m']
    x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

    for i in nids:
      print("\n############# Orbit for particle %s (snapshot %s of %s) #############" % (str(i),str(snap), str(totsnap)))
      dict_ids[i].append([x[id.index(i)], y[id.index(i)], z[id.index(i)]])  

  for key in dict_ids:
    # fig, axs = plt.subplots()
    fig = plt.figure(dpi=180)
    # axs = fig.add_subplot(1, 1, 1)
    axs = fig.add_subplot(1, 1, 1, projection="3d")

    maxx = minx = maxy = miny = maxz = minz = 0

    dict_ids[key] = np.array(dict_ids[key])
    px = dict_ids[key][:,0]
    pz = dict_ids[key][:,2]
    py = dict_ids[key][:,1]
    px = np.concatenate((px, [px[0]]))
    py = np.concatenate((py, [py[0]]))
    pz = np.concatenate((pz, [pz[0]]))
    
    ''' Soften orbits '''
    nump = 1000
    f, u = interpolate.splprep([px, py, pz], s=0, per=True)
    # f, u = interpolate.splprep([px, py], s=0, per=True)
    xnew, smoothy, smoothz = interpolate.splev(np.linspace(0, 1, nump), f)
    # xnew, smoothy = interpolate.splev(np.linspace(0, 1, nump), f)
    lastp = nump - 2*nump/(totsnap-initsnap)
    axs.plot(xnew[:lastp], smoothy[:lastp],smoothz[:lastp], '-', linewidth=0.7, alpha=0.8, label=key)
    # axs.plot(xnew[:lastp], smoothy[:lastp], '-', color = 'gray', linewidth=0.7, alpha=0.8, label=key)
    #axs.annotate(str(key), (xnew[0], smoothy[0], smoothz[0]))
    axs.text(xnew[0], smoothy[0], smoothz[0], '%s' % (str(key)), size=7, zorder=1, color='k')
    # axs.text(xnew[0], smoothy[0], '%s' % (str(key)), size=7, zorder=1, color='k')
    if max(xnew) > maxx: maxx = max(xnew)
    if max(smoothy) > maxy: maxy = max(smoothy)
    if min(xnew) < minx: minx = min(xnew)
    if min(smoothy) < miny: miny = min(smoothy)
    if max(smoothz) > maxz: maxz = max(smoothz)
    if min(smoothz) < minz: minz = min(smoothz)
    axs.set_xlabel("$x$", fontsize=9)
    axs.set_ylabel("$y$", fontsize=9, labelpad=-2)
    axs.set_zlabel("$z$")
    
    lim = 0.5
    # axs.set_xlim(-0.6, 0.4)
    # axs.set_ylim(-0.55, 0.4)
    axs.set(adjustable='box',aspect='equal')
    axs.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=8)
    
    # plt.show()
    # plt.clf()

    def animate(frame):
      axs.view_init(30, frame)
      #plt.pause(.001)
      return fig
    
    anim = animation.FuncAnimation(fig, animate, frames=400, interval=40)
    fv = fdir2+model+'/'+str(initsnap)+"-"+str(totsnap)+'/'
    if not os.path.exists(fv): # create path if does not exist
      os.makedirs(fv)
    iname = fv+"plot3d_orbit_"+model+"_"+str(key)+".mp4"
    anim.save(iname)
  
    # fv = fdir2+model+'/'+str(initsnap)+"-"+str(totsnap)+'/'
    # if not os.path.exists(fv): # create path if does not exist
    #   os.makedirs(fv)
    # iname = "plot_orbit_"+model+"_"+str(key)+".png"
    # plt.savefig(fv+iname)
    plt.close()


  ''' Soften orbits '''

  #   ''' Regular orbits '''
  #   # axs.plot(px, py, 'o-', linewidth=0.5, alpha=0.7, label=key)
  #   # axs.annotate(str(key), (px[0], py[0]))

  #   # if max(px) > maxx: maxx = max(px)
  #   # if max(py) > maxy: maxy = max(py)
  #   # if min(px) < minx: minx = min(px)
  #   # if min(py) < miny: miny = min(py)
  #   ''' Regular orbits '''

    # per_ax = 0.12
    # limx = (minx+minx*per_ax, maxx+maxx*per_ax)
    # limy = (miny+miny*per_ax, maxy+maxy*per_ax)
    # axs.set_xlim(limx[0], limx[1])
    # axs.set_ylim(limy[0], limy[1])
    # axs.legend(loc='best')

    # fv = fdir2+model+'/'+str(initsnap)+"-"+str(totsnap)+'/'
    # if not os.path.exists(fv): # create path if does not exist
    #   os.makedirs(fv)
    # iname = "plot_orbit_"+model+"_"+str(key)+".png"
    # plt.savefig(fv+iname)

    # plt.close()

  
  ''' Plotting accelerations '''
  # #rmax = 20
  # dr = 200
  # #edges = np.arange(0., rmax + 1.1 * dr, dr)
  # # edgesx = np.arange(min(ax), max(ax) + 1.1 * dr, dr)
  # # edgesy = np.arange(min(ay), max(ay) + 1.1 * dr, dr)
  # # edgesz = np.arange(min(az), max(az) + 1.1 * dr, dr)
  # #edgesa = np.arange(min(norma), max(norma), (max(norma)-min(norma))/dr)
  # #edgesan = np.arange(min(norman), max(norman), (max(norman)-min(norman))/dr)
  # #edgesa1 = np.arange(min(norma1), max(norma1), (max(norma1)-min(norma1))/dr)
  # edgesa30 = np.arange(min(norma30), max(norma30), (max(norma30)-min(norma30))/dr)
  # edgesa1 = edgesa30
  # #result, bins = np.histogram(ax, bins=edgesx, normed=False)
  # #result, bins = np.histogram(ay, bins=edgesy, normed=False)
  # #result, bins = np.histogram(az, bins=edgesz, normed=False)
  # result1, bins1 = np.histogram(norma1, bins=edgesa1, normed=False)
  # result30, bins30 = np.histogram(norma30, bins=edgesa30, normed=False)
  # result = np.abs(result30-result1)
  # #edgesa = np.arange(min(result), max(result), (max(result)-min(result))/(dr-1))
  # #result, bins = np.histogram(norma, bins=edgesa, normed=False)
  # #resultn, binsn = np.histogram(norman, bins=edgesan, normed=False)
  # #print(result, edgesx)

  # fig, axs = plt.subplots(1,2)
  # #fig, axs = plt.subplots()
  # #axs.plot(ax, az, 'k.')
  # #axs.plot(edgesx[:-1], result, 'k-')
  # #axs.plot(edgesy[:-1], result, 'k-')
  # axs[0].plot(edgesa30[:-1], result1, 'k-', label='$a_{t=0.1\,Gy}$')
  # axs[0].plot(edgesa30[:-1], result30, '--', color='dimgray', label='$a_{t=10\,Gy}$')
  # axs[0].plot(edgesa30[:-1], result, '-.', color='darkgray', label='$\delta a_{t=10\,Gy}$')
  # #axs[1].plot(edgesan[:-1], resultn, '-.', color='black', label='$\delta a_{t=10\,Gy}$')
  # axs[1].plot(x, y, 'k,', alpha=0.2, label="Total")
  # axs[1].plot(x3, y3, 'r,', alpha=0.3, label="$%d<a<%d$" % (r1, r2))
  # axs[1].plot(x2, y2, 'b,', alpha=0.3, label="$a>%d$"%(r2))

  # axs[0].legend(loc='upper right')
  # axs[1].legend(loc='upper right', markerscale=0.9)

  # ##generating custom legend
  # handles, labels = axs[1].get_legend_handles_labels()
  # patches = []
  # for handle, label in zip(handles, labels):
  #     patches.append(mpatches.Patch(color=handle.get_color(), label=label))

  # legend = axs[1].legend(handles=patches)

  # #leg1.legendHandles[0]._legmarker.set_markersize(15)
  # #leg2.legendHandles[0]._legmarker.set_markersize(10000)

  # axs[0].set_xlabel('$a$')
  # axs[0].set_ylabel('$N$')
  # axs[1].set_xlabel('$x$')
  # axs[1].set_ylabel('$y$')

  # lim=15
  # axs[1].set_xlim(-lim,lim)
  # axs[1].set_ylim(-lim,lim)

  # axs[0].set_title('Accelerations - %s' % (model))
  # axs[1].set_title('Particle distribution - %s' % (model))

  # plt.show()
  # plt.clf()
  

if __name__ == '__main__':
  main()
