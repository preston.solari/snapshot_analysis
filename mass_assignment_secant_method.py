#!/usr/bin/env python
from random import seed
from random import random
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma, gammainc, gammaincc, exp1

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
#from readsnap import readsnap
from readsnap_no_acc import readsnap

def main():
  # c = 10.0
  r200 = 160.0
  N = 200000
  error = 0.00005
  # htype = 0
  nrings = 10
  lq = 0.95
  qi = 0.999
  gam = 0.17
  
  print("\nSpreading particles...\n")
  ch = 10
  cn = 4.11
  ci = 1.91
  cb = 2.06
  ce = 34.7
  a = r200/ch * np.sqrt(2 * (np.log(1 + ch) - ch / (1 + ch)))
  xh, yh, zh = spread_particles(0, N, ch, r200, a, lq, qi, error, nrings)
  xn, yn, zn = spread_particles(1, N, cn, r200, r200/cn, lq, qi, error, nrings, gam)
  xi, yi, zi = spread_particles(2, N, ci, r200, r200/ci, lq, qi, error, nrings, gam)
  xb, yb, zb = spread_particles(3, N, cb, r200, r200/cb, lq, qi, error, nrings, gam)
  xe, ye, ze = spread_particles(4, N, ce, r200, r200/ce, lq, qi, error, nrings, gam)

  print("Getting density...\n")
  lx = 160
  newrh, densityh = volumetric_density(xh, yh, zh, rmax=lx)
  newrn, densityn = volumetric_density(xn, yn, zn, rmax=lx)
  newri, densityi = volumetric_density(xi, yi, zi, rmax=lx)
  newrb, densityb = volumetric_density(xb, yb, zb, rmax=lx)
  newre, densitye = volumetric_density(xe, ye, ze, rmax=lx)

  fname = "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/python/"
  listsims=["hernquist", "nfw", "iso", "bur", "ein"] 
  
  init_radii=np.linspace(0,60,121)

  plt.style.use('seaborn-bright') # seaborn plottin style
  fig, axs = plt.subplots(1, 2)

  ls = ['--', '-', '-.', '+-', '.-']
  
  for i in range(len(listsims)):
    print("\n=================== Rotation curve from file %s of %s ===================\n" % (str(i+1), str(len(listsims))))

    # Disk data
    frun = fname + "rotcurve_" + listsims[i] + ".txt" # location of the file

    dat = data(frun)
    r, vct, vch, vcd = dat[0], dat[1], dat[2], dat[3]

    for k in range(len(r)):
      if r[k] >= 100:
        ind = k
        break
    
    axs[0].plot(r[:ind:12], vct[:ind:12], ls[i])

  #prof = ["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]

  # axs[0].plot(x, y, 'k.', ms = 0.5, alpha = 0.5)
  # axs[0].axis('square')
  # axs[0].set_xlim(-lx, lx)
  # axs[0].set_ylim(-lx, lx)

  axs[1].plot(newrh, densityh, '--', label="Hernquist")
  axs[1].plot(newrn, densityn, '-', label="NFW")
  axs[1].plot(newri, densityi, '-.', label="PISO")
  axs[1].plot(newrb, densityb, '+-', label="Burkert")
  axs[1].plot(newre, densitye, '.-', label="Einasto")
  # axs.set_xlim(0, lx)
  axs[1].set_xscale('log')
  axs[1].set_yscale('log')
  axs[1].set_xlabel("Radius [kpc]", fontsize = 16)
  axs[1].set_ylabel(r'$\rho$ [M$_{\odot}$ kpc$^{-3}$]', fontsize = 16)
  axs[1].legend(loc="best")
  axs[1].tick_params(axis='both',which='both',direction='in',
                  top=True,left=True,right=True,bottom=True,
                  labelsize=14)
  #axs[1].set_ylim(0, 100000)

  axs[0].set_xlabel("Radius [kpc]", fontsize = 16)
  axs[0].set_ylabel(r'$v_{\mathrm{c}}$ [km s$^{-1}$]', fontsize = 16)
  # axs[0].legend(loc="best")
  axs[0].tick_params(axis='both',which='both',direction='in',
                  top=True,left=True,right=True,bottom=True,
                  labelsize=14)

  # axs[1].plot(newr, density, 'k-')
  # axs[1].axis('square')
  # axs[1].set_xlim(np.min(newr), np.max(newr))
  # axs[1].set_ylim(np.min(density), np.max(density))

  plt.show()
  plt.clf()
  
def inc_gamma(a, x):
    return exp1(x) if a == 0 else gamma(a)*gammaincc(a, x)

def cumulative_mass(x, q, htype, c, r0, gam):
  if htype == 0:
    return r0 * np.sqrt(q) / (1 - np.sqrt(q))
  elif htype == 1:
    return (np.log(1 + x / r0) - x / (x + r0)) / (np.log(1+c) - c/(1+c)) - q
  elif htype == 2:
    return (x / r0 - np.arctan(x / r0)) / (c - np.arctan(c)) - q
  elif htype == 3:
    return (np.log(1 + x / r0) + 0.5 * np.log(1 + (x * x) / (r0 * r0)) - np.arctan(x / r0)) \
           / (np.log(1+c) + 0.5*np.log(1+c*c) - np.arctan(c)) - q
  elif htype == 4:
    return (gamma(3/gam) - inc_gamma(3/gam, 2 * np.power(x / r0, gam) / gam)) \
            / (gamma(3/gam) - inc_gamma(3/gam, 2 * np.power(c, gam) / gam)) - q

def spread_particles(htype, N, c, r200, r0, lq, qi, error, nrings, gam):
  x = []
  y = []
  z = []

  x0 = r0 * qi
  x1 = x0 + 1
  rmax = secant(cumulative_mass, qi, htype, c, r0, x0, x1, error, gam)
  if (htype == 1 or htype == 2 or htype == 3):
    rcut = secant(cumulative_mass, lq, htype, c, r0, x0, x1, error, gam)
  else:
    rcut = rmax
  rad = np.linspace(rcut, rmax, nrings, endpoint=True)
  probr = np.exp(np.linspace(-1, -0.1, nrings-1, endpoint=True))
  print(rmax, rcut)
  for _ in range(N):
    while True:
      q = random()

      x0 = r0 * q
      x1 = x0 + 1

      if q > 0:
        r = secant(cumulative_mass, q, htype, c, r0, x0, x1, error, gam)
      else:
        r = 0

      out = 0
      if rcut <= r <= rmax:
        p = random()
        for k in range(len(rad)-1):
          if rad[k] <= r <= rad[k+1]:
            if p > probr[k]:
              out = 1
              break
        if out:
          break
        else:
          continue
      if r < rcut:
        break
    
    phi = random() * np.pi * 2
    theta = np.arccos(random() * 2 - 1)

    x.append(r * np.sin(theta) * np.cos(phi))  
    y.append(r * np.sin(theta) * np.sin(phi))  
    z.append(r * np.cos(theta))
  
  x = np.asarray(x)
  y = np.asarray(y)
  z = np.asarray(z)

  return x, y, z

def secant(f, q, htype, c, r0, x0, x1, tol, gam, max_iterations=10000):
  if htype == 0:
    return cumulative_mass(x0, q, htype, c, r0, gam)

    # keep initial values for error reporting
  init_x0 = x0
  init_x1 = x1

    # store y values instead of recomputing them
  fx0 = f(x0, q, htype, c, r0, gam)
  fx1 = f(x1, q, htype, c, r0, gam)

    # iterate up to maximum number of times
  for _ in range(max_iterations):
    # see whether the answer has converged
    if abs(fx1) < tol:
      return x1

    # do calculation
    x2 = (x0 * fx1 - x1 * fx0) / (fx1 - fx0)
    # shift variables (prepare for next loop)

    x0,  x1  = x1,  x2
    fx0, fx1 = fx1, f(x2, q, htype, c, r0, gam)

  # for loop has ended - failed to converge
  raise ValueError(
    "call to secant(f={}, x0={}, x1={}, tol={}," \
    " max_iterations={}) has failed to converge"
    .format(
            f.__name__,
            init_x0,
            init_x1,
            tol,
            max_iterations
    )
  )

def volumetric_density(x, y, z, rmax = None, shells = 100):
  nden = []
  newr = []

  if rmax == None:
    rmax = np.max(np.sqrt(x**2 + y**2 + z**2))

  rings = np.linspace(0, rmax, shells)
  numpart = []
  for j in range(len(rings)-1):
    npar = 0
    v = 4/3*np.pi * (rings[j+1]**3 - rings[j]**3)
    for i in range(len(x)):
      r = np.sqrt(x[i]**2 + y[i]**2 + z[i]**2)
      if rings[j] <= r <= rings[j+1]:
        npar = npar + 1
    nden.append(npar / v)
    newr.append((rings[j+1] + rings[j]) / 2)

    numpart.append(npar)
  print(numpart)
  
  return np.asarray(newr), np.asarray(nden)

def volumetric_density_imp(x, y, z, mp, rmax = None, shells = 100, scale='linear'):

  if rmax == None:
    rmax = np.max(np.sqrt(x**2 + y**2 + z**2))
  
  r = np.sqrt(x**2 + y**2 + z**2)
  
  if scale == 'linear':
    rings = np.linspace(0, rmax, shells)
  elif scale == 'log':
    rings = np.logspace(-0.3, np.log10(rmax), shells, endpoint=True)
  
  den = []
  newr = []
  numpart = []
  for j in range(len(rings)-1):
    ir = np.where((r >= rings[j]) & (r <= rings[j+1]))[0]
    v = 4/3*np.pi * (rings[j+1]**3 - rings[j]**3)

    den.append(len(ir)*mp/v)
    numpart.append(len(ir))
    newr.append((rings[j+1] + rings[j]) / 2)
  return np.array(newr), np.array(den)

def mass_dist(x, y, z, mp, rmax = None, shells = 100):
  nmass = []
  newr = []
  
  if rmax == None:
    rmax = np.max(np.sqrt(x**2 + y**2 + z**2))

  rings = np.linspace(0, rmax, shells)
  npar = 0
  for j in range(len(rings)-1):
    for i in range(len(x)):
      r = np.sqrt(x[i]**2 + y[i]**2 + z[i]**2)
      if rings[j] <= r <= rings[j+1]:
        npar = npar + 1
    nmass.append(npar*mp)
    newr.append((rings[j+1] + rings[j]) / 2)
  
  return np.asarray(newr), np.asarray(nmass)

def surface_density(x, y, mp, rmax = None, rad = 100):
  nden = []
  newr = []
  
  if rmax == None:
    rmax = np.max(np.sqrt(x**2 + y**2))
  
  r = np.sqrt(x**2 + y**2) 
  
  rings = np.linspace(0, rmax, rad)
  # npar = 0
  for j in range(len(rings)-1):
    ir = np.where((r >= rings[j]) & (r <= rings[j+1]))[0]
    # for i in range(len(x)):
    #   r = np.sqrt(x[i]**2 + y[i]**2)
    #   if rings[j] <= r <= rings[j+1]:
    #     npar = npar + 1
    npar = len(ir)
    surf = np.pi * (rings[j+1]**2 - rings[j]**2)
    nden.append(npar*mp/surf)
    newr.append((rings[j+1] + rings[j]) / 2)
  
  return np.asarray(newr), np.asarray(nden)

if __name__ == '__main__':
  main()