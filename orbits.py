#!/usr/local/bin python3

from cProfile import label
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict

from readsnap import readsnap
from resonances import snap_mask
from plot_den_maps import adjust_center_mass

np.set_printoptions(threshold=np.inf)


def main():
  fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  models = ['SGS5','SGS21']
  ids = [2521418, 1353680, 2880228]
  
  # snapshots properties
  initsnap = 0
  totsnap = 120
  skip = 1
  ts = 0.1
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = time * ts

  fig, axs = plt.subplots(1,2)
  marker = ['k*', 'bD', 'rp']

  for id in ids:
    for snap in rsnap:
      print("========== particle %s of %s (id=%s) - snapshot %s of %s ==========\n" % (ids.index(id)+1, len(ids), id, str(snap), str(totsnap)))

      ext = snap_mask(snap, 3) # file's mask (last three digits)
      dd = readsnap(fname+models[0], ext, 2)

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      ind = np.where(dd['id'] == id)[0][0] # index within dd dictionary arrays
      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[ind]
      y = y[ind]
      z = z[ind]

      label = str(id)
      axs[0].plot(x, y, marker[ids.index(id)], label=label)
      axs[1].plot(x, z, marker[ids.index(id)], label=label)

      print("=====================================================================\n")
      
    #if id == ids[2]:
    #  axs[0].legend(loc="best")
  
  handles, labels = axs[0].get_legend_handles_labels()
  unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
  axs[0].legend(*zip(*unique), loc="best")

  for ax in axs.flat:
    ax.grid(False)
    #ax.set_xlim(-dist, dist)
    #ax.set_ylim(-dist, dist)
    # ax.label_outer()
    #ax.set_xticks([-1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3])
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 10)
  
  dist1 = 30
  dist2 = 5
  axs[0].set_xlim(-dist1, dist1)
  axs[0].set_ylim(-dist1, dist1)
  axs[1].set_xlim(-dist1, dist1)
  axs[1].set_ylim(-dist2, dist2)
  
  fig.tight_layout()
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()