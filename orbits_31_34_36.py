import os
from xml.parsers.expat import model
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from scipy import interpolate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask

def main():
  #fdir="/media/cronex/HV620S/sm-04-10-2018/sim_12/"
  #fdir="/media/cronex/HV620S/primer-art-rerun/SGS5/"
  #fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  # model = "SGS34"
  models = ['SGS31r', 'SGS34', 'SGS36']
  names = ['SGS31p', 'SGS34p', 'SGS36p']

  initsnap = 0
  totsnap = 60
  skip = 1
  rsnap = range(initsnap,totsnap+1,skip)
  
  initsnap2 = 1140
  totsnap2 = 1200
  skip = 1
  rsnap2 = range(initsnap2,totsnap2+1,skip)

  #nids = np.array([2817882, 1286894, 2047104, 250436, 2039759, 2021200, 487409]) # SGS36 ID's
  #nids = np.array([2047104, 250436, 2039759, 487409])
  #nids = np.array([2817882])
  nids = [2816747, 1287033, 1814640, 1259304, 1243005, 1558727, 1559407, 2555454, 2564773,
          2305634, 2829614,  461608,  202032, 3060971, 1547590, 3062652, 1812543, 3079279,
          747631,  230533, 1002720, 2579478, 2321715,  208622, 2591488, 3111439, 1512209,
          1269939, 2544412, 1288658, 1263766, 1281770, 1031500,  492341,  729172, 2832055] # SGS36 ID's

  # nids = [2206340, 2831625, 2851304, 2984955, 1792549, 1679456, 2449301] # SGS31r ID's 
  nids = [2835343, 1922039, 1157225, 2206340, 1541603, 2449810, 2831625, 2851304, 2984955,
          2051684,  765978,  774488, 2206133, 2590445, 1792549, 1679456, 2449301, 1151678,
          2702893, 2209652,  376855, 3107692, 2186929,  252667, 1799542, 1681632, 1664085,
          1012735, 2833830, 1543921, 1553585,  883919,  765306, 1534328,  244139,  379571,
          2065647, 2332261, 2832452, 1926706] # SGS31r ID's


  # nids = [1512385, 1254936, 1009562, 1792485, 2304649, 731436, 2552323, 496660] # SGS34 ID's
  nids = [1512385, 1254936, 1009562, 1792485, 2304649,  731436, 2552323, 496660, 2336743,
          1765169, 1768714, 211236, 2571508, 1780488, 1272437, 771081, 2324277,  777038,
          489369,  774499, 1282182, 3062577,  778559, 1291120,  774285, 1539524, 1555125,
          2043000] # SGS34 ID's
  
  nids = [1534328, 731436, 2022089] # combination of three particles from SGS31r, SGS34, SGS36
  #2022089, 2286944 -- IDs SGS36
  
  
  maxx = minx = maxy = miny = maxz = minz = 0
  
  # fig, axs = plt.subplots(1, 3)
  fig,axs=plt.subplots(2, 3, sharex=False, sharey=False,
                       gridspec_kw={'hspace': 0.2, 'wspace': 0.3}, dpi = 160)
  plt.style.use('seaborn-bright')
  
  for model in models:
    col = models.index(model)
    idp = nids[col]
    dict_ids = {idp: []}
    
    # ''' Orbits '''
    # #maxx = minx = maxy = miny = 0
    for snap in rsnap:
      ext = snap_mask(snap, 3) # file's mask (last three digits)

      dd = readsnap(fdir+model+'/', snap, 2)

      pd = dd['p']
      #vd = dd['v']
      id = dd['id']
      id = list(id)
      #md = dd['m']
      x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

      print("\n############# Orbit for particle %s (snapshot %s of %s) #############" % (str(idp),str(snap), str(totsnap)))
      dict_ids[idp].append([x[id.index(idp)], y[id.index(idp)], z[id.index(idp)]])  

    for key in dict_ids:
      # maxx = minx = maxy = miny = maxz = minz = 0

      dict_ids[key] = np.array(dict_ids[key])
      px = dict_ids[key][:,0]
      pz = dict_ids[key][:,2]
      py = dict_ids[key][:,1]
      px = np.concatenate((px, [px[0]]))
      py = np.concatenate((py, [py[0]]))
      pz = np.concatenate((pz, [pz[0]]))
      
      ''' Soften orbits '''
      nump = 1000
      # f, u = interpolate.splprep([px, py, pz], s=0, per=True)
      f, u = interpolate.splprep([px, py], s=0, per=True)
      # xnew, smoothy, smoothz = interpolate.splev(np.linspace(0, 1, nump), f)
      xnew, smoothy = interpolate.splev(np.linspace(0, 1, nump), f)
      lastp = nump - 2*nump/(totsnap-initsnap)
      # axs[col].plot(xnew[:lastp], smoothy[:lastp],smoothz[:lastp], '-', linewidth=0.7, alpha=0.8, label=key)
      axs[0,col].plot(xnew[:lastp], smoothy[:lastp], '-', color = 'gray', linewidth=0.9, alpha=0.8, label=key)
      #axs.annotate(str(key), (xnew[0], smoothy[0], smoothz[0]))
      # axs[col].text(xnew[0], smoothy[0], smoothz[0], '%s' % (str(key)), size=7, zorder=1, color='k')
      axs[0,col].set_title(names[col], fontsize = 9)
      # axs[col].axis('square')
      
      if max(xnew) > maxx: maxx = max(xnew)
      if max(smoothy) > maxy: maxy = max(smoothy)
      if min(xnew) < minx: minx = min(xnew)
      if min(smoothy) < miny: miny = min(smoothy)
      # if max(smoothz) > maxz: maxz = max(smoothz)
      # if min(smoothz) < minz: minz = min(smoothz)
    
    dict_ids = {idp: []}
    
    # ''' Orbits '''
    # #maxx = minx = maxy = miny = 0
    for snap in rsnap2:
      ext = snap_mask(snap, 3) # file's mask (last three digits)

      dd = readsnap(fdir+model+'/', snap, 2)

      pd = dd['p']
      #vd = dd['v']
      id = dd['id']
      id = list(id)
      #md = dd['m']
      x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

      print("\n############# Orbit for particle %s (snapshot %s of %s) #############" % (str(idp),str(snap), str(totsnap2)))
      dict_ids[idp].append([x[id.index(idp)], y[id.index(idp)], z[id.index(idp)]])  

    for key in dict_ids:
      # maxx = minx = maxy = miny = maxz = minz = 0

      dict_ids[key] = np.array(dict_ids[key])
      px = dict_ids[key][:,0]
      pz = dict_ids[key][:,2]
      py = dict_ids[key][:,1]
      px = np.concatenate((px, [px[0]]))
      py = np.concatenate((py, [py[0]]))
      pz = np.concatenate((pz, [pz[0]]))
      
      ''' Soften orbits '''
      nump = 1000
      # f, u = interpolate.splprep([px, py, pz], s=0, per=True)
      f, u = interpolate.splprep([px, py], s=0, per=True)
      # xnew, smoothy, smoothz = interpolate.splev(np.linspace(0, 1, nump), f)
      xnew, smoothy = interpolate.splev(np.linspace(0, 1, nump), f)
      lastp = nump - 2*nump/(totsnap2-initsnap2)
      # axs[col].plot(xnew[:lastp], smoothy[:lastp],smoothz[:lastp], '-', linewidth=0.7, alpha=0.8, label=key)
      axs[1,col].plot(xnew[:lastp], smoothy[:lastp], '-', color = 'gray', linewidth=0.9, alpha=0.8, label=key)
      #axs.annotate(str(key), (xnew[0], smoothy[0], smoothz[0]))
      # axs[col].text(xnew[0], smoothy[0], smoothz[0], '%s' % (str(key)), size=7, zorder=1, color='k')
      #axs[1,col].set_title(names[col], fontsize = 9)
      # axs[col].axis('square')
    
  
  for ax in axs.flat:  
    ax.grid(False)
    ax.set(adjustable='box',aspect='equal')
    # ax.label_outer()
    lim = 2.5
    ax.set_xlim(-lim, lim)
    ax.set_ylim(-lim, lim)
    # ax.set_xlabel("$x$", fontsize = 12)
    # ax.set_ylabel("$y$", fontsize = 12)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 8)

  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('$x$', fontsize = 11, labelpad = -3)
  plt.ylabel("$y$", fontsize = 11, labelpad = -2)
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()