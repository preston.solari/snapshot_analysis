import os
from xml.parsers.expat import model
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from scipy import interpolate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask

def main():
  #fdir="/media/cronex/HV620S/sm-04-10-2018/sim_12/"
  #fdir="/media/cronex/HV620S/primer-art-rerun/SGS5/"
  #fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  model = 'SGS36'

  initsnap = 1140
  totsnap = 1200
  skip = 1
  rsnap = range(initsnap,totsnap+1,skip)
  
  # initsnap2 = 1140
  # totsnap2 = 1200
  # skip = 1
  # rsnap2 = range(initsnap2,totsnap2+1,skip)
  
  nids = 2022089
  
  fig, axs = plt.subplots()
  # fig,axs=plt.subplots(2, 3, sharex=False, sharey=False,
  #                      gridspec_kw={'hspace': 0.2, 'wspace': 0.3}, dpi = 160)
  plt.style.use('seaborn-bright')
  
  dict_ids = {nids: []}
    
  # ''' Orbits '''
  for snap in rsnap:
    ext = snap_mask(snap, 3) # file's mask (last three digits)

    dd = readsnap(fdir+model+'/', snap, 2)

    pd = dd['p']
    #vd = dd['v']
    id = dd['id']
    id = list(id)
    #md = dd['m']
    x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

    print("\n############# Orbit for particle %s (snapshot %s of %s) #############" % (str(nids),str(snap), str(totsnap)))
    dict_ids[nids].append([x[id.index(nids)], y[id.index(nids)], z[id.index(nids)]])  

  for key in dict_ids:
    # maxx = minx = maxy = miny = maxz = minz = 0

    dict_ids[key] = np.array(dict_ids[key])
    px = dict_ids[key][:,0]
    pz = dict_ids[key][:,2]
    py = dict_ids[key][:,1]
    px = np.concatenate((px, [px[0]]))
    py = np.concatenate((py, [py[0]]))
    pz = np.concatenate((pz, [pz[0]]))
    
    ''' Soften orbits '''
    nump = 1000
    # f, u = interpolate.splprep([px, py, pz], s=0, per=True)
    f, u = interpolate.splprep([px, py], s=0, per=True)
    # xnew, smoothy, smoothz = interpolate.splev(np.linspace(0, 1, nump), f)
    xnew, smoothy = interpolate.splev(np.linspace(0, 1, nump), f)
    lastp = nump - 2*nump/(totsnap-initsnap)
    # axs[col].plot(xnew[:lastp], smoothy[:lastp],smoothz[:lastp], '-', linewidth=0.7, alpha=0.8, label=key)
    axs.plot(xnew[:lastp], smoothy[:lastp], '-', color = 'gray', linewidth=0.9, alpha=0.8, label=key)
    #axs.annotate(str(key), (xnew[0], smoothy[0], smoothz[0]))
    # axs[col].text(xnew[0], smoothy[0], smoothz[0], '%s' % (str(key)), size=7, zorder=1, color='k')
    # axs.set_title(names[col], fontsize = 9)
    # axs[col].axis('square')
  
  
  axs.grid(False)
  axs.set(adjustable='box',aspect='equal')
  # axs.label_outer()
  # lim = 2.5
  # axs.set_xlim(-lim, lim)
  # axs.set_ylim(-lim, lim)
  axs.set_xlabel("$x$", fontsize = 12)
  axs.set_ylabel("$y$", fontsize = 12)
  axs.tick_params(axis = 'both', which = 'both', direction = 'in',
                  top = True, left = True, right = True, bottom = True,
                  labelsize = 8)
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()