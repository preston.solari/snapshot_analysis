#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline, BSpline

from den_map import data
import resonances as res
from plot_den_maps import adjust_center_mass
from readsnap import readsnap
from fourier_components import *

def surface_density(x, y, R, ftheta = False, theta1 = 0, theta2 = 0):
  if not ftheta:
    r = np.sqrt(x**2 + y**2)
    part = [e for e in r if e <= R]
    area = np.pi * R**2
    
    sur_den = len(part)/area
  else:
    if theta1 >= theta2:
      print("surface_density: theta1 must be smaller than theta2.")
      return 0

    n = 0
    r = [np.sqrt(x[i]**2 + y[i]**2) for i in range(len(x)) if np.sqrt(x[i]**2 + y[i]**2) <= R]
    for e in range(len(r)):
      phi = np.arctan2(y[e], x[e])
      if theta1 < phi <= theta2:
        n = n + 1
    area = (theta2 - theta1)/ 2 * np.pi * R**2
    
    sur_den = n / area
    
  return sur_den 
  
def main():
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/sim_11/"
  fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/bar-pattern-test/"
  
  snaps = 20
  R = 20
  dt = 0.6
  t = np.linspace(0.6, 12, int(12/dt))
  theta = np.linspace(0, 2 * np.pi, 37)

  
  dd = readsnap(fname, '000', 2) # disk particles' data of initial snapshot

  pd = dd['p']  # disc particle positions
  vd = dd['v']

  # remove center of mass (disc)
  xi, yi, zi, vxi, vyi, vzi = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

  sd_0 = surface_density(xi, yi, R) # surface density at t = 0 up to R

  phase = []
  for k in range(0, snaps):
    ext = res.snap_mask(k, 3) # file's mask (last three digits)

    print("\n################### Snapshot %s of %s ###################\n" % (str(k), str(snaps-1)))

    # dd = readsnap(fname, ext, 2) # disk particles' data

    # pd = dd['p']  # disc particle positions
    # vd = dd['v']

    pd = load_snap('Coordinates', 2, fname, ext)
    vd = load_snap('Velocities', 2, fname, ext)

    # remove center of mass (disc)
    x0, y0, z0, vx0, vy0, vz0 = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

    ext = res.snap_mask(k+1, 3) # file's mask (last three digits)

    # dd = readsnap(fname, ext, 2) # disk particles' data

    # pd = dd['p']  # disc particle positions
    # vd = dd['v']

    pd = load_snap('Coordinates', 2, fname, ext)
    vd = load_snap('Velocities', 2, fname, ext)

    x1, y1, z1, vx1, vy1, vz1 = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

    print("Center of mass adjusted.\n")

    print("\n################### Calculating C(R, theta, t) ###################\n")

    # Calculate C(R, theta, t) for each snapthot and determine theta_max
    maxC = tmax = 0
    for i in range(len(theta)-1):
      dss = surface_density(x0, y0, R) - sd_0
      dsst = surface_density(x1, y1, R, ftheta = True, theta1 = theta[i], theta2 = theta[i+1]) \
             - surface_density(xi, yi, R, ftheta = True, theta1 = theta[i], theta2 = theta[i+1])
      
      C = dss * dsst / sd_0**2
      if C > maxC:
        tmax = theta[i+1]
    phase.append(tmax)
  
  phase = np.asarray(phase)

  # number of points for time data
  tnew = np.linspace(t.min(), t.max(), 300) 

  spl = make_interp_spline(t, phase, k=3)  # type: BSpline
  phase_smooth = spl(tnew)

  # calculate derivative of phase with the central finite differences method
  dt = tnew[1] - tnew[0]
  dphase = np.gradient(phase_smooth, dt)

  print("\nDone.\n")

  fig, axs = plt.subplots(1, 2)

  axs[0].plot(tnew, dphase, 'k-')
  axs[1].plot(tnew, phase_smooth, 'k-')

  plt.show()

  plt.clf()
  
if __name__ == '__main__':
  main()