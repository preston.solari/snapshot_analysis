#!/usr/bin/env python

# script to calculate the phase of a bar (if any) in a snapshot

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from readsnap import readsnap
from fourier_components import *

def phase_of_bar_complete_2d(x, y, threshold):
  phi = []
  for i in range(len(x)):
    r = np.sqrt(x[i]**2 + y[i]**2)
    if threshold[0] <= r <= threshold[1]:
      phi.append(np.arctan2(y[i], x[i]))
  phi = np.asarray(phi)
  args = np.sin(2 * phi)
  argc = np.cos(2 * phi)

  a2 = np.mean(argc)
  b2 = np.mean(args)

  phase = np.arctan2(a2, b2)

  return np.mean(phase)

def phase_of_bar_annuli_2d(x, y, radii):
  A2 = []
  phase = []
  newr = []

  pr = np.sqrt(x**2 + y**2)

  for i in range(len(radii)-1):
    phi = []
    for j in range(len(pr)):
      if radii[i] <= pr[j] <= radii[i+1]:
        phi.append(np.arctan2(y[j], x[j]))
        #phi.append(np.arctan(y[j]/x[j]))
    phi = np.asarray(phi)
    argc = np.cos(2 * phi)
    args = np.sin(2 * phi)

    a2 = np.mean(argc)
    b2 = np.mean(args)

    A22 = (a2**2 + b2**2) / 2

    A2.append(np.sqrt(np.mean(A22)) * np.mean(phi))
    phase.append(np.arctan2(b2, a2))
    newr.append((radii[i] + radii[i+1]) / 2)

  A2 = np.asarray(A2)
  phase = np.asarray(phase)
  newr = np.asarray(newr)

  return newr, A2, phase

def convert_radians_to_degrees(angles, positive = False):
  if isinstance(angles, (list, tuple, np.ndarray)):
    if positive:
      ang = [angle + 2*np.pi if angle < 0 else angle for angle in angles]
    else:
      ang = angles
    ang = np.asarray(ang)
  elif isinstance(angles, (int, float)):
    if positive and (angles < 0):
      ang = angles + 2*np.pi
    else:
      ang = float(angles)
  
  return ang * 180 / np.pi

def bar_len_phase(aphase, cphase, radii, max_diff):
  if max_diff <= 0:
    print("max_diff parameter cannot be less or equal.")
    exit()

  i = 0
  while (np.abs(aphase[i] - cphase) < max_diff):
    i = i + 1

  return radii[i]

def bar_len_phase_imp(radius, phase, threshold):
  ph_thres = []
  final_ph = [] # final values of phase
  lenph = 0 # length of phase
  indph = 0 # index of current phase

  baseph = phase[0] # reference phase
  for j in range(len(phase)):
    if np.abs(phase[j]-baseph) < threshold:
      ph_thres.append(phase[j])
      if len(ph_thres) > lenph: # phase remains semi-constant
        final_ph = ph_thres
        lenph = len(final_ph)
        indph = j
    else:
      ph_thres = [] # empty phase
      baseph = phase[j] # drag new reference phase

  ir = list(phase).index(phase[indph]) # index to extract radius
  return radius[ir]

def phase_of_bar_direct(x, y, radii):
  phi_avg = []
  newr = []

  pr = np.sqrt(x**2 + y**2)

  for i in range(len(radii)-1):
    phi = []
    for j in range(len(pr)):
      if radii[i] <= pr[j] <= radii[i+1]:
        phi.append(np.arctan2(y[j], x[j]))
    phi = np.asarray(phi)

    phi_avg.append(np.mean(phi))
    newr.append((radii[i] + radii[i+1]) / 2)

  phi_avg = np.asarray(phi_avg)
  newr = np.asarray(newr)

  return newr, phi_avg
  
def main():
  # fname = "/home/cronex/GD-gaia/Doctorado/bar_len_letter/python/particles_python_disc_020_12.dat"
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname = "/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/bar-pattern-test/"
  fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/SGS8/"
  
  listsims = ['12','11','13','14','15','16',
              '22','21','23','24','25','26',
              '32','31','33','34','35','36',
              '42','41','43','44','45','46',
              '52','51','53','54','55','56',
              '62','61','63','64','65','66'] 
  lensims = len(listsims)

  max_r = 20
  nrings = 20
  radii = np.linspace(0, max_r, nrings+1)

  fig, axs = plt.subplots(6, 6, sharex = False, sharey = False,
                          gridspec_kw = {'hspace': 0.0, 'wspace': 0.0})

  row = col = 0 # rows and columns of plot array
  # for k in range(lensims):
  #   frun = fname + "sim_" + listsims[k] + "/" # location of the file

  #   dd = readsnap(frun, '020', 2) # disk particles' data

  #   pd = dd['p']  # disc particle positions
  #   vd = dd['v']

  #   # remove center of mass (disc)
  #   x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
  #   pd = np.asarray([x, y, z])
  #   pd = pd.T

  #   r, A2, aphase = phase_of_bar_annuli_2d(pd[:, 0], pd[:, 1], radii)

  #   col = k % 6
  #   if k > 0 and col == 0:
  #     row = row + 1

  #   axs[row, col].plot(r, aphase, 'k-')

  # frun = fname + "sim_" + listsims[2] + "/" # location of the file
  frun = fname
  i = 0
  for k in range(80, 116, 1):
    ext = res.snap_mask(k, 3) # file's mask (last three digits)

    dd = readsnap(frun, ext, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']

    # pd = load_snap('Coordinates', 2, fname, ext) # For HDF5 format
    # vd = load_snap('Velocities', 2, fname, ext)

    # remove center of mass (disc)
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

    r, A2, aphase = phase_of_bar_annuli_2d(x, y, radii)
    # aphase = convert_radians_to_degrees(aphase, positive=False)
    # aphase = aphase % 90

    col = i % 6
    if i > 0 and col == 0:
      row = row + 1
    i = i + 1

    axs[row, col].plot(r, aphase, 'k-')

  # cphase = phase_of_bar_complete_2d(x, y, (1, 20))
  # r, aA2, aphase = phase_of_bar_annuli_2d(x, y, radii)

  # cphase_arr = [cphase for e in r]
  # cphase_arr = np.asarray(cphase_arr)

  # len_bar = bar_len_phase(aphase, cphase, r, max_diff)

  # aux = np.linspace(-1, 1, 100)
  # len_bar_arr = [len_bar for e in aux]

  # r, phase = phase_of_bar_direct(x, y, radii)
  # r, A2, phase = phase_of_bar_annuli_2d(x, y, radii)
  # phase = convert_radians_to_degrees(phase, positive = False)

  # cphase = phase_of_bar_complete_2d(x, y, (1, 20))

  # cphase_arr = [cphase for e in r]
  # cphase_arr = np.asarray(cphase_arr)

  # fig, axs = plt.subplots()

  # axs.plot(r, phase, 'k-')
  # axs.plot(r, cphase_arr, 'b.')
  # axs.plot(r, cphase_arr, 'k-')
  # axs.plot(r, aphase, 'k.-')
  # axs.plot(len_bar_arr, aux, 'r-')

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
