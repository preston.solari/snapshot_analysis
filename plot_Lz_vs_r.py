import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl

from den_map import data
import resonances as res
from plot_particles_disc import snaps_range
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from plot_grid import plot_grid_2d, plot_grid_1d
from ang_momentum_norm import angular_momentum_rad

def main():
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/" # rerun
  #fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/halos/" # rerun

  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS5','SGS8','SGS9','SGS11']
  #listsims = ['SGS1','SGS2','SGS3','SGS4', 'SGS5','SGS6']
  #listsims = ['SGS1', 'SGS2']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']
  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']

  spines = [0, 0, 0, 0, 0, 2,
            0, 0, 0, 0, 0, 2,
            0, 0, 0, 0, 0, 2,
            3, 0, 3, 0, 0, 2,
            0, 0, 0, 0, 0, 2,
            0, 0, 3, 0, 0, 2]
  
  # listsims = ['SGS19r', 'SGS21r', 'SGS1o', 'SGS35o', 'SGS29o', 
  #             'SGS31r', 'SGS34r', 'SGS36o', 'SGS36p']


  firstsnap = 1
  initsnap = 10
  totsnap = 120
  skip = initsnap
  step = 1

  timestep = 0.1
  rsnap = range(initsnap,totsnap+1,skip)
  rsnap = [firstsnap] + rsnap[:]
  times = np.round(np.array(rsnap)*timestep, 1)
  # labels = ['T='+str(t)+' Gyrs' for t in times]
  labels = []
  lab_loc = 'upper left'
  
  row = 6
  col = 6

  #fsuffix = 'eta_'
  fsuffix = 'acc_halo_z_'
  #fsuffix = 'vel_'
  extension = '.txt'
  prop = 'accelerations'
  dig = 4

  data_axis = (0,1)
  normy=1000.0
  limx=(0.0, 15.0)
  #limy=(0, 160)
  limy=(0/normy, 2e4/normy)

  xlab = '$R$ [kpc]'
  #ylab = '$A_{2}/A_{0}$'
  #ylab = '$\eta$'
  #ylab = '$\Omega_{b}$'
  #ylab = '$v_{\\theta}$ [km/s]'
  #ylab = '$\sigma_{z}$ [km/s]'
  ylab = '$a_{z}$ [$\\times 10^{3}$ km$^{2}$ s$^{-2}$ kpc$^{-1}$]'
  #title = 'Distorsion parameter for all models'
  title=''

  xp = 0.80
  yp = 0.85
  text_pos = (limx[1]*xp,limy[1]*yp)
  # text_pos = (limx[1]*xp,limy[0]*yp) # for radial acc

  colormap = mpl.cm.rainbow
  cb_label = 'Time [Gyrs]'

  fig, axs = plot_grid_2d(row=row, col=col, listsims=listsims, rsnap=rsnap, title=title,
               labels=labels, lab_loc=lab_loc, fdir=fdir, fsuffix=fsuffix,
               data_axis=data_axis, limx=limx, limy=limy, normy=normy,
               dig=dig, extension=extension, xlab=xlab, ylab=ylab, 
               prop=prop, step=step, text=listsims, text_pos=text_pos,
               timestep=timestep, colormap=colormap, cb_label=cb_label)
  
  rows = columns = 0
  for s in spines:
    lw = 1.5
    for side in axs[rows,columns].spines.keys():
      if s == 0: # barred 
        axs[rows,columns].spines[side].set_color('blue')
        axs[rows,columns].spines[side].set_linewidth(lw)
      elif s == 1: # transient
        axs[rows,columns].spines[side].set_color('green')
        axs[rows,columns].spines[side].set_linewidth(lw)
      elif s == 2: # completely unbarred
        axs[rows,columns].spines[side].set_color('red')
        axs[rows,columns].spines[side].set_linewidth(lw)
      elif s == 3: # delayed bar
        axs[rows,columns].spines[side].set_color('deepskyblue')
        axs[rows,columns].spines[side].set_linewidth(lw)
    columns+=1
    if columns == col:
      columns = 0
      rows+=1
  
  plt.show()
  plt.clf()

  # plot_grid_1d(row=row, col=col, listsims=listsims, 
  #              fdir=fdir, fsuffix=fsuffix,
  #              data_axis=data_axis, skip=skip, limx=limx, limy=limy,
  #              extension=extension, xlab=xlab, ylab=ylab, prop=prop)

if __name__ == '__main__':
  main()