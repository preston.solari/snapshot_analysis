import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from copy import deepcopy

from den_map import data
import resonances as res
from plot_particles_disc import snaps_range
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from plot_grid import plot_grid_2d, plot_grid_1d
from force_calc import force_spline

def main():
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/" # rerun
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/gas/dm_only/" # rerun
  #fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/halos/" # rerun
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/softening/low/"
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/softening/high/"
  # fdir = "/home/cronex/Documentos/Doctorado/soft_testing/high/"
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"
  
  

  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  #listsims = ['SGS5','SGS8','SGS9','SGS11']
  #listsims = ['SGS1','SGS2','SGS3','SGS4', 'SGS5','SGS6']
  #listsims = ['SGS1', 'SGS2']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']
  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #          'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #          'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #          'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #          'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #          'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  # listsims = ["hernquist", "piso", "burkert", "einasto"]
  # listsims = ["002", "003", "004", "006", "007", "008", "009"]
  # listsims = ["002", "003", "004", "out"]
  listsims = ["SGS5o", "SGS5", "out"]
  # listsims = ["001", "005"]
  # listsims = ["001", "002", "003", "004", "005", "006", "007", "008", "009"] # low
  # listsims = ["001", "002", "003", "004", "005"] # high
  
  # sim_name = ["15", "out"]
  sim_name = deepcopy(listsims)

  # names = ['SGS1p', 'SGS2p', 'SGS3p', 'SGS4p', 'SGS5p', 'SGS6p',
  #          'SGS7p', 'SGS8p', 'SGS9p', 'SGS10p', 'SGS11p', 'SGS12p',
  #          'SGS13p', 'SGS14p', 'SGS15p', 'SGS16p', 'SGS17p', 'SGS18p',
  #          'SGS19p', 'SGS20p', 'SGS21p', 'SGS22p', 'SGS23p', 'SGS24p',
  #          'SGS25p', 'SGS26p', 'SGS27p', 'SGS28p', 'SGS29p', 'SGS30p',
  #          'SGS31p', 'SGS32p', 'SGS33p', 'SGS34p', 'SGS35p', 'SGS36p']
  # names = ["hernquist", "piso", "burkert", "einasto"]
  # names = ["002", "003", "004", "006", "007", "008", "009"]
  # names = ["002", "003", "004", "out"]
  names = ["SGS5 low", "SGS5 high", "SGS5 low\neq force"]
  
  # names = ["0.001", "0.002", "0.003", "0.004", "0.005", "0.006", "0.007", "0.008", "0.009"] # low
  # names = ["0.001", "0.002", "0.003", "0.004", "0.005"] # high
  # names = [r"$\epsilon = $"+i for i in names]
  

  spines = [0, 0, 0, 0, 0, 1,
            0, 0, 0, 0, 0, 1,
            0, 0, 0, 0, 0, 1,
            3, 0, 3, 0, 0, 1,
            0, 0, 0, 0, 0, 1,
            0, 0, 3, 0, 0, 1]
  
  # listsims = ['SGS19r', 'SGS21r', 'SGS1o', 'SGS35o', 'SGS29o', 
  #             'SGS31r', 'SGS34r', 'SGS36o', 'SGS36p']

  
  """ For uniformly spaced data """
  # firstsnap = 1
  # initsnap = 30
  # totsnap = 240
  # skip = initsnap
  # step = 1

  # timestep = 0.05
  # rsnap = range(initsnap,totsnap+1,skip)
  # rsnap = [firstsnap] + rsnap[:]
  # print(rsnap)
  # times = np.round(np.array(rsnap)*timestep, 1)
  # # labels = ['T='+str(t)+' Gyrs' for t in times]
  # labels = []
  # lab_loc = 'upper left'
  """ ends uniform processing """
  
  """ For non-uniform spaced data """
  # timestep = [0.1,0.05,0.05,0.05,0.1,0.05,0.05,0.05,0.05]
  # timestep = [0.1,0.05,0.05,0.05,0.1]
  timestep = [0.05,0.1,0.05]
  
  firstsnap = 1
  step = 1
  rsnap1 = range(40,240+1,40)
  rsnap1 = [firstsnap] + rsnap1[:]
  print(rsnap1)
  
  rsnap2 = range(20,120+1,20)
  rsnap2 = [firstsnap] + rsnap2[:]
  print(rsnap2)
  
  snaps = [rsnap1,rsnap2,rsnap1]
  
  rsnap = {sim: snap for sim, snap in zip(listsims, snaps)}
  
  labels = []
  lab_loc = 'upper left'
  """ ends non-uniform processing """
  
  row = 2
  col = 2

  # fsuffix = 'eta_'
  # fsuffix = 'strength_'
  # fsuffix = 'acc_abs_z_'
  # fsuffix = 'acc_halo_z_'
  # fsuffix = 'den_'
  fsuffix = 'vel_'
  extension = '.txt'
  prop = 'velocities'
  # prop = 'strength'
  # prop = 'accelerations'
  # prop = 'density'
  # dig = 3
  dig = (3,4,3)

  data_axis = (0,6)
  # normy=1000.0
  limx=(0.0, 20.0)
  # limy=(0.0, 0.08)
  limy=(0.0, 250)
  # limy=(0/normy, 4e4/normy)

  xlab = 'Radius [kpc]'
  # ylab = '$A_{2}/A_{0}$'
  # ylab = '$\eta$'
  #ylab = '$\Omega_{b}$'
  # ylab = '$v_{\\theta}$ [km/s]'
  # ylab = '$v_{r}$ [km/s]'
  # ylab = '$\sigma_{\\theta}$ [km/s]'
  # ylab = '$\sigma_{r}$ [km/s]'
  ylab = '$\sigma_{z}$ [km/s]'
  # ylab = '$|a_{z}|$ [$\\times 10^{3}$ km$^{2}$ s$^{-2}$ kpc$^{-1}$]'
  # ylab = "Density"
  #title = 'Distorsion parameter for all models'
  title=''

  xp = 0.77
  yp = 0.85
  text_pos = (limx[1]*xp,limy[1]*yp)
  # text_pos = (limx[1]*xp,limy[0]*yp) # for radial acc

  colormap = mpl.cm.rainbow
  cb_label = 'Time [Gyrs]'

  fig, axs = plot_grid_2d(row=row, col=col, listsims=listsims, rsnap=rsnap, title=title,
               labels=labels, lab_loc=lab_loc, fdir=fdir, fsuffix=fsuffix,
               data_axis=data_axis, limx=limx, limy=limy, #normy=normy,
               dig=dig, extension=extension, xlab=xlab, ylab=ylab, 
               prop=prop, step=step, text=names, text_pos=text_pos,
               timestep=timestep, colormap=colormap, cb_label=cb_label, sim_name=sim_name)
  
  # rows = columns = 0
  # for s in spines:
  #   lw = 1.1
  #   for side in axs[rows,columns].spines.keys():
  #     if s == 0: # barred 
  #       axs[rows,columns].spines[side].set_color('deepskyblue')
  #       axs[rows,columns].spines[side].set_linewidth(lw)
  #     elif s == 1: # transient
  #       axs[rows,columns].spines[side].set_color('gold')
  #       axs[rows,columns].spines[side].set_linewidth(lw)
  #     elif s == 2: # completely unbarred
  #       axs[rows,columns].spines[side].set_color('orangered')
  #       axs[rows,columns].spines[side].set_linewidth(lw)
  #     elif s == 3: # delayed bar
  #       axs[rows,columns].spines[side].set_color('limegreen')
  #       axs[rows,columns].spines[side].set_linewidth(lw)
  #   columns+=1
  #   if columns == col:
  #     columns = 0
  #     rows+=1
  
  """ max accelerations """
  # G = 43007.1
  
  # ri = 0
  # rmax = 2
  # s = 100000
  # epsilon = [0.1, 0.05, 0.025, 0.01, 0.005, 0.001]
  # epsilon = [0.005, 0.005, 0.005]
  # # epsilon = [0.01]
  
  
  # for e, ax in zip(epsilon, axs.flat):
  #   u, acc = force_spline(ri, rmax, s, e, mass=1.3885e-5)
  #   macc = np.sqrt(acc[:,0]**2 + acc[:,1]**2 + acc[:,2]**2)*G
    
  #   max_acc = max(macc)/normy
  #   print(max_acc)
  #   ax.plot([0,limx[1]], [max_acc,max_acc], 'k-')
    
  #   ax.text(6, max_acc-1.8,r"max accel for single particle ($\epsilon=0.005$)", fontsize=8)
  """ end of max accelerations """
  
  plt.show()
  plt.clf()

  # plot_grid_1d(row=row, col=col, listsims=listsims, 
  #              fdir=fdir, fsuffix=fsuffix,
  #              data_axis=data_axis, skip=skip, limx=limx, limy=limy,
  #              extension=extension, xlab=xlab, ylab=ylab, prop=prop)

if __name__ == '__main__':
  main()

# 1. transferencia de momento para el disco de modelos de SGS5
# 2. 
