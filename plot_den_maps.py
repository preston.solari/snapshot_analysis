#!/user/bin/env python

#Plot density maps from a set of GIZMO simulations

import numpy as np
import matplotlib.pyplot as plt

# Adjust particles for center of mass (for now positions and velocities only)
def adjust_center_mass(x, y, z, vx, vy, vz):
  # if ax == None:
  #   ax = []
  # if ay == None:
  #   ay = []
  # if az == None:
  #   az = []

  pos_cm = np.zeros(3)
  vel_cm = np.zeros(3)
  # acc_cm = np.zeros(3)

  p = [x, y, z]
  v = [vx, vy, vz]
  # a = [ax, ay, az]
  N = len(x)

  for j in range(3):
    pos_cm[j] = pos_cm[j] + np.sum(p[j])
    vel_cm[j] = vel_cm[j] + np.sum(v[j])
    # if len(a[0]) == len(p[0]):
    #   acc_cm[j] = acc_cm[j] + np.sum(a[j])

  for j in range(3):
    p[j] = p[j] - pos_cm[j]/N
    v[j] = v[j] - vel_cm[j]/N
    # if len(a[0]) == len(p[0]):
    #   a[j] = a[j] - acc_cm[j]/N
  
  # if len(a[0]) == len(p[0]):
  #   return p[0], p[1], p[2], v[0], v[1], v[2], a[0], a[1], a[2]
  return p[0], p[1], p[2], v[0], v[1], v[2]

  # Adjust particles for center of mass (for now positions and velocities only)
def adjust_center_mass_comp(x, y, z, vx, vy, vz, ax, ay, az):
  pos_cm = np.zeros(3)
  vel_cm = np.zeros(3)
  acc_cm = np.zeros(3)

  p = [x, y, z]
  v = [vx, vy, vz]
  a = [ax, ay, az]

  N = len(x)

  for j in range(3):
    pos_cm[j] = pos_cm[j] + np.sum(p[j])
    vel_cm[j] = vel_cm[j] + np.sum(v[j])
    acc_cm[j] = acc_cm[j] + np.sum(a[j])

  for j in range(3):
    p[j] = p[j] - pos_cm[j]/N
    v[j] = v[j] - vel_cm[j]/N
    a[j] = a[j] - acc_cm[j]/N
  
    return p[0], p[1], p[2], v[0], v[1], v[2], a[0], a[1], a[2]
  

# Adjust particles for center of mass
def adjust_center_mass_unit(x, y, z):
  pos_cm = np.zeros(3)

  p = [x, y, z]
  N = len(x)

  for j in range(3):
    pos_cm[j] = pos_cm[j] + np.sum(p[j])

  for j in range(3):
    p[j] = p[j] - pos_cm[j]/N
  
  return p[0], p[1], p[2]

