from den_map import data

import matplotlib.pyplot as plt
import numpy as np
from scipy.special import gamma, gammainc, gammaincc

import resonances as res
from plot_den_maps import adjust_center_mass
#from readsnap import readsnap
import readsnap_no_acc as reads
from plot_den_maps import adjust_center_mass, adjust_center_mass_unit
from mass_assignment_secant_method import volumetric_density, volumetric_density_imp, spread_particles, inc_gamma

G      = 43007.1
HUBBLE = 0.1
EINC   = 0.17

def rho_hernquist(r, a, mh):
  return mh * a / 2 / np.pi / r / (r+a)**3

def rho_nfw(r, rs, mh, c):
  cterm = np.log(1+c) - c/(1+c)
  return mh / 4 / np.pi / r / cterm / (r+rs)**2

def rho_piso(r, rs, mh, c):
  cterm = c - np.arctan(c)
  return mh / 4 / np.pi / cterm / rs / (r**2 + rs**2)

def rho_burkert(r, rs, mh, c):
  cterm = np.log(1+c) + 0.5 * np.log(1+c**2) - np.arctan(c)
  return mh / 2 / np.pi / cterm / (r + rs) / (r**2 + rs**2)

def rho_einasto(r, rs, mh, gam, c):
  x = r / rs
  rho_2 = mh * gam / 4 / np.pi / rs**3 / (2/gam)**(-3/gam) / np.exp(2/gam) / (gamma(3/gam) - inc_gamma(3/gam, 2 * np.power(c, gam) / gam))
  return rho_2 * np.exp(-2 / gam * (x**gam - 1))
 

def main():
  # fdir = "/media/cronex/TOSHIBA EXT/sims/density_test/"
  fdir = "/home/cronex/Documentos/Doctorado/gizmo-public/sims/gas/dm_only/"
  model = "einasto"
  frun = fdir + model + "/"
  
  rmax = 300
  shells = 100
  rad = np.linspace(0, rmax, shells)
  
  ########### hernquist ###########
  v200 = 160.0
  c = 10.0
  md = 0.05
  M = v200**3 / (10 * HUBBLE * G)
  r200 = v200 / 10 / HUBBLE
  a = r200 / c * np.sqrt(2 * (np.log(1+c) - c / (1+c)))
  
  mhh = M - md*M
  den_real_hern = rho_hernquist(rad, a, mhh)
  ########### hernquist ###########
  
  ########### nfw ###########
  v200 = 200.0
  c = 8.0
  md = 4.5
  M = v200**3 / (10 * HUBBLE * G)
  r200 = v200 / 10 / HUBBLE
  rs = r200 / c
  
  mhn = M - md
  den_real_nfw = rho_nfw(rad, rs, mhn, c)
  ########### nfw ###########
  
  ########### piso ###########
  v200 = 64.9
  c = 34.4
  md = 4.76
  M = v200**3 / (10 * HUBBLE * G)
  r200 = v200 / 10 / HUBBLE
  rs = r200 / c
  
  mhp = M - md
  den_real_piso = rho_piso(rad, rs, mhp, c)
  ########### piso ###########
  
  ########### burkert ###########
  v200 = 91.04
  c = 13.97
  md = 4.76
  M = v200**3 / (10 * HUBBLE * G)
  r200 = v200 / 10 / HUBBLE
  rs = r200 / c
  
  mhb = M - md
  den_real_burkert = rho_burkert(rad, rs, mhb, c)
  ########### burkert ###########
  
  ########### einasto ###########
  v200 = 164.06
  c = 9.30
  md = 4.76
  gamma = 0.17
  M = v200**3 / (10 * HUBBLE * G)
  r200 = v200 / 10 / HUBBLE
  rs = r200 / c
  
  mhe = M - md
  den_real_einasto = rho_einasto(rad, rs, mhe, gamma, c)
  ########### einasto ###########
  
  jump = 1
  
  N = int(2.4e4)
  mph = mhe/N
  r200h = 164.06
  ch = 9.30
  nrings = 10
  lq = 0.95
  qi = 0.999
  gam = 0.17
  error = 0.00005
  htype = 4
  xh, yh, zh = spread_particles(htype, N, ch, r200h, r200h/ch, lq, qi, error, nrings, gam)
  
  # dd = reads.readsnap(frun, 10, 1, snapshot_name='snap') # halo particles' data
  dd = reads.readsnap(frun, 10, 1, snapshot_name='snap') # halo particles' data

  pd = dd['p']  # halo particle positions
  mp = dd['m'][0]
  print(mph, mp)

  # remove center of mass (halo)
  x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
  x = x[::jump]
  y = y[::jump]
  z = z[::jump]
  
  # rd, den = volumetric_density(x, y, z, rmax=100, shells=100)
  rd, den = volumetric_density_imp(x, y, z, mp, rmax=rmax, shells=shells)
  rdh, denh = volumetric_density_imp(xh, yh, zh, mph, rmax=rmax, shells=shells)
  
  fig, axs = plt.subplots()
  
  axs.plot(rd, den, 'k-', label='galic')
  # axs.plot(rad, den_real_nfw, 'k--', label='real')
  # axs.plot(rad, den_real_hern, 'k--', label='real')
  # axs.plot(rad, den_real_piso, 'k--', label='real')
  # axs.plot(rad, den_real_burkert, 'k--', label='real')
  axs.plot(rad, den_real_einasto, 'k--', label='real')
  axs.plot(rdh, denh, 'k.-', label='python')
  axs.text(3.5, 4e-7, "Hernquist", fontsize=15)
  axs.legend(loc='upper right')
  axs.set_yscale('log')
  axs.set_xscale('log')
  axs.set_xlim((3, 300))
  axs.set_ylim((1e-7, 2e-2))
  axs.set_xlabel("Radio [kpc]")
  axs.set_ylabel(r"$\rho$ [$M_{\odot}$ kpc$^{-3}$]")
  
  plt.show()
  plt.clf()
  

if __name__ == '__main__':
  main()