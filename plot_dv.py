#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from plot_particles_grid import snaps_range

from den_map import data
import resonances as res
from phase_of_bar import phase_of_bar_annuli_2d

# Main definition
def main():
  # fdir="/home/cronex/Documentos/Doctorado/sims/sm-04-10-2018/"
  fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  

  # models=[('sim_11/','SGS2'),('sim_23/','SGS9'),('sim_34/','SGS16'),('sim_44/','SGS22'),
  #         ('sim_46/','SGS24'),('sim_55/','SGS29'),('sim_56/','SGS30'),('sim_66/','SGS36')]
  
  models=[('sim_11/','SGS2'),('sim_22/','SGS7'),('sim_23/','SGS9'),('sim_32/','SGS13'),
          ('sim_34/','SGS16'),('sim_36/','SGS18'),('sim_44/','SGS22'),('sim_46/','SGS24'),
          ('sim_55/','SGS29'),('sim_56/','SGS30'),('sim_62/','SGS31'),('sim_66/','SGS36')]
  
  title=[r'$\sigma_{R}$',r'$\sigma_{\mathrm{t}}$',r'$\sigma_{z}$']

  init_radii=res.radii_alt(25,50)
  
  plt.style.use('seaborn-bright')

  # fig,axs=plt.subplots(6, 3, gridspec_kw={'hspace': 0.02, 'wspace': 0.02})

  rowp=12 # rows of plot
  colp=3 # columns of plot
  fig,axs=plt.subplots(rowp, colp, sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0.1, 'wspace': 0.04}, dpi=150)

  # Colorbar properties of velocity map
  axb1=rowp-1
  axb2=colp-1
  # axb1=0
  # axb2=0
  xins = inset_axes(axs[axb1,axb2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[axb1,axb2].transAxes,
                    borderpad=0,
                    )        

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'normal',
             'size':6}
  
  isnap=0
  fsnap=20
  ss=3
  num_snaps = 8
  
  ts = 0.6 # timestep of snapshot
  nlines = np.linspace(isnap, fsnap, num_snaps).astype(int)
  time = nlines*ts
  print(nlines)
  
  norm = mpl.colors.Normalize(vmin=time.min(), vmax=time.max())
  cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
  cmap.set_array([])
  
  spines = [0, 0, 0, 0, 3, 1, 3, 1, 2, 1, 0, 2] # 0=regular bar, 1=transient bar, 2=no bar, 3=delayed bar

  for model,lab in models:
    frun=fdir+model
    
    bl_file = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/bar_length/bar_length_"+lab+".txt"
    bdat = data(bl_file)
    # print (bdat)
    tb = bdat[0]
    lb = bdat[1]
    tb = np.append(tb, tb[-1])
    lb = np.append(lb, lb[-1])

    for i in nlines:
      ext=res.snap_mask(i,3)
      fnamed=frun+"particles_python_disc_"+ext+".dat"

      print("\n============== Plotting rotation curve simulation %s - snapshot %s of %s ==============\n"
            % (lab,ext,str(fsnap)))

      pd=data(fnamed)
      x,y,z,vx,vy,vz=pd[0].astype(float),pd[1].astype(float),pd[2].astype(float),pd[3].astype(float),pd[4].astype(float),pd[5].astype(float)

      pr=res.particle_radius(x,y)
      pv=res.velocities(x,y,vx,vy,vz)
      
      values=(pr,) + pv

      newr,avg=res.avg_velocities(values,init_radii)

      # print("\n############# Calculating length of bar #############\n")
      # r, A2, ph = phase_of_bar_annuli_2d(x, y, init_radii)

      # bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      # ph_thres = []
      # final_ph = ph_thres # final values of phase
      # lenph = 0 # length of phase
      # indph = 0 # index of current phase
      # baseph = ph[0] # reference phase
      # for j in range(len(ph)):
      #   if np.abs(ph[j]-baseph) < bt:
      #     ph_thres.append(ph[j]) 
      #     if len(ph_thres) > lenph: # phase remains semi-constant
      #       final_ph = ph_thres
      #       lenph = len(final_ph)
      #       indph = j
      #   else:
      #     ph_thres = [] # empty phase
      #     baseph = ph[j] # drag new reference phase
      
      # avgph = np.mean(final_ph)
      # print(final_ph, avgph)
      # ir = list(ph).index(ph[indph])
      
      k=models.index((model,lab))
      for j in range(3):
        # axs[k,j].plot(fin_radii,avg[j+3],'-',label=r'$t = %s \,\,\mathrm{[Gyrs]}$' %(float(ext)*0.6))
        axs[k,j].plot(newr,avg[j+3],'-',color=cmap.to_rgba(i*ts),lw=0.7)
        axs[k,j].plot((lb[i],lb[i]),(-20,-2),'-', color=cmap.to_rgba(i*ts), linewidth=1.3)
        
        lw = 0.9
        for side in axs[k,j].spines.keys():
          if spines[k] == 0: # barred 
            axs[k,j].spines[side].set_color('deepskyblue')
            axs[k,j].spines[side].set_linewidth(lw)
          elif spines[k] == 1: # transient
            axs[k,j].spines[side].set_color('gold')
            axs[k,j].spines[side].set_linewidth(lw)
          elif spines[k] == 2: # completely unbarred
            axs[k,j].spines[side].set_color('orangered')
            axs[k,j].spines[side].set_linewidth(lw)
          elif spines[k] == 3: # delayed bar
            axs[k,j].spines[side].set_color('limegreen')
            axs[k,j].spines[side].set_linewidth(lw)

        # Put colorbar
        if(k == axb1 and j == axb2 and i == nlines[-1]):
          cbar=fig.colorbar(cmap,cax=xins,ticks=time)
          cbar.set_label(r'Time [Gyrs]',fontsize=6)
          cbar.ax.tick_params(labelsize=4)

        # if j == k == 0:
        #   axs[k,j].legend(loc='upper right',prop={'size':6},shadow=True)
  
        if j == 0:
          axs[k,j].text(16,120,lab,**fontlabel)

        if k == 0:
          axs[k,j].set_title(title[j], fontsize=8)

  for ax in axs.flat:
    ax.grid()
    ax.label_outer()
    ax.set_xlim(0,22)
    ax.set_ylim(-20,170)
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=5)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel('Radius [kpc]',fontsize=6, labelpad=-4)
  plt.ylabel(r'$\sigma \,\,[\mathrm{km}\,\,\mathrm{s}^{-1}]$',fontsize=6, labelpad=-4)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
