#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from den_map import data
import resonances as res
from phase_of_bar import phase_of_bar_annuli_2d
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa

# Main definition
def main():
  #fdir="/home/cronex/Documentos/Doctorado/sims/sm-04-10-2018/"
  fnamer="/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  fnameo="/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"

  # models=[('sim_12/','SGS1'),('sim_21/','SGS8'),('sim_25/','SGS11'),('sim_31/','SGS14'),
  #         ('sim_44/','SGS22'),('sim_54/','SGS28'),('sim_55/','SGS29'),('sim_66/','SGS36')]
  #models=[('sim_32/','SGS13')]
  #models=[('sim_15/','SGS5')]
  models=[('sim_55/','SGS29')]
  
  title=[r'$v_{c}$',r'$\sigma_{R}$',r'$\sigma_{\mathrm{t}}$',r'$\sigma_{z}$']

  init_radii=res.radii_alt(25,50)
  
  plt.style.use('seaborn-bright')

  # fig,axs=plt.subplots(6, 3, gridspec_kw={'hspace': 0.02, 'wspace': 0.02})

  rowp=2 # rows of plot
  colp=4 # columns of plot
  fig,axs=plt.subplots(rowp, colp, sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0.1, 'wspace': 0.04}, dpi=230)

  # Colorbar properties of velocity map
  axb1=rowp-1
  axb2=colp-1
  # axb1=0
  # axb2=0
  xins = inset_axes(axs[axb1,axb2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[axb1,axb2].transAxes,
                    borderpad=0,
                    )        

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'normal',
             'size':7}
  
  isnap=0
  fsnap=21
  ss=1
  
  ts = 0.6 # timestep of snapshot
  nlines = np.arange(isnap+1, fsnap+1, ss)
  time = (nlines-1)*ts

  isnapr=0
  fsnapr=120
  ssr=6
  
  tsr = 0.1 # timestep of snapshot
  nlinesr = np.arange(isnapr+1, fsnapr+1, ssr)
  timer = (nlinesr-1)*tsr

  norm = mpl.colors.Normalize(vmin=time.min(), vmax=time.max())
  cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
  cmap.set_array([])

  for model,lab in models:
    fruno=fnameo+model
    frunr=fnamer+lab

    for i in nlines:
      ext=res.snap_mask(i-1,3)

      print("\n============== Plotting rotation curve simulation %s - snapshot %s of %s ==============\n"
            % (lab,ext,str(fsnap)))

      dd = rsa.readsnap(fruno, ext, 2) # disk particles' data
      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      pr=res.particle_radius(x,y)
      pv=res.velocities(x,y,vx,vy,vz)
      
      values=(pr,) + pv

      newr,avg=res.avg_velocities(values,init_radii)
      
      k=models.index((model,lab))
      axs[k,0].plot(newr,avg[1],'-',color=cmap.to_rgba((i-1)*ts),lw=1.0)
      axs[k,0].text(15.5,19,lab,**fontlabel)
      axs[k,0].set_title(title[0], fontsize=8)

      for j in range(1,4):
        # axs[k,j].plot(fin_radii,avg[j+3],'-',label=r'$t = %s \,\,\mathrm{[Gyrs]}$' %(float(ext)*0.6))
        axs[k,j].plot(newr,avg[j+2],'-',color=cmap.to_rgba((i-1)*ts),lw=1.0)
        #axs[k,j].plot((r[ir+1],r[ir+1]),(-20,-4),'-', color=cmap.to_rgba((i-1)*ts), linewidth=1.3)
        axs[k,j].set_title(title[j], fontsize=8)

      #ext=res.snap_mask((i-1)*ssr,4)
      ext=(i-1)*ssr
      print(ext)
      dd = rsa.readsnap(frunr, ext, 2) # disk particles' data
      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

      pr=res.particle_radius(x,y)
      pv=res.velocities(x,y,vx,vy,vz)
      
      values=(pr,) + pv

      newr,avg=res.avg_velocities(values,init_radii)
      
      k+=1
      axs[k,0].plot(newr,avg[1],'-',color=cmap.to_rgba((i-1)*ts),lw=1.0)
      for j in range(1,4):
        # axs[k,j].plot(fin_radii,avg[j+3],'-',label=r'$t = %s \,\,\mathrm{[Gyrs]}$' %(float(ext)*0.6))
        axs[k,j].plot(newr,avg[j+2],'-',color=cmap.to_rgba((i-1)*ts),lw=1.0)
        #axs[k,j].plot((r[ir+1],r[ir+1]),(-20,-4),'-', color=cmap.to_rgba((i-1)*ts), linewidth=1.3)

        # Put colorbar
        if(j == axb2 and i == nlines[-1]):
          cbar=fig.colorbar(cmap,cax=xins,ticks=time)
          cbar.set_label(r'Time [Gyrs]',fontsize=6)
          cbar.ax.tick_params(labelsize=4)

        # if j == k == 0:
        #   axs[k,j].legend(loc='upper right',prop={'size':6},shadow=True)
  
  for ax in axs.flat:
    ax.grid()
    ax.label_outer()
    ax.set_xlim(0,22)
    ax.set_ylim(-20,250)
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=5)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel('Radius [kpc]',fontsize=6, labelpad=-4)
  plt.ylabel(r'$\sigma \,\,[\mathrm{km}\,\,\mathrm{s}^{-1}]$',fontsize=6, labelpad=-4)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
