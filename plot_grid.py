import matplotlib.pyplot as plt
import numpy as np

from den_map import data
import resonances as res
from plot_particles_disc import snaps_range
from ang_momentum_norm import angular_momentum_xyz, angular_momentum_xyz_norm
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

def plot_grid_2d(row, col, listsims, rsnap, fdir, fsuffix, labels, lab_loc='lower right',
                 data_axis=(0,1), limx=(0, 10), limy=(0, 10), normy=None, title='',
                 dig=3, extension='.txt', xlab='', ylab='', prop='', step=1,
                 text=None, text_pos=(0,0), timestep=0, colormap=mpl.cm.jet, cb_label='',
                 sim_name=''):

  fig, axs = plt.subplots(row, col, sharex = True, sharey = True,
                          gridspec_kw = {'hspace': 0.15, 'wspace': 0.1}, 
                          figsize=(10,10),dpi=130)
  
  # Colorbar properties of velocity map
  # axb1=row-1
  axb1=0
  axb2=col-1
  # axb2=0
  xins = inset_axes(axs[axb1,axb2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[axb1,axb2].transAxes,
                    borderpad=0,
                    )
  # if isinstance(rsnap, dict):
  #   time = np.array(rsnap[listsims[0]])*timestep
  # else:
  #   time = np.array(rsnap)*timestep
  # norm = mpl.colors.Normalize(vmin=time.min(), vmax=time.max())
  # cmap = mpl.cm.ScalarMappable(norm=norm, cmap=colormap)
  # cmap.set_array([])

  r = c = 0
  for sim in listsims:
    # frun = fname + listsims[sim] + "/output/" # location of the file
    frun = fdir + sim + "/" + prop + "/" # location of the file
    
    if isinstance(rsnap, dict):
      ntimestep = timestep[listsims.index(sim)]
      time = np.array(rsnap[listsims[listsims.index(sim)]])*ntimestep
    else:
      ntimestep = timestep
      time = np.array(rsnap)*ntimestep
    norm = mpl.colors.Normalize(vmin=time.min(), vmax=time.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=colormap)
    cmap.set_array([])
    
    if isinstance(rsnap, dict):
      lsnaps = rsnap[sim]
    else:
      lsnaps = rsnap
    for snap in lsnaps:
      if isinstance(dig, (tuple, list)):
        ext = res.snap_mask(snap, dig[listsims.index(sim)])
      else:
        ext = res.snap_mask(snap, dig) # file's mask
        
      dat = data(frun+fsuffix+sim_name[listsims.index(sim)]+"_"+ext+extension, step=step)
      x, y = dat[data_axis[0]].astype(float), dat[data_axis[1]].astype(float)
      if normy != None:
        y = y/float(normy)
      
      if len(labels):
        axs[r, c].plot(x, y, '-', linewidth=0.8,
                      color=cmap.to_rgba(snap*ntimestep),
                      label=labels[int(rsnap.index(snap))])
      else:
        axs[r, c].plot(x, y, '-', linewidth=0.8,
                      color=cmap.to_rgba(snap*ntimestep))

      if text != None and len(text):
        axs[r, c].text(text_pos[0], text_pos[1], text[listsims.index(sim)], fontsize=8)
      
      # Put colorbar
      if(r == axb1 and c == axb2 and snap == lsnaps[-1]):
        cbar=fig.colorbar(cmap,cax=xins,ticks=time)
        cbar.set_label(cb_label,fontsize=8)
        cbar.ax.tick_params(labelsize=6)
    
    if len(labels) and c==col-1 and r==0:
      axs[r, c].legend(loc=lab_loc, bbox_to_anchor=(1.02, 1), fontsize=7)

    c += 1
    if c % col == 0:
      r += 1
      c = 0

  for ax in axs.flat:
    for axis in ['top','bottom','left','right']:
      ax.spines[axis].set_linewidth(0.5)
    ax.grid(linewidth=0.4)
    ax.label_outer()
    ax.set_xlim(limx[0], limx[1])
    ax.set_ylim(limy[0], limy[1])
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                   top = True, left = True, right = True, bottom = True,
                   labelsize = 8, width=0.5)
  
  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel(xlab, fontsize = 11)
  plt.ylabel(ylab, fontsize = 11, labelpad=-1.0)
  plt.title(title, fontdict={'fontsize': 10})

  # plt.show()
  # plt.clf()
  
  return fig, axs

def plot_grid_1d(row, col, listsims, fdir, fsuffix, data_axis=(0,1), yscale='linear',
                 skip=1, limx=(0, 10), limy=(0, 10), extension='.txt', 
                 xlab='', ylab='', prop='', title='', text=None, text_pos=(0,0)):

  fig, axs = plt.subplots(row, col, sharex = True, sharey = True,
                          gridspec_kw = {'hspace': 0.15, 'wspace': 0.08}, 
                          figsize=(10,10),dpi=150)
  
  #rsnap = np.arange(initsnap, totsnap+1, skip)

  r = c = 0
  for sim in listsims:
    # frun = fname + listsims[sim] + "/output/" # location of the file
    frun = fdir + prop + "/" # location of the file
    
    dat = data(frun+fsuffix+sim+extension, step=skip)
    x, y = dat[data_axis[0]].astype(float), dat[data_axis[1]].astype(float)
    print(x,y)
      
    axs[r, c].plot(x, y, '-', color='darkgray')

    if text != None and len(text):
      axs[r, c].text(text_pos[0], text_pos[1], text[listsims.index(sim)], fontsize=6)
    
    c += 1
    if c % col == 0:
      r += 1
      c = 0

  for ax in axs.flat:
    for axis in ['top','bottom','left','right']:
      ax.spines[axis].set_linewidth(0.5)
    ax.grid(linewidth=0.4)
    ax.label_outer()
    ax.set_xlim(limx[0], limx[1])
    ax.set_ylim(limy[0], limy[1])
    ax.set_yscale(yscale)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                   top = True, left = True, right = True, bottom = True,
                   labelsize = 6, width = 0.5)
  
  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel(xlab, fontsize = 7)
  plt.ylabel(ylab, fontsize = 7, labelpad=10.0)
  plt.title(title, fontdict={'fontsize': 9})

  # plt.show()
  # plt.clf()
  
  return fig, axs

def main():
  #fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/" # rerun
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/" # rerun
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/" # rerun

  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  # listsims = ['SGS5','SGS8','SGS9','SGS11']
  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #          'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #          'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #          'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #          'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #          'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  
  # listsims = ['SGS19', 'SGS19r', 'SGS21', 'SGS21r']
  # listsims = ['SGS1', 'SGS1o', 'SGS35', 'SGS35o']
  # listsims = ['SGS29', 'SGS29o', 'SGS31', 'SGS31r', 'SGS34', 'SGS34r']
  # listsims = ['SGS36', 'SGS36o', 'SGS36p']
  # listsims = ['SGS19r', 'SGS21r', 'SGS1o', 'SGS35o', 'SGS29o', 'SGS31r', 'SGS34r', 'SGS36o', 'SGS36p']
  listsims = ['15', 'out']
  
  #listsims = ['12', '55', '66']


  firstsnap = 1
  initsnap = 20
  totsnap = 240
  skip = 1

  timestep = 0.1
  rsnap = range(initsnap,totsnap+1,skip)
  rsnap = [firstsnap] + rsnap[:]
  times = np.round(np.array(rsnap)*timestep, 1)
  text = ['T='+str(t)+' Gyrs' for t in times]
  
  row = 2
  col = 2

  #fsuffix = 'pattern_speed_'
  fsuffix = 'strength_'
  #fsuffix = 'eta_'
  extension = '.txt'
  prop = 'strength'
  dig = 3

  data_axis = (0,1)
  limx=(0,12)
  limy=(0, 1)
  #yscale='log'
  yscale='linear'

  xp = 0.05
  yp = 0.85
  text_pos = (limx[1]*xp,limy[1]*yp)

  xlab = '$T$ [Gyrs]'
  ylab = '$A_{2}/A_{0}$'
  #ylab = '$\eta$'
  #ylab = '$\Omega_{b}$ [km s$^{-1}$ kpc$^{-1}$]'
  title = 'Strength for all models'

  # plot_grid_2d(row=row, col=col, listsims=listsims, initsnap=initsnap, 
  #              totsnap=totsnap, fdir=fdir, fsuffix=fsuffix,
  #              data_axis=data_axis, skip=skip, limx=limx, limy=limy,
  #              dig=dig, extension=extension, xlab=xlab, ylab=ylab, prop=prop)

  plot_grid_1d(row=row, col=col, listsims=listsims, text=listsims, text_pos=text_pos,
               fdir=fdir, fsuffix=fsuffix, title=title, yscale=yscale,
               data_axis=data_axis, skip=skip, limx=limx, limy=limy,
               extension=extension, xlab=xlab, ylab=ylab, prop=prop)

if __name__ == '__main__':
  main()