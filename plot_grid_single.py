import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl

from den_map import data
import resonances as res
from plot_particles_disc import snaps_range
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from plot_grid import plot_grid_2d, plot_grid_1d

def main():
  #fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/" # rerun
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/" # rerun
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/gas/all/" # rerun
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/softening/low/all/"
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/softening/high/all/"
  # fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/" # rerun
  # fdir = "/home/cronex/Documentos/Doctorado/soft_testing/low/all/"
  # fdir = "/home/cronex/Documentos/Doctorado/soft_testing/high/all/"

  #listsims = ['12', '55', '66']
  #listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
  #            'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  #listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  #listsims = ['SGS34', 'SGS36']
  # listsims = ['SGS5','SGS8','SGS9','SGS11']
  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
  #          'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
  #          'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
  #          'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
  #          'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
  #          'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  
  # listsims = ['SGS19', 'SGS19r', 'SGS21', 'SGS21r']
  # listsims = ['SGS1', 'SGS1o', 'SGS35', 'SGS35o']
  # listsims = ['SGS29', 'SGS29o', 'SGS31', 'SGS31r', 'SGS34', 'SGS34r']
  # listsims = ['SGS36', 'SGS36o', 'SGS36p']
  # listsims = ['SGS19r', 'SGS21r', 'SGS1o', 'SGS35o', 'SGS29o', 'SGS31r', 'SGS34r', 'SGS36o', 'SGS36p']
  # listsims = ['hernquist', 'piso', 'burkert', 'einasto']
  # listsims = ["002", "003", "004", "006", "007", "008", "009"]
  # listsims = ["002", "003", "004", "out"]
  # listsims = ['SGS5', 'out']
  listsims = ["SGS5o", "SGS5", "out"]
  # listsims = ["001", "002", "003", "004", "005", "006", "007", "008", "009", "010"] # low
  # listsims = ["001", "002", "003", "004", "005"] # high
  
  names = ["SGS5 low", "SGS5 high", "SGS5 low\neq force"]
  
  
  firstsnap = 1
  initsnap = 1
  totsnap = 240
  skip = 1

  timestep = 0.05
  rsnap = range(initsnap,totsnap+1,skip)
  rsnap = [firstsnap] + rsnap[:]
  times = np.round(np.array(rsnap)*timestep, 1)
  text = ['T='+str(t)+' Gyrs' for t in times]
  
  row = 2
  col = 2

  fsuffix = 'pattern_speed_'
  # fsuffix = 'strength_'
  # fsuffix = 'eta_'
  extension = '.txt'
  # prop = 'strength'
  # prop = 'eta'
  prop = 'pattern_speed'
  dig = 3

  data_axis = (0, 1)
  limx=(0, 12)
  # limy=(0.001, 0.3)
  # limy=(0,0.8)
  limy=(0,60)
  # yscale='log'
  yscale='linear'

  xp = 0.80
  yp = 0.90
  text_pos = (limx[1]*xp,limy[1]*yp)

  xlab = 'Time [Gyrs]'
  # ylab = '$A_{2}/A_{0}$'
  # ylab = '$\eta$'
  ylab = '$\Omega_{b}$ [km s$^{-1}$ kpc$^{-1}$]'
  # title = 'Strength for all models'
  # title = 'Distorsion parameter for all models'
  title = 'Pattern speed for all models'


  fig, axs = plot_grid_1d(row=row, col=col, listsims=listsims, text=names, text_pos=text_pos,
                          fdir=fdir, fsuffix=fsuffix, title=title, yscale=yscale,
                          data_axis=data_axis, skip=skip, limx=limx, limy=limy,
                          extension=extension, xlab=xlab, ylab=ylab, prop=prop)
  
  
  # for eta graph
  # for ax in axs.flat:
  #   ax.plot([0,12.0],[0.02,0.02],'--k', linewidth=1.5, alpha=0.6)
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
