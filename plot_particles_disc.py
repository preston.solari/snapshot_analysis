#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np

from den_map import data 
import resonances as res
from readsnap_no_acc import readsnap
from plot_den_maps import adjust_center_mass

# Creates a range of values for the available number of snapshots
def snaps_range(init,fin,num):
  step=(fin-init)/float(num)
  
  ls=[init]
  for n in range(1,num-1):
    ls.append(init+int(np.ceil(n*step)))
  ls.append(fin)

  return ls

# Main definition
def main():
  # fdir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fdir="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fdir="/home/cronex/Documentos/Doctorado/gadget4-diciembre/ugc-628-test/"
  fdir="/home/cronex/Documentos/Doctorado/ugc-presentacion/"
  # fdir = "/home/cronex/Documentos/Maestria/Tesis/primer-art-rerun/"
  # fdir = "/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fdir = "/run/media/cronex/Elements/Doctorado/Simulaciones/ugc-628/"

  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"]
  # listsims=["hernquist"]
  listsims=["SGS21", "SGS25"]
  # listsims1=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15", "SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"]
  # listsims1=["SGS2", "SGS9", "SGS15", "SGS19", "SGS26", "SGS28", "SGS33"]
  # listsims=["sim_11", "sim_23", "sim_33", "sim_42", "sim_51", "sim_54", "sim_63"] 
  # listsims1=["SGS6", "SGS12", "SGS17", "SGS24", "SGS29", "SGS30", "SGS36"]
  # listsims=["sim_16", "sim_26", "sim_35", "sim_46", "sim_55", "sim_56", "sim_66"] 
  #listsims=["sim_11", "sim_23"] 
  #listsims = ["hernquist", "hernquist_gas_only", "hernquist_gas"]
  # listsims = ["hernquist_gas_only", "hernquist_gas"]
  listsims1 = ["Hernquist", "Hernquist with gas (No SFR)", "Hernquist with gas and SFR"]
  # listsims1 = ["Hernquist with gas (No SFR)", "Hernquist with gas and SFR"]
  lensims=len(listsims)

  nsnaps=5 # number of snapshots to plot
  totsnap=60 # total snapshots
  initsnap=60 # initial snapshot
  snaps_per_col = 5
  timestep = 0.1

  steps=snaps_range(initsnap,totsnap,nsnaps) # steps between snapshots
  # steps=snaps_range(initsnap,totsnap,snaps_per_col) # steps between snapshots
  # print(steps)

  la=18 # absolute maximum for axis
  jump = 25

  # fig,axs=plt.subplots(nsnaps/snaps_per_col, snaps_per_col, sharex=False, sharey=False,
  fig,axs=plt.subplots(lensims, snaps_per_col, sharex=False, sharey=False,
                       gridspec_kw={'hspace': 0.02, 'wspace': 0.02},
                       figsize=(10,10))

  plt.style.use('seaborn-bright')

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':7}

  fonts=9

  mod=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  
  rows=columns=0
  for sim in listsims:
    #frun=fdir+sim+"/output/"
    frun=fdir+sim+"/"
    
    # columns=0

    # for snap in steps:
    #   print("\n=============== Plotting %s simulation - snapshot %s of %s ===============\n" % (sim, str(steps.index(snap)), str(len(steps)-1)))

    #   ext=res.snap_mask(snap,3)

    #   dd = readsnap(frun, ext, 2) # disk particles' data
    #   #dg = readsnap(frun, ext, 0) # disk particles' data

    #   pd = dd['p']  # disc particle positions
    #   vd = dd['v']  # disc particle velocities

    #   #pg = dg['p']  # disc particle positions
    #   #vg = dg['v']  # disc particle velocities

    #   # remove center of mass (disc)
    #   x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    #   #xg, yg, zg, vxg, vyg, vzg = adjust_center_mass(pg[:, 0], pg[:, 1], pg[:, 2], vg[:, 0], vg[:, 1], vg[:, 2])
      
    #   # axs[rows,columns].plot(x[::jump], y[::jump], '.k', markersize=0.5, alpha=0.6)
    #   axs[rows,columns].plot(x[::jump], y[::jump], '.k', markersize=0.5, alpha=0.4, label="Stellar disc")
    #   #axs[rows,columns].plot(xg[::jump], yg[::jump], ',r', markersize=0.5, alpha=0.2, label="Gas disc")

    #   if sim is not listsims[0]:
    #     dg = readsnap(frun, ext, 0) # disk particles' data

    #     pg = dg['p']  # disc particle positions
    #     vg = dg['v']  # disc particle velocities

    #     xg, yg, zg, vxg, vyg, vzg = adjust_center_mass(pg[:, 0], pg[:, 1], pg[:, 2], vg[:, 0], vg[:, 1], vg[:, 2])
    #     axs[rows,columns].plot(xg[::jump], yg[::jump], '.r', markersize=0.5, alpha=0.2, label="Gas disc")
      
    #   if sim is listsims[2] and snap > 0:
    #     ds = readsnap(frun, ext, 4) # disk particles' data

    #     ps = ds['p']  # disc particle positions
    #     vs = ds['v']  # disc particle velocities

    #     xs, ys, zs, vxs, vys, vzs = adjust_center_mass(ps[:, 0], ps[:, 1], ps[:, 2], vs[:, 0], vs[:, 1], vs[:, 2])
    #     axs[rows,columns].plot(xs[::jump], ys[::jump], '.b', markersize=0.5, alpha=0.2, label="New stars")


    #   leg = axs[rows,columns].legend(loc="upper right", fancybox=True, framealpha=0.5, markerscale = 20)
    #   for lh in leg.legendHandles: 
    #     lh.set_alpha(1)

    #   #lev=np.asarray([5.,12.,50.,150.,280.,500.,900.]) # levels for density contour map

    #   # calculate the 2D density of the data given
    #   #counts,xbins,ybins=np.histogram2d(pd[0],pd[1],bins=60)
    #   # make the contour plot
    #   #CS=axs[rows,columns].contour(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
    #   #    ybins.min(),ybins.max()],origin='lower',linewidths=1.5,colors='red',
    #   #    linestyles='solid')

    #   # For text indicating model
    #   if columns == 0:
    #     axs[rows,columns].text(-15.2,-11.5,'%s'%(listsims1[listsims.index(sim)]),
    #     # axs[rows,columns].text(-15.2,11.5,'%s'%(sim),
    #                            bbox=dict(facecolor='white',edgecolor='none',alpha=0.7),
    #                            **fontlabel) 
    #   # text indicating time
    #   axs[rows,columns].text(-15.2,11.5,r'$t = %s$'%(str(snap*timestep)),
    #                            bbox=dict(facecolor='white',edgecolor='none',alpha=0.7),
    #                            **fontlabel) 
      
    #   columns=columns+1
    #   if columns % snaps_per_col == 0:
    #     columns = 0
    #     rows=rows+1

    # print("\n=============== Plotting %s simulation - last snapshot ===============\n" % (sim))
    print("\n=============== Plotting %s simulation - last snapshot ===============\n" % (listsims1[listsims.index(sim)]))

    ext=res.snap_mask(str(totsnap),3)

    dd = readsnap(frun, ext, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']  # disc particle velocities

    # remove center of mass (disc)
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    
    # axs[rows,columns].plot(x[::jump], y[::jump], '.k', markersize=0.5, alpha=0.6)
    axs[rows,columns].plot(x[::jump], y[::jump], '.k', markersize=0.3, alpha=0.2)

    lev=np.asarray([5.,12.,50.,150.,280.,500.,900.]) # levels for density contour map

    #calculate the 2D density of the data given
    counts,xbins,ybins=np.histogram2d(pd[0],pd[1],bins=60)
    #make the contour plot
    CS=axs[rows,columns].contour(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
       ybins.min(),ybins.max()],origin='lower',linewidths=1.5,colors='red',
       linestyles='solid')

    #For text indicating model
    #if columns == 0:
    #  axs[rows,columns].text(-15.2,12.5,r'$t = %s$'%(str(snap*0.1)),
    #                         bbox=dict(facecolor='white',edgecolor='none',alpha=1.0),
    #                         **fontlabel) 
    #text indicating time
    #axs[rows,columns].text(-15.2,11.5,'%s'%(listsims1[listsims.index(sim)]),
    axs[rows,columns].text(-15.2,11.5,'%s'%(sim),
                              bbox=dict(facecolor='white',edgecolor='none',alpha=1.0),
                              **fontlabel) 
    
    columns+=1
    if columns % snaps_per_col == 0:
      columns = 0
      rows+=1

  for ax in axs.flat:
    ax.label_outer()
    ax.set(adjustable='box',aspect='equal')
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=fonts)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel(r'$x \,\,\mathrm{[kpc]}$', fontsize=fonts)
  plt.ylabel(r'$y \,\,\mathrm{[kpc]}$', fontsize=fonts)

  xlim = (-la, la)
  ylim = (-la, la)

  # Setting the values for all axes.
  plt.setp(axs, xlim=xlim, ylim=ylim)

  #plt.gca().set_aspect('equal', adjustable='box') # for equal axis
  #plt.axis('equal')

  #iname=fdir+"plot_part_"+ext+".png"
  #plt.savefig(iname)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
