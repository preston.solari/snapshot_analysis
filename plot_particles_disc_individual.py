#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot
import os
import matplotlib.pyplot as plt
import numpy as np

from den_map import data 
import resonances as res
#from readsnap_no_acc import readsnap
import readsnap_no_acc as reads
from plot_den_maps import adjust_center_mass, adjust_center_mass_unit
from density_scatter import density_scatter

# Creates a range of values for the available number of snapshots
def snaps_range(init,fin,num):
  step=(fin-init)/float(num)
  
  ls=[init]
  for n in range(1,num-1):
    ls.append(init+int(np.ceil(n*step)))
  ls.append(fin)

  return ls

# Main definition
def main():
  # fdir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fdir="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fdir="/home/cronex/Documentos/Doctorado/gadget4-diciembre/ugc-628-test/"
  #fdir="/home/cronex/Documentos/Doctorado/ugc-presentacion/"
  # fdir = "/home/cronex/Documentos/Maestria/Tesis/primer-art-rerun/"
  # fdir = "/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fdir = "/media/cronex/Elements/Doctorado/Simulaciones/ugc-628/"
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun


  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"]
  # listsims=["hernquist"]
  # listsims=["SGS21", "SGS25"]
  # listsims1=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15", "SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"]
  # listsims1=["SGS2", "SGS9", "SGS15", "SGS19", "SGS26", "SGS28", "SGS33"]
  # listsims=["sim_11", "sim_23", "sim_33", "sim_42", "sim_51", "sim_54", "sim_63"] 
  # listsims1=["SGS6", "SGS12", "SGS17", "SGS24", "SGS29", "SGS30", "SGS36"]
  # listsims=["sim_16", "sim_26", "sim_35", "sim_46", "sim_55", "sim_56", "sim_66"] 
  #listsims=["sim_11", "sim_23"] 
  #listsims = ["hernquist", "hernquist_gas_only", "hernquist_gas"]
  # listsims = ["hernquist_gas_only", "hernquist_gas"]
  # listsims1 = ["Hernquist", "Hernquist with gas (No SFR)", "Hernquist with gas and SFR"]
  # listsims1 = ["Hernquist with gas (No SFR)", "Hernquist with gas and SFR"]
  listsims = ['SGS5','SGS8','SGS9','SGS11','SGS13','SGS14',
             'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
             'SGS25','SGS26','SGS27','SGS29','SGS35']
  # listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7',
  #            'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
  #            'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  # listsims = ['SGS34', 'SGS36']
  # listsims = ['SGS33']
  # listsims = ['SGS6']
  # listsims = ['SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  # listsims = ['SGS1o', 'SGS16r', 'SGS29o', 'SGS34r', 'SGS35o', 'SGS36o'] # 240 snaps
  # listsims = ['SGS19r', 'SGS21r'] # 120 snaps
  #listsims = ['SGS31r', 'SGS36p'] # 1200 snaps
  lensims=len(listsims)

  initsnap=0 # initial snapshot
  totsnap=120 # total snapshots
  skip = 1
  step = 1
  dig = 3

  timestep = 0.1
  rsnap = np.arange(initsnap,totsnap+1,skip)
  times = np.round(rsnap*timestep, 1)

  la=12 # absolute maximum for axis
  jump = 1

  # fig,axs=plt.subplots(nsnaps/snaps_per_col, snaps_per_col, sharex=False, sharey=False

  plt.style.use('seaborn-bright')

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':7}

  fonts=9

  mod=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  
  for sim in listsims:
    #frun=fdir+sim+"/output/"
    frun=fdir+sim+"/"
    
    for snap in rsnap:
      fig,axs=plt.subplots(1, 1, sharex=False, sharey=False,
                          gridspec_kw={'hspace': 0.02, 'wspace': 0.02},
                          figsize=(10,10), dpi=150)

      print("\n=============== Plotting %s simulation - snapshot %s of %s ===============\n" % (sim, str(np.where(rsnap == snap)[0][0]), str(len(rsnap)-1)))

      ext=res.snap_mask(snap/skip,dig)

      dd = reads.readsnap(frun, snap, 2) # disk particles' data
      #dg = readsnap(frun, ext, 0) # disk particles' data

      pd = dd['p']  # disc particle positions
      #vd = dd['v']  # disc particle velocities

      #pg = dg['p']  # disc particle positions
      #vg = dg['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]

      # axs.plot(x, y, 'k,')

      bins = 100
      axs = density_scatter(x, y, ax = (fig, axs), bins=[bins,bins], 
                            cmap=plt.cm.jet, ext=(0, 0.03), marker='.', s=5)

      axs.set_title(sim)
      axs.text(la*0.70, la*0.85, "T=%.1f Gyrs" % (times[np.where(rsnap == snap)[0][0]]), bbox=dict(facecolor='white'))

      xlim = (-la, la)
      ylim = (-la, la)
      #axs.grid()
      axs.label_outer()
      axs.set_xlim(xlim[0], xlim[1])
      axs.set_ylim(ylim[0], ylim[1])
      axs.set_xlabel("$x$")
      axs.set_ylabel("$y$")
      axs.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 10)
      axs.set_aspect("equal")
    

      # Setting the values for all axes.
      #plt.setp(axs, xlim=xlim, ylim=ylim)

      #plt.gca().set_aspect('equal', adjustable='box') # for equal axis
      #plt.axis('equal')

      fv = fdir+"/snap_plot/"+sim+"/density_map/"
      if not os.path.exists(fv): # create path fdat if does not exist
        os.makedirs(fv)
      #iname = "plot_part_"+ext+".png"
      iname = "plot_den_"+ext+".png"
      plt.savefig(fv+iname)

      # plt.show()
      plt.close()

if __name__ == '__main__':
  main()
