#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np

from den_map import data 
import resonances as res
from readsnap_no_acc import readsnap
from plot_den_maps import adjust_center_mass
from load_from_snapshot import load_from_snapshot as load_snap

# Creates a range of values for the available number of snapshots
def snaps_range(init,fin,num):
  step=(fin-init)/float(num)
  
  ls=[init]
  for n in range(1,num-1):
    ls.append(init+int(np.ceil(n*step)))
  ls.append(fin)

  return ls

# Main definition
def main():
  # fdir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fdir="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fdir="/home/cronex/Documentos/Doctorado/gadget4-diciembre/ugc-628-test/"
  # fdir="/home/cronex/Documentos/Doctorado/ugc-presentacion/"
  # fdir = "/home/cronex/Documentos/Maestria/Tesis/primer-art-rerun/"
  # fdir = "/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fdir = "/run/media/cronex/Elements/Doctorado/Simulaciones/ugc-628/"
  # fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  # fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  # fdir = "/media/cronex/ADATA HD330/gas/dm_only/"
  # fdir = "/media/cronex/ADATA HD330/softening/high/"
  fdir = "/home/cronex/Documentos/Doctorado/eqforce/"  
  

  # listsims=["hernquist", "piso", "burkert", "einasto"]
  # listsims = ["002", "003", "004"]
  listsims = ["out","out"]
  # listsims=["hernquist"]
  # listsims=["SGS21", "SGS25"]
  # listsims1=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15", "SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"]
  # listsims1=["SGS2", "SGS9", "SGS16", "SGS22", "SGS26", "SGS28", "SGS33"]
  # listsims=["sim_11", "sim_23", "sim_34", "sim_44", "sim_51", "sim_54", "sim_63"] 
  spines=[0, 0, 3, 3, 0, 3, 0]
  # listsims1=["SGS6", "SGS12", "SGS17", "SGS24", "SGS29", "SGS30", "SGS36"]
  # listsims=["sim_16", "sim_26", "sim_35", "sim_46", "sim_55", "sim_56", "sim_66"] 
  # spines=[1, 1, 2, 1, 2, 1, 2]
  # listsims=["sim_11", "sim_23"] 
  #listsims = ["hernquist", "hernquist_gas_only", "hernquist_gas"]
  # listsims = ["hernquist_gas_only", "hernquist_gas"]
  #listsims = ["hernquist_gas_sfr_0.2"]
  #listsims1 = ["Hernquist", "Hernquist with gas (No SFR)", "Hernquist with gas and SFR"]
  # listsims1 = ["Hernquist with gas (No SFR)", "Hernquist with gas and SFR"]
  lensims=len(listsims)

  snapcol=8 # columns to plot
  snaprow=lensims # rows to plot
  totsnap=240 # total snapshots
  initsnap=0 # initial snapshot
  #snaps_per_col = 5
  timestep = 0.05

  # steps=snaps_range(initsnap,totsnap,snapcol*snaprow) # steps between snapshots
  steps=snaps_range(initsnap,totsnap,snapcol) # steps between snapshots

  la=18 # absolute maximum for axis
  jump = 40

  fig,axs=plt.subplots(snaprow, snapcol, sharex=False, sharey=False,
                       gridspec_kw={'hspace': 0.06, 'wspace': 0.02},
                       figsize=(10,10), dpi=130)

  plt.style.use('seaborn-bright')

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':8}

  fonts=8.5
  fontl=10

  #mod=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  
  rows=columns=0
  for sim in listsims:
    # frun=fdir+sim+"/output/"
    # frun=fdir+sim+"/hdf5/output/"
    #frun=fdir+sim+"/output-ic-hdf5/"
    frun=fdir+sim+"/"
    
    columns=0

    for snap in steps:
      print("\n=============== Plotting %s simulation - snapshot %s of %s ===============\n" % (sim, str(steps.index(snap)), str(len(steps)-1)))

      ext=res.snap_mask(snap,3)

      dd = readsnap(frun, ext, 2) # disk particles' data
      #dg = readsnap(frun, ext, 0) # disk particles' data
      # dns = readsnap(frun, ext, 4) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities
      
      # pd = load_snap("Coordinates", 2, frun, snap)
      # vd = load_snap("Velocities", 2, frun, snap)

      #pg = dg['p']  # disc particle positions
      #vg = dg['v']  # disc particle velocities

      # pns = dns['p']  # disc particle positions
      # vns = dns['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      #xg, yg, zg, vxg, vyg, vzg = adjust_center_mass(pg[:, 0], pg[:, 1], pg[:, 2], vg[:, 0], vg[:, 1], vg[:, 2])
      # xs, ys, zs, vxs, vys, vzs = adjust_center_mass(pns[:, 0], pns[:, 1], pns[:, 2], vns[:, 0], vns[:, 1], vns[:, 2])
      
      axs[rows,columns].plot(x[::jump], y[::jump], ',k', markersize=1.0, alpha=0.6)
      #axs[rows,columns].plot(x[::jump], y[::jump], '.k', markersize=0.3, alpha=0.4, label="Stellar disc")
      #axs[rows,columns].plot(xg[::jump], yg[::jump], '.b', markersize=0.5, alpha=0.3, label="Gas disc")
      #axs[rows,columns].plot(xs[::jump], ys[::jump], '.r', markersize=0.3, alpha=0.4, label="New Stars")
      
      # lw = 1.5
      # for side in axs[rows,columns].spines.keys():
      #   if spines[listsims.index(sim)] == 0: # barred 
      #     axs[rows,columns].spines[side].set_color('deepskyblue')
      #     axs[rows,columns].spines[side].set_linewidth(lw)
      #   elif spines[listsims.index(sim)] == 1: # transient
      #     axs[rows,columns].spines[side].set_color('gold')
      #     axs[rows,columns].spines[side].set_linewidth(lw)
      #   elif spines[listsims.index(sim)] == 2: # completely unbarred
      #     axs[rows,columns].spines[side].set_color('orangered')
      #     axs[rows,columns].spines[side].set_linewidth(lw)
      #   elif spines[listsims.index(sim)] == 3: # delayed bar
      #     axs[rows,columns].spines[side].set_color('limegreen')
      #     axs[rows,columns].spines[side].set_linewidth(lw)

      #lev=np.asarray([5.,12.,50.,150.,280.,500.,900.]) # levels for density contour map

      # calculate the 2D density of the data given
      #counts,xbins,ybins=np.histogram2d(pd[0],pd[1],bins=60)
      # make the contour plot
      #CS=axs[rows,columns].contour(counts.T,levels=lev,extent=[xbins.min(),xbins.max(),
      #    ybins.min(),ybins.max()],origin='lower',linewidths=1.5,colors='red',
      #    linestyles='solid')

      # For text indicating model
      if columns == 0:
        # axs[rows,columns].text(-15.5,-13.5,'%s'%(listsims1[listsims.index(sim)]),
        axs[rows,columns].text(-15.2,-13.5,'%s'%(sim),
                               bbox=dict(facecolor='white',edgecolor='none',alpha=0.7),
                               **fontlabel) 
      # text indicating time
      axs[rows,columns].text(-15.5,13.5,r'$t = %s$'%(str(snap*timestep)),
                               bbox=dict(facecolor='white',edgecolor='none',alpha=0.7),
                               **fontlabel) 
      
      columns=columns+1
      if columns % snapcol == 0:
        columns = 0
        rows=rows+1

    # columns+=1
    # if columns % snapcol == 0:
    #   columns = 0
    #   rows+=1

  for ax in axs.flat:
    ax.label_outer()
    ax.set(adjustable='box',aspect='equal')
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=fonts)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel(r'$x \,\,\mathrm{[kpc]}$', fontsize=fontl)
  plt.ylabel(r'$y \,\,\mathrm{[kpc]}$', fontsize=fontl)

  xlim = (-la, la)
  ylim = (-la, la)

  # Setting the values for all axes.
  plt.setp(axs, xlim=xlim, ylim=ylim)

  #plt.gca().set_aspect('equal', adjustable='box') # for equal axis
  #plt.axis('equal')

  #iname=fdir+"plot_part_"+ext+".png"
  #plt.savefig(iname)

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
