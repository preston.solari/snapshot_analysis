#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np
from pylab import Circle

#from load_from_snapshot import load_from_snapshot as data
from readsnap import readsnap
from plot_den_maps import adjust_center_mass

# Main definition
def main():
  #fdir="/home/kaicudon/Documentos/Maestria/Tesis/sm-04-10-2018/sim_31/"
  #fdir="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/sim_21/"
  #fdir="/home/cronex/Documentos/Doctorado/gizmo-public/run/galic-halos/Einasto/"
  #fdir="/home/cronex/Descargas/GIZMO/sin-gas/Hernquist"
  #fdir="/home/cronex/Descargas/GADGET-2/con-gas/Hernquist"
  #fdir="/home/cronex/Documentos/Doctorado/GALIC-master-mod-halos/test/Model-D1-mod"
  #fdir="/home/cronex/Documentos/Doctorado/Gadget-2.0.7/Gadget2/run/galic-halos/Iso-test/"
  #fdir="/home/cronex/Descargas/GIZMO/sin-gas/NFW"
  fdir="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/hernquist/output/"
  fdir2="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/nfw/output/"

  lx=140 # absolute maximum for axis
  ly=140
  jump=15

  # dd = readsnap(fdir, 0, 2) # disk particles' data
  dh = readsnap(fdir, 0, 1) # halo particles' data
  dh2 = readsnap(fdir2, 0, 1) # halo particles' data

  # pd = dd['p']  # disc particle positions
  # vd = dd['v']  # disc particle velocities

  ph = dh['p']  # halo particle positions
  vh = dh['v']  # halo particle velocities
  ph2 = dh['p']  # halo particle positions
  vh2 = dh['v']  # halo particle velocities

  # remove center of mass (disc and halo)
  # xd, yd, zd, vxd, vyd, vzd = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
  xh, yh, zh, vxh, vyh, vzh = adjust_center_mass(ph[:, 0], ph[:, 1], ph[:, 2], vh[:, 0], vh[:, 1], vh[:, 2])
  xh2, yh2, zh2, vxh2, vyh2, vzh2 = adjust_center_mass(ph2[:, 0], ph2[:, 1], ph2[:, 2], vh2[:, 0], vh2[:, 1], vh2[:, 2])

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':12,
             'color':'r'}

  fig,axs=plt.subplots(1, 2, sharex=True, sharey=False,
                       gridspec_kw={'hspace': 0, 'wspace': 0.1},figsize=(10,5))

  #(ax1,ax2),(ax3,ax4),(ax5,ax6)=axs
  (ax1,ax2)=axs

  alpha=0.25
  ms=0.5

  #ax1.plot(pd[0], pd[1], '.k', markersize=ms, alpha=alpha)
  #ax1.text(16,25,'Disk XY',**fontlabel)

  #ax2.plot(pd[0], pd[2], '.k', markersize=ms, alpha=alpha)
  #ax2.text(16,25,'Disk XZ',**fontlabel)

  #ax3.plot(pg[0], pg[1], '.k', markersize=ms, alpha=alpha)
  #ax3.text(8,25,'Gas disk XY',**fontlabel)

  #ax4.plot(pg[0], pg[2], '.k', markersize=ms, alpha=alpha)
  #ax4.text(8,25,'Gas disk XZ',**fontlabel)

  #p1=ph[0]
  #p2=ph[1]
  #p3=ph[2]
  ax1.plot(xh[::jump], yh[::jump], '.k', markersize=ms, alpha=alpha)
  # ax1.plot(xd, yd, '.k', markersize=ms, alpha=alpha)
  # ax1.add_patch(plt.Circle((0,0), 146.1, fill = False, edgecolor='k', linestyle = '--', linewidth = 2.0, label="Cut-off radius"))
  # ax1.set_aspect('equal', adjustable='datalim')
  #ax1.plot(pg[0, ::5], pg[1, ::5], '.r', markersize=ms, alpha=alpha*.5)
  #ax1.plot(p1[::8], p2[::8], '.k', markersize=ms, alpha=alpha)
  ax1.text(60,120,'Hernquist',**fontlabel)

  ax2.plot(xh2[::jump], zh2[::jump], '.k', markersize=ms, alpha=alpha, label="Halo")
  # ax2.plot(xd, zd, '.k', markersize=ms, alpha=alpha, label="Stellar disk")
  # ax2.add_patch(plt.Circle((0,0), 146.1, fill = False, edgecolor='k', linestyle = '--', linewidth = 2.0, label="Cut-off radius"))
  #ax2.plot(pg[0, ::5], pg[2, ::5], '.r', markersize=ms, alpha=alpha*.5, label="Gas disk")
  #ax2.plot(p1[::8], p3[::8], '.k', markersize=ms, alpha=alpha)
  ax2.text(100,120,'NFW',**fontlabel)
  # ax2.legend(loc="best", markerscale=25)
  ax2.set_aspect('equal', adjustable='datalim')

  for ax in axs.flat:
    ax.label_outer()
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=14)

  xlim = (-lx, lx)
  ylim = (-ly, ly)

  # Setting the values for all axes.
  plt.setp(axs, xlim=xlim, ylim=ylim)

  #iname=fdir+"plot_part_"+ext+".png"
  #plt.savefig(iname)

  plt.show()

if __name__ == '__main__':
  main()
