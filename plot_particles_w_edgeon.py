#!/user/bin/env python

#Plot particles of a GIZMO snapshot

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors
import numpy as np
import scipy.stats as st
import seaborn as sns
from scipy.interpolate import griddata as gd
import scipy.ndimage as ndimage
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from midpoint_colorbar import MidPointNorm

from readsnap import readsnap
from plot_den_maps import adjust_center_mass
from den_map_edgeon import rotate_snap

def main():
    #fdir = '../galic-halos/Hernquist/' 
    #fdir = './output/' 
    fdir="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/einasto/output/"

    dd = readsnap(fdir, 120, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']  # disc particle velocities

    # remove center of mass (disc)
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    x, y, z = rotate_snap(x, y, z, rot = -23)

    lev = np.logspace(1, 4.0, num = 18)
    levc = np.logspace(1, 3.7, num = 14)
    # levc2 = np.logspace(1, 3.7, num = 12)
    cmap='jet'
    origin='lower'

    #fig, axs = plt.subplots(2, 1)
    fig = plt.figure(figsize = (12, 12))
    gs = gridspec.GridSpec(6, 6)

    big_ax = plt.subplot(gs[1:, :-2])
    #corner_ax = plt.subplot(gs[0:, -1], sharey = big_ax)
    corner_ax = plt.subplot(gs[1:, -2:-1])
    corner_ax_2 = plt.subplot(gs[0, :-2])
    corner_text = plt.subplot(gs[0, -2])

    fontlabel = {'fontname':'DejaVu Sans Mono',
               'color':'black',
               'weight':'bold',
               'size':12}

    # calculate the 2D density of the data given
    bins = 60
    counts, xbins, ybins = np.histogram2d(x, y, bins = bins)
    # make the contour plot
    norm = MidPointNorm(midpoint = 500, vmin = lev.min(), vmax = lev.max())
    cm = big_ax.contourf(counts.T, levels = lev, extent = [xbins.min(), xbins.max(),
                           ybins.min(), ybins.max()], origin = origin, norm = norm,
                           extend = 'both', cmap = cmap)
    big_ax.contour(cm, colors = 'k', linewidths = 0.7, linestyles = 'solid')

    counts, xbins, ybins = np.histogram2d(z, x, bins = bins)

    cmc = corner_ax.contourf(counts.T, levels = levc, extent = [xbins.min(), xbins.max(),
                           ybins.min(), ybins.max()], origin = origin, norm = norm,
                           extend = 'both', cmap = cmap)
    corner_ax.contour(cmc, colors = 'k', linewidths = 0.7, linestyles = 'solid')

    counts, xbins, ybins = np.histogram2d(y, z, bins = bins)

    cmc2 = corner_ax_2.contourf(counts.T, levels = levc, extent = [xbins.min(), xbins.max(),
                           ybins.min(), ybins.max()], origin = origin, norm = norm,
                           extend = 'both', cmap = cmap)
    corner_ax_2.contour(cmc2, colors = 'k', linewidths = 0.7, linestyles = 'solid')

    corner_text.text(0.2, 0.4, "Einasto\n$t = 12$ Gyrs", **fontlabel)

    # Colorbar properties
    xins = inset_axes(corner_ax,
                        width = "10%",  # width = 5% of parent_bbox width
                        height ="100%",  # height : 50%
                        loc = 'lower left',
                        bbox_to_anchor = (1.1, 0., 1, 1),
                        bbox_transform = corner_ax.transAxes,
                        borderpad = 0,
                        )

    # Put colorbar
    #norm = matplotlib.colors.Normalize(vmin = lev[0], vmax = lev[-1])
    sm = matplotlib.cm.ScalarMappable(norm = norm, cmap = cmc.cmap)
    sm.set_array([])
    cbar = fig.colorbar(sm, ticks = np.concatenate((np.linspace(0, 500, 5), np.linspace(500, 10000, 5))), cax = xins)
    cbar.set_label(r'$\Sigma_{N}$', fontsize = 10)
    cbar.ax.tick_params(labelsize = 8)

    # remove ticks
    big_ax.set_xticks([])
    corner_ax.set_xticks([])
    corner_ax_2.set_xticks([])
    corner_text.set_xticks([])
    big_ax.set_yticks([])
    corner_ax.set_yticks([])
    corner_ax_2.set_yticks([])
    corner_text.set_yticks([])

    #big_ax.plot(x, y, 'k.')
    #corner_ax.plot(z, x, 'k.')
    #corner_ax_2.plot(y, z, 'k.')

    lx = 10
    ly = 5
    big_ax.set_xlim(-lx,lx)
    big_ax.set_ylim(-lx,lx)
    corner_ax.set_xlim(-ly,ly)
    corner_ax.set_ylim(-lx,lx)
    corner_ax_2.set_xlim(-lx,lx)
    corner_ax_2.set_ylim(-ly,ly)

    #for ax in axs.flat:
    #    ax.label_outer()
    #    ax.set(adjustable = 'box', aspect = 'equal')

    plt.show()

    plt.clf()

if __name__ == '__main__':
    main()
