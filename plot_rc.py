#!/usr/bin/env python

# Python program to plot the particles of any GADGET-2 snapshot

import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
# from readsnap import readsnap
import readsnap_no_acc  as rsnap
from phase_of_bar import *
from distorsion_parameter import distorsion_parameter

# Main definition
def main():
  # fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/"
  # fname = "/run/media/cronex/Elements/primer-art-rerun/"
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  # fname = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # rerun
  fname = "/media/cronex/ADATA HD330/gas/dm_only/"

  # listsims=["hernquist", "nfw", "iso", "burkert", "einasto"] 
  # listsims=["hernquist"]
  # listsims1=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15"]
  # listsims=["SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"]
  # listsims=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"]
  # listsims=["SGS5"]
  # listsims=["sim_21", "sim_23", "sim_32", "sim_31", "sim_33"]
  # listsims=["sim_42", "sim_41", "sim_43", "sim_52", "sim_51", "sim_53"] 
  # listsims=["sim_15", "sim_25", "sim_35", "sim_45", "sim_55", "sim_65"] 
  listsims = ['12','11','13','14','15','16',
              '22','21','23','24','25','26',
              '32','31','33','34','35','36',
              '42','41','43','44','45','46',
              '52','51','53','54','55','56',
              '62','61','63','64','65','66'] 
  # listsims = ['12', '11']
  # listsims=["21", "23", "32", "31"]
  listsims = ["hernquist", "piso", "burkert", "einasto"]
  lensims = len(listsims)

  # names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  # names=["SGS8", "SGS9", "SGS13", "SGS14", "SGS15"]
  # names=["SGS19", "SGS20", "SGS21", "SGS25", "SGS26", "SGS27"]
  # names=["SGS5", "SGS11", "SGS17", "SGS23", "SGS29", "SGS35"]
  names = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  title=[r'$v_{\mathrm{c}}$',r'$\sigma_{R}$',r'$\sigma_{\mathrm{t}}$',r'$\sigma_{z}$']

  init_radii=res.radii_alt(20,50)
  # rarr = [25, 30, 70, 70, 15]
  
  plt.style.use('seaborn-bright')

  rowp=2 # rows of plot
  colp=2 # columns of plot
  fig,axs=plt.subplots(rowp, colp, sharex=True, sharey=True,
                       gridspec_kw={'hspace': 0.1, 'wspace': 0.08}, dpi=120)

  # Colorbar properties of velocity map
  axb1=rowp-1
  axb2=colp-1
  # axb1=0
  # axb2=0
  xins = inset_axes(axs[axb1,axb2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[axb1,axb2].transAxes,
                    borderpad=0,
                    )                       

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'normal',
             'size':10}
  
  spines = [0, 0, 0, 0, 0, 1,
            0, 0, 0, 0, 1, 1,
            0, 0, 0, 3, 2, 1,
            0, 0, 0, 3, 1, 1,
            0, 0, 0, 3, 2, 1,
            0, 0, 0, 0, 1, 2,] 
  
  isnap=0
  fsnap=240

  ss=120
  num_snaps = 2

  ts = 0.05 # timestep of snapshot
  nlines = np.linspace(isnap, fsnap, num_snaps).astype(int)
  time = nlines*ts
  print(nlines)

  norm = mpl.colors.Normalize(vmin=time.min(), vmax=time.max())
  cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
  cmap.set_array([])
  
  row=0 # rows of plot
  col=0 # columns of plot
  for k in range(lensims):
    # frun = fname + listsims[k] + "/" # location of the file
    # frun = fname + listsims[k] + "/output/" # location of the file
    # frun = fname + "sim_" + listsims[k] + "/" # location of the file
    frun = fname + listsims[k] + "/output/" # location of the file
    # long_bar = 10 # bar longitude for first column
    # col=0
    
    # bar longitude
    # bl_file = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/all/bar_length/bar_length_"+names[k]+".txt"
    # bdat = data(bl_file)
    # print(bdat)
    # tb = bdat[0]
    # lb = bdat[1]
    # tb = np.append(tb, tb[-1])
    # lb = np.append(lb, lb[-1])

    for i in nlines:
      ext=res.snap_mask(i,3)
      dd = rsnap.readsnap(frun, ext, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      # x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x, y, z, vx, vy, vz = pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2]

      print("\n============== Plotting rotation curve simulation %s (%s of %s) - snapshot %s ==============\n"
            % (names[k], str(k+1), str(lensims), ext))

      pr=res.particle_radius(x,y)
      pv=res.velocities(x,y,vx,vy,vz)

      values=(pr,) + pv

      # init_radii=res.radii_alt(rarr[k],50)
      newr,avg=res.avg_velocities(values,init_radii)
      vr=avg[0]
      vt=avg[1]
      vz=avg[2]
      dr=avg[3]
      dt=avg[4]
      dz=avg[5]
      

      # eta = []
      # r = []
      # for j in range(len(init_radii)-1):
      #   nx = []
      #   ny = []
      #   for m in range(len(x)):
      #     nr = np.sqrt(x[m]**2 + y[m]**2)
      #     if init_radii[j] <= nr <= init_radii[j+1]:
      #       nx.append(x[m])
      #       ny.append(y[m])
      #   nx = np.asarray(nx)
      #   ny = np.asarray(ny)
      #   eta.append(distorsion_parameter(nx, ny))
      #   r.append((init_radii[j+1] + init_radii[j])/2)
      
      # eta = np.asarray(eta)
      # r = np.asarray(r)

      # ietamax = np.argmax(eta)

      # uplimi = 0
      # for n in range(ietamax, len(eta)):
      #   if eta[n] < 0.1*eta[ietamax]:
      #     uplimi = n
      #     break
      
      ''' Calculating bar length in-place '''
      # print("\n############# Calculating length of bar #############\n")
      # r, A2, ph = phase_of_bar_annuli_2d(x, y, init_radii)

      # bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      # ph_thres = []
      # final_ph = ph_thres # final values of phase
      # lenph = 0 # length of phase
      # indph = 0 # index of current phase
      # baseph = ph[0] # reference phase
      # for j in range(len(ph)):
      #   if np.abs(ph[j]-baseph) < bt:
      #     ph_thres.append(ph[j]) 
      #     if len(ph_thres) > lenph: # phase remains semi-constant
      #       final_ph = ph_thres
      #       lenph = len(final_ph)
      #       indph = j
      #   else:
      #     ph_thres = [] # empty phase
      #     baseph = ph[j] # drag new reference phase
      
      # avgph = np.mean(final_ph)
      # print(final_ph, avgph)
      # ir = list(ph).index(ph[indph])

      # axs[row,col].plot(newr,avg[1],'-', color=cmap.to_rgba((i-1)*tstamp), lw = 1.0)
      # axs[row,col].text(15.5, 10, names[k], **fontlabel)
      # axs[row,col].set_xlabel("Radius [kpc]",fontsize=10)
      # axs[row,col].set_ylabel("$v_{\mathrm{c}}$ [km s$^{-1}$]",fontsize=10)
      # axs[row,col].plot((r[ir+1],r[ir+1]),(0,15),'-', color=cmap.to_rgba((i-1)*tstamp), linewidth=2.1)
      #long_bar = long_bar + 5
      ''' Calculating bar length in-place '''
      
      ''' Calculating bar length using measurement from file '''
      axs[row,col].plot(newr,vt,'-', color=cmap.to_rgba(i*ts), lw = 1.0)
      # axs[row,col].plot(newr,dz,'-', color=cmap.to_rgba((i-1)*tstamp), lw = 1.0)
      # axs[row,col].plot(newr,dz,'-', color=cmap.to_rgba((i-1)*tstamp), lw = 1.0)
      # axs[row,col].plot(newr,dz,'-', color=cmap.to_rgba((i-1)*tstamp), lw = 1.0)
      axs[row,col].text(15.6, 150, names[k], **fontlabel)
      # axs[row,col].set_xlabel("Radius [kpc]",fontsize=10)
      # axs[row,col].set_ylabel("$v_{\mathrm{c}}$ [km s$^{-1}$]",fontsize=10)
      # axs[row,col].set_ylabel("$\sigma_{\mathrm{z}}$ [km s$^{-1}$]",fontsize=10)
      # axs[row,col].set_ylabel("$\sigma_{\mathrm{z}}$ [km s$^{-1}$]",fontsize=10)
      # axs[row,col].set_ylabel("$\sigma_{\mathrm{z}}$ [km s$^{-1}$]",fontsize=10)
      # axs[row,col].plot((lb[i],lb[i]),(0,18),'-', color=cmap.to_rgba(i*ts), linewidth=2.1)
      ''' Calculating bar length using measurement from file '''
      
      # coloring of spines
      # lw = 1.5
      # for side in axs[row,col].spines.keys():
      #   if spines[k] == 0: # barred 
      #     axs[row,col].spines[side].set_color('deepskyblue')
      #     axs[row,col].spines[side].set_linewidth(lw)
      #   elif spines[k] == 1: # transient
      #     axs[row,col].spines[side].set_color('gold')
      #     axs[row,col].spines[side].set_linewidth(lw)
      #   elif spines[k] == 2: # completely unbarred
      #     axs[row,col].spines[side].set_color('orangered')
      #     axs[row,col].spines[side].set_linewidth(lw)
      #   elif spines[k] == 3: # delayed bar
      #     axs[row,col].spines[side].set_color('limegreen')
      #     axs[row,col].spines[side].set_linewidth(lw)

      # if listsims[k] == 'einasto':
      #   axs[row,col].text(0.3,310,names[k],**fontlabel)
      # else:
      #   axs[row,col].text(0.3,190,names[k],**fontlabel)
      # axs[row,col].set_xlabel("Radius [kpc]")
      # axs[row,col].set_ylabel("$v_{\mathrm{c}} \,\,\mathrm{[km\,\,s^{-1}]}$",fontsize=12)
      # if row == 0:
      #   axs[row,col].set_title(title[0])

      # for j in range(1,colp):
      #   # axs[k,j].plot(fin_radii,avg[j+3],'-',label=r'$t = %s \,\,\mathrm{[Gyrs]}$' %(float(ext)*0.6))
      #   axs[row,j].plot(newr,avg[j+2],'-',color=cmap.to_rgba((i-1)*tstamp))
      #   axs[row,j].set_xlabel("Radius [kpc]",fontsize=10)
      #   axs[row,j].set_ylabel("%s [km s$^{-1}$]" % (title[j]),fontsize=10)
      #   # if row == 0:
      #   #   axs[row,j].set_title(title[j])
      #   col = j

      # Put colorbar
      if(row == axb1 and col == axb2 and i == nlines[-1]):
        cbar=fig.colorbar(cmap,cax=xins,ticks=time)
        cbar.set_label(r'Time [Gyrs]',fontsize=8)
        cbar.ax.tick_params(labelsize=6.7)

      #if m == k == 0:
      #  axs[k,m].legend(loc='lower right',prop={'size':6},shadow=True)

    #   col += 1
    #   if col % colp == 0:
    #     # row += 1
    #     col = 0
    # row += 1
    col += 1
    if col % colp == 0:
      col = 0
      row += 1
    

  # aux = len(axs.flat)-4
  # n = 0
  for ax in axs.flat:
    ax.grid()
    # ax.label_outer()
    # if n == aux:
    #   ax.set_ylim(0,350)
    # elif n > aux:
    #   ax.set_ylim(0,250)
    # elif n % 4 == 0 and n != aux:
    #   ax.set_ylim(0,220)
    # else:
    #   ax.set_ylim(0,150)
    ax.set_xlim(0,20)
    ax.set_ylim(0,220)
    ax.set_xticks([0, 5, 10, 15, 20])
    ax.set_yticks([0, 50, 100, 150, 200])
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=10)
    # n = n + 1

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel('Radius [kpc]',fontsize=10)
  plt.ylabel(r'$v_{\mathrm{c}}\,\,\mathrm{[km\,\,s^{-1}]}$',fontsize=10, labelpad=1)

  # plt.tight_layout()
  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
