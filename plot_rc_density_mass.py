#!/usr/bin/env python

# Script to plot the rotation curve of IC
# of simulation 'sim_11'

from den_map import data

import matplotlib.pyplot as plt
import numpy as np

import resonances as res
from plot_den_maps import adjust_center_mass
#from readsnap import readsnap
from readsnap_no_acc import readsnap
from mass_assignment_secant_method import volumetric_density, mass_dist

def main():
  # filename=["/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/hernquist/output/",
  #           "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/nfw/output/",
  #           "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/iso/output/",
  #           "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/burkert/output/",
  #           "/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/einasto/output/"]
  
  filename=["/home/cronex/Documentos/Doctorado/GALIC-master-mod-halos/test/density-test-hern/",
            "/home/cronex/Documentos/Doctorado/GalIC/test/hernquist/"]
  
  prof = ["Hernquist", "NFW", "PISO", "Burkert", "Einasto"]
  concentration = ['10.0', '4.114', '1.917', '2.059', '34.703']

  plt.style.use('seaborn-bright') # seaborn plottin style

  fig,axs=plt.subplots(5, 3, gridspec_kw={'hspace':0.4, 'wspace':0.3}) # axes object for plotting
  
  rmax=160
  init_radii=np.linspace(0,rmax,151)

  md = 0.035
  mt = 95.24
  massh = mt * (1-md)
  Nh = 50000.0
  jump = 1
  mph = massh/Nh*jump

  for i in range(len(filename)):
    print("\n=================== Data from file %s of %s ===================\n" % (str(i+1), str(len(filename))))

    # Disk data
    dd = readsnap(filename[i], 0, 2)

    pd = dd['p']
    vd = dd['v']

    xd, yd, zd, vdx, vdy, vdz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])

    # Halo data
    if i == 1:
      dh = readsnap(filename[i], 10, 1)
    else:
      dh = readsnap(filename[i], 0, 1)


    ph = dh['p']
    vh = dh['v']
    
    xh, yh, zh, vhx, vhy, vhz = adjust_center_mass(ph[:, 0], ph[:, 1], ph[:, 2], vh[:, 0], vh[:, 1], vh[:, 2])

    # Rotation curve
    rad=np.sqrt(xd**2 + yd**2)

    vel=res.velocities(xd, yd, vdx, vdy, vdz)
    values=(rad,) + vel

    newr, avg=res.avg_velocities(values,init_radii)

    for k in range(len(avg[1])):
      if avg[1][k] == 0:
        veli = k
        break
    print(newr,avg[1])
    
    # Density and mass of halo
    rm, mass = mass_dist(xh[::jump], yh[::jump], zh[::jump], mph, rmax=rmax, shells=50)
    rd, den = volumetric_density(xh, yh, zh, rmax=rmax, shells=100)

    # if avg[1].max() > maxavg:
    #   maxavg = avg[1].max()
    # if mass.max() > maxmass:
    #   maxmass = mass.max()
    # if den.max() > maxden:
    #   maxden = den.max()

    # Plotting density
    axs[i, 0].plot(newr[:veli],avg[1][:veli],'k-',label='Total')
    axs[i, 0].set_xlabel("Radius [kpc]")
    axs[i, 0].set_ylabel(r'$v_{\mathrm{c}}$ [km s$^{-1}$]')

    # Plotting cumulative mass
    axs[i, 1].plot(rm,mass,'k-',label='Total')
    axs[i, 1].set_xlabel("Radius [kpc]")
    axs[i, 1].set_ylabel(r'Mass [$10^{10} M_{\odot}$]')

    # Plotting rotation curve
    axs[i, 2].plot(rd,den,'k-',label='Total')
    axs[i, 2].set_xlabel("Radius [kpc]")
    axs[i, 2].set_ylabel(r'$\rho$ [$M_{\odot}$ kpc$^{-3}$]')
    axs[i, 2].text(rd.max()+10, den.max()/2., prof[i], fontsize=12)
    axs[i, 2].set_xscale('log')
    axs[i, 2].set_yscale('log')
    if i < len(filename)-1:
      axs[i, 2].text(rd.max()*0.8, den.max()*0.8, '$c = %s$'%(concentration[i]), fontsize=9)
    if i == len(filename)-1:
      axs[i, 2].text(rd.max()*0.8, den.max()*0.7, '$c = %s$\n$\gamma = 0.17$'%(concentration[i]), fontsize=9)
 
    #axs.plot(r,vch,'--',label='Halo')
    #axs.plot(r,vcd,'-.',label='Disc')

  # Parameters for ticks
  # ylimit = [maxavg, maxmass, maxden]
  # k = 0
  for ax in axs.flat:
    # ax.set_xlim(0,150)
    # ax.set_ylim(0,ylimit[k])
    ax.grid(True)
    ax.tick_params(axis='both',which='both',direction='in',
                  top=True,left=True,right=True,bottom=True,
                  labelsize=10)
    # k = k + 1
    # if k % 3 == 0:
    #   k = 0

  # Legends assiged in labels when plotting
  # axs.legend(loc='lower right',shadow=True)

  # axs.set_xlabel("Radius [kpc]")
  # axs.set_ylabel(r"$v_{c}$")

  plt.show()

  plt.clf()

if __name__ == '__main__':
  main()
