#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors
import numpy as np
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from readsnap import readsnap
from phase_of_bar import *

def main():
  # fname = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  # fname = "/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # fname="/home/cronex/Documentos/Doctorado/gadget4/sims-predoc/python/"
  # fname="/home/cronex/Documentos/Doctorado/gizmo-public/sims/gas/dm_only/"
  fname="/media/cronex/ADATA HD330/gas/dm_only/"

  # listsims=["hernquist", "nfw", "iso", "bur", "ein"] 
  listsims=["hernquist", "piso", "burkert", "einasto"] 
  # listsims=["einasto"]
  # listsims=["11"]
  lensims = len(listsims)

  names=["Hernquist", "PISO", "Burkert", "Einasto"] 

  plt.style.use('seaborn-bright')
  #ls = ['solid', 'dashed', 'dotted', 'dashdot']
  ls = ['-', '--', '-.', ':', 'x', '*']

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':9}
  
  # fig = plt.figure(figsize = (12, 12))
  # gs = gridspec.GridSpec(2, 3)

  # hern = plt.subplot(gs[0, 0])
  # nfw = plt.subplot(gs[0, 1])
  # iso = plt.subplot(gs[0, 2])
  # bur = plt.subplot(gs[1, 0])
  # ein = plt.subplot(gs[1, 1])

  # axes = [hern, nfw, iso, bur, ein]
  rmax = 20.0
  fig, axs = plt.subplots(2,2,dpi=120)
  
  row = col = 0
  for k in range(lensims):
    # frun = fname + "rotcurve_" + listsims[k] + ".txt" # location of the file
    frun = fname + listsims[k] + "/rotcurve.txt" # location of the file
    # frun = fname + "sim_" + listsims[k] + "/rotcurve.txt" # location of the file

    dat = data(frun)
    r, vct, vch, vcd = dat[0], dat[1], dat[2], dat[3]

    for i in range(len(r)):
      if r[i] >= rmax:
        ind = i
        break
    
    axs[row, col].plot(r[:i], vct[:i], 'b-', label="Total")
    axs[row, col].plot(r[:i], vch[:i], 'g--', label="Halo")
    axs[row, col].plot(r[:i], vcd[:i], 'r-.', label="Disk")

    if k == 0:
      axs[row, col].legend(loc = "lower right", ncol = 1, shadow=True) 

    axs[row, col].text(3, 15, names[k])
    
    col += 1
    if col == 2:
      row += 1
      col = 0

  for ax in axs.flat:
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 250)
    ax.grid(True)
    # ax.set_xlabel("Radius [kpc]", fontsize=16)
    # ax.set_ylabel(r'$v_{\mathrm{c}}$ [km s$^{-1}$]', fontsize=16)
    ax.tick_params(axis='both',which='both',direction='in',
                  top=True,left=True,right=True,bottom=True,
                  labelsize=11)
  
  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel("Radius [kpc]", fontsize=16)
  plt.ylabel(r'$v_{\mathrm{c}}$ [km s$^{-1}$]', fontsize=16)
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()