import numpy as np

def data_for_cylinder_along_z(center_x,center_y,radius,height_z):
  z = np.linspace(0, height_z, 50)
  theta = np.linspace(0, 2*np.pi, 50)
  theta_grid, z_grid=np.meshgrid(theta, z)
  x_grid = radius*np.cos(theta_grid) + center_x
  y_grid = radius*np.sin(theta_grid) + center_y
  return x_grid,y_grid,z_grid

def rdf_3d(x, y, z, rMax, dr, ids):
  r = np.sqrt(x**2 + y**2 + z**2)
  ri = np.where(r <= rMax)
  x = x[ri[0]]
  y = y[ri[0]]
  z = z[ri[0]]
  id = ids[ri[0]]

  edges = np.arange(0., rMax + 1.1 * dr, dr)
  vol = 4*np.pi*rMax**3/3
  numberDensity = len(x)/vol

  num_increments = len(edges) - 1
  g = np.zeros([len(x), num_increments])
  radii = np.zeros(num_increments)

  # Compute pairwise correlation for each interior particle
  for p in range(len(x)):
    d = np.sqrt((x[p] - x)**2 + (y[p] - y)**2 + (z[p] - z)**2)
    #d[p] = 0
    d[p] = 2*rMax

    (result, bins) = np.histogram(d, bins=edges, normed=False)
    g[p, :] = result/numberDensity

  # Average g(r) for all interior particles and compute radii
  g_average = np.zeros(num_increments)
  for i in range(num_increments):
    radii[i] = (edges[i] + edges[i+1]) / 2.
    rOuter = edges[i + 1]
    rInner = edges[i]
    g_average[i] = np.mean(g[:, i]) / (4.0 / 3.0 * np.pi * (rOuter**3 - rInner**3))

  return g_average, radii, id

def hist_density_3d(x, y, z, rMax, dr):
  r = np.sqrt(x**2 + y**2 + z**2)
  ri = np.where(r <= rMax)
  x = x[ri[0]]
  y = y[ri[0]]
  z = z[ri[0]]

  edges = np.arange(0., rMax + 1.1 * dr, dr)
  vol = 4*np.pi*rMax**3/3
  numberDensity = len(x)/vol

  num_increments = len(edges) - 1
  g = np.zeros([len(x), num_increments])
  radii = np.zeros(num_increments)

  # Compute pairwise correlation for each interior particle
  for p in range(len(x)):
    d = np.sqrt((x[p] - x)**2 + (y[p] - y)**2 + (z[p] - z)**2)
    d[p] = 2*rMax

    (result, bins) = np.histogram(d, bins=edges, normed=False)
    g[p, :] = result/numberDensity
  
  num_den = np.zeros(num_increments)
  for i in range(num_increments):
    radii[i] = (edges[i] + edges[i+1]) / 2.
    num_den[i] = np.mean(g[:, i])
  
  return num_den, radii

def hist_density_3d_ngb(x, y, z, rMax, dr, limit_ngb):
  r = np.sqrt(x**2 + y**2 + z**2)
  ri = np.where(r <= rMax)
  x = x[ri[0]]
  y = y[ri[0]]
  z = z[ri[0]]

  edges = np.arange(0., rMax + 1.1 * dr, dr)
  vol = 4*np.pi*rMax**3/3
  numberDensity = len(x)/vol

  num_increments = len(edges) - 1
  g = np.zeros([len(x), num_increments])
  radii = np.zeros(num_increments)

  # Compute pairwise correlation for each interior particle
  for p in range(len(x)):
    rp = np.sqrt(x[p]**2 + y[p]**2 + z[p]**2)
    #d = np.array([np.sqrt((x[p] - x[i])**2 + (y[p] - y[i])**2 + (z[p] - z[i])**2) for i in range(len(x)) if np.sqrt((x[p] - x[i])**2 + (y[p] - y[i])**2 + (z[p] - z[i])**2) < limit_ngb])
    d = []
    for i in range(len(x)):
      nd = np.sqrt((x[p] - x[i])**2 + (y[p] - y[i])**2 + (z[p] - z[i])**2)
      if nd < rp+limit_ngb and rp+limit_ngb < rMax:
        d.append(nd)
    print(np.mean(d))
    d = np.array(d)
    #d[p] = 2*rMax

    (result, bins) = np.histogram(d, bins=edges, normed=False)
    g[p, :] = result/numberDensity
  
  num_den = np.zeros(num_increments)
  for i in range(num_increments):
    radii[i] = (edges[i] + edges[i+1]) / 2.
    num_den[i] = np.mean(g[:, i])
  
  return num_den, radii

def hist_density_3d_ngb_cyl(x, y, z, rMax, dr, limit_ngb):
  r = np.sqrt(x**2 + y**2 + z**2)
  ri = np.where(r <= rMax)
  x = x[ri[0]]
  y = y[ri[0]]
  z = z[ri[0]]

  edges = np.arange(0., rMax + 1.1 * dr, dr)
  vol = 4*np.pi*rMax**3/3
  numberDensity = len(x)/vol

  num_increments = len(edges) - 1
  g = np.zeros([len(x), num_increments])
  radii = np.zeros(num_increments)

  # Compute pairwise correlation for each interior particle
  for p in range(len(x)):
    #d = np.array([np.sqrt((x[p] - x[i])**2 + (y[p] - y[i])**2 + (z[p] - z[i])**2) for i in range(len(x)) if np.sqrt((x[p] - x[i])**2 + (y[p] - y[i])**2 + (z[p] - z[i])**2) < limit_ngb])
    d = []
    for i in range(len(x)):
      nd = np.sqrt((x[p] - x[i])**2 + (y[p] - y[i])**2 + (z[p] - z[i])**2)
      if nd < limit_ngb:
        d.append(nd)
    print(np.mean(d))
    d = np.array(d)
    #d[p] = 2*rMax

    (result, bins) = np.histogram(d, bins=edges, normed=False)
    g[p, :] = result/numberDensity
  
  num_den = np.zeros(num_increments)
  for i in range(num_increments):
    radii[i] = (edges[i] + edges[i+1]) / 2.
    num_den[i] = np.mean(g[:, i])
  
  return num_den, radii

