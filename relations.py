#!/usr/local/bin/ python

# Plot the relations between the parameters m_d and softening and some of the bars properties

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
from readsnap_no_acc import readsnap

def main():
  #feta="/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/" # original
  fdato="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/original/"
  fdatr="/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/"

  ''' models with identical mass '''
  #models=['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']
  # models=['SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12']
  models=[['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6'],
          #['SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12'],
          ['SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18'],
          #['SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24'],
          ['SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30'],
          ['SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']]
  #modelso=['12', '11', '13', '14', '15', '16']
  # modelso=['22', '21', '23', '24', '25', '26']
  modelso=[['12', '11', '13', '14', '15', '16'],
           #['22', '21', '23', '24', '25', '26'],
           ['32', '31', '33', '34', '35', '36'],
           #['42', '41', '43', '44', '45', '46'],
           ['52', '51', '53', '54', '55', '46'],
           ['62', '61', '63', '64', '65', '66']]
  mrange=[0.035, 0.041, 0.047, 0.050]
  

  ''' models comparing two different values of mass'''
  models1=['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6']
  #models2=['SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12']
  #models2=['SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18']
  # models2=['SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24']
  # models2=['SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30']
  models2=['SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  

  ''' models with identical softening '''
  #models=['SGS1', 'SGS7', 'SGS13', 'SGS19', 'SGS25', 'SGS31']
  #models=['SGS2', 'SGS8', 'SGS14', 'SGS20', 'SGS26', 'SGS32']
  #models=['SGS3', 'SGS9', 'SGS15', 'SGS21', 'SGS27', 'SGS33']
  #models=['SGS4', 'SGS10', 'SGS16', 'SGS22', 'SGS28', 'SGS34']
  #models=['SGS5', 'SGS11', 'SGS17', 'SGS23', 'SGS29', 'SGS35']
  #models=['SGS6', 'SGS12', 'SGS18', 'SGS24', 'SGS30', 'SGS36']
  models=[['SGS1', 'SGS7', 'SGS13', 'SGS19', 'SGS25', 'SGS31'],
          #['SGS2', 'SGS8', 'SGS14', 'SGS20', 'SGS26', 'SGS32'],
          ['SGS3', 'SGS9', 'SGS15', 'SGS21', 'SGS27', 'SGS33'],
          #['SGS4', 'SGS10', 'SGS16', 'SGS22', 'SGS28', 'SGS34'],
          ['SGS5', 'SGS11', 'SGS17', 'SGS23', 'SGS29', 'SGS35'],
          ['SGS6', 'SGS12', 'SGS18', 'SGS24', 'SGS30', 'SGS36']]
  #modelso=['12', '22', '32', '42', '52', '62']
  #modelso=['11', '21', '31', '41', '51', '61']
  #modelso=['13', '23', '33', '43', '53', '63']
  #modelso=['14', '24', '34', '44', '54', '64']
  #modelso=['15', '25', '35', '45', '55', '65']
  #modelso=['16', '26', '36', '46', '56', '66']
  modelso=[['12', '22', '32', '42', '52', '62'],
           #['11', '21', '31', '41', '51', '61'],
           ['13', '23', '33', '43', '53', '63'],
           #['14', '24', '34', '44', '54', '64'],
           ['15', '25', '35', '45', '55', '65'],
           ['16', '26', '36', '46', '56', '66']]
  srange=[0.1, 0.025, 0.005, 0.001]
  
  
  mass=[0.035, 0.038, 0.041, 0.044, 0.047, 0.050]
  soft=[0.1, 0.05, 0.025, 0.01, 0.005, 0.001]
  
  ncol = 2
  nrow = 2
  # fig,axs=plt.subplots(nrow, ncol, sharex=True, sharey=True,
  #                      gridspec_kw={'hspace': 0.2, 'wspace': 0.05}, dpi = 130)
  fig, axs = plt.subplots()
  plt.style.use('seaborn-bright')
  
  row = col = 0
  # for i in range(len(models)):
  for model in models1:
    ''' models with identical mass (eta)'''
    dato=data(fdato+"/all/eta/eta_"+model+".txt")
    datr=data(fdatr+"/all/eta/eta_"+model+".txt")
    etao=dato[1]
    etar=datr[1]
    to = dato[0]
    tr = datr[0]
    t_ntho = to[np.where(etao >= 0.02)[0]][0]
    t_nthr = tr[np.where(etar >= 0.02)[0]][0]

    var=soft[models1.index(model)]

    # axs.plot(var, etao[-1], '+', color='orangered')
    # axs.plot(var, etar[-1], 'x', color='dodgerblue')
    axs.plot(var, t_ntho, '+', color='orangered')
    axs.plot(var, t_nthr, 'x', color='dodgerblue')
    
    ''' models with identical mass (force)'''
    # dato=data(fdato+"/all/strength/strength_"+modelso[models.index(model)]+".txt")
    # datr=data(fdatr+"/all/strength/strength_"+model+".txt")
    # stro=dato[1]
    # strr=datr[1]

    # axs.plot(var, stro[-1], '+', color='orangered')
    # axs.plot(var, strr[-1], 'x', color='dodgerblue')
    # for model in models[i]:
    #   dat1=data(fdato+"all/strength/strength_"+modelso[i][models[i].index(model)]+".txt")
    #   dat2=data(fdatr+"all/strength/strength_"+model+".txt")
    #   str1=dat1[1]
    #   str2=dat2[1]

    #   var=soft[models[i].index(model)]      
      
    #   axs[row, col].plot(var, str1[-1], 'o', markersize = 6.3, color='orangered', label = 'low res')
    #   axs[row, col].plot(var, str2[-1], '*', markersize = 7.9, color='dodgerblue', label = 'high res')
    #   # axs[row, col].set_ylim(0, 0.8)
    #   axs[row, col].set_title("$m_d = {0}$".format(mrange[i]))
    #   if models[i].index(model) == 0 and i == 0:
    #     leg = axs[row, col].legend(loc = 'lower right')
    #     for hand in leg.legendHandles:
    #       hand._legmarker.set_markersize(8)
      
    # col += 1
    # if col % ncol == 0:
    #   col = 0
    #   row += 1

    ''' models comparing two adjacent values of mass (force)'''
    # dat1=data(fdatr+"all/strength/strength_"+model+".txt")
    # dat2=data(fdatr+"all/strength/strength_"+models2[models1.index(model)]+".txt")
    # str1=dat1[1]
    # str2=dat2[1]

    # var=soft[models1.index(model)]
    
    # axs.plot(var, str1[-1], '+', color='orangered')
    # axs.plot(var, str2[-1], 'x', color='dodgerblue')

    ''' models with identical softening (force)'''
    # for model in models[i]:
    #   dat1=data(fdato+"all/strength/strength_"+modelso[i][models[i].index(model)]+".txt")
    #   dat2=data(fdatr+"all/strength/strength_"+model+".txt")
    #   str1=dat1[1]
    #   str2=dat2[1]

    #   var=mass[models[i].index(model)]
      
    #   axs[row, col].plot(var, str1[-1], 'o', markersize = 6.3, color='orangered', label = 'low res')
    #   axs[row, col].plot(var, str2[-1], '*', markersize = 7.9, color='dodgerblue', label = 'high res')
    #   # axs[row, col].set_ylim(0, 0.8)
    #   axs[row, col].set_title("$\epsilon = {0}$".format(srange[i]))
    #   if models[i].index(model) == 0 and i == 0:
    #     leg = axs[row, col].legend(loc = 'lower right')
    #     for hand in leg.legendHandles:
    #       hand._legmarker.set_markersize(8) 
      
    col += 1
    if col % ncol == 0:
      col = 0
      row += 1
      
  
  # for ax in axs.flat:  
  #   ax.grid(False)
  #   ax.label_outer()
  #   # ax.set_xlim(0, np.max(mass))
  #   ax.set_ylim(0, 0.8)
  #   ax.tick_params(axis = 'both', which = 'both', direction = 'in',
  #                   top = True, left = True, right = True, bottom = True,
  #                   labelsize = 7)

  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  # plt.xlabel('$\epsilon$ [kpc]', fontsize = 13)
  plt.xlabel('$m_{d}$', fontsize = 13)
  plt.ylabel("$A_{2}$", fontsize = 13, labelpad = 2)
  
  plt.show()
  plt.clf()


if __name__ == '__main__':
  main()