#!/usr/bin/env python

# Script to estimate the Linblad resonances in any snapshot

import numpy as np
#import matplotlib.pyplot as plt
import math
import scipy.stats as st
#import matplotlib.mlab as mlab
#import seaborn as sns
from scipy.interpolate import griddata

#from den_map import data
import readsnap_no_acc as rsa
from plot_den_maps import adjust_center_mass
#from phase_of_bar import *

# Returns array with the number of desired rings
def radii(max_r,rings):
  rad=np.linspace(0.0,float(max_r),rings)

  rad_frac=0.001
  # Insert a small value at rad[1]
  rad_ext=np.concatenate((rad[:1],[(float(max_r)/(rings+1))*rad_frac],rad[1:]))

  return rad_ext

# Alternative of function 'radii'
def radii_alt(max_r,rings):
  return np.linspace(0.0,float(max_r),rings+1)

# Function to create a string of characters
# corresponding to GADGET's snapshot mask
def snap_mask(char,length=3):
  ext='0'*length
  ext=ext[0:-1]+str(char)
  return ext[-length:]

# Returns array with the radius of every particle
def particle_radius(x,y):
  return np.sqrt(x**2 + y**2)

# Returns the velocities (tangential, radial and z) of every particle
def velocities(x,y,vx,vy,vz):
  r=np.sqrt(x**2 + y**2)

  # Tangential valocity
  dtan=(1/(x**2 + y**2))*(x*vy - y*vx)
  tan_vel=r*dtan

  # Radial velocity
  vel_exp=x*vx + y*vy
  rad_vel=vel_exp/r

  # Z velocity
  z_vel=vz

  return tan_vel,rad_vel,z_vel

# Routine to calculate the average velocity for every ring
def avg_velocities((rad_arr,tan_arr,radial_arr,z_arr),radii):
  length=len(rad_arr)
  # Compare length in tuple
  if any(len(arr) != length for arr in [tan_arr,radial_arr,z_arr]):
    print("First argument (tuple) in avg_velocities must be of equal dimensions.")
  else:
    new_rad_arr=[] # new array for radius

    vel_avgt=[] # arrays with all the average velocities of every ring
    vel_avgr=[]
    vel_avgz=[]

    sigma_avgt=[] # arrays with all the average dispersions of every ring
    sigma_avgr=[]
    sigma_avgz=[]
    for i in range(len(radii)-1):
      vel_rt=[]
      vel_rr=[]
      vel_rz=[]

      for j in range(len(rad_arr)):
        if radii[i] <= rad_arr[j] <= radii[i+1]:
          vel_rt.append(tan_arr[j])
          vel_rr.append(radial_arr[j])
          vel_rz.append(z_arr[j])

      if len(vel_rt):
        vel_rt=np.asarray(vel_rt)
        vel_avgt.append(np.mean(vel_rt)) # average tangential velocity in ring
        sigmat=(vel_rt-np.mean(vel_rt))**2
        sigma_avgt.append(np.sqrt(np.mean(sigmat))) # average tangential dispersion in ring
      else:
        vel_avgt.append(0)
        sigma_avgt.append(0)
      if len(vel_rr):
        vel_rr=np.asarray(vel_rr)
        vel_avgr.append(np.mean(vel_rr)) # average radial velocity in ring
        sigmar=(vel_rr-np.mean(vel_rr))**2
        sigma_avgr.append(np.sqrt(np.mean(sigmar))) # average radial dispersion in ring
      else:
        vel_avgr.append(0)
        sigma_avgr.append(0)
      if len(vel_rz):
        vel_rz=np.asarray(vel_rz)
        vel_avgz.append(np.mean(vel_rz)) # average vertical velocity in ring
        sigmaz=(vel_rz-np.mean(vel_rz))**2
        sigma_avgz.append(np.sqrt(np.mean(sigmaz))) # average vertical dispersion in ring
      else:
        vel_avgz.append(0)
        sigma_avgz.append(0)

      new_rad_arr.append((radii[i]+radii[i+1])/2)

    velocities=(np.asarray(vel_avgr),np.asarray(vel_avgt),np.asarray(vel_avgz),
                np.asarray(sigma_avgr),np.asarray(sigma_avgt),np.asarray(sigma_avgz))

  return np.asarray(new_rad_arr),velocities

# Returns new forms for radii and omega to estimate
# kappa and resonances
def omegar_fun(radii,vel):
  if len(radii) != len(vel):
    print("Arguments in function omegar_fun must have the same length and dimensions.")
    exit()

  rad=[]
  radn=[]
  omega=[]
  omegan=[]
  for i in range(len(radii)-1):
    r=radii[i]
    rn=radii[i+1]

    cv=vel[i] # Circular velocities
    cvn=vel[i+1]

    omega.append(cv/r)
    omegan.append(cvn/rn)
    rad.append(r)
    radn.append(rn)

  rad,radn=np.asarray((rad,radn))
  omega,omegan=np.asarray((omega,omegan))

  return rad,radn,omega,omegan

# Function to calculate epicyclic frequency (kappa)
def kappa((r,rn,omega,omegan)):
  length=len(r)
  if any(len(val) != length for val in [rn,omega,omegan]):
    print("Arguments in function kappa must have the same length and dimensions.")
    exit()

  kappa2 = []
  for i in range(length):
     kappa2.append(r[i]*((omegan[i]**2+omega[i-1]**2-2*omega[i]**2)/(rn[i]-r[i-1])**2) + 4*omega[i]**2)

  # kappa2=r*((omegan**2-omega**2)/(rn-r)**2) + 4*omega**2 # kappa^2

  if any(k < 0 for k in kappa2):
    print("Unvalid velocity structure. Negative kappa^2")
    exit()

  return np.sqrt(kappa2)

# Calculates all the angular velocities
def omegas(omega,kappa):
  if len(omega) != len(kappa):
    print("Arguments in function omegas must have the same length and dimensions.")
    exit()

  omegailr=omega-kappa/2
  omegaolr=omega+kappa/2
  omegacr=omega

  return omegailr,omegaolr,omegacr

# Function to draw density contours
def density_estimation(x,y,nbins):
  k=st.kde.gaussian_kde(np.vstack([x,y]))
  xi,yi=np.mgrid[x.min():x.max():nbins*1j,y.min():y.max():nbins*1j]
  #xgrid=np.linspace(0,30,60)
  #ygrid=np.linspace(-10,60,60)
  #xi,yi=np.meshgrid(xgrid,ygrid)
  zi=k.evaluate(np.vstack([xi.ravel(),yi.ravel()]))
  return xi,yi,zi

# Alternative to plot density contours
def plot_contour(x,y,resolution = 50,contour_method='linear'):
    resolution = str(resolution)+'j'
    X,Y = np.mgrid[min(x):max(x):complex(resolution), min(y):max(y):complex(resolution)]
    points = [[a,b] for a,b in zip(x,y)]
    #counts,xbins,ybins=np.histogram2d(X,Y,bins=60)
    Z = griddata(points, counts.T, (X, Y), method=contour_method)
    return X,Y,Z

def surf_density(rad, radii, mp):
  nc = []
  nrad = []
  for i in range(len(radii)-1):
    nump = len(rad[(rad >= radii[i])*(rad < radii[i+1])])
    area = np.pi*(radii[i+1]**2-radii[i]**2)
    nc.append(nump*mp/area)
    nrad.append((radii[i]+radii[i+1])/2)
  return np.array(nrad), np.array(nc)

def main():
  fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/" # Main directory
  #fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"

  #sims=(('sim_11/','SGS2'),('sim_32/','SGS13'),('sim_66/','SGS36')) # tuple of simulations
  #sims=(('sim_11/','SGS2'),('sim_66/','SGS36')) # tuple of simulations
  #sims=(('SGS5/','SGS5'),('SGS8/','SGS8'),('SGS9/','SGS9'),('SGS11/','SGS11'),('SGS13/','SGS13'),('SGS14/','SGS14')) # tuple of simulations
  #sims=(('SGS15/','SGS15'),('SGS17/','SGS17'),('SGS19/','SGS19'),('SGS20/','SGS20'),('SGS21/','SGS21'),('SGS23/','SGS23')) # tuple of simulations
  #sims=(('SGS25/','SGS25'),('SGS26/','SGS26'),('SGS27/','SGS27'),('SGS29/','SGS29'),('SGS35/','SGS35')) # tuple of simulations
  #sims=(('SGS5/','SGS5'),('SGS11/','SGS11'),('SGS17/','SGS17'),('SGS23/','SGS23'),('SGS29/','SGS29'),('SGS35/','SGS35')) # tuple of simulations
  sims=(('sim_15/','SGS5'),('sim_25/','SGS11'),('sim_35/','SGS17'),('sim_45/','SGS23'),('sim_55/','SGS29'),('sim_65/','SGS35')) # tuple of simulations
  #sims=(('SGS8/','SGS8'),('SGS14/','SGS14')) # tuple of simulations
  #snapshot=('004','008','012','016','020') # tuple of snapshots to plot
  #snapshot=('060','120') # tuple of snapshots to plot
  snapshot=('010','020') # tuple of snapshots to plot

  max_r = 20
  rings = 25

  r = radii(max_r, rings) # adjacent radii that represent the ring

  #### Plotting resonances ####
  #plt.style.use('classic')
  plt.style.use('seaborn-bright')

  fig,axs=plt.subplots(len(sims), len(snapshot), sharex = True, sharey = True,
                       gridspec_kw = {'hspace': 0.03, 'wspace': 0.1})

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':12}

  origin = 'lower'
  lev = np.asarray([1000, 5000]) # levels for density contour map
  nbins = 100

  # create dictionary for bar pattern speeds
  #barp = [20.5, 18, 19, 19, 17.5, 21] # for set SGS8, SGS9, ...
  #dphase = zip([e[1] for e in sims], barp)
  #dphase = dict(dphase)
  #print(dphase)

  for sim,name in sims:
    fsim=fname+sim

    for snap in snapshot:
      #fsnap=fsim+"particles_python_disc_"+snap+".dat"

      dd = rsa.readsnap(fsim, snap, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      #pd = np.asarray([x, y, z])
      #vd = np.asarray([vx, vy, vz])
      #pd = pd.T
      #vd = vd.T
    
      jump = 10 # skip every n particles
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      vx = vx[::jump]
      vy = vy[::jump]
      vz = vz[::jump]

      # calculate radius and velocities (radial, tangential and vertical) for each particle
      radius=particle_radius(x,y)
      v=velocities(x,y, vx, vy, vz)
      
      # velocities per annuli
      values=(radius,) + v
      newrad,avg=avg_velocities(values,r)
      
      # determine angular velocity, epicyclic frequency and resonances (CR, ILR and OLR) per annuli
      mod_r=r[1:]
      omg_val=omegar_fun(mod_r,avg[1])
      kap=kappa(omg_val)
      resonances=omegas(omg_val[2],kap)

      row=sims.index((sim,name))
      col=snapshot.index(snap)

      radius_mod=radius[::]
      newv=v[0][::]
      omega_indiv_p=newv/radius_mod # Circular frequencies for individual particles

      # # arrange radius_mod and omega_indiv_p to make contour plot
      # maxon=40
      # l=list(omega_indiv_p)
      # indexr=[l.index(ele) for ele in l if ele <= maxon]
      # newo=np.asarray([omega_indiv_p[ind] for ind in indexr])
      # newr=np.asarray([radius_mod[ind] for ind in indexr])

      # xc,yc,zc=density_estimation(radius_mod,omega_indiv_p,nbins)

      # xc, yc, zc = plot_contour(radius_mod, omega_indiv_p, resolution=60)

      axs[row,col].plot(omg_val[0],resonances[1],'r-',label='OLR')
      axs[row,col].plot(omg_val[0],resonances[0],'b-.',label='ILR')
      axs[row,col].plot(omg_val[0],resonances[2],'k--',label='Corrotation')
      axs[row,col].plot(radius_mod,omega_indiv_p,'k.',markersize=0.3,alpha=0.5)
      #axs[row,col].plot((0, 20), (dphase[name], dphase[name]), 'darkslateblue', ls = 'dotted', lw = 2.5)
      #axs[row,col].contour(xc,yc,zc.reshape(xc.shape))
      #axs[row,col].imshow(zc.reshape(xc.shape),origin='lower',aspect='auto',cmap='Blues')
      #sns.kdeplot(radius_mod,omega_indiv_p,ax=axs[row,col])

      if row == col == 0:
        axs[row,col].legend(loc='upper right',shadow=True)

      if col == 1:
        axs[row,col].text(18,52,name,**fontlabel)

      if row == 0:
        times=float(snap)*0.6
        axs[row,col].set_title(r'$t = %s \,\,\mathrm{[Gyrs]}$' %(times))

  for ax in axs.flat:
    ax.grid()
    ax.label_outer()
    ax.set_xlim(0,20)
    ax.set_ylim(-5,60)
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=10)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel('Radius [kpc]',fontsize=12)
  plt.ylabel(r'$\Omega \,\,[\mathrm{s}^{-1}]$',fontsize=12)

  plt.show()

  plt.clf()
  ###########################

if __name__ == '__main__':
  main()
