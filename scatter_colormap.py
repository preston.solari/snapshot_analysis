import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize 
from scipy.interpolate import interpn
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import griddata as gd
from scipy.interpolate import interp2d
import matplotlib.ticker as tk

def scatter_cm(x ,y, a, ax=None, sort=True, bins=20,
              cmap=plt.cm.jet, ext=None, cb_label='Density',
              inverted_cm=False, sci=False, color_bar=True, **kwargs):
  """
  Scatter plot colored by 2d histogram
  """
  if ax is None :
    fig , ax = plt.subplots()
  else:
    fig , ax = ax[0], ax[1]

  # if ext is not None:
  #   z = np.where((a >= ext[0]) & (a <= ext[1]), a, 0)
  # else:
  #   z = a

  z = a
  # Sort the points by density, so that the densest points are plotted last
  if sort :
      idx = z.argsort()
      x, y, z = x[idx], y[idx], z[idx]

  if inverted_cm:
    cmap = cmap.reversed()
  
  if ext is not None:
    ax.hexbin(x, y, C=z, gridsize=bins, cmap=cmap, vmin=ext[0], vmax=ext[1], **kwargs)
    norm = Normalize(vmin=ext[0], vmax=ext[1])
  else:
    ax.hexbin(x, y, C=z, gridsize=bins, cmap=cmap, **kwargs)
    norm = Normalize(vmin = np.min(z), vmax = np.max(z))

  sm = cm.ScalarMappable(norm = norm, cmap = cmap)
  sm.set_array([])
  if color_bar:
    if sci:
      fmt = tk.ScalarFormatter(useMathText=True)
      fmt.set_powerlimits((0, 0))
      cbar = fig.colorbar(sm, ax=ax, format=fmt)
    else:
      cbar = fig.colorbar(sm, ax=ax)
    cbar.ax.set_ylabel(cb_label)

  return ax, sm