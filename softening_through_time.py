import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from scipy.optimize import curve_fit
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import matplotlib.colors as mcolors

from den_map import data

def expFit(x, m, t, b):
    return m * np.exp(-t * x) + b

def main():
  fdir = "/home/cronex/Documentos/Doctorado/snapshot_analysis/"
  # filename = "tabla-excel.ods"
  filename = "tabla-excel1-final.xlsx"
  # filename = 'tabla-excel.txt'
  
  df = pd.read_excel(fdir+filename, sheet_name='grafica_soft')
  df = df.fillna('')
  # df = pd.read_excel(fdir+filename)
  # print(df.iloc[:,0])
  df_star = df[df['simbolo'] == '*']
  df_circ = df[df['simbolo'] != '*']
  
  xs = np.array(df_star.iloc[:,0])
  ys = np.array(df_star.iloc[:,7])
  arts = np.array(df_star.iloc[:,2])
  colors = np.array(df_star.iloc[:,6])
  syms = np.array(df_star.iloc[:,5])
  
  xc = np.array(df_circ.iloc[:,0])
  yc = np.array(df_circ.iloc[:,7])
  artc = np.array(df_circ.iloc[:,2])
  
  # nx = np.array(list(set(x)))
  # indx = [np.where(x == i)[0][0] for i in nx]
  
  # p0 = (1, 0.000001, 1) # start with values near those we expect
  # params, cv = curve_fit(expFit, nx, y[indx])
  # m, t, b = params
  
  # x_new= np.linspace(min(x), max(x), len(nx))
  # f = CubicSpline(nx, y[indx], bc_type='natural')
  
  
  # dat = data(fdir+filename)
  # year, soft = np.asarray(dat[0], dtype=float), np.asarray(dat[1], dtype=float)
  
  # art = ''.join([text for text in dat[2:]])
  # print(art)
  
  plt.style.use('seaborn-paper')
  fig, axs = plt.subplots(dpi=120)
  
  for i in range(len(xs)):
    axs.plot(xs[i], ys[i], syms[i], color=colors[i], markersize=7.6, label=arts[i])
  
  for i in range(len(xc)):
    if i != len(xc)-1:
      axs.plot(xc[i], yc[i], 'ko', markersize=6.6)
    else:
      axs.plot(xc[i], yc[i], 'ko', markersize=6.6, label="Others")

  # axs.plot(year, soft, 'ko')
  axs.set_ylabel("$\epsilon/N\,\,$", fontsize=12)
  axs.set_xlabel("year", fontsize=12)
  axs.legend(loc='upper center', ncol=3, bbox_to_anchor=(0.5, 1.2), fontsize=7.2)
  
  plt.tight_layout()
  
  #for i in range(len(art)):
  #  axs.annotate(art[i], xy=(x[i], y[i]), xytext=(x[i]+0.15, y[i]+15), fontsize=5.2, alpha=0.6)
    
  # axs.xaxis.set_minor_locator(AutoMinorLocator())
  axs.xaxis.set_minor_locator(MultipleLocator(1))
  axs.yaxis.set_minor_locator(MultipleLocator(0.005))
  axs.set_yscale('log')
  # axs.tick_params(which='minor')
  axs.tick_params(axis='both',which='both',direction='in',
                  top=True,left=True,right=True,bottom=True,
                  labelsize=9)
    
  plt.show()
  plt.clf()
  

if __name__ == '__main__':
  main()