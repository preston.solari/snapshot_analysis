#!/usr/local/bin/ python

# Spectral analysis using the angular velocity of bars following Athanassoula (2002)

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from phase_of_bar import *

# Find number n closest to nd when n^2 >= nd
def find_n2(nd):
  n = 0
  while n**2 < nd:
    n += 1
  return n

def main():
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # model = 'SGS5/'
  model = 'sim_16/'
  smodel = model[:-1]

  frun = fname + model

  # time variable
  initsnap = 7
  totsnap = 7
  skip = 1
  ts = 0.1
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = time * ts

  # sizegrid = find_n2(len(rsnap))

  #rmax = [25, 30, 70, 70, 8]
  #rmax = [25, 30, 8]
  rmax = 25
  rings = 32
  radii = np.linspace(0, rmax, rings)

  rangeomegal = -2
  rangeomegar = 3
  sizehist = 100
  sizegrid = find_n2(rings-2)

  plt.style.use('seaborn-bright')

  jump = 2 # skip through particle data

  fig, axs = plt.subplots(sizegrid, sizegrid, sharex = False, sharey = False,
                       gridspec_kw = {'hspace': 0.2, 'wspace': 0.33})

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':8}

  row = col = 0
  for snap in rsnap:
    ext = res.snap_mask(snap, 3) # file's mask (last three digits)

    dd = rsa.readsnap(frun, ext, 2) # disk particles' data

    pd = dd['p']  # disc particle positions
    vd = dd['v']  # disc particle velocities

    # remove center of mass (disc)
    x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
    x = x[::jump]
    y = y[::jump]
    z = z[::jump]
    vx = vx[::jump]
    vy = vy[::jump]
    vz = vz[::jump]

    print("\n############# Phase of Bar (snapshot %s of %s) #############\n" % (str(snap), str(totsnap)))
    r, A2, ph = phase_of_bar_annuli_2d(x, y, radii)

    bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
    ph_thres = []
    final_ph = ph_thres # final values of phase
    lenph = 0 # length of phase
    indph = 0 # index of current phase
    baseph = ph[0] # reference phase
    for i in range(len(ph)):
      if np.abs(ph[i]-baseph) < bt:
        ph_thres.append(ph[i]) 
        if len(ph_thres) > lenph: # phase remains semi-constant
          final_ph = ph_thres
          lenph = len(final_ph)
          indph = i
      else:
        ph_thres = [] # empty phase
        baseph = ph[i] # drag new reference phase
    
    avgph = np.mean(final_ph)
    # print(final_ph, avgph)
    # ir = list(ph).index(ph[indph])

    # axs[row, col].plot(r, ph, 'k-') 
    # axs[row, col].plot((r[ir], r[ir]), (ph.min(), ph.max()), 'k--') 

    # radomeg = np.linspace(0, r[ir+1], rings)
    omegahist = np.linspace(rangeomegal, rangeomegar, sizehist+1)

    radius = res.particle_radius(x, y)
    v = res.velocities(x, y, vx, vy, vz)

    values = (radius,) + v
    newrad, avg = res.avg_velocities(values, radii)

    # mod_r = r[1:]
    # mod_r = radomeg[1:]
    mod_r = radii[1:]
    omg_val = res.omegar_fun(mod_r, avg[1])

    kap = res.kappa(omg_val)
    
    print("################ Getting histogram ################")
    for ro in range(len(mod_r)-1):
      npom = np.zeros(len(omegahist))
      for i in range(len(radius)):
        if mod_r[ro] <= radius[i] <= mod_r[ro+1]:
          om = np.cross([x[i], y[i], z[i]], [vx[i], vy[i], vz[i]])/radius[i]**2
          om = np.sqrt(om.dot(om))
          ratio = (om-avgph) / kap[ro]
          for j in range(len(omegahist)-1):
            if omegahist[j] <= ratio <= omegahist[j+1]:
              npom[j] += 1
              #radius = np.delete(radius, radius[i])
      print(npom)

      axs[row, col].hist(x = omegahist, bins = omegahist[:], weights = npom) # histogram with bars
      axs[row, col].plot(omegahist, npom, 'k-')
      axs[row, col].text(1.8, 260, str(round(mod_r[ro], 2)) + '$\,$kpc', **fontlabel)

      col += 1
      if col % sizegrid == 0:
        col = 0
        row += 1

    #resonances = omegas(omg_val[2], kap)

    print("\n###############################################\n")

  for ax in axs.flat:
    ax.grid(False)
    ax.set_xlim(-1, 3)
    ax.set_ylim(0, 600)
    # ax.label_outer()
    ax.set_xticks([-1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3])
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 7)
  
  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('$(\Omega-\Omega_{p}) / \kappa$', fontsize = 12)
  plt.ylabel(r'$N_{p}$', fontsize = 12)

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
