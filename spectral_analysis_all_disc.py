#!/usr/local/bin/ python

# Spectral analysis using the angular velocity of bars following Athanassoula (2002)

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from phase_of_bar import *
from spectral_analysis_all_disc_particles import calculate_correct_omega

# Find number n closest to nd when n^2 >= nd
def find_n2(nd):
  n = 0
  while n**2 < nd:
    n += 1
  return n

def main():
  # fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  #fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  fname="/run/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/"
  #fname="/run/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/"
  # model = 'SGS5/'
  #models = ['sim_11/','sim_23/', 'sim_33/', 'sim_42/', 'sim_51/', 'sim_53/', 'sim_63/']
  models =['SGS1', 'SGS2', 'SGS3']
  #models = ['sim_16/','sim_26/', 'sim_35/', 'sim_46/', 'sim_55/', 'sim_56/', 'sim_66/']
  # smodel = model[:-1]
  #nmodels =['SGS2', 'SGS9', 'SGS15', 'SGS19', 'SGS26', 'SGS28', 'SGS33']
  nmodels =['SGS1', 'SGS2', 'SGS3']
  #nmodels =['SGS6', 'SGS12', 'SGS17', 'SGS24', 'SGS29', 'SGS30', 'SGS36']

  # frun = fname + model

  # time variable
  initsnap = 235
  totsnap = 240
  skip = 1
  ts = 0.05
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = time * ts

  # sizegrid = find_n2(len(rsnap))

  #rmax = [25, 30, 70, 70, 8]
  #rmax = [25, 30, 8]
  rmax = 10
  rings = 22
  radii = np.linspace(0, rmax, rings)

  rangeomegal = -2
  rangeomegar = 3
  sizehist = 100
  #sizegrid = find_n2(rings-2)
  #sizegrid = find_n2(totsnap-initsnap+1)

  plt.style.use('seaborn-bright')

  jump = 10 # skip through particle data

  #fig, axs = plt.subplots(sizegrid, sizegrid, sharex = False, sharey = False,
  fig, axs = plt.subplots(len(models), len(rsnap), sharex = False, sharey = False,
                       gridspec_kw = {'hspace': 0.2, 'wspace': 0.33})

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':8}

  row = col = 0
  max = 0
  for mod in models:
    frun = fname + mod
    cph = avgph = 0
    for snap in rsnap:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)

      dd = rsa.readsnap(frun, ext, 2) # disk particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      # remove center of mass (disc)
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      vx = vx[::jump]
      vy = vy[::jump]
      vz = vz[::jump]

      cph = avgph # save previous phase value

      print("\n############# Phase of Bar (snapshot %s of %s) #############\n" % (str(snap), str(totsnap)))
      r, A2, ph = phase_of_bar_annuli_2d(x, y, radii)

      bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      ph_thres = []
      final_ph = ph_thres # final values of phase
      lenph = 0 # length of phase
      indph = 0 # index of current phase
      baseph = ph[0] # reference phase
      for i in range(len(ph)):
        if np.abs(ph[i]-baseph) < bt:
          ph_thres.append(ph[i]) 
          if len(ph_thres) > lenph: # phase remains semi-constant
            final_ph = ph_thres
            lenph = len(final_ph)
            indph = i
        else:
          ph_thres = [] # empty phase
          baseph = ph[i] # drag new reference phase
      
      avgph = np.mean(final_ph)
      # print(final_ph, avgph)
      # ir = list(ph).index(ph[indph])

      # axs[row, col].plot(r, ph, 'k-') 
      # axs[row, col].plot((r[ir], r[ir]), (ph.min(), ph.max()), 'k--') 

      # radomeg = np.linspace(0, r[ir+1], rings)
      omegahist = np.linspace(rangeomegal, rangeomegar, sizehist+1)

      radius = res.particle_radius(x, y)
      v = res.velocities(x, y, vx, vy, vz)

      values = (radius,) + v
      newrad, avg = res.avg_velocities(values, radii)

      # mod_r = r[1:]
      # mod_r = radomeg[1:]
      mod_r = radii[1:]
      omg_val = res.omegar_fun(mod_r, avg[1])

      kap = res.kappa(omg_val)
      omegab = calculate_correct_omega(avgph, cph, ts)
      print(omegab)

      print("################ Getting histogram ################")
      npom = np.zeros(len(omegahist))
      zm = 0.01
      for ro in range(len(mod_r)-1):
        for i in range(len(radius)):
          if mod_r[ro] <= radius[i] <= mod_r[ro+1]:
            if -zm < z[i] < zm:
              #om = np.cross([x[i], y[i], z[i]], [vx[i], vy[i], vz[i]])/radius[i]**2
              #om = np.sqrt(om.dot(om))
              om = np.cross([x[i], y[i], 0], [vx[i], vy[i], 0])/radius[i]**2
              om = np.sqrt(om.dot(om))
              ratio = (om-omegab) / kap[ro]
              for j in range(len(omegahist)-1):
                if omegahist[j] <= ratio <= omegahist[j+1]:
                  npom[j] += 1
                  #radius = np.delete(radius, radius[i])
      print(npom)
      if npom.max() > max:
        max = npom.max()

      # axs[row, col].hist(x = omegahist, bins = omegahist[:], weights = npom) # histogram with bars
      axs[row, col].plot(omegahist, npom, '-', label = nmodels[models.index(mod)])
      # axs[row, col].text(1.8, 260, str(round(mod_r[ro], 2)) + '$\,$kpc', **fontlabel)
      #axs[row, col].text(2.0, 400, str(snap*ts) + '$\,\,$Gyrs', **fontlabel)
      if mod == models[0]:
        axs[row, col].text(1.8, npom.max()-2600, str(snap*ts) + '$\,\,$Gyrs', **fontlabel)

      if snap == 0:
        axs[row, col].text(1.8, npom.max()-1300, nmodels[models.index(mod)], **fontlabel)

      col += 1
      if col % len(rsnap) == 0:
        col = 0
        row += 1

      #resonances = omegas(omg_val[2], kap)

      print("\n###############################################\n")

  # axs[0, 0].legend(loc="upper right", shadow=True)
  
  for ax in axs.flat:
    ax.grid(False)
    ax.set_xlim(-1, 3)
    ax.set_ylim(0, max)
    # ax.label_outer()
    ax.set_xticks([-1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3])
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 7)
  
  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('$(\Omega-\Omega_{p}) / \kappa$', fontsize = 12)
  plt.ylabel(r'$N_{p}$', fontsize = 12)

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
