#!/usr/local/bin/ python

# Spectral analysis using the angular velocity of bars following Athanassoula (2002)

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter

import resonances as res
from den_map import data
from plot_den_maps import adjust_center_mass
import readsnap_no_acc as rsa
from phase_of_bar import *

# Find number n closest to nd when n^2 >= nd
def find_n2(nd):
  n = 0
  while n**2 < nd:
    n += 1
  return n

def calculate_correct_omega(om1, om2, ts):
  if om1 < 0 and om2 > 0:
    return (2*np.pi+om1-om2)/ts
  return (om1-om2)/ts

def main():
  #fname="/run/media/cronex/Elements/Doctorado/Simulaciones/primer-art-rerun/"
  fname="/run/media/cronex/HV620S/primer-art-rerun/"
  #fname="/home/cronex/Documentos/Maestria/Tesis/sm-04-10-2018/"
  # model = 'SGS5/'
  #models = ['sim_16/','sim_26/', 'sim_35/', 'sim_46/', 'sim_55/', 'sim_56/', 'sim_66/']
  #models = ['sim_16/','sim_26/']
  #models = ['sim_11/','sim_21/']
  models = ['SGS5','SGS21']
  # smodel = model[:-1]
  #nmodels =['SGS6', 'SGS12', 'SGS17', 'SGS24', 'SGS29', 'SGS30', 'SGS36']
  nmodels = ['SGS5','SGS21']


  # frun = fname + model

  # time variable
  initsnap = 119
  totsnap = 120
  skip = 1
  ts = 0.1
  rsnap = range(initsnap,totsnap+1,skip)
  time = np.asarray(rsnap)
  time = time * ts

  #omegb = [30, 25] # omega_b for sim_11, sim_21

  # sizegrid = find_n2(len(rsnap))

  #rmax = [25, 30, 70, 70, 8]
  #rmax = [25, 30, 8]
  rmax = 18
  rings = 36
  radii = np.linspace(0, rmax, rings)

  rangeomegal = -2
  rangeomegar = 3
  sizehist = 200
  #sizegrid = find_n2(rings-2)
  #sizegrid = find_n2(totsnap-initsnap+1)

  plt.style.use('seaborn-bright')

  jump = 10 # skip through particle data

  #fig, axs = plt.subplots(sizegrid, sizegrid, sharex = False, sharey = False,
  fig, axs = plt.subplots(len(models), len(rsnap), sharex = False, sharey = False,
                       gridspec_kw = {'hspace': 0.2, 'wspace': 0.33})

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':8}

  row = col = 0
  max = 0
  for mod in models:
  #for mod in nmodels:
    frun = fname + mod
    cph = avgph = 0
    print("=============== Processing model %s (%s of %s) ==================\n" %(nmodels[models.index(mod)], str(models.index(mod)+1), str(len(models))))
    for snap in rsnap:
      ext = res.snap_mask(snap, 3) # file's mask (last three digits)

      dd = rsa.readsnap(frun, ext, 2) # disk particles' data
      dh = rsa.readsnap(frun, ext, 1) # halo particles' data

      pd = dd['p']  # disc particle positions
      vd = dd['v']  # disc particle velocities

      pha = dh['p']  # halo particle positions
      vh = dh['v']  # halo particle velocities

      # remove center of mass
      x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
      xh, yh, zh, vxh, vyh, vzh = adjust_center_mass(pha[:, 0], pha[:, 1], pha[:, 2], vh[:, 0], vh[:, 1], vh[:, 2])
      x = x[::jump]
      y = y[::jump]
      z = z[::jump]
      vx = vx[::jump]
      vy = vy[::jump]
      vz = vz[::jump]
      xh = xh[::jump]
      yh = yh[::jump]
      zh = zh[::jump]
      vxh = vxh[::jump]
      vyh = vyh[::jump]
      vzh = vzh[::jump]

      cph = avgph

      print("\n############# Phase of Bar (snapshot %s of %s) #############\n" % (str(snap), str(totsnap)))
      r, A2, ph = phase_of_bar_annuli_2d(x, y, radii)

      bt = np.arcsin(0.3) # from Athanassoula & Misiriotis (2002)
      ph_thres = []
      final_ph = ph_thres # final values of phase
      lenph = 0 # length of phase
      indph = 0 # index of current phase
      baseph = ph[0] # reference phase
      for i in range(len(ph)):
        if np.abs(ph[i]-baseph) < bt:
          ph_thres.append(ph[i]) 
          if len(ph_thres) > lenph: # phase remains semi-constant
            final_ph = ph_thres
            lenph = len(final_ph)
            indph = i
        else:
          ph_thres = [] # empty phase
          baseph = ph[i] # drag new reference phase
      
      avgph = np.mean(final_ph)
      print(avgph, time[rsnap.index(snap)])
      # print(final_ph)
      # print(final_ph, avgph)
      # ir = list(ph).index(ph[indph])

      # axs[row, col].plot(r, ph, 'k-') 
      # axs[row, col].plot((r[ir], r[ir]), (ph.min(), ph.max()), 'k--') 

      # radomeg = np.linspace(0, r[ir+1], rings)
      omegahist = np.linspace(rangeomegal, rangeomegar, sizehist+1)

      omeg = np.zeros(len(x))
      omegh = np.zeros(len(x))
      rp = np.sqrt(x**2 + y**2)
      zm = .1
      for i in range(len(x)):
        if -zm < z[i] < zm:
          # = np.cross([x[i], y[i]], [vx[i], vy[i]])/rp[i]**2
          #if np.abs(vz[i]) < 30:
            #om = np.cross([x[i], y[i], z[i]], [vx[i], vy[i], vz[i]])/rp[i]**2
          om = np.cross([x[i], y[i], 0], [vx[i], vy[i], 0])/rp[i]**2
          om = np.sqrt(om.dot(om))

          #omh = np.cross([xh[i], yh[i], zh[i]], [vxh[i], vyh[i], vzh[i]])/rp[i]**2
          #omh = np.sqrt(omh.dot(omh))

          omeg[i] = om
          #omegh[i] = omh

      # for i in range(len(xh)):
      #   if -zm < zh[i] < zm:
      #     #om = np.cross([x[i], y[i]], [vx[i], vy[i]])/rp[i]**2
      #     if np.abs(vzh[i]) < 30:
      #       omh = np.cross([xh[i], yh[i], zh[i]], [vxh[i], vyh[i], vzh[i]])/rp[i]**2
      #       omh = np.sqrt(omh.dot(omh))

      #     omegh[i] = omh 

      do = np.gradient(omeg**2, ts)
      #doh = np.gradient(omegh**2, ts)
      #print(do)
      kap = np.sqrt(rp*do + 4*omeg**2)
      #kaph = np.sqrt(rp*doh + 4*omegh**2)
 
      omegab = calculate_correct_omega(avgph, cph, ts)
      print(omegab)

      ratio = (omeg-omegab) / kap
      ratioh = (omegh) / kaph
      #ratioh = (omegh-omegab) / kaph
      #ratio = (omeg-omegb[models.index(mod)]) / kap
      #ratio = kap

      print("################ Getting histogram ################")
      npom = np.zeros(len(omegahist))
      npomh = np.zeros(len(omegahist))
      for i in range(len(ratio)):
        for j in range(len(omegahist)-1):
          if omegahist[j] <= ratio[i] <= omegahist[j+1]:
            npom[j] += 1
          if omegahist[j] <= ratioh[i] <= omegahist[j+1]:
            npomh[j] += 1
      print(npomh)
      if npom.max() > max:
        max = npom.max()
      #if npomh.max() > max:
      #  max = npomh.max()
      
      print(len(omegahist), len(npomh))

      # axs[row, col].hist(x = omegahist, bins = omegahist[:], weights = npom) # histogram with bars
      axs[row, col].plot(omegahist, npom, '-', label = nmodels[models.index(mod)])
      axs[row, col].plot(omegahist, npomh, 'r--', label = nmodels[models.index(mod)])
      # axs[row, col].text(1.8, 260, str(round(mod_r[ro], 2)) + '$\,$kpc', **fontlabel)
      #axs[row, col].text(2.0, 400, str(snap*ts) + '$\,\,$Gyrs', **fontlabel)
      # if mod == models[0]:
      #   axs[row, col].text(1.8, max-1200, str(snap*ts) + '$\,\,$Gyrs', **fontlabel)

      # if snap == 0:
      #   axs[row, col].text(1.8, max-600, nmodels[models.index(mod)], **fontlabel)

      col += 1
      if col % len(rsnap) == 0:
        col = 0
        row += 1

  #     #resonances = omegas(omg_val[2], kap)

      print("\n###############################################\n")

  # axs[0, 0].legend(loc="upper right", shadow=True)
  
  for ax in axs.flat:
    ax.grid(False)
    ax.set_xlim(rangeomegal, rangeomegar)
    ax.set_ylim(0, max)
    # ax.label_outer()
    #ax.set_xticks([-1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3])
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 7)
  
  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('$(\Omega-\Omega_{p}) / \kappa$', fontsize = 12)
  plt.ylabel(r'$N_{p}$', fontsize = 12)


  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
