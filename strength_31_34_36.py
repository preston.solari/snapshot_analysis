import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter

from den_map import data

def main():
  # fname = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun atocatl 
  fname = "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/rerun/all/" # rerun

  m = 2 # fourier mode to compute

  # list of simulations
  #listsims = ['21', '23', '32', '31']
  #listsims = ['33', '42', '41', '43']
  #listsims = ['52', '51', '53']
  #listsims = ['15', '25', '35', '45', '55', '65']
  #listsims = ['21']
  #listsims = ['SGS5', 'SGS8', 'SGS9', 'SGS11', 'SGS13', 'SGS14']
  #listsims = ['SGS15', 'SGS17', 'SGS19', 'SGS20', 'SGS21', 'SGS23']
  #listsims = ['SGS25', 'SGS26', 'SGS27', 'SGS29', 'SGS35']
  #listsims = ['SGS5', 'SGS11', 'SGS17', 'SGS23', 'SGS29', 'SGS35']
  #listsims = ['SGS19','SGS21']
  # listsims = ['SGS6']
  listsims = ['SGS31r', 'SGS34', 'SGS36']
  #listsims=["hernquist", "nfw", "iso", "burkert", "einasto"] 
  #listsims=["hernquist", "nfw", "einasto"] 
  #listsims=["hernquist", "nfw"]
  #listsims=["hernquist", "hernquist_gas_only_0.05", "hernquist_gas_only_0.2", "hernquist_gas_sfr_0.2"]
  lensims = len(listsims)

  # names of simulations
  #names = ['SGS1', 'SGS2', 'SGS7', 'SGS8']
  #names = ['SGS8', 'SGS9', 'SGS13', 'SGS14']
  #names = ['SGS15', 'SGS19', 'SGS20', 'SGS21']
  #names = ['SGS25', 'SGS26', 'SGS27']
  #names = ['SGS5', 'SGS8', 'SGS9', 'SGS11', 'SGS13', 'SGS14']
  #names = ['SGS15', 'SGS17', 'SGS19', 'SGS20', 'SGS21', 'SGS23']
  #names = ['SGS25', 'SGS26', 'SGS27', 'SGS29', 'SGS35']
  #names = ['SGS5', 'SGS11', 'SGS17', 'SGS23', 'SGS29', 'SGS35']
  #names = ['SGS19','SGS21']
  # names = ['SGS6']
  names = ['SGS31p', 'SGS34p', 'SGS36p']
  #names=["Hernquist", "NFW", "PISO", "Burkert", "Einasto"] 
  #names=["Hernquist", "NFW", "Einasto"]
  #names=["Hernquist", "Hernquist with gas only", "Hernquist with gas (SFR)"]
  #names = ["HSG", "HGO-05", "HGO-2", "HGS-2"]

  plt.style.use('seaborn-bright')
  #ls = ['solid', 'dashed', 'dotted', 'dashdot']
  ls = ['-', '--', '-.', ':', 'x', '*']

  fig,axs=plt.subplots(2, 3, sharex=False, sharey=False,
                       gridspec_kw={'hspace': 0.1, 'wspace': 0.07, 'height_ratios':[2.8,3]}, 
                       dpi = 160)

  fontlabel={'fontname':'DejaVu Sans Mono',
             'weight':'bold',
             'size':9}

  spines=[0, 0, 1]

  for k in range(lensims):
    #frun = fname + "sim_" + listsims[k] + "/" # location of the file
    #frun = fname + listsims[k] + "/output/" # location of the file
    frun = fname + '/strength/strength_' + listsims[k] + '.txt'# location of the file
    feta = fname + '/eta/eta_' + listsims[k] + '.txt'

    time, famp = data(frun)
    timed, eta = data(feta)
    
    axs[1,k].plot(time, famp, '-', color = 'black', linewidth = 1.0, label = names[k])
    axs[0,k].plot(timed, eta, '-', color = 'black', linewidth = 1.0, label = names[k])
    axs[0,k].plot([0,12.0],[0.02,0.02],'--', color='dimgrey', linewidth=0.9)
    axs[0,k].set_title(names[k])
    
    lw = 1.5
    for side in axs[0,k].spines.keys():
      if spines[k] == 0: # barred 
        axs[0,k].spines[side].set_color('deepskyblue')
        axs[0,k].spines[side].set_linewidth(lw)
        axs[1,k].spines[side].set_color('deepskyblue')
        axs[1,k].spines[side].set_linewidth(lw)
      elif spines[k] == 1: # transient
        axs[0,k].spines[side].set_color('gold')
        axs[0,k].spines[side].set_linewidth(lw)
        axs[1,k].spines[side].set_color('gold')
        axs[1,k].spines[side].set_linewidth(lw)
      elif spines[k] == 2: # completely unbarred
        axs[0,k].spines[side].set_color('orangered')
        axs[0,k].spines[side].set_linewidth(lw)
        axs[1,k].spines[side].set_color('orangered')
        axs[1,k].spines[side].set_linewidth(lw)
      elif spines[k] == 3: # delayed bar
        axs[0,k].spines[side].set_color('limegreen')
        axs[0,k].spines[side].set_linewidth(lw)
        axs[1,k].spines[side].set_color('limegreen')
        axs[1,k].spines[side].set_linewidth(lw)
  
  for ax in axs[1,:].flat:  
    ax.grid()
    # ax.label_outer()
    ax.set_xlim(0, np.max(time))
    # ax.set_xlabel("Time [Gyrs]")
    ax.set_ylabel("$A_2$")
    ax.set_ylim(0, 0.7)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 10)
  
  for i in range(1,len(axs[1,:])):
    axs[1,i].set_ylim(axs[1,0].get_ylim() ) # align axes
    # axs[1,i].set_yticks([]) # set ticks to be empty (no ticks, no tick-labels)
    axs[1,i].tick_params(labelleft=False)
    axs[1,i].set_xlabel("")
    axs[1,i].set_ylabel("")
    
    
  for ax in axs[0,:].flat:  
    ax.grid()
    # ax.label_outer()
    # ax.set_xlabel("Time [Gyrs]")
    ax.set_ylabel("$\eta$")
    ax.set_xlim(0, np.max(time))
    ax.set_yscale('log')
    ax.set_ylim(0.001, 0.1)
    ax.tick_params(axis = 'both', which = 'both', direction = 'in',
                    top = True, left = True, right = True, bottom = True,
                    labelsize = 10, labelbottom=False)
    
  for i in range(1,len(axs[0,:])):
    axs[0,i].set_ylim(axs[0,0].get_ylim() ) # align axes
    # axs[1,i].set_yticks([]) # set ticks to be empty (no ticks, no tick-labels)
    axs[0,i].tick_params(labelleft=False)
    axs[0,i].set_xlabel("")
    axs[0,i].set_ylabel("")

  fig.add_subplot(111, frameon = False)
  plt.tick_params(labelcolor = 'none', top = False, bottom = False, left = False, right = False)
  plt.grid(False)
  plt.xlabel('Time [Gyrs]', fontsize = 10)
  # plt.ylabel("$A_{2}$", fontsize = 12, labelpad = 2)
  
  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()