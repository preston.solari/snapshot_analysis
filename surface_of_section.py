import os
import sys
from xml.parsers.expat import model
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from scipy import interpolate

sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/scripts/")

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask
#from plot_den_maps import adjust_center_mass
from velocities import vel_components_cyl

def main():
  #fdir="/media/cronex/HV620S/sm-04-10-2018/sim_12/"
  #fdir="/media/cronex/HV620S/primer-art-rerun/SGS5/"
  #fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  #fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/halos/" # rerun
  # model = "SGS34"
  model = "SGS1"

  initsnap = 10
  totsnap = 10
  skip = 1
  rsnap = range(initsnap,totsnap+1,skip)


  ''' pick particles'''
  dd = readsnap(fdir+model+'/', 300, 1) # disk particles' data

  a = dd['a']  # disc particle accelerations
  pd = dd['p']  # disc particle positions
  ids = dd['id']

  ax, ay, az = a[:, 0], a[:, 1], a[:, 2]
  norma30 = np.sqrt(ax**2 + ay**2 + az**2)

  r1 = 1000
  r2 = 4000
  index20 = np.where(norma30 > r2)[0]
  index30 = np.where((norma30 > r1) & (norma30 <= r2))[0]
  #print(len(index20))
  # x, y, z = adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])

  # x2 = x[index20]
  # y2 = y[index20]
  # z2 = z[index20]

  # x3 = x[index30]
  # y3 = y[index30]
  # z3 = z[index30]
  idsr = ids[index20]
  print("numpart = {0}".format(len(idsr)))
  ''' pick particles'''


  nids = get_random_ids(idsr, 100)
  
  seen = set()
  dup = [i for i in nids if i in seen or seen.add(i)]
  print(dup)

  nids = list(set(nids))
  #nids = [1512385, 1254936, 1009562, 1792485, 2304649, 731436, 2552323, 496660]
  # nids = [1512385]
  dict_pos = {id: [] for id in nids}
  dict_vel = {id: [] for id in nids}
  # ''' indices '''


  #fig, axs = plt.subplots()
  mark=['ko','r*','bs']
  
  ''' Orbits '''
  #maxx = minx = maxy = miny = 0
  for snap in rsnap:
    ext = snap_mask(snap, 3) # file's mask (last three digits)

    dd = readsnap(fdir+model+'/', snap, 1)

    pd = dd['p']
    vd = dd['v']
    id = dd['id']
    id = list(id)
    #md = dd['m']
    x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
    vx, vy, vz= adjust_center_mass_unit(vd[:, 0], vd[:, 1], vd[:, 2])

    r = np.sqrt(x**2 + y**2 + z**2)
    vr, vt, vz = vel_components_cyl((x,y,z), (vx, vy, vz))

    for i in nids:
      print("\n############# Phase-space for particle %s (snapshot %s of %s) #############" % (str(i),str(snap), str(totsnap)))
      # dict_pos[i].append([x[id.index(i)], y[id.index(i)], z[id.index(i)]])  
      # dict_vel[i].append([vx[id.index(i)], vy[id.index(i)], vz[id.index(i)]])
      dict_pos[i].append(r[id.index(i)])
      dict_vel[i].append(vr[id.index(i)])
  

  px = []
  py = []
  vx = []
  vy = []
  pr = []
  vr = []
  fig, axs = plt.subplots()
  for key in dict_pos:

    maxx = minx = maxy = miny = 0

    dict_pos[key] = np.array(dict_pos[key])
    # px.append(dict_pos[key][:,0])
    # py.append(dict_pos[key][:,1])
    pr.append(dict_pos[key])
    # px = np.concatenate((px, [px[0]]))
    # py = np.concatenate((py, [py[0]]))

    dict_vel[key] = np.array(dict_vel[key])
    # vx.append(dict_vel[key][:,0])
    # vy.append(dict_vel[key][:,1])
    vr.append(dict_vel[key])
    # vx = np.concatenate((vx, [vx[0]]))
    # vy = np.concatenate((vy, [vy[0]]))
    #pz = dict_ids[key][:,2]
    
    ''' Soften orbits '''
    # nump = 1000
    # f, u = interpolate.splprep([px, py], s=0, per=True)
    # xnew, smoothy = interpolate.splev(np.linspace(0, 1, nump), f)
    # lastp = nump - nump/(totsnap-initsnap)
    # axs.plot(xnew[:lastp], smoothy[:lastp], '-', linewidth=0.5, alpha=0.7, label=key)
    # axs.annotate(str(key), (xnew[0], smoothy[0]))
    # if max(xnew) > maxx: maxx = max(xnew)
    # if max(smoothy) > maxy: maxy = max(smoothy)
    # if min(xnew) < minx: minx = min(xnew)
    # if min(smoothy) < miny: miny = min(smoothy)
    ''' Soften orbits '''

    ''' Regular orbits '''
  # axs.plot(px, vx, '.', linewidth=0.5, alpha=0.7, label=key)
  # axs.annotate(str(key), (px[0], vx[0]))

  pr = np.array(pr)[:,0]
  vr = np.array(vr)[:,0]
  # axs.plot(pr, vr, '.', linewidth=0.5, alpha=0.7, label=key)
  
  cmap='binary'
  origin='lower'
  bins = 30
  print(np.shape(pr), np.shape(vr), pr)
  counts, xbins, ybins = np.histogram2d(pr, vr, bins = bins)
  maxc = counts.max().max()
  minc = counts.min().min()
  lev = np.linspace(minc, maxc, 35)
  # make the contour plot
  # cm = axs.contourf(counts.T, levels = lev, extent = [xbins.min(), xbins.max(),
  #                         ybins.min(), ybins.max()], origin = origin,
  #                         extend = 'both', cmap = cmap)
  
  cm = axs.contour(counts.T, levels = lev, extent = [xbins.min(), xbins.max(),
                          ybins.min(), ybins.max()], origin = origin,
                          extend = 'both')
  # ticks = np.linspace(minc, maxc, 6)
  # fig.colorbar(cm, format="%.3f", ticks=ticks)

  # axs.set_xlabel("$x$")
  # axs.set_ylabel("$v_x$")
  axs.set_xlabel("$r$")
  axs.set_ylabel("$v_r$")
  axs.set_xlim(2, 50)
  axs.set_ylim(-300, 300)

 
  #   # if max(px) > maxx: maxx = max(px)
  #   # if max(py) > maxy: maxy = max(py)
  #   # if min(px) < minx: minx = min(px)
  #   # if min(py) < miny: miny = min(py)
  #   ''' Regular orbits '''

    # per_ax = 0.12
    # limx = (minx+minx*per_ax, maxx+maxx*per_ax)
    # limy = (miny+miny*per_ax, maxy+maxy*per_ax)
    # axs.set_xlim(limx[0], limx[1])
    # axs.set_ylim(limy[0], limy[1])
    # axs.legend(loc='best')

    # fv = fdir2+model+'/'+str(initsnap)+"-"+str(totsnap)+'/'
    # if not os.path.exists(fv): # create path if does not exist
    #   os.makedirs(fv)
    # iname = "plot_orbit_"+model+"_"+str(key)+".png"
    # plt.savefig(fv+iname)

    # plt.close()

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()
