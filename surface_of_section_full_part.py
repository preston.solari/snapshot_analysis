import os
import sys
from xml.parsers.expat import model
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import matplotlib.patches as mpatches
from scipy import interpolate
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.ticker as tk

sys.path.insert(0, "/home/cronex/Documentos/Doctorado/snapshot_analysis/data/scripts/")

from readsnap import readsnap
from plot_den_maps import adjust_center_mass_unit
from spectral_analysis_revised import get_random_ids
from resonances import snap_mask
#from plot_den_maps import adjust_center_mass
from velocities import vel_components_cyl
from accelerations import acc_components_cyl
from density_scatter import density_scatter
from scatter_colormap import scatter_cm

def main():
  #fdir="/media/cronex/HV620S/sm-04-10-2018/sim_12/"
  #fdir="/media/cronex/HV620S/primer-art-rerun/SGS5/"
  #fdir = "/media/cronex/TOSHIBA EXT/sims/sm-04-10-2018/SGS5/" # original
  fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/" # rerun
  # fdir = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/halos/" # rerun
  # model = "SGS34"
  # model = "SGS1"
  listsims120 = ['SGS5','SGS8','SGS9','SGS11', 'SGS13', 'SGS14', # 120 snaps
             'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
             'SGS25','SGS26','SGS27','SGS29','SGS35']
  # listsims = ['SGS14', 'SGS15','SGS17','SGS19','SGS20','SGS21','SGS23',
  #            'SGS25','SGS26','SGS27','SGS29','SGS35']
  # listsims = ['SGS13']
  listsims240 = ['SGS1', 'SGS2', 'SGS3', 'SGS4','SGS6', 'SGS7', # 240 snaps
             'SGS10', 'SGS12', 'SGS16', 'SGS18', 'SGS22', 'SGS24',
             'SGS28','SGS30','SGS31', 'SGS32', 'SGS33']
  listsims1200 = ['SGS34', 'SGS36'] # 1200 snaps
  # listsims = ['SGS6']
  listsims = ['SGS1', 'SGS2', 'SGS3', 'SGS4', 'SGS5', 'SGS6',
           'SGS7', 'SGS8', 'SGS9', 'SGS10', 'SGS11', 'SGS12',
           'SGS13', 'SGS14', 'SGS15', 'SGS16', 'SGS17', 'SGS18',
           'SGS19', 'SGS20', 'SGS21', 'SGS22', 'SGS23', 'SGS24',
           'SGS25', 'SGS26', 'SGS27', 'SGS28', 'SGS29', 'SGS30',
           'SGS31', 'SGS32', 'SGS33', 'SGS34', 'SGS35', 'SGS36']
  # listsims = listsims[:4]

  initsnap = 0
  totsnap = 240
  skip = 2
  rsnap = range(initsnap,totsnap+1,skip)
  
  nrow = 6
  ncol = 6
  fig, axs = plt.subplots(nrow, ncol, gridspec_kw={'hspace': 0.1, 'wspace': 0.05}, 
                          sharex=True, sharey=True, figsize=(13, 13), dpi=150)
  
  # Colorbar properties of velocity map
  axb1=nrow-1
  axb2=ncol-1
  # axb1=0
  # axb2=0
  xins = inset_axes(axs[axb1,axb2],
                    width="4%",  # width = 5% of parent_bbox width
                    height="100%",  # height : 50%
                    loc='lower left',
                    bbox_to_anchor=(1.05, 0., 1, 1),
                    bbox_transform=axs[axb1,axb2].transAxes,
                    borderpad=0,
                    )
  row = col = 0
  for model in listsims:
    if model in listsims120:
      totsnap = 120
      skip = 1
    elif model in listsims240:  
      totsnap = 240
      skip = 2
    elif model in listsims1200:
      totsnap = 1200
      skip = 10
      
    initsnap = totsnap
    rsnap = range(initsnap,totsnap+1,skip)
    
    for snap in rsnap:
      print("Scatter plot for %s (%d of %d) for snapshot %d (of %d)" % (model, listsims.index(model)+1, len(listsims), rsnap.index(snap)+1, len(rsnap)))
      ext = snap_mask(snap/skip, 3) # file's mask (last three digits)

      dd = readsnap(fdir+model+'/', snap, 2)

      pd = dd['p']
      vd = dd['v']
      ad = dd['a']
      id = dd['id']
      id = list(id)
      #md = dd['m']
      x, y, z= adjust_center_mass_unit(pd[:, 0], pd[:, 1], pd[:, 2])
      vx, vy, vz= adjust_center_mass_unit(vd[:, 0], vd[:, 1], vd[:, 2])
      ax, ay, az = adjust_center_mass_unit(ad[:, 0], ad[:, 1], ad[:, 2])

      r = np.sqrt(x**2 + y**2 + z**2)
      theta = np.arctan2(y, x)
      vr, vt, vz = vel_components_cyl((x,y,z), (vx, vy, vz))
      ar, at, az = acc_components_cyl((x, y, z), (vx, vy, vz), (ax, ay, az))
      ar = np.abs(ar)

      axs[row, col], sm = scatter_cm(np.log10(r), vr, ar, ax=(fig,axs[row, col]), bins=100, ext=(0, 25000), 
                       color_bar=False, cb_label='$a_r$', inverted_cm=False, alpha=0.6)
      
      axs[row, col].text(1.35, 310, model, fontsize=5)
      
      # Put colorbar
      if row == axb1 and col == axb2:
        fmt = tk.ScalarFormatter(useMathText=True)
        fmt.set_powerlimits((0, 0))
        cbar = fig.colorbar(sm, cax=xins, format=fmt)
        cbar.set_label(r'$a_r$',fontsize=10)
        cbar.ax.tick_params(labelsize=7)
        cbar.ax.yaxis.set_offset_position('left')
        cbar.ax.yaxis.get_offset_text().set_fontsize(7)
      
      col += 1
      if col % ncol == 0:
        col = 0
        row += 1
      
      # fig, axs = plt.subplots()
      
      # axs = density_scatter(np.log10(r), vr, ax=(fig,axs), bins=[30, 30], ext=(0, 0.02), marker=',', s=1)
      # axs = density_scatter(theta, vt, ax=(fig,axs), bins=[30, 30], ext=(0, 0.0018), sci=True, marker=',', s=1)
      # axs = density_scatter(z, vz, ax=(fig,axs), bins=[30, 30], ext=(0, 0.01), sci=True, marker=',', s=1)

      # axs.set_xlim(-3.2, 3.2)
      # axs.set_ylim(-400, 400)
      # axs.set_xlabel("$log_{10}(r)$")    
      # axs.set_xlabel("$\\theta$")    
      # axs.set_xlabel("$z$")    
      # axs.set_ylabel("$v_r$")
      # axs.set_ylabel("$v_{\\theta}$")
      # axs.set_ylabel("$v_{z}$")

      # fdir2 = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/surface_of_section/r_vr/" # rerun
      # fdir2 = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/surface_of_section/t_vt/" # rerun
      # fdir2 = "/media/cronex/TOSHIBA EXT/sims/primer-art-rerun/surface_of_section/z_vz/" # rerun
      # fv = fdir2+'density/'+model+'/'
      # if not os.path.exists(fv): # create path if does not exist
      #   os.makedirs(fv)
      # iname = 'sos_den_'+model+'_'+ext+'.png'
      # plt.savefig(fv+iname)
      # plt.close()

      # fig, axs = plt.subplots()

      # axs = scatter_cm(np.log10(r), vr, ar, ax=(fig,axs), bins=100, ext=(-25000, 2000), cb_label='$a_r$', inverted_cm=True)
      # axs = scatter_cm(theta, vt, at, ax=(fig,axs), bins=150, cb_label='$a_{\\theta}$', ext=(-1e5, 1e5), inverted_cm=True, sci=True)
      # axs = scatter_cm(z, vz, az, ax=(fig,axs), bins=150, cb_label='$a_z$', ext=(-1e5, 1e5), inverted_cm=True, sci=True)
      
      # axs.set_xlim(-3.2, 3.2)
      # axs.set_ylim(-400, 400)
      # axs.set_xlabel("$log_{10}(r)$")    
      # axs.set_xlabel("$\\theta$")    
      # axs.set_xlabel("$z$")    
      # axs.set_ylabel("$v_r$")
      # axs.set_ylabel("$v_{\\theta}$")
      # axs.set_ylabel("$v_{z}$")

      # fv = fdir2+'acceleration/'+model+'/'
      # if not os.path.exists(fv): # create path if does not exist
      #   os.makedirs(fv)
      # iname = 'sos_acc_'+model+'_'+ext+'.png'
      # plt.savefig(fv+iname)     
      # plt.close()
  
  for ax in axs.flat:
    ax.grid(False)
    ax.label_outer()
    ax.set_xlim(-2,2)
    ax.set_ylim(-420,420)
    ax.tick_params(axis='both',which='both',direction='in',
                   top=True,left=True,right=True,bottom=True,
                   labelsize=6)

  fig.add_subplot(111, frameon=False)
  plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
  plt.grid(False)
  plt.xlabel('$log_{10}(r)$',fontsize=10, labelpad=-2)
  plt.ylabel(r'$v_r$',fontsize=10, labelpad=1)

  plt.show()
  plt.clf()

if __name__ == '__main__':
  main()