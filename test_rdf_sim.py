import numpy as np
#import matplotlib as mpl
#mpl.use('tkagg')
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

from utilities import *
from paircorrelation import pairCorrelationFunction_3D, pairCorrelationFunction_2D
#from rdfpy import rdf
#from radialdf import inner_rdf

# from generate_disk import scatter_particles_3d, scatter_particles_2d 
from readsnap_no_acc import readsnap
from plot_den_maps import adjust_center_mass
from rdf_alex import hist_density_3d_ngb, rdf_3d, hist_density_3d

def create_uniform_mesh(n=100, values=1, ds=(0,0,0)):
  x = list(np.ravel(np.array([[np.linspace(-values,values,n)] for _ in range(n*n)])))
  y = list(np.ravel(np.array([[e]*n for e in x[:n]]*n)))
  z = list(np.ravel(np.array([[e]*n*n for e in x[:n]])))

  # adding space differences
  if ds[0] > 0: 
    x=[e-ds[0] if not e%2 and x.index(e)!=0 and x.index(e)!=n-1 else e for e in x] 
    x=[e+ds[0] if x.index(e)%2 and x.index(e)!=0 and x.index(e)!=n-1 else e for e in x]

  if ds[1] > 0: 
    y=[e-ds[1] if not e%2 and y.index(e)!=0 and y.index(e)!=n-1 else e for e in y] 
    y=[e+ds[1] if y.index(e)%2 and y.index(e)!=0 and y.index(e)!=n-1 else e for e in y]
  
  if ds[2] > 0: 
    z=[e-ds[2] if not e%2 and z.index(e)!=0 and z.index(e)!=n-1 else e for e in z] 
    z=[e+ds[2] if z.index(e)%2 and z.index(e)!=0 and z.index(e)!=n-1 else e for e in z]

  return np.array(x), np.array(y), np.array(z)

def main():
  #fdir="/home/cronex/Documentos/Doctorado/gadget4-marzo/sims-predoc/hernquist/output/"
  fdir="/run/media/cronex/HV620S/sm-04-10-2018/sim_66/"
  #fdir="/run/media/cronex/HV620S/primer-art-rerun/SGS35/"
  #model=fdir.split("/")
  #model=[e for e in model if e != ''][-1]
  #fil = open(fdir + "RDF_"+model, "w")


  # disk parameters
  # diskh = 3.0
  # mdisk = 5
  # z0 = 0.2*diskh 
  # G = 43007.1

  # Particle setup
  domain_size = 50.0
  n = 45
  val = 50.0
  #ds = 0.1

  # Calculation setup
  dr = 0.1
  jump = 100

  ### Random arrangement of particles ###
  rmaxo = domain_size / 4
  rmax = 25

  # create disk
  # part = scatter_particles_2d(diskh, domain_size, z0, n)
  # #x, y, z = part[0], part[1], part[2]
  # x, y = part[0], part[1]

  #dd = readsnap(fdir, 20, 2) # disk particles' data

  #pd = dd['p']  # disc particle positions
  #vd = dd['v']  # disc particle velocities
  #idd = dd['id']
  #print(type(idd))

  # remove center of mass (disc)
  #x, y, z, vx, vy, vz = adjust_center_mass(pd[:, 0], pd[:, 1], pd[:, 2], vd[:, 0], vd[:, 1], vd[:, 2])
  #x = x[::jump]
  #y = y[::jump]
  #z = z[::jump]
  #c = np.array([[xc, yc, zc] for xc, yc, zc in zip(x, y, z)])

  #x = np.random.uniform(low=-domain_size, high=domain_size, size=n**3)
  #y = np.random.uniform(low=-domain_size, high=domain_size, size=n**3)
  #z = np.random.uniform(low=-domain_size, high=domain_size, size=n**3)
  # print(x)
  
  x, y, z = create_uniform_mesh(n=n,values=val,ds=(1, 0, 0))

  # Compute pair correlation
  print("Calculating RDF.")
  g_r, r, reference_indices = pairCorrelationFunction_3D(x, y, z, domain_size, rmaxo, dr)
  #g_r, r, reference_indices = pairCorrelationFunction_3D(x, y, z, val, rmaxo, dr)
  #g_r, r, reference_indices = pairCorrelationFunction_2D(x, y, domain_size, rmaxo, dr)
  #g_r, r, reference_indices = rdf_3d(x, y, z, rmax, dr, idd)
  #g_r, r, reference_indices = rdf_3d(x, y, z, rmax, dr, np.array(range(len(x))))
  #g_r, r = hist_density_3d(x, y, z, rmax, dr)
  #g_r, r = hist_density_3d_ngb(x, y, z, 40, dr, 1)
  #print(g_r)
  #g_r, rad = rdf(c, dr=0.1, parallel=False)

  # lx, ly, lz = 40, 40, 40
  # rd = int(lx/2.0)
  # # box = [[0,lx]]+[[,ly]]+[[0,lz]]
  # box = [[-lx,lx]]+[[-ly,ly]]+[[-lz,lz]]
  # g_r = inner_rdf(box, c, rd, 0.1)
  # r = np.linspace(0, rd, len(g_r))
  print("Done.")

  # print(g_r)

  # Visualize
  plt.figure()
  plt.plot(r, g_r, color='black')
  plt.xlabel('r')
  #plt.ylabel('g(r)')
  plt.ylabel('N')
  plt.xlim((0, rmax))
  plt.ylim((0, 1.05 * g_r.max()))
  plt.title("Funcion de distribucion radial para una malla uniforme con espaciado en x")

  
  #plt.plot(x, y, 'k.')
  # ax = plt.axes(projection='3d')
  # ax.scatter3D(x, y, z, s=0.5)
  #ax.scatter3D(x[:,0], y[:,0], z[:,0], s=0.5)
  #plt.savefig('../test_rdf.png')
  plt.show()

  #for i in range(len(g_r)):
  #  fil.write(str(r[i]) + ' ' + str(g_r[i]) + '\n')
  
  #fil.close()

if __name__ == '__main__':
  main()
