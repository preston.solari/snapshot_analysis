#!/usr/bin/env python

# Python program to graph the velocity map of any GADGET-2 snapshot

from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

from scipy.interpolate import griddata as gd
from scipy.interpolate import interp2d

import scipy.ndimage as ndimage

# This function obtains and formats the data from a file with 
# all the positions and velocities
def data(d_file):
  print("Reading file...")

  f=open(d_file,"r")
  
  ud=f.readlines()
  li=[line.split() for line in ud]  
  
  print("Done.")
  
  print("Obtaining positions and velocities...")
  div=[ind for ind,el in enumerate(li) if el == ['o']] # index of each split section
  
  px=[float(x) for el in li[0:div[0]] for x in el] # coordinate x positions
  py=[float(y) for el in li[div[0]+1:div[1]] for y in el] # coordinate y positions
  vel=[float(v) for el in li[div[1]+1:div[2]] for v in el] # velocities
  
  print("Done.")

  f.close()

  return px,py,vel

# This function creates a grid and countours to map
# the velocities of all the system using matplotlib's griddata
def mapping_mpl(x,y,v,iname):
  xi=np.linspace(min(x),max(x),256) # grid definition 
  yi=np.linspace(min(y),max(y),256)  

  vel=griddata(x,y,v,xi,yi,interp='linear') # data grid

  CS=plt.contour(xi,yi,vel,15,linewidths=0.5,colors='k')
  CS=plt.contourf(xi,yi,vel,15,vmax=abs(vel).max(),vmin=-abs(vel).max())
  
  plt.colorbar()  # draw colorbar
  plt.xlim(min(x),max(x))
  plt.ylim(min(y),max(y))
  plt.title("Contours of isovelocities")
  
  plt.savefig(iname)
  plt.close()

# This function creates a grid and countours to map
# the velocities of all the system using scipy's griddata
def mapping_scipy(x,y,v,rx,ry,rv,iname,method='linear'):
  print("Creating data grid...")
  fs=21

  pxy=np.asarray([[i,j] for ex,i in enumerate(x) for ey,j in enumerate(y) if ex == ey]) # creating array for all x,y positions
  vel=np.asarray(v) # array with all velocities
  
  xi,yi=np.mgrid[-rx:rx:80j,-ry:ry:80j] # grid definition

  # data grid depending on a method of choosing
  # default method is 'linear'
  if method == 'nearest':
    vm=gd(pxy,vel,(xi,yi),method='nearest') 
  elif method == 'linear':
    vm=gd(pxy,vel,(xi,yi),method='linear') 
  elif method == 'cubic':
    vm=gd(pxy,vel,(xi,yi),method='cubic')
  else:
    print("There is no method %s in function mapping_scipy." % method)
  
  print("Done.")

  vm=ndimage.gaussian_filter(vm,sigma=1.5,order=0)

  print("Plotting positions and velocities into the grid...")

  ax=plt.subplot()
  # plotting contours
  CS=plt.contour(xi,yi,vm,15,linewidths=0.5,colors='k')
  CS=plt.contourf(xi,yi,vm,15,vmax=rv,vmin=-rv,cmap=cm.jet)

  # plotting grid
  plt.imshow(vm.T, extent=(-rx,rx,-ry,ry), origin='lower', cmap=cm.jet)
  cb=plt.colorbar()
  cb.set_label(r'$V_{\mathrm{obs}}$', fontsize=fs)
  ax.set_xlabel('x (kpc)')
  ax.set_ylabel('y (kpc)')
  #plt.plot(pxy[:,0], pxy[:,1], 'k.', ms=1)
  #plt.title('Velocity map using '+method+' interpolation')
  plt.gcf().set_size_inches(14, 10)

  for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                ax.get_xticklabels() + ax.get_yticklabels()):
      item.set_fontsize(fs)

  cb.ax.set_yticklabels(cb.ax.get_yticklabels(), fontsize=fs)

  plt.savefig(iname)
  plt.close()

  print("Done.")

def mapping_scipy2(x,y,v,rx,ry,rv,iname,method='linear'):
  print("Creating data grid...")

  x=np.asarray(x)
  y=np.asarray(y)
  vel=np.asarray(v) # array with all velocities
  
  # data grid depending on a method of choosing
  # default method is 'linear'
  if method == 'quintic':
    vm=interp2d(x,y,vel,kind='quintic') 
  elif method == 'linear':
    vm=interp2d(x,y,vel,kind='linear') 
  elif method == 'cubic':
    vm=interp2d(x,y,vel,kind='cubic')
  else:
    print("There is no method %s in function mapping_scipy." % method)
  
  print("Done.")

  xnew=np.arange(-rx,rx,5.0)
  ynew=np.arange(-ry,ry,5.0)
  vnew=vm(xnew,ynew)

  xn, yn = np.meshgrid(xnew, ynew)

  print("Plotting positions and velocities into the grid...")

  ax=plt.subplot()
  # plotting contours
  #CS=plt.contour(xi,yi,vm,15,linewidths=0.5,colors='k')
  #CS=plt.contourf(xi,yi,vm,15,vmax=rv,vmin=-rv)

  # plotting grid
  plt.pcolormesh(xn, yn, vnew, cmap='RdBu')
  cb=plt.colorbar()
  cb.set_label('Observed velocity')
  ax.set_xlabel('x (kpc)')
  ax.set_ylabel('y (kpc)')
  #plt.plot(pxy[:,0], pxy[:,1], 'k.', ms=1)
  plt.title('Velocity map using '+method+' interpolation')
  plt.gcf().set_size_inches(14, 10)

  plt.savefig(iname)
  plt.close()

  print("Done.")
  
# Main definition
def main():
  fdir="/home/kaicudon/Documentos/Maestria/Tesis/sm-12-09-2018/sim_6/"
  #fdir="/home/kaicudon/Documentos/Maestria/Tesis/sm-04-10-2018/sim_56/"
  
  isnap=0 # initial snapshot to read
  fsnap=21 # final snapshot to be read

  ran=20 # range for coordinates (x,y)
  rv=120 # maximum velocity

  for i in range(isnap,fsnap+1):
    ext='000'
    ext=ext[0:-1]+str(i)
    ext=ext[-3:]
    fname=fdir+"colormap_data_"+ext+".dat"
    
    x,y,v=data(fname)
    
    img=fdir+"vm_"+ext+".png"

    #mapping_mpl(x,y,v)
    mapping_scipy(x,y,v,ran,ran,rv,img,'nearest')

if __name__ == '__main__':
  main()
